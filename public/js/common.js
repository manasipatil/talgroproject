$(function(){

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});


    $('[data-toggle="popover"]').popover();   


    //----------------Full Screen Toggle---------------------
     $("#panel-fullscreen").click(function (e) {
        e.preventDefault();
        
        var $this = $(this);
    
        if ($this.children('i').hasClass('icon-expand'))
        {
            $this.children('i').removeClass('icon-expand');
            $this.children('i').addClass('icon-compress');
        }
        else if ($this.children('i').hasClass('icon-compress'))
        {
            $this.children('i').removeClass('icon-compress');
            $this.children('i').addClass('icon-expand');
        }
        $(this).closest('.layout-content').toggleClass('panel-fullscreen');
    });
    //----------------End Full Screen Toggle-----------------    

});