$(function(){

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});


    $('#close_edit_modal').click(function(){  
        window.location = "contentlibrary";;
    });

    $('#close_add_modal').click(function(){  
        window.location = "contentlibrary";
    });

    $('#close_doc_modal').click(function(){  
        window.location = "contentlibrary";
    });    

    $('#close_av_modal').click(function(){  
        window.location = "contentlibrary";
    }); 

    $('#close_webinar_modal').click(function(){  
        window.location = "contentlibrary";
    });     

    $('#competency_id').on('change',function(){
      $.ajax({
        'type':'POST',
        'url':"getDevelopementAreaCL",
        data: {
          "competency_id" : $(this).val()
          },
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Development Area</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tda_id']+"'>"+response[i]['tda_development']+"</option>";
          }

          $('#development_area_id').html(html);
        }
      });       
    });

    $('#edit_copentency').on('change',function(){
      $.ajax({
        'type':'POST',
        'url':"getDevelopementAreaCL",
        data: {
          "competency_id" : $(this).val()
          },
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Development Area</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tda_id']+"'>"+response[i]['tda_development']+"</option>";
          }

          $('#edit_development_area').html(html);
        }
      });       
    });   

    $('#doc_competency_id').on('change',function(){
      $.ajax({
        'type':'POST',
        'url':"getDevelopementAreaCL",
        data: {
          "competency_id" : $(this).val()
          },
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Development Area</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tda_id']+"'>"+response[i]['tda_development']+"</option>";
          }

          $('#doc_development_area_id').html(html);
        }
      });       
    });  

    $('#av_competency_id').on('change',function(){
      $.ajax({
        'type':'POST',
        'url':"getDevelopementAreaCL",
        data: {
          "competency_id" : $(this).val()
          },
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Development Area</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tda_id']+"'>"+response[i]['tda_development']+"</option>";
          }

          $('#av_development_area_id').html(html);
        }
      });       
    });        

    $('#webinar_competency_id').on('change',function(){
      $.ajax({
        'type':'POST',
        'url':"getDevelopementAreaCL",
        data: {
          "competency_id" : $(this).val()
          },
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Development Area</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tda_id']+"'>"+response[i]['tda_development']+"</option>";
          }

          $('#webinar_development_area_id').html(html);
        }
      });       
    }); 

    $('#article_tab_id').click(function(){
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
      ajaxPagination('article','',filter_list);
      $('.tab_name').val('article');
      $('#pagination_drop').attr("id","pagination_drop_article");
    });

    $('#document_tab_id').click(function(){
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
      ajaxPagination('documents','',filter_list);
      $('.tab_name').val('documents');
      $('#pagination_drop').attr("id","pagination_drop_document");
    });  

    $('#av_tab_id').click(function(){
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
      ajaxPagination('videos','',filter_list);
      $('.tab_name').val('videos');
      $('#pagination_drop').attr("id","pagination_drop_av");
    }); 

    $('#fav_tab_id').click(function(){
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
      ajaxPagination('favourite','',filter_list);
      $('.tab_name').val('favourite');
      $('#pagination_drop').attr("id","pagination_drop_fav");
    });   

    $('#webinar_tab_id').click(function(){
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
      ajaxPagination('webinar','',filter_list);
      $('.tab_name').val('webinar');
      $('#pagination_drop').attr("id","pagination_drop_webinar");
    });


    $('#all_tab_id').click(function(){
      $('.tab_name').val('');
      $('.pagination_count').attr('id','pagination_drop');
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';         
      ajaxPagination('','',filter_list);
    });

    $('.tab_name').val('');
    ajaxPagination('','','displayallcontent');


    $("#art_upload_img").on('change',function(){
      // upload image on add article
       var check_img_type = "addimage";
        $('.hrtigm').html('');
        $('.image_src').val('');
        $('#upload_demo').html('');
        $('#crop_image_div').hide();


      readURL(this,check_img_type);
    });

    $("#art_upload_img_small").on('change',function(){
      // upload image on add article
       var check_img_type_small = "addimage";
        $('.hrtigm_small').html('');
        $('.artnoimg').hide();
        $('.image_src_small').val('');
        $('#upload_demo_small').html('');
        $('#crop_image_div_small').hide();


      readURLSmall(this,check_img_type_small);
    });    

    $("#art_edit_upload_img").on('change',function(){
      // upload image on edit article
       var check_img_type = "editimage";
        $('.hrtigm').html('');
        $('.image_src_edit').val('');
        $('#upload_demo_edit').html('');
        $('#crop_image_div_edit').hide();


      readURL(this,check_img_type);
    });

    $("#doc_upload_img").on('change',function(){
      // upload image on add document
       var check_img_type = "docaddimage";
        $('.hrtigmw').html('');
         $('.hrtigmw').hide();       
        $('.doc_image_src').val('');
        $('#doc_upload_demo').html('');
        $('#doc_crop_image_div').hide();


      readURL(this,check_img_type);
    }); 

    $("#doc_upload_img_small").on('change',function(){
      // upload image on add document
      alert('hi');
       var check_img_type_small = "docaddimage";
        $('.hrtigmw_small').html('');
         $('.hrtigmw_small').hide();
         $('.docnoimg').hide();       
        $('.doc_image_src_small').val('');
        $('#doc_upload_demo_small').html('');
        $('#doc_crop_image_div_small').hide();


      readURLSmall(this,check_img_type_small);
    });       

    $("#av_upload_img").on('change',function(){
      // upload image on add audio/video
       var check_img_type = "avaddimage";
        $('.hrtigav').html('');
         $('.hrtigav').hide();       
        $('.av_image_src').val('');
        $('#av_upload_demo').html('');
        $('#av_crop_image_div').hide();


      readURL(this,check_img_type);
    });  

    $("#av_upload_img_small").on('change',function(){
      alert('hi');
      // upload small image on add audio/video
       var check_img_type_small = "avaddimage";
        $('.hrtigav_small').html('');
         $('.hrtigav_small').hide();   
         $('.avnoimg').hide();    
        $('.av_image_src_small').val('');
        $('#av_upload_demo_small').html('');
        $('#av_crop_image_div_small').hide();


      readURLSmall(this,check_img_type_small);
    });     

    $("#webinar_upload_img").on('change',function(){
      // upload image on add audio/video
       var check_img_type = "webinaraddimage";
        $('.hrtigweb').html('');
         $('.hrtigweb').hide();       
        $('.webinar_image_src').val('');
        $('#webinar_upload_demo').html('');
        $('#webinar_crop_image_div').hide();


      readURL(this,check_img_type);
    }); 

    $("#webinar_upload_img_small").on('change',function(){
      // upload image on add webinar
       var check_img_type_small = "webinaraddimage";
        $('.hrtigwebinar_small').html('');
         $('.hrtigwebinar_small').hide();   
         $('.webinarnoimg').hide();       
        $('.webinar_image_src_small').val('');
        $('#webinar_upload_demo_small').html('');
        $('#webinar_crop_image_div_small').hide();


      readURLSmall(this,check_img_type_small);
    });       

    $("#select_video").on('change',function(){
            var input = document.getElementById("select_video");
        file = input.files[0]; 
        $('#video_name').html(file['name']);
        $('#show_vid_name').show();
    });

    $("#select_document").on('change',function(){
            var input = document.getElementById("select_document");
        file = input.files[0]; 
        $('#document_name').html(file['name']);
        $('#show_doc_name').show();
    });    

    $('#add_article_form').validate({
      ignore: [],  
          rules: {
          title: {
               required: true,
               minlength: 3
           },
          body :{
               required: function(textarea){
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
          var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
          return editorcontent.length === 0;
               },
               minlength: 5            
          },
          level :{
            required: true
          },
          keywords :{
            required: true
          },
          author :{
             required: true
          }
        },
        messages: {
          title: {
               required: "Title is required",
               minlength: "Title must contain at least {0} characters"
           },
          body: {
               required: "Body is required",
               minlength: "Body must contain at least {0} characters"
           },
           level :{
                required: "Please select level"
           },
           keywords:{
                required: "Keywords are required"
           },
           author : {
                required: "Author is required"
           }
        }, 
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
          $(element).parents('.form-group').addClass('has-success');
        },
          submitHandler: function(form) {
            var get_crop_img = "";
            var get_crop_img2 = "";
            var get_crop_img_small = "";
            var get_crop_img2_small = "";
            //code for image upload //
            //if user crop image then get upload image with following code

            if($('.image_src').val() != ''){
              if($('#get_crop_img_add').html() == ""){
                $.holdReady( true );
                  $uploadCrop.croppie('result', {
                      type: 'canvas',
                      size: 'viewport'
                  }).then(function (resp) {
                        $.ajax({
                            url: "image-crop",
                            type: "POST",
                            data: {"image_path": resp},
                            success: function (data) {
                              if(data != ""){
                                $.holdReady( false );
                              }
                              console.log("1="+data);
                              get_crop_img = data;
                              $('#get_crop_img_add').html(data);
                            }
                        });
                  });
                  get_crop_img = $('#get_crop_img_add').html();
                  console.log("result1="+get_crop_img);
              }
              if(get_crop_img == ""){
                //if user did not crop image then bind inner square image
                  $uploadCrop.croppie('bind', {
                        url: $('.image_src').val()
                        }).then(function(){
                        console.log('jQuery bind complete');
                    });
                        $.holdReady( true );
                    $uploadCrop.croppie('result', {
                        type: 'canvas',
                        size: 'viewport'
                    }).then(function (resp) {
                          $.ajax({
                              url: "image-crop",
                              type: "POST",
                              data: {"image_path": resp},
                              success: function (data) {
                              if(data != ""){
                                $.holdReady( false );
                              }
                              get_crop_img2 = data;
                                $('#get_crop_img_add').html(data);
                              }
                          });
                    });
                    get_crop_img2 = $('#get_crop_img').html();
              }
              console.log("result2="+get_crop_img2);
              $('#get_crop_img_add').html('');  
            }

                        if($('.image_src_small').val() != ''){
              if($('#get_crop_img_add_small').html() == ""){
                $.holdReady( true );
                  $uploadCropSmall.croppie('result', {
                      type: 'canvas',
                      size: 'viewport'
                  }).then(function (resp) {
                        $.ajax({
                            url: "image-crop-small",
                            type: "POST",
                            data: {"image_path": resp},
                            success: function (data) {
                              if(data != ""){
                                $.holdReady( false );
                              }
                              console.log("1="+data);
                              get_crop_img_small = data;
                              $('#get_crop_img_add_small').html(data);
                            }
                        });
                  });
                  get_crop_img_small = $('#get_crop_img_add_small').html();
                  // console.log("result1="+get_crop_img);
              }
              if(get_crop_img_small == ""){
                //if user did not crop image then bind inner square image
                  $uploadCropSmall.croppie('bind', {
                        url: $('.image_src_small').val()
                        }).then(function(){
                        console.log('jQuery bind complete');
                    });
                        $.holdReady( true );
                    $uploadCropSmall.croppie('result', {
                        type: 'canvas',
                        size: 'viewport'
                    }).then(function (resp) {
                          $.ajax({
                              url: "image-crop-small",
                              type: "POST",
                              data: {"image_path": resp},
                              success: function (data) {
                              if(data != ""){
                                $.holdReady( false );
                              }
                              get_crop_img2_small = data;
                                $('#get_crop_img_add_small').html(data);
                              }
                          });
                    });
                    get_crop_img2_small = $('#get_crop_img_small').html();
              }
              // console.log("result2="+get_crop_img2);
              $('#get_crop_img_add_small').html('');  
            }

          $("#show_loader").show();
          $('.layout-content').attr('id','edit_overlay');
           setTimeout(function () {  
            if(get_crop_img == ""){
              get_crop_img = get_crop_img2;
            }

            if(get_crop_img_small == ""){
              get_crop_img_small = get_crop_img2_small;
            }
             //code for image upload ends here //

            var title = $('#set_title').val();
            var description = $('#description').val();
            var body = $('#body').val();
            var competency = $('#competency_id').val();
            var development_area = $('#development_area_id').val();
            var level = $('#level').val();

            var keywords = $('#keywords').val();
            var author = $('#author').val();
            // var is_active = $('#is_active').val();
            // var is_available = $('#is_available').val();
            var is_active = $('#is_active').prop("checked");
            if(is_active == true){
              is_active = 1;
            }else{
              is_active = 0;
            }
            var is_available = $('#is_available').prop("checked");
            if(is_available == true){
              is_available = 1;
            }else{
              is_available = 0;
            }         

            if($('#edit_article_id').val() == ""){

            $.ajax({
              'type':'POST',
              'url':"addArticle",
              data: {
                "title": title,"get_crop_img":get_crop_img,"get_crop_img_small":get_crop_img_small,"description":description,"body":body,"competency":competency,"development_area":development_area,"level":level,"keywords":keywords,"author":author,"is_active":is_active,"is_available":is_available
              },
            success:function(response){
              console.log(response);
              response = JSON.parse(response);
              if(response['status'] == "success"){
                  var getno = 0;
                  if($('.tab_name').val() == "article"){
                      getno = $('#pagination_drop_article').val();
                      ajaxPagination('article',getno,'');
                      $('#your_msg_art').html(response['message']);

                      $('#show_msg_art').show();
                      $('#show_msg_art').delay(4000).fadeOut();
                      
                    window.location = "contentlibrary";
                  }else{
                      getno = $('#pagination_drop').val();
                      ajaxPagination('',getno,'');
                      $('#your_msg').html(response['message']);

                      $('#show_msg').show();
                      $('#show_msg').delay(4000).fadeOut();
                      window.location = "contentlibrary";           
                  }
              }else{
                    $('#add_art_error').html(response['message']);

                    $('#addarticle').find('.upload_error_msg').show();
                    $('#addarticle').find('.upload_error_msg').delay(6000).fadeOut();
              }

            }
              

            });


            }else{
                var edit_article_id = $('#edit_article_id').val();
                $.ajax({
                  'type':'POST',
                  'url':"updateArticle",
                  data: {
                    "edit_article_title": title,"get_crop_img":get_crop_img,"get_crop_img_small":get_crop_img_small,"edit_article_description":description,"edit_article_body":body,"edit_copentency":competency,"edit_development_area":development_area,"edit_level":level,"edit_article_keywords":keywords,"edit_article_author":author,"edit_article_id":edit_article_id,"edit_article_active":is_active,"edit_article_available":is_available
                  },
                success:function(response){
                  console.log(response);
              response = JSON.parse(response);
              if(response['status'] == "success"){
                  var getno = 0;
                  if($('.tab_name').val() == "article"){
                      getno = $('#pagination_drop_article').val();
                      ajaxPagination('article',getno,'');
                      $('#your_msg_art').html(response['message']);

                      $('#show_msg_art').show();
                      $('#show_msg_art').delay(4000).fadeOut();
                      
                    // window.location = "contentlibrary";
                  }else{
                      getno = $('#pagination_drop').val();
                      ajaxPagination('',getno,'');
                      $('#your_msg').html(response['message']);

                      $('#show_msg').show();
                      $('#show_msg').delay(4000).fadeOut();
                      // window.location = "contentlibrary";           
                  }
              }else{
                    $('#add_art_error').html(response['message']);

                    $('#addarticle').find('.upload_error_msg').show();
                    $('#addarticle').find('.upload_error_msg').delay(6000).fadeOut();
              }
                }
                });  
              }  
              $('.layout-content').removeAttr('id','edit_overlay');
              $('#show_loader').hide();
           }, 10000);           

            return false;
          }
      });  

//validate and add document form starts here

    $('#add_document_form').validate({ 
        rules: {
          title: {
               required: true,
               minlength: 3
           },
          level :{
            required: true
          },
          keywords :{
            required: true
          },
          author :{
             required: true
          }
        },
        messages: {
          title: {
               required: "Title is required",
               minlength: "Title must contain at least {0} characters"
           },
           level :{
                required: "Please select level"
           },
           keywords:{
                required: "Keywords are required"
           },
           author : {
                required: "Author is required"
           }
        }, 
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
          $(element).parents('.form-group').addClass('has-success');
        },
          submitHandler: function(form) {

                var get_crop_img = "";
                var get_crop_img2 = ""; 
                var get_crop_img_small = "";
                var get_crop_img2_small = "";

                if($('.doc_image_src').val() != ''){
                  if($('#doc_get_crop_img_add').html() == ""){
                    $.holdReady( true );
                      $uploadCropDoc.croppie('result', {
                          type: 'canvas',
                          size: 'viewport'
                      }).then(function (resp) {
                            $.ajax({
                                url: "image-crop",
                                type: "POST",
                                data: {"image_path": resp},
                                success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  console.log("1="+data);
                                  get_crop_img = data;
                                  $('#doc_get_crop_img_add').html(data);
                                }
                            });
                      });
                      get_crop_img = $('#doc_get_crop_img_add').html();
                      console.log("result1="+get_crop_img);
                  }
                  if(get_crop_img == ""){
                    //if user did not crop image then bind inner square image
                      $uploadCropDoc.croppie('bind', {
                            url: $('.doc_image_src').val()
                            }).then(function(){
                            console.log('jQuery bind complete');
                        });
                            $.holdReady( true );
                        $uploadCropDoc.croppie('result', {
                            type: 'canvas',
                            size: 'viewport'
                        }).then(function (resp) {
                              $.ajax({
                                  url: "image-crop",
                                  type: "POST",
                                  data: {"image_path": resp},
                                  success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  get_crop_img2 = data;
                                    $('#doc_get_crop_img_add').html(data);
                                  }
                              });
                        });
                        get_crop_img2 = $('#doc_get_crop_img_add').html();
                  }
                  console.log("result2="+get_crop_img2);
                  $('#doc_get_crop_img_add').html('');  
                }    

                if($('.doc_image_src_small').val() != ''){
                  if($('#doc_get_crop_img_add_small').html() == ""){
                    $.holdReady( true );
                      $uploadCropDocSmall.croppie('result', {
                          type: 'canvas',
                          size: 'viewport'
                      }).then(function (resp) {
                            $.ajax({
                                url: "image-crop-small",
                                type: "POST",
                                data: {"image_path": resp},
                                success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  console.log("1="+data);
                                  get_crop_img_small = data;
                                  $('#doc_get_crop_img_add_small').html(data);
                                }
                            });
                      });
                      get_crop_img_small = $('#doc_get_crop_img_add_small').html();
                      console.log("result1="+get_crop_img_small);
                  }
                  if(get_crop_img_small == ""){
                    //if user did not crop image then bind inner square image
                      $uploadCropDocSmall.croppie('bind', {
                            url: $('.doc_image_src_small').val()
                            }).then(function(){
                            console.log('jQuery bind complete');
                        });
                            $.holdReady( true );
                        $uploadCropDocSmall.croppie('result', {
                            type: 'canvas',
                            size: 'viewport'
                        }).then(function (resp) {
                              $.ajax({
                                  url: "image-crop-small",
                                  type: "POST",
                                  data: {"image_path": resp},
                                  success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  get_crop_img2_small = data;
                                    $('#doc_get_crop_img_add_small').html(data);
                                  }
                              });
                        });
                        get_crop_img2_small = $('#doc_get_crop_img_add_small').html();
                  }
                  console.log("result2="+get_crop_img2_small);
                  $('#doc_get_crop_img_add_small').html('');  
                } 
        $("#show_loader_doc").show();
        $('.layout-content').attr('id','edit_overlay');
        setTimeout(function () { 
        var input = document.getElementById("select_document");
        file = input.files[0];

        var formData = new FormData(this);
        formData.append("select_document", file);
        var edit_document_id = "";
        var get_url = "";
        if($('#edit_document_id').val() == ""){     
            get_url = "addArticle";       
            formData.append("title", $('#doc_title').val());
            formData.append("description", $('#doc_description').val());
            formData.append("competency", $('#doc_competency_id').val());
            formData.append("development_area", $('#doc_development_area_id').val());
            formData.append("level", $('#doc_level').val());
            formData.append("keywords", $('#doc_keywords').val());
            formData.append("author", $('#doc_author').val());
            var is_active = $('#doc_is_active').prop("checked");
            if(is_active == true){
              is_active = 1;
            }else{
              is_active = 0;
            }
            formData.append("is_active", is_active);
            var is_available = $('#doc_is_available').prop("checked");
            if(is_available == true){
              is_available = 1;
            }else{
              is_available = 0;
            }
            formData.append("is_available", is_available);
            formData.append("form_type", "documents");
            formData.append("get_crop_img", get_crop_img);
            formData.append("get_crop_img_small", get_crop_img_small);
        }else{
            get_url = "updateArticle";  
            edit_document_id = $('#edit_document_id').val();
            formData.append("edit_article_title", $('#doc_title').val());
            formData.append("edit_article_description", $('#doc_description').val());
            formData.append("edit_copentency", $('#doc_competency_id').val());
            formData.append("edit_development_area", $('#doc_development_area_id').val());
            formData.append("edit_level", $('#doc_level').val());
            formData.append("edit_article_keywords", $('#doc_keywords').val());
            formData.append("edit_article_author", $('#doc_author').val());
            var is_active = $('#doc_is_active').prop("checked");
            if(is_active == true){
              is_active = 1;
            }else{
              is_active = 0;
            }
            formData.append("edit_article_active", is_active);
            var is_available = $('#doc_is_available').prop("checked");
            if(is_available == true){
              is_available = 1;
            }else{
              is_available = 0;
            }
            formData.append("edit_article_available", is_available);
            formData.append("form_type", "documents");
            formData.append("get_crop_img", get_crop_img);
            formData.append("get_crop_img_small", get_crop_img_small);
            formData.append("edit_article_id", edit_document_id);
        }
        $.ajax({
            url: get_url,
            type: 'POST',
            data:formData,
            dataType: 'JSON',
            contentType : false,
            cache : false,
            processData : false,
            success: function(response) {
              if(edit_document_id == ""){
                if(response['status'] == "success"){
                    var getno = 0;
                  if($('.tab_name').val() == "documents"){
                        getno = $('#pagination_drop_document').val();
                        ajaxPagination('documents',getno,'');
                        $('#your_msg_doc').html(response['message']);

                        $('#show_msg_doc').show();
                        $('#show_msg_doc').delay(4000).fadeOut();
                        window.location = "contentlibrary";

                    }else{
                        getno = $('#pagination_drop').val();
                        ajaxPagination('',getno,'');
                        $('#your_msg').html(response['message']);

                        $('#show_msg').show();
                        $('#show_msg').delay(4000).fadeOut();
                        window.location = "contentlibrary";       
                    }
                }else{
                      $('#update_doc_error').html(response['message']);

                      $('#adddocs').find('.upload_error_msg').show();
                      $('#adddocs').find('.upload_error_msg').delay(6000).fadeOut();
                }
              }else{
                if(response['status'] == "success"){
                    var getno = 0;
                    if($('.tab_name').val() == "documents"){
                        getno = $('#pagination_drop_document').val();
                        ajaxPagination('documents',getno,'');
                        $('#your_msg_doc').html(response['message']);

                        $('#show_msg_doc').show();
                        $('#show_msg_doc').delay(4000).fadeOut();
                       
                    }else{
                              getno = $('#pagination_drop').val();
                              ajaxPagination('',getno,'');
                              $('#your_msg').html(response['message']);

                              $('#show_msg').show();
                              $('#show_msg').delay(4000).fadeOut();
                                       
                    }
                }else{
                      $('#update_doc_error').html(response['message']);

                      $('#adddocs').find('.upload_error_msg').show();
                      $('#adddocs').find('.upload_error_msg').delay(6000).fadeOut();
                }
              }
            }
        });
          $('.layout-content').removeAttr('id','edit_overlay');
            $('#show_loader_doc').hide();
          }, 10000); 
          return false;              
          }
      });  

//validate and add document form ends here    


//validate add edit audio/video form starts here
$('#add_av_form').validate({ 
      rules: {
          title: {
               required: true,
               minlength: 3
           },
          level :{
            required: true
          },
          av_keywords :{
            required: true
          },
          author :{
             required: true
          }
        },
        messages: {
          title: {
               required: "Title is required",
               minlength: "Title must contain at least {0} characters"
           },
           level :{
                required: "Please select level"
           },
           av_keywords:{
                required: "Keywords are required"
           },
           author : {
                required: "Author is required"
           }
        }, 
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
          $(element).parents('.form-group').addClass('has-success');
        },
          submitHandler: function(form) {
            if(!is_url($('#embed_url').val())){
                $('#update_av_error').html("Invalid Embed URL");

                $('#addav').find('.upload_error_msg').show();
                $('#addav').find('.upload_error_msg').delay(6000).fadeOut();
                return false;
            }else{
              var embed_url = $('#embed_url').val();
            }
                var get_crop_img = "";
                var get_crop_img2 = ""; 
                var get_crop_img_small = "";
                var get_crop_img2_small = "";

                if($('.av_image_src').val() != ''){
                  if($('#av_get_crop_img_add').html() == ""){
                    $.holdReady( true );
                      $uploadCropAv.croppie('result', {
                          type: 'canvas',
                          size: 'viewport'
                      }).then(function (resp) {
                            $.ajax({
                                url: "image-crop",
                                type: "POST",
                                data: {"image_path": resp},
                                success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  console.log("1="+data);
                                  get_crop_img = data;
                                  $('#av_get_crop_img_add').html(data);
                                }
                            });
                      });
                      get_crop_img = $('#av_get_crop_img_add').html();
                      console.log("result1="+get_crop_img);
                  }
                  if(get_crop_img == ""){
                    //if user did not crop image then bind inner square image
                      $uploadCropAv.croppie('bind', {
                            url: $('.av_image_src').val()
                            }).then(function(){
                            console.log('jQuery bind complete');
                        });
                            $.holdReady( true );
                        $uploadCropAv.croppie('result', {
                            type: 'canvas',
                            size: 'viewport'
                        }).then(function (resp) {
                              $.ajax({
                                  url: "image-crop",
                                  type: "POST",
                                  data: {"image_path": resp},
                                  success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  get_crop_img2 = data;
                                    $('#av_get_crop_img_add').html(data);
                                  }
                              });
                        });
                        get_crop_img2 = $('#av_get_crop_img_add').html();
                  }
                  console.log("result2="+get_crop_img2);
                  $('#av_get_crop_img_add').html('');  
                }

                if($('.av_image_src_small').val() != ''){
                  if($('#av_get_crop_img_add_small').html() == ""){
                    $.holdReady( true );
                      $uploadCropAvSmall.croppie('result', {
                          type: 'canvas',
                          size: 'viewport'
                      }).then(function (resp) {
                            $.ajax({
                                url: "image-crop-small",
                                type: "POST",
                                data: {"image_path": resp},
                                success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  console.log("1="+data);
                                  get_crop_img_small = data;
                                  $('#av_get_crop_img_add_small').html(data);
                                }
                            });
                      });
                      get_crop_img_small = $('#av_get_crop_img_add_small').html();
                      console.log("result1="+get_crop_img_small);
                  }
                  if(get_crop_img_small == ""){
                    //if user did not crop image then bind inner square image
                      $uploadCropAvSmall.croppie('bind', {
                            url: $('.av_image_src_small').val()
                            }).then(function(){
                            console.log('jQuery bind complete');
                        });
                            $.holdReady( true );
                        $uploadCropAvSmall.croppie('result', {
                            type: 'canvas',
                            size: 'viewport'
                        }).then(function (resp) {
                              $.ajax({
                                  url: "image-crop-small",
                                  type: "POST",
                                  data: {"image_path": resp},
                                  success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  get_crop_img2_small = data;
                                    $('#av_get_crop_img_add_small').html(data);
                                  }
                              });
                        });
                        get_crop_img2_small = $('#av_get_crop_img_add_small').html();
                  }
                  console.log("result2="+get_crop_img2_small);
                  $('#av_get_crop_img_add_small').html('');  
                }
              //image upload code ends here
        $("#show_loader_av").show();
        $('.layout-content').attr('id','edit_overlay');
        setTimeout(function () { 
        var input = document.getElementById("select_video");
        file = input.files[0];

        var formData = new FormData(this);
        formData.append("select_video", file);
        var edit_document_id = "";
        var get_url = "";
        if($('#edit_av_id').val() == ""){    
            get_url = "addArticle";    
            formData.append("title", $('#av_title').val());
            formData.append("description", $('#av_description').val());
            formData.append("competency", $('#av_competency_id').val());
            formData.append("development_area", $('#av_development_area_id').val());
            formData.append("level", $('#av_level').val());
            formData.append("keywords", $('#av_keywords').val());
            formData.append("author", $('#av_author').val());
            var is_active = $('#av_is_active').prop("checked");
            if(is_active == true){
              is_active = 1;
            }else{
              is_active = 0;
            }
            formData.append("is_active", is_active);
            var is_available = $('#av_is_available').prop("checked");
            if(is_available == true){
              is_available = 1;
            }else{
              is_available = 0;
            }
            formData.append("is_available", is_available);
            formData.append("form_type", "videos");
            formData.append("get_crop_img", get_crop_img);
            formData.append("get_crop_img_small", get_crop_img_small);
            formData.append("embed_url",embed_url);
        }else{
            get_url = "updateArticle";
            edit_document_id = $('#edit_av_id').val();
            formData.append("edit_article_title", $('#av_title').val());
            formData.append("edit_article_description", $('#av_description').val());
            formData.append("edit_copentency", $('#av_competency_id').val());
            formData.append("edit_development_area", $('#av_development_area_id').val());
            formData.append("edit_level", $('#av_level').val());
            formData.append("edit_article_keywords", $('#av_keywords').val());
            formData.append("edit_article_author", $('#av_author').val());
            var is_active = $('#av_is_active').prop("checked");
            if(is_active == true){
              is_active = 1;
            }else{
              is_active = 0;
            }
            formData.append("edit_article_active", is_active);
            var is_available = $('#av_is_available').prop("checked");
            if(is_available == true){
              is_available = 1;
            }else{
              is_available = 0;
            }
            formData.append("edit_article_available", is_available);
            formData.append("form_type", "videos");
            formData.append("get_crop_img", get_crop_img);
            formData.append("get_crop_img_small", get_crop_img_small);
            formData.append("edit_article_id", edit_document_id);
            formData.append("embed_url",embed_url);
        }
        $.ajax({
            url: get_url,
            type: 'POST',
            data:formData,
            dataType: 'JSON',
            contentType : false,
            cache : false,
            processData : false,
            success: function(response) {
              if(edit_document_id == ""){
                  if(response['status'] == "success"){
                      var getno = 0;
                      if($('.tab_name').val() == "videos"){
                          getno = $('#pagination_drop_av').val();
                          ajaxPagination('videos',getno,'');
                          $('#your_msg_av').html(response['message']);

                          $('#show_msg_av').show();
                          $('#show_msg_av').delay(4000).fadeOut();
                          $('#addav').modal('hide');
                      }else{
                          getno = $('#pagination_drop').val();
                          ajaxPagination('',getno,'');
                          $('#your_msg').html(response['message']);

                          $('#show_msg').show();
                          $('#show_msg').delay(4000).fadeOut();
                          $('#addav').modal('hide');           
                      }
                  }else{
                        $('#update_av_error').html(response['message']);

                        $('#adddocs').find('.upload_error_msg').show();
                        $('#adddocs').find('.upload_error_msg').delay(6000).fadeOut();
                  }
              }else{
                if(response['status'] == "success"){
                    var getno = 0;
                    if($('.tab_name').val() == "videos"){
                        getno = $('#pagination_drop_av').val();
                        ajaxPagination('videos',getno,'');
                        $('#your_msg_av').html(response['message']);

                        $('#show_msg_av').show();
                        $('#show_msg_av').delay(4000).fadeOut();
                        $('#addav').modal('hide');
                    }else{
                              getno = $('#pagination_drop').val();
                              ajaxPagination('',getno,'');
                              $('#your_msg').html(response['message']);

                              $('#show_msg').show();
                              $('#show_msg').delay(4000).fadeOut();
                              $('#addav').modal('hide');           
                    }
                }else{
                      $('#update_av_error').html(response['message']);

                      $('#addav').find('.upload_error_msg').show();
                      $('#addav').find('.upload_error_msg').delay(6000).fadeOut();
                }
              }
            }
        });
          $('.layout-content').removeAttr('id','edit_overlay');
            $('#show_loader_av').hide();
          }, 10000); 
          return false;
        }
    });
//validate add edit audio/video ends here    

//validate add edit webinar starts here
$('#add_webinar_form').validate({ 
      rules: {
          title: {
               required: true,
               minlength: 3
           },
          level :{
            required: true
          },
          webinar_keywords :{
            required: true
          },
          author :{
             required: true
          }
        },
        messages: {
          title: {
               required: "Title is required",
               minlength: "Title must contain at least {0} characters"
           },
           level :{
                required: "Please select level"
           },
           webinar_keywords:{
                required: "Keywords are required"
           },
           author : {
                required: "Author is required"
           }
        }, 
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
          $(element).parents('.form-group').addClass('has-success');
        },
          submitHandler: function(form) {
                var get_crop_img = "";
                var get_crop_img2 = ""; 
                var get_crop_img_small = "";
                var get_crop_img2_small = "";                 

                if($('.webinar_image_src').val() != ''){
                  if($('#webinar_get_crop_img_add').html() == ""){
                    $.holdReady( true );
                      $uploadCropWebinar.croppie('result', {
                          type: 'canvas',
                          size: 'viewport'
                      }).then(function (resp) {
                            $.ajax({
                                url: "image-crop",
                                type: "POST",
                                data: {"image_path": resp},
                                success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  console.log("1="+data);
                                  get_crop_img = data;
                                  $('#av_get_crop_img_add').html(data);
                                }
                            });
                      });
                      get_crop_img = $('#webinar_get_crop_img_add').html();
                      console.log("result1="+get_crop_img);
                  }
                  if(get_crop_img == ""){
                    //if user did not crop image then bind inner square image
                      $uploadCropWebinar.croppie('bind', {
                            url: $('.webinar_image_src').val()
                            }).then(function(){
                            console.log('jQuery bind complete');
                        });
                            $.holdReady( true );
                        $uploadCropWebinar.croppie('result', {
                            type: 'canvas',
                            size: 'viewport'
                        }).then(function (resp) {
                              $.ajax({
                                  url: "image-crop",
                                  type: "POST",
                                  data: {"image_path": resp},
                                  success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  get_crop_img2 = data;
                                    $('#webinar_get_crop_img_add').html(data);
                                  }
                              });
                        });
                        get_crop_img2 = $('#webinar_get_crop_img_add').html();
                  }
                  console.log("result2="+get_crop_img2);
                  $('#webinar_get_crop_img_add').html('');  
                }

                if($('.webinar_image_src_small').val() != ''){
                  if($('#webinar_get_crop_img_add_small').html() == ""){
                    $.holdReady( true );
                      $uploadCropWebinarSmall.croppie('result', {
                          type: 'canvas',
                          size: 'viewport'
                      }).then(function (resp) {
                            $.ajax({
                                url: "image-crop-small",
                                type: "POST",
                                data: {"image_path": resp},
                                success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  console.log("1="+data);
                                  get_crop_img_small = data;
                                  $('#av_get_crop_img_add_small').html(data);
                                }
                            });
                      });
                      get_crop_img_small = $('#webinar_get_crop_img_add_small').html();
                      console.log("result1="+get_crop_img_small);
                  }
                  if(get_crop_img_small == ""){
                    //if user did not crop image then bind inner square image
                      $uploadCropWebinarSmall.croppie('bind', {
                            url: $('.webinar_image_src_small').val()
                            }).then(function(){
                            console.log('jQuery bind complete');
                        });
                            $.holdReady( true );
                        $uploadCropWebinarSmall.croppie('result', {
                            type: 'canvas',
                            size: 'viewport'
                        }).then(function (resp) {
                              $.ajax({
                                  url: "image-crop-small",
                                  type: "POST",
                                  data: {"image_path": resp},
                                  success: function (data) {
                                  if(data != ""){
                                    $.holdReady( false );
                                  }
                                  get_crop_img2_small = data;
                                    $('#webinar_get_crop_img_add_small').html(data);
                                  }
                              });
                        });
                        get_crop_img2_small = $('#webinar_get_crop_img_add_small').html();
                  }
                  console.log("result2="+get_crop_img2_small);
                  $('#webinar_get_crop_img_add_small').html('');  
                }
              //image upload code ends here
              //upload code starts here    
            if($('#edit_webinar_id').val() == ""){

                $("#show_loader_webinar").show();
                $('.layout-content').attr('id','edit_overlay');
                 setTimeout(function () { 
                  if(get_crop_img == ""){
                    get_crop_img = get_crop_img2;
                  }

                  if(get_crop_img_small == ""){
                    get_crop_img_small = get_crop_img2_small;
                  }
                   //code for image upload ends here //
                  var title = $('#webinar_title').val();
                  var description = $('#webinar_description').val();
                  var competency = $('#webinar_competency_id').val();
                  var development_area = $('#webinar_development_area_id').val();
                  var level = $('#webinar_level').val();

                  var keywords = $('#webinar_keywords').val();
                  var author = $('#webinar_author').val();
                  var is_active = $('#webinar_is_active').prop("checked");
                  var webinar_type = $('#webinar_type').val();
                  var webinar_seats_available = $('#webinar_seats_available').val();
                  var webinar_date = $('#webinar_date').val();
                  var webinar_timezone = $('#webinar_timezone').val();
                  var webinar_from_time = $('.webinar_from_time').val();
                  var webinar_to_time = $('.webinar_to_time').val();
                  var webinar_instructor = $('#webinar_instructor').val();

                  if(is_active == true){
                    is_active = 1;
                  }else{
                    is_active = 0;
                  }
                  var is_available = $('#webinar_is_available').prop("checked");
                  if(is_available == true){
                    is_available = 1;
                  }else{
                    is_available = 0;
                  }
                  console.log(get_crop_img_small);
                  $.ajax({
                    'type':'POST',
                    'url':"addArticle",
                    data: {
                      "webinar_instructor":webinar_instructor,"webinar_type":webinar_type,"webinar_seats_available":webinar_seats_available,"webinar_date":webinar_date,"webinar_timezone":webinar_timezone,"webinar_from_time":webinar_from_time,"webinar_to_time":webinar_to_time,"title": title,"get_crop_img":get_crop_img,"get_crop_img_small":get_crop_img_small,"description":description,"competency":competency,"development_area":development_area,"level":level,"keywords":keywords,"author":author,"is_active":is_active,"is_available":is_available,"form_type":"webinar"
                    },
                  success:function(response){
                    response = JSON.parse(response);
                        if(response['status'] == "success"){
                            var getno = 0;
                            if($('.tab_name').val() == "webinar"){
                                getno = $('#pagination_drop_webinar').val();
                                ajaxPagination('webinar',getno,'');
                                $('#your_msg_webinar').html(response['message']);

                                $('#show_msg_webinar').show();
                                $('#show_msg_webinar').delay(4000).fadeOut();
                                $('#addwebinar').modal('hide');
                            }else{
                                getno = $('#pagination_drop').val();
                                ajaxPagination('',getno,'');
                                $('#your_msg').html(response['message']);

                                $('#show_msg').show();
                                $('#show_msg').delay(4000).fadeOut();
                                $('#addwebinar').modal('hide');           
                            }
                        }else{
                              $('#update_webinar_error').html(response['message']);

                              $('#addwebinar').find('.upload_error_msg').show();
                              $('#addwebinar').find('.upload_error_msg').delay(6000).fadeOut();
                        }

                  }
                    
                  });         
                    $('.layout-content').removeAttr('id','edit_overlay');
                    $('#show_loader_webinar').hide();
                 }, 10000); 
                  return false;
            }else{
              var edit_document_id = $('#edit_webinar_id').val();

                $("#show_loader_webinar").show();
                $('.layout-content').attr('id','edit_overlay');
                 setTimeout(function () { 
                  if(get_crop_img == ""){
                    get_crop_img = get_crop_img2;
                  }

                  if(get_crop_img_small == ""){
                    get_crop_img_small = get_crop_img2_small;
                  }
                   //code for image upload ends here //
                  var title = $('#webinar_title').val();
                  var description = $('#webinar_description').val();
                  var competency = $('#webinar_competency_id').val();
                  var development_area = $('#webinar_development_area_id').val();
                  var level = $('#webinar_level').val();

                  var keywords = $('#webinar_keywords').val();
                  var author = $('#webinar_author').val();
                  var is_active = $('#webinar_is_active').prop("checked");
                  var webinar_type = $('#webinar_type').val();
                  var webinar_seats_available = $('#webinar_seats_available').val();
                  var webinar_date = $('#webinar_date').val();
                  var webinar_timezone = $('#webinar_timezone').val();
                  var webinar_from_time = $('.webinar_from_time').val();
                  var webinar_to_time = $('.webinar_to_time').val();
                  var webinar_instructor = $('#webinar_instructor').val();

                  if(is_active == true){
                    is_active = 1;
                  }else{
                    is_active = 0;
                  }
                  var is_available = $('#webinar_is_available').prop("checked");
                  if(is_available == true){
                    is_available = 1;
                  }else{
                    is_available = 0;
                  }
                  $.ajax({
                    'type':'POST',
                    'url':"updateArticle",
                    data: {
                      "webinar_instructor":webinar_instructor,"webinar_type":webinar_type,"webinar_seats_available":webinar_seats_available,"webinar_date":webinar_date,"webinar_timezone":webinar_timezone,"webinar_from_time":webinar_from_time,"webinar_to_time":webinar_to_time,"edit_article_title": title,"get_crop_img":get_crop_img,"get_crop_img_small":get_crop_img_small,"edit_article_description":description,"edit_copentency":competency,"edit_development_area":development_area,"edit_level":level,"edit_article_keywords":keywords,"edit_article_author":author,"edit_article_active":is_active,"edit_article_available":is_available,"form_type":"webinar","edit_article_id":edit_document_id
                    },
                  success:function(response){
                    response = JSON.parse(response);
                        if(response['status'] == "success"){
                            var getno = 0;
                            if($('.tab_name').val() == "webinar"){
                                getno = $('#pagination_drop_webinar').val();
                                ajaxPagination('webinar',getno,'');
                                $('#your_msg_webinar').html(response['message']);

                                $('#show_msg_webinar').show();
                                $('#show_msg_webinar').delay(4000).fadeOut();
                                $('#addwebinar').modal('hide');
                            }else{
                                      getno = $('#pagination_drop').val();
                                      ajaxPagination('',getno,'');
                                      $('#your_msg').html(response['message']);

                                      $('#show_msg').show();
                                      $('#show_msg').delay(4000).fadeOut();
                                      $('#addwebinar').modal('hide');           
                            }
                        }else{
                              $('#update_webinar_error').html(response['message']);

                              $('#addwebinar').find('.upload_error_msg').show();
                              $('#addwebinar').find('.upload_error_msg').delay(6000).fadeOut();
                        }

                  }
                    
                  });         
                    $('.layout-content').removeAttr('id','edit_overlay');
                    $('#show_loader_webinar').hide();
                 }, 10000); 
                  return false;              
            }              

        return false;
        }
    });


    $('#list_competency_filter').on('change',function(){
      $.ajax({
        'type':'POST',
        'url':"getDevelopementAreaCL",
        data: {
          "competency_id" : $(this).val()
          },
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Development Area</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tda_id']+"'>"+response[i]['tda_development']+"</option>";
          }

          $('#list_dev_area_filter').html(html);
        }
      });       
    });

    $('#con_lib_filter_btn').on('click',function(e){
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';
        getno = $('#pagination_drop').val();
        var tab_name = $('.tab_name').val();
        ajaxPagination(tab_name,getno,filter_list);
         $('.Cus_card-body').slideToggle("slow");
        $('.Cus_card-toggler').removeClass("active");
        e.preventDefault();
    });

    $('#con_lib_filter_close_btn').on('click',function(){
        $('#library_keyword_filter').val('');
        $('#list_category_filter').val('');
        $('#list_course_filter').val('');
        $('#list_competency_filter').val('');
        $('#list_dev_area_filter').val('');
        $('#list_senority_level_filter').val('');

        getno = $('#pagination_drop').val();
        var tab_name = $('.tab_name').val();
        ajaxPagination(tab_name,getno,'');
         $('.Cus_card-body').slideToggle("slow");
        $('.Cus_card-toggler').removeClass("active");
        e.preventDefault();
    });
    
    setEditArticleForm();
    setEditAVForm();
    setEditDocumentForm()
    setEditWebinarForm()
//validate add edit webinar ends here
});



$(document).on('change','#pagination_drop',function(e){
  var getno = $(this).val();
    e.preventDefault();

   var tab_name = $('.tab_name').val();
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
   ajaxPagination(tab_name,getno,filter_list);
});

$(document).on('change','#pagination_drop_article',function(e){
  var getno = $(this).val();
    e.preventDefault();

   var tab_name = $('.tab_name').val();
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
   ajaxPagination(tab_name,getno,filter_list);
});

$(document).on('change','#pagination_drop_document',function(e){
  var getno = $(this).val();
    e.preventDefault();

   var tab_name = $('.tab_name').val();
         var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';  
   ajaxPagination(tab_name,getno,filter_list);
});

$(document).on('change','#pagination_drop_av',function(e){
  var getno = $(this).val();
    e.preventDefault();

   var tab_name = $('.tab_name').val();
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
   ajaxPagination(tab_name,getno,filter_list);
});

$(document).on('change','#pagination_drop_fav',function(e){
  var getno = $(this).val();
    e.preventDefault();

   var tab_name = $('.tab_name').val();
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
   ajaxPagination(tab_name,getno,filter_list);
});

$(document).on('change','#pagination_drop_webinar',function(e){
  var getno = $(this).val();
    e.preventDefault();

   var tab_name = $('.tab_name').val();
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';   
   ajaxPagination(tab_name,getno,filter_list);
});

function ajaxPagination($tab_name,$getno,$filterlist){
  var filterlist = $filterlist;
  var getno = $getno;
  var tab_name = $tab_name;
  console.log(tab_name);
    $.ajax({
        'type':'POST',
        'url':"ajax/pagination",
        'data':{
          "pagination_no": getno,
          "tab_name" : tab_name,
          "filterlist":filterlist
        },
        success:function(response){
          if(tab_name == ""){
            $('#displayallcontent').html(response);
            $('.pagination_count').attr('id','pagination_drop');
          }else if(tab_name == "article"){
            $('#article_div').html(response);
            $('.pagination_count').attr('id','pagination_drop_article');
          }else if(tab_name == "documents"){
            $('#document_div').html(response);
            $('.pagination_count').attr('id','pagination_drop_document');
          }else if(tab_name == "videos"){
            $('#av_div').html(response);
            $('.pagination_count').attr('id','pagination_drop_av');
          }else if(tab_name == "webinar"){
            $('#webinar_div').html(response);
            $('.pagination_count').attr('id','pagination_drop_webinar');
          }else if(tab_name == "favourite"){
            $('#favourite_div').html(response);
            $('.pagination_count').attr('id','pagination_drop_fav');
          }
          $('.tab_name').val($tab_name);
          if(getno != ""){
            $("#pagination_drop option[value='"+getno+"']").attr("selected", "selected");
          }
        }
      });

}

$(document).on('click','.pagination a',function(e){
  e.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
 
  readPage(page);
});


function readPage(page){
        var library_keyword_filter = $('#library_keyword_filter').val();
        var list_category_filter = $('#list_category_filter').val();
        var list_course_filter = $('#list_course_filter').val();
        var list_competency_filter  = $('#list_competency_filter').val();
        var list_dev_area_filter = $('#list_dev_area_filter').val();
        var list_senority_level_filter = $('#list_senority_level_filter').val();

        var filter_list = '{"library_keyword_filter":"'+library_keyword_filter+'","list_category_filter":"'+list_category_filter+'","list_course_filter":"'+list_course_filter+'","list_competency_filter":"'+list_competency_filter+'","list_dev_area_filter":"'+list_dev_area_filter+'","list_senority_level_filter":"'+list_senority_level_filter+'"}';
  var pagination_drop = $('#pagination_drop').val();

  var tab_name = $('.tab_name').val();
        $.ajax({
          'url':"ajax/getNextArt?page="+page,
          'data':{
            'pagination_drop':pagination_drop,
            'tab_name':tab_name,
            'filterlist':filter_list
          }
        }).done(function(data){
          if(tab_name == ""){
            $('#displayallcontent').html(data);
            $('.pagination_count').attr('id','pagination_drop');
          }else if(tab_name == "article"){
            $('#article_div').html(data);
            $('.pagination_count').attr('id','pagination_drop_article');
          }else if(tab_name == "documents"){
            $('#document_div').html(data);
            $('.pagination_count').attr('id','pagination_drop_document');
          }else if(tab_name == "videos"){
            $('#av_div').html(data);
            $('.pagination_count').attr('id','pagination_drop_av');
          }else if(tab_name == "webinar"){
            $('#webinar_div').html(data);
            $('.pagination_count').attr('id','pagination_drop_webinar');
          }else if(tab_name == "favourite"){
            $('#favourite_div').html(data);
            $('.pagination_count').attr('id','pagination_drop_fav');
          }
          $("#pagination_drop option[value='"+pagination_drop+"']").attr("selected", "selected");          
        });
}

function deleteContent($id){
    swal({
      title: "Are you sure?",
      text: "You want to delete the content?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      closeOnConfirm: true,
      confirmButtonText: "Delete",
      showLoaderOnConfirm: true
    }, function () {
          $.ajax({
              'type':'POST',
              'url':"deleteArticle",
              data: {
                "article_id": $id
            },
            success:function(response){
              response = JSON.parse(response);
              if(response['status'] == "success"){
                // $("#carddiv"+$id).remove();
                // swal("Deleted!", "Content has been deleted.", "success");
                  if($('.tab_name').val() == "article"){
                    getno = $('#pagination_drop_article').val();
                      ajaxPagination('article',getno,'');
                        $('#your_msg_art').html('Article has been deleted successfully!!');

                        $('#show_msg_art').show();
                        $('#show_msg_art').delay(4000).fadeOut();
                  }else if($('.tab_name').val() == "documents"){
                    getno = $('#pagination_drop_document').val();
                      ajaxPagination('documents',getno,'');
                        $('#your_msg_doc').html('Document has been deleted successfully!!');

                        $('#show_msg_doc').show();
                        $('#show_msg_doc').delay(4000).fadeOut();
                  }else if($('.tab_name').val() == "videos"){
                    getno = $('#pagination_drop_av').val();
                      ajaxPagination('videos',getno,'');
                        $('#your_msg_av').html('Content has been deleted successfully!!');

                        $('#show_msg_av').show();
                        $('#show_msg_av').delay(4000).fadeOut();
                  }else if($('.tab_name').val() == "webinar"){
                    getno = $('#pagination_drop_webinar').val();
                      ajaxPagination('webinar',getno,'');
                        $('#your_msg_webinar').html('Content has been deleted successfully!!');

                        $('#show_msg_webinar').show();
                        $('#show_msg_webinar').delay(4000).fadeOut();
                  }else{
                    getno = $('#pagination_drop').val();
                      ajaxPagination('',getno,'');
                        $('#your_msg').html('Content has been deleted successfully!!');

                        $('#show_msg').show();
                        $('#show_msg').delay(4000).fadeOut();              
                  }
              }else{
                if($('.tab_name').val() == "article"){
                  $('.upload_error_msg').show();
                  $('#art_div_error').html(response['message']);
                  $('.upload_error_msg').delay(4000).fadeOut();
                }else{
                  $('.upload_error_msg').show();
                  $('#all_div_error').html(response['message']);
                  $('.upload_error_msg').delay(4000).fadeOut();
                }   
              }
            }
          });
    });

}

function updateIsActiveContent($id){
  // update active or inactive content from the list
      var status  = 1;
      if($("#carddiv"+$id).find('#check_switch_'+$id).prop("checked") == true){
          // $('#check_switch_60').prop('checked',true);
          status  = 1;
      }else if($("#carddiv"+$id).find('#check_switch_'+$id).prop("checked") == false){
           status = 0;
      }
      swal({
        title: "Are you sure?",
        text: "You want to save changes?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        closeOnConfirm: true,
        confirmButtonText: 'Submit',
        showLoaderOnConfirm: true
      }, function () {
        $.ajax({
            'type':'POST',
            'url':"updateIsActiveContent",
            data: {
              "article_id": $id,
              "is_active":status
          },
          success:function(response){
            response = JSON.parse(response);
            if(response['status'] == "success"){
              if($('.tab_name').val() == "article"){
                    $('#your_msg_art').html('Changes has been done successfully!!');

                    $('#show_msg_art').show();
                    $('#show_msg_art').delay(4000).fadeOut();
              }else if($('.tab_name').val() == "documents"){
                    $('#your_msg_doc').html('Changes has been done successfully!!');

                    $('#show_msg_doc').show();
                    $('#show_msg_doc').delay(4000).fadeOut();
              }else if($('.tab_name').val() == "videos"){
                    $('#your_msg_av').html('Changes has been done successfully!!');

                    $('#show_msg_av').show();
                    $('#show_msg_av').delay(4000).fadeOut();
              }else if($('.tab_name').val() == "webinar"){
                    $('#your_msg_webinar').html('Changes has been done successfully!!');

                    $('#show_msg_webinar').show();
                    $('#show_msg_webinar').delay(4000).fadeOut();
              }else{
                    $('#your_msg').html('Changes has been done successfully!!');

                    $('#show_msg').show();
                    $('#show_msg').delay(4000).fadeOut();              
              }
            }else{
              if($('.tab_name').val() == "article"){
                $('.upload_error_msg').show();
                $('#art_div_error').html(response['message']);
                $('.upload_error_msg').delay(4000).fadeOut();
              }else{
                $('.upload_error_msg').show();
                $('#all_div_error').html(response['message']);
                $('.upload_error_msg').delay(4000).fadeOut();
              }              
            }

          }
        });
  });
}

function addToFavourite($id,$status){
  // update active or inactive content from the list
      var status = 1;
      if($status == 1){
        status = 0;
      }else{
        status = 1;
      }
      console.log(status);
      swal({
        title: "Are you sure?",
        text: "You want to save changes?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        closeOnConfirm: true,
        confirmButtonText: 'Submit',
        showLoaderOnConfirm: true
      }, function () {
        $.ajax({
            'type':'POST',
            'url':"updateIsFavContent",
            data: {
              "article_id": $id,
              "is_active":status
          },
          success:function(response){
            response = JSON.parse(response);
              if(response['status'] == "success"){
                // $("#carddiv"+$id).remove();
                // swal("Deleted!", "Content has been deleted.", "success");
                if($('.tab_name').val() == "article"){
                    getno = $('#pagination_drop_article').val();
                      ajaxPagination('article',getno,'');
                        $('#your_msg_art').html('Content has been changed successfully!!');

                        $('#show_msg_art').show();
                        $('#show_msg_art').delay(4000).fadeOut();
                  }else if($('.tab_name').val() == "documents"){
                    getno = $('#pagination_drop_document').val();
                      ajaxPagination('documents',getno,'');
                        $('#your_msg_doc').html('Content has been changed successfully!!');

                        $('#show_msg_doc').show();
                        $('#show_msg_doc').delay(4000).fadeOut();
                  }else if($('.tab_name').val() == "videos"){
                    getno = $('#pagination_drop_av').val();
                      ajaxPagination('videos',getno,'');
                        $('#your_msg_av').html('Content has been changed successfully!!');

                        $('#show_msg_av').show();
                        $('#show_msg_av').delay(4000).fadeOut();
                  }else if($('.tab_name').val() == "webinar"){
                    getno = $('#pagination_drop_webinar').val();
                      ajaxPagination('webinar',getno,'');
                        $('#your_msg_webinar').html('Content has been changed successfully!!');

                        $('#show_msg_webinar').show();
                        $('#show_msg_webinar').delay(4000).fadeOut();
                  }else if($('.tab_name').val() == "favourite"){
                    getno = $('#pagination_drop_fav').val();
                      ajaxPagination('favourite',getno,'');
                        $('#your_msg_fav').html('Content has been changed successfully!!');

                        $('#show_msg_av').show();
                        $('#show_msg_fav').delay(4000).fadeOut();
                  }else{
                    getno = $('#pagination_drop').val();
                      ajaxPagination('',getno,'');
                        $('#your_msg').html('Content has been changed successfully!!');

                        $('#show_msg').show();
                        $('#show_msg').delay(4000).fadeOut();              
                  }
              }else{
              if($('.tab_name').val() == "article"){
                $('.upload_error_msg').show();
                $('#art_div_error').html(response['message']);
                $('.upload_error_msg').delay(4000).fadeOut();
              }else{
                $('.upload_error_msg').show();
                $('#all_div_error').html(response['message']);
                $('.upload_error_msg').delay(4000).fadeOut();
              }              
            }

          }
        });
  });
}

function readURL(input,$check_img_type) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {

          console.log(e.target.result);
            if($check_img_type == "editimage"){
            $('.image_src_edit').val(e.target.result);

                $uploadCropEdit = $('#upload_demo_edit').croppie({
                          enableExif: true,
                          viewport: {
                              width: 1129,
                              height: 228,
                              type: 'square'
                          },
                          boundary: {
                              width: 1129,
                              height: 228
                          }
                      });                                         

                      $uploadCropEdit.croppie('bind', {
                          url: $('.image_src_edit').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });
                                                                              
                      $('#crop_image_div_edit').show();

            }else if($check_img_type == "docaddimage"){
              $('.doc_image_src').val(e.target.result);
                $uploadCropDoc = $('#doc_upload_demo').croppie({
                          enableExif: true,
                          viewport: {
                              width: 1129,
                              height: 228,
                              type: 'square'
                          },
                          boundary: {
                              width: 1129,
                              height: 228
                          }
                      });                      
                      $uploadCropDoc.croppie('bind', {
                          url: $('.doc_image_src').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });                   
                                                                    
                      $('#doc_crop_image_div').show();
            }else if($check_img_type == "avaddimage"){
              $('.av_image_src').val(e.target.result);
                $uploadCropAv = $('#av_upload_demo').croppie({
                          enableExif: true,
                          viewport: {
                              width: 1129,
                              height: 228,
                              type: 'square'
                          },
                          boundary: {
                              width: 1129,
                              height: 228
                          }
                      });                      
                      $uploadCropAv.croppie('bind', {
                          url: $('.av_image_src').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });                   
                                                                    
                      $('#av_crop_image_div').show();
            }else if($check_img_type == "webinaraddimage"){
              $('.webinar_image_src').val(e.target.result);
                $uploadCropWebinar = $('#webinar_upload_demo').croppie({
                          enableExif: true,
                          viewport: {
                              width: 1129,
                              height: 228,
                              type: 'square'
                          },
                          boundary: {
                              width: 1129,
                              height: 228
                          }
                      });                      
                      $uploadCropWebinar.croppie('bind', {
                          url: $('.webinar_image_src').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });                   
                                                                    
                      $('#webinar_crop_image_div').show();
            }else{
            $('.image_src').val(e.target.result);

                $uploadCrop = $('#upload_demo').croppie({
                          enableExif: true,
                          viewport: {
                              width: 1129,
                              height: 228,
                              type: 'square'
                          },
                          boundary: {
                              width: 1129,
                              height: 228
                          }
                      });

                      $uploadCrop.croppie('bind', {
                          url: $('.image_src').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });
                          console.log('2');
                      $('#crop_image_div').show();
            }

        }
        reader.readAsDataURL(input.files[0]);
    }
}


function readURLSmall(input,$check_img_type_small) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {

          console.log(e.target.result);
            if($check_img_type_small == "editimage"){
            $('.image_src_edit_small').val(e.target.result);

                $uploadCropEditSmall = $('#upload_demo_edit_small').croppie({
                          enableExif: true,
                          viewport: {
                              width: 256,
                              height: 256,
                              type: 'square'
                          },
                          boundary: {
                              width: 256,
                              height: 256
                          }
                      });                                         

                      $uploadCropEditSmall.croppie('bind', {
                          url: $('.image_src_edit_small').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });
                                                                              
                      $('#crop_image_div_edit_small').show();

            }else if($check_img_type_small == "docaddimage"){
              $('.doc_image_src_small').val(e.target.result);
                $uploadCropDocSmall = $('#doc_upload_demo_small').croppie({
                          enableExif: true,
                          viewport: {
                              width: 256,
                              height: 256,
                              type: 'square'
                          },
                          boundary: {
                              width: 256,
                              height: 256
                          }
                      });                      
                      $uploadCropDocSmall.croppie('bind', {
                          url: $('.doc_image_src_small').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });                   
                                                                    
                      $('#doc_crop_image_div_small').show();
            }else if($check_img_type_small == "avaddimage"){
              $('.av_image_src_small').val(e.target.result);
                $uploadCropAvSmall = $('#av_upload_demo_small').croppie({
                          enableExif: true,
                          viewport: {
                              width: 256,
                              height: 256,
                              type: 'square'
                          },
                          boundary: {
                              width: 256,
                              height: 256
                          }
                      });                      
                      $uploadCropAvSmall.croppie('bind', {
                          url: $('.av_image_src_small').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });                   
                                                                    
                      $('#av_crop_image_div_small').show();
            }else if($check_img_type_small == "webinaraddimage"){
              $('.webinar_image_src_small').val(e.target.result);
                $uploadCropWebinarSmall = $('#webinar_upload_demo_small').croppie({
                          enableExif: true,
                          viewport: {
                              width: 256,
                              height: 256,
                              type: 'square'
                          },
                          boundary: {
                              width: 256,
                              height: 256
                          }
                      });                      
                      $uploadCropWebinarSmall.croppie('bind', {
                          url: $('.webinar_image_src_small').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });                   
                                                                    
                      $('#webinar_crop_image_div_small').show();
            }else{
            $('.image_src_small').val(e.target.result);

                $uploadCropSmall = $('#upload_demo_small').croppie({
                          enableExif: true,
                          viewport: {
                              width: 256,
                              height: 256,
                              type: 'square'
                          },
                          boundary: {
                              width: 256,
                              height: 256
                          }
                      });

                      $uploadCropSmall.croppie('bind', {
                          url: $('.image_src_small').val()
                          }).then(function(){
                          console.log('jQuery bind complete');
                      });
                          console.log('2');
                      $('#crop_image_div_small').show();
            }

        }
        reader.readAsDataURL(input.files[0]);
    }
}

function is_url(str)
{
  regexp =  /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        if (regexp.test(str))
        {
          return true;
        }
        else
        {
          return false;
        }
}

function addarticle($articleid){

    if($articleid === null || $articleid === undefined){
      //add article
        $.ajax({
            type: "POST",
            url: "set_session_content",
            data: {
                content_id:"",
                content_type:"add_article"
            }, 
            success: function(data)
            {
              window.location = "openAddArticle";
            }
        });
    }else{
      //edit article
        $.ajax({
            type: "POST",
            url: "set_session_content",
            data: {
                content_id:$articleid,
                content_type:"edit_article"
            }, 
            success: function(data)
            {
              window.location = "openAddArticle";
            }
        });
    }

}

function setEditArticleForm(){
            $.ajax({
               'type':'POST',
               'url':"editArticle",
              success:function(response){
                console.log(response);
                if(response != "false"){
                 response = JSON.parse(response);

                var tcl_developement_area = response['cont_lib'][0]['tcl_developement_area'];
                var selected = '';
                $.ajax({
                    'type':'POST',
                    'url':"getDevelopementAreaCL",
                    data: {
                      "competency_id" : response['cont_lib'][0]['tcl_competency']
                      },
                    success:function(getresponse){
                      console.log(getresponse);
                      getresponse = JSON.parse(getresponse);
                      var html = "<option value=''>Select Development Area</option>";
                      for(var i=0;i<getresponse.length;i++){
                          if(getresponse[i]['tda_id'] == tcl_developement_area){
                             selected = 'selected';
                          }else{
                              selected = '';
                          }
                          html += "<option value='"+getresponse[i]['tda_id']+"' "+selected+">"+getresponse[i]['tda_development']+"</option>";
                      }

                      $('#development_area_id').html(html);
                    }
                });

                  $('#upload_demo').html('');
                  $('#crop_image_div').hide();                
                $('#edit_article_id').val(response['cont_lib'][0]['tcl_id']);
                if(response['cont_lib'][0]['tcl_cover_image'] != ""){
                  // $('#edit_article_cover').css('background-image', 'url(../public/img/' + response['cont_lib'][0]['tcl_cover_image'] + ')');
                  var path = '../public/img/contentlibrary/articles/590X140/' + response['cont_lib'][0]['tcl_cover_image'];
                  $('.hrtigm').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  $('.hrtigm').show();
                }else{
                  // $('.hrtigm').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  // $('#edit_article_cover').css('background-image', '');
                }
                                  $('#upload_demo_small').html('');
                  $('#crop_image_div_small').hide();                

                if(response['cont_lib'][0]['tcl_cover_image_small'] != ""){
                  // $('#edit_article_cover').css('background-image', 'url(../public/img/' + response['cont_lib'][0]['tcl_cover_image'] + ')');
                  var path = '../public/img/contentlibrary/articles/256X256/' + response['cont_lib'][0]['tcl_cover_image_small'];
                  $('.hrtigm_small').html("<img src="+"'"+path+"'"+ " class='img-responsive' width='100%' >");
                  $('.hrtigm_small').show();
                  $('.artnoimg').hide();
                }
                $('#set_title').val(response['cont_lib'][0]['tcl_title']);
                // $('.cke_editable').prepend(response['cont_lib'][0]['tcl_description']);

                $('#description').val(response['cont_lib'][0]['tcl_description']);

                if(CKEDITOR.instances['description']){
                  CKEDITOR.instances['description'].destroy(true);
                }
                CKEDITOR.replace( 'description' );
                $('#body').val(response['cont_lib'][0]['tcl_body']);
                if(CKEDITOR.instances['body']){
                  CKEDITOR.instances['body'].destroy(true);
                }
                CKEDITOR.replace( 'body' );
                $('#author').val(response['cont_lib'][0]['tcl_author']);
                $("#competency_id option[value='"+response['cont_lib'][0]['tcl_competency']+"']").attr("selected", "selected");
                
                $("#level option[value='"+response['cont_lib'][0]['tcl_level']+"']").attr("selected", "selected");

                if(response['cont_lib'][0]['tcl_is_active'] == 1){
                  $("#is_active").prop("checked", true);
                }else{
                  $("#is_active").prop("checked", false);
                }
                if(response['cont_lib'][0]['tcl_is_available'] == 1){
                  $("#is_available").prop("checked", true);
                }else{
                  $("#is_available").prop("checked", false);
                }
                var html = "";
                var html = '<i class="icon icon-book m-r-sm"></i>Leadership, <i class="icon icon-book m-r-sm"></i>Communication';
                for(var i=0;i<response['course_article'].length;i++){
                  html += '<i class="icon icon-book m-r-sm"></i>'+response['course_article'][i]['tc_title']+',';
                }
                html = html.slice(0, -1);
                $('#coursearticle').html(html);
                $('.link-course-panel').show();

                var getkeywords = "";
                $('#keywords').tagsinput('removeAll');
                getkeywords = response['cont_lib'][0]['tcl_keywords'].split(',');
                for(var i=0;i<getkeywords.length;i++){
                  $('#keywords').tagsinput('add',getkeywords[i]);
                }
                }


              }
            });
}


function addav($avid){
    if($avid === null || $avid === undefined){
      //add article
        $.ajax({
            type: "POST",
            url: "set_session_content",
            data: {
                content_id:"",
                content_type:"add_av"
            }, 
            success: function(data)
            {
              window.location = "openAddAudVid";
            }
        });
    }else{
      //edit article
        $.ajax({
            type: "POST",
            url: "set_session_content",
            data: {
                content_id:$avid,
                content_type:"edit_av"
            }, 
            success: function(data)
            {
              window.location = "openAddAudVid";
            }
        });
    }  
}

function setEditAVForm(){
            $('.hrtigav').html('');
            $('.hrtigav').hide();     
            $.ajax({
               'type':'POST',
               'url':"editArticle",
              success:function(response){
                if(response != "false"){
                console.log(response);
                response = JSON.parse(response);

                var tcl_developement_area = response['cont_lib'][0]['tcl_developement_area'];
                var selected = '';
                  $.ajax({
                      'type':'POST',
                      'url':"getDevelopementAreaCL",
                      data: {
                        "competency_id" : response['cont_lib'][0]['tcl_competency']
                        },
                      success:function(getresponse){
                        console.log(getresponse);
                        getresponse = JSON.parse(getresponse);
                        var html = "<option value=''>Select Development Area</option>";
                        for(var i=0;i<getresponse.length;i++){
                            if(getresponse[i]['tda_id'] == tcl_developement_area){
                               selected = 'selected';
                            }else{
                                selected = '';
                            }
                            html += "<option value='"+getresponse[i]['tda_id']+"' "+selected+">"+getresponse[i]['tda_development']+"</option>";
                        }

                        $('#av_development_area_id').html(html);
                      }
                  });

               
                  $('#av_upload_demo').html('');
                  $('#av_crop_image_div').hide();
                $('#edit_av_id').val(response['cont_lib'][0]['tcl_id']);
                if(response['cont_lib'][0]['tcl_cover_image'] != ""){
                  // $('#edit_article_cover').css('background-image', 'url(../public/img/' + response['cont_lib'][0]['tcl_cover_image'] + ')');
                  var path = '../public/img/contentlibrary/articles/590X140/' + response['cont_lib'][0]['tcl_cover_image'];
                  $('.hrtigav').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  $('.hrtigav').show();   
                }else{
                  $('.hrtigav').html('');
                  $('.hrtigav').hide(); 
                  // $('.hrtigm').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  // $('#edit_article_cover').css('background-image', '');
                }

                  $('#av_upload_demo_small').html('');
                  $('#av_crop_image_div_small').hide();
                if(response['cont_lib'][0]['tcl_cover_image_small'] != ""){
                  // $('#edit_article_cover').css('background-image', 'url(../public/img/' + response['cont_lib'][0]['tcl_cover_image'] + ')');
                  var path = '../public/img/contentlibrary/articles/256X256/' + response['cont_lib'][0]['tcl_cover_image_small'];
                  $('.hrtigav_small').html("<img src="+"'"+path+"'"+ " class='img-responsive' width='100%' >");
                  $('.hrtigav_small').show(); 

                  $('.avnoimg').hide();  
                }else{
                  $('.hrtigav_small').html('');
                  $('.hrtigav_small').hide(); 
                  // $('.hrtigm').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  // $('#edit_article_cover').css('background-image', '');
                }
              
                $('#av_title').val(response['cont_lib'][0]['tcl_title']);
                $('#video_name').html(response['cont_lib'][0]['tcl_document']);
                $('#show_vid_name').show();
                $('#embed_url').val(response['cont_lib'][0]['tcl_embed_video']);
                // $('.cke_editable').prepend(response['cont_lib'][0]['tcl_description']);

                $('#av_description').val(response['cont_lib'][0]['tcl_description']);

                if(CKEDITOR.instances['av_description']){
                  CKEDITOR.instances['av_description'].destroy(true);
                }
                CKEDITOR.replace( 'av_description' );
          
                $('#av_author').val(response['cont_lib'][0]['tcl_author']);
                $("#av_competency_id option[value='"+response['cont_lib'][0]['tcl_competency']+"']").attr("selected", "selected");
                
                $("#av_level option[value='"+response['cont_lib'][0]['tcl_level']+"']").attr("selected", "selected");

                if(response['cont_lib'][0]['tcl_is_active'] == 1){
                  $("#av_is_active").prop("checked", true);
                }else{
                  $("#av_is_active").prop("checked", false);
                }
                if(response['cont_lib'][0]['tcl_is_available'] == 1){
                  $("#av_is_available").prop("checked", true);
                }else{
                  $("#av_is_available").prop("checked", false);
                }

                var getkeywords = "";
                $('#av_keywords').tagsinput('removeAll');
                getkeywords = response['cont_lib'][0]['tcl_keywords'].split(',');
                for(var i=0;i<getkeywords.length;i++){
                  $('#av_keywords').tagsinput('add',getkeywords[i]);
                }
              }
            }
        });          
}

function adddocument($docid){
    if($docid === null || $docid === undefined){
      //add article
        $.ajax({
            type: "POST",
            url: "set_session_content",
            data: {
                content_id:"",
                content_type:"add_doc"
            }, 
            success: function(data)
            {
              window.location = "openAddDocument";
            }
        });
    }else{
      //edit article
        $.ajax({
            type: "POST",
            url: "set_session_content",
            data: {
                content_id:$docid,
                content_type:"edit_doc"
            }, 
            success: function(data)
            {
              window.location = "openAddDocument";
            }
        });
    }   
}

function setEditDocumentForm(){
        $('.hrtigmw').html('');
        $('.hrtigmw').hide();     
            $.ajax({
               'type':'POST',
               'url':"editArticle",
              success:function(response){
                console.log(response);
                if(response != "false"){
                response = JSON.parse(response);

                var tcl_developement_area = response['cont_lib'][0]['tcl_developement_area'];
                var selected = '';
                $.ajax({
                    'type':'POST',
                    'url':"getDevelopementAreaCL",
                    data: {
                      "competency_id" : response['cont_lib'][0]['tcl_competency']
                      },
                    success:function(getresponse){
                      console.log(getresponse);
                      getresponse = JSON.parse(getresponse);
                      var html = "<option value=''>Select Development Area</option>";
                      for(var i=0;i<getresponse.length;i++){
                          if(getresponse[i]['tda_id'] == tcl_developement_area){
                             selected = 'selected';
                          }else{
                              selected = '';
                          }
                          html += "<option value='"+getresponse[i]['tda_id']+"' "+selected+">"+getresponse[i]['tda_development']+"</option>";
                      }

                      $('#doc_development_area_id').html(html);
                    }
                });

                
                  $('#doc_upload_demo').html('');
                  $('#doc_crop_image_div').hide();
                $('#edit_document_id').val(response['cont_lib'][0]['tcl_id']);
                if(response['cont_lib'][0]['tcl_cover_image'] != ""){
                  // $('#edit_article_cover').css('background-image', 'url(../public/img/' + response['cont_lib'][0]['tcl_cover_image'] + ')');
                  var path = '../public/img/contentlibrary/articles/590X140/' + response['cont_lib'][0]['tcl_cover_image'];
                  $('.hrtigmw').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  $('.hrtigmw').show();   
                }else{
                  $('.hrtigmw').html('');
                  $('.hrtigmw').hide(); 
                  // $('.hrtigm').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  // $('#edit_article_cover').css('background-image', '');
                }

                  $('#doc_upload_demo_small').html('');
                  $('#doc_crop_image_div_small').hide();
                if(response['cont_lib'][0]['tcl_cover_image_small'] != ""){
                  // $('#edit_article_cover').css('background-image', 'url(../public/img/' + response['cont_lib'][0]['tcl_cover_image'] + ')');
                  var path = '../public/img/contentlibrary/articles/256X256/' + response['cont_lib'][0]['tcl_cover_image_small'];
                  $('.hrtigmw_small').html("<img src="+"'"+path+"'"+ " class='img-responsive' width='100%' >");
                  $('.hrtigmw_small').show();   
                  $('.docnoimg').hide();
                }else{
                  $('.hrtigmw_small').html('');
                  $('.hrtigmw_small').hide(); 
                  // $('.hrtigm').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  // $('#edit_article_cover').css('background-image', '');
                }

                $('#doc_title').val(response['cont_lib'][0]['tcl_title']);
                $('#document_name').html(response['cont_lib'][0]['tcl_document']);
                $('#show_doc_name').show();
                // $('.cke_editable').prepend(response['cont_lib'][0]['tcl_description']);

                $('#doc_description').val(response['cont_lib'][0]['tcl_description']);

                if(CKEDITOR.instances['doc_description']){
                  CKEDITOR.instances['doc_description'].destroy(true);
                }
                CKEDITOR.replace( 'doc_description' );
                $('#doc_author').val(response['cont_lib'][0]['tcl_author']);
                $("#doc_competency_id option[value='"+response['cont_lib'][0]['tcl_competency']+"']").attr("selected", "selected");
                
                $("#doc_level option[value='"+response['cont_lib'][0]['tcl_level']+"']").attr("selected", "selected");

                if(response['cont_lib'][0]['tcl_is_active'] == 1){
                  $("#doc_is_active").prop("checked", true);
                }else{
                  $("#doc_is_active").prop("checked", false);
                }
                if(response['cont_lib'][0]['tcl_is_available'] == 1){
                  $("#doc_is_available").prop("checked", true);
                }else{
                  $("#doc_is_available").prop("checked", false);
                }

                var getkeywords = "";
                $('#doc_keywords').tagsinput('removeAll');
                getkeywords = response['cont_lib'][0]['tcl_keywords'].split(',');
                for(var i=0;i<getkeywords.length;i++){
                  $('#doc_keywords').tagsinput('add',getkeywords[i]);
                }
                }
              }
            });  

}

function addwebinar($webinarid){
    if($webinarid === null || $webinarid === undefined){
      //add article
        $.ajax({
            type: "POST",
            url: "set_session_content",
            data: {
                content_id:"",
                content_type:"add_webinar"
            }, 
            success: function(data)
            {
              window.location = "openAddWebinar";
            }
        });
    }else{
      //edit article
        $.ajax({
            type: "POST",
            url: "set_session_content",
            data: {
                content_id:$webinarid,
                content_type:"edit_webinar"
            }, 
            success: function(data)
            {
              window.location = "openAddWebinar";
            }
        });
    }  
}

function setEditWebinarForm(){
        $('.hrtigwebinar').html('');
        $('.hrtigwebinar').hide();  
            $.ajax({
               'type':'POST',
               'url':"editArticle",
              success:function(response){
                console.log(response);
                if(response != "false"){
                response = JSON.parse(response);

                var tcl_developement_area = response['cont_lib'][0]['tcl_developement_area'];
                var selected = '';
                $.ajax({
                    'type':'POST',
                    'url':"getDevelopementAreaCL",
                    data: {
                      "competency_id" : response['cont_lib'][0]['tcl_competency']
                      },
                    success:function(getresponse){
                      console.log(getresponse);
                      getresponse = JSON.parse(getresponse);
                      var html = "<option value=''>Select Development Area</option>";
                      for(var i=0;i<getresponse.length;i++){
                          if(getresponse[i]['tda_id'] == tcl_developement_area){
                             selected = 'selected';
                          }else{
                              selected = '';
                          }
                          html += "<option value='"+getresponse[i]['tda_id']+"' "+selected+">"+getresponse[i]['tda_development']+"</option>";
                      }

                      $('#webinar_development_area_id').html(html);
                    }
                });

                
                  $('#webinar_upload_demo').html('');
                  $('#webinar_crop_image_div').hide();
                $('#edit_webinar_id').val(response['cont_lib'][0]['tcl_id']);
                if(response['cont_lib'][0]['tcl_cover_image'] != ""){
                  // $('#edit_article_cover').css('background-image', 'url(../public/img/' + response['cont_lib'][0]['tcl_cover_image'] + ')');
                  var path = '../public/img/contentlibrary/articles/590X140/' + response['cont_lib'][0]['tcl_cover_image'];
                  $('.hrtigwebinar').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  $('.hrtigwebinar').show();   
                }else{
                  $('.hrtigwebinar').html('');
                  $('.hrtigwebinar').hide(); 
                  // $('.hrtigm').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  // $('#edit_article_cover').css('background-image', '');
                }

                $('#webinar_seats_available').val(response['cont_lib'][0]['tcl_seats_available']);
                $('#webinar_type').val(response['cont_lib'][0]['tcl_webinar_type']);
                $('#webinar_date').val(response['cont_lib'][0]['tcl_webinar_date']);
                $('.webinar_from_time').val(response['cont_lib'][0]['tcl_webinar_from_time']);
                $('.webinar_to_time').val(response['cont_lib'][0]['tcl_webinar_to_time']);
                $('#webinar_timezone').val(response['cont_lib'][0]['tcl_webinar_timezone']);
                $('#webinar_instructor').val(response['cont_lib'][0]['tcl_webinar_instructor']);

                $('#webinar_upload_demo_small').html('');
                $('#webinar_crop_image_div_small').hide();
                // console.log(response['cont_lib'][0]['tcl_cover_image_small']);
                if(response['cont_lib'][0]['tcl_cover_image_small'] != "" || response['cont_lib'][0]['tcl_cover_image_small'] != null){
                  // $('#edit_article_cover').css('background-image', 'url(../public/img/' + response['cont_lib'][0]['tcl_cover_image'] + ')');
                  var path = '../public/img/contentlibrary/articles/256X256/' + response['cont_lib'][0]['tcl_cover_image_small'];
                  $('.hrtigwebinar_small').html("<img src="+"'"+path+"'"+ " class='img-responsive' width='100%' >");
                  $('.hrtigwebinar_small').show();   
                  $('.webinarnoimg').hide();
                }else{
                  $('.hrtigwebinar_small').html('');
                  $('.hrtigwebinar_small').hide(); 
                  $('.webinarnoimg').show();
                  // $('.hrtigm').html("<img src="+"'"+path+"'"+ "class='logo-upload-icon upload-coverpic' >");
                  // $('#edit_article_cover').css('background-image', '');
                }
                $('#webinar_title').val(response['cont_lib'][0]['tcl_title']);
                // $('.cke_editable').prepend(response['cont_lib'][0]['tcl_description']);

                $('#webinar_description').val(response['cont_lib'][0]['tcl_description']);

                if(CKEDITOR.instances['webinar_description']){
                  CKEDITOR.instances['webinar_description'].destroy(true);
                }
                CKEDITOR.replace( 'webinar_description' );
                $('#webinar_author').val(response['cont_lib'][0]['tcl_author']);
                $("#webinar_competency_id option[value='"+response['cont_lib'][0]['tcl_competency']+"']").attr("selected", "selected");
                
                $("#webinar_level option[value='"+response['cont_lib'][0]['tcl_level']+"']").attr("selected", "selected");

                if(response['cont_lib'][0]['tcl_is_active'] == 1){
                  $("#webinar_is_active").prop("checked", true);
                }else{
                  $("#webinar_is_active").prop("checked", false);
                }
                if(response['cont_lib'][0]['tcl_is_available'] == 1){
                  $("#webinar_is_available").prop("checked", true);
                }else{
                  $("#webinar_is_available").prop("checked", false);
                }

                var getkeywords = "";
                $('#webinar_keywords').tagsinput('removeAll');
                getkeywords = response['cont_lib'][0]['tcl_keywords'].split(',');
                for(var i=0;i<getkeywords.length;i++){
                  $('#webinar_keywords').tagsinput('add',getkeywords[i]);
                }
              }
              }
            });      
}
