$(function(){
//testing

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#user_type').on('change',function(){
        var user_type = $(this).val();
        $.ajax({
               'type':'POST',
               'url':"../permissions_list",
               data: {
                "user_type": user_type
                },
               success:function(response){
                // console.log(response);
                response = JSON.parse(response);
                var html = "";

                var title = "";var i=0;var j=0;var check_id=0;
                html += '<tr>';
                for(var q = 0 ;q< response.length; q++){
                    
                    if(response[q]['parent_id'] == 0){
                        response[q]['parent_id'] = "parent_"+response[q]['moduleid'];
                    }else{
                        response[q]['parent_id'] = "child_"+response[q]['parent_id'];
                    }

                    if(check_id == 0 || check_id == ""){
                        check_id = response[q]['header_id'];
                    }

                    if(response[q]['header_id'] == check_id || check_id == 0){

                        if(i==0){
                        
                        html += '<td><input class="rowcheckall" name="'+response[q]['parent_id']+'" type="checkbox" value="selectall"></td>';
                        html += '<td>'+response[q]['header_name']+'</td>';
                        }

                        html += '<td><input class="checkbox" id="'+response[q]['id']+'" name="permission[]" type="checkbox" value="'+response[q]['id']+'"></td>';
                    
                        check_id = response[q]['header_id'];
                      
                        i++;
                    }else{
                        check_id = "";
                         
                        html += '</tr>';
                      
                        html += '<td><input class="rowcheckall" name="'+response[q]['parent_id']+'" type="checkbox" value="selectall"></td>';
                        html += '<td>'+response[q]['header_name']+'</td>';
                        html += '<td><input class="checkbox" id="'+response[q]['id']+'" name="permission[]" type="checkbox" value="'+response[q]['id']+'"></td>';
                    
                        check_id = response[q]['header_id'];
                    }

                }
                html += '</tr>';
                $('#display_permissions').html(html);
               }

        });        
    });    

    $('#user_type_edit').on('change',function(){
        var user_type = $(this).val();
        $.ajax({
               'type':'POST',
               'url':"../../permissions_list",
               data: {
                "user_type": user_type
                },
               success:function(response){
                // console.log(response);
                response = JSON.parse(response);
                var html = "";

                var title = "";var i=0;var j=0;var check_id=0;
                html += '<tr>';
                for(var q = 0 ;q< response.length; q++){
                    
                    if(response[q]['parent_id'] == 0){
                        response[q]['parent_id'] = "parent_"+response[q]['moduleid'];
                    }else{
                        response[q]['parent_id'] = "child_"+response[q]['parent_id'];
                    }

                    if(check_id == 0 || check_id == ""){
                        check_id = response[q]['header_id'];
                    }

                    if(response[q]['header_id'] == check_id || check_id == 0){

                        if(i==0){
                        
                        html += '<td><input class="rowcheckall" name="'+response[q]['parent_id']+'" type="checkbox" value="selectall"></td>';
                        html += '<td>'+response[q]['header_name']+'</td>';
                        }

                        html += '<td><input class="checkbox" id="'+response[q]['id']+'" name="permission[]" type="checkbox" value="'+response[q]['id']+'"></td>';
                    
                        check_id = response[q]['header_id'];
                      
                        i++;
                    }else{
                        check_id = "";
                         
                        html += '</tr>';
                      
                        html += '<td><input class="rowcheckall" name="'+response[q]['parent_id']+'" type="checkbox" value="selectall"></td>';
                        html += '<td>'+response[q]['header_name']+'</td>';
                        html += '<td><input class="checkbox" id="'+response[q]['id']+'" name="permission[]" type="checkbox" value="'+response[q]['id']+'"></td>';
                    
                        check_id = response[q]['header_id'];
                    }

                }
                html += '</tr>';
                $('#display_permissions_edit').html(html);

                    $.ajax({
                               'type':'POST',
                               'url':"../../getSelectedPermissions",
                               data: {
                                "roleid": $('#roleid').val()
                                },
                               success:function(response){
                                response = JSON.parse(response);
                                for(var i=0;i<response.length;i++){

                                    $("#"+response[i]['permission_id']).prop('checked',true);   

                                }
                                if ($('.checkbox:checked').length == $('.checkbox').length) {
                                                $('.checkbox').closest('tr').find(".rowcheckall").prop('checked', true);
                                            $("#selectall").prop('checked', true);
                                    }

                                $('tbody tr').each(function(){

                                  if($(this).find(':checkbox:checked').length == $(this).find('.checkbox').length){
                                    $(this).find(".rowcheckall").prop('checked', true);
                                  }
                                });
                               }

                    }); 
               }

        });        
    });     

    $("#selectall").on('change',function () {
      $(":checkbox").prop('checked', $(this).prop("checked"));
    });
   
    $(document).on('change', '.rowcheckall', function() {

        var check = $(this).attr('name').split('_')[0];
        var id = $(this).attr('name').split('_')[1];
        
        if(check == "parent"){
            id = "child_"+id;
            $("input[name='"+id+"']").prop('checked', $(this).prop("checked"));
            $("input[name='"+id+"']").closest('tr').find(":checkbox").prop('checked', $(this).prop("checked"));
        }else if(check == "child"){
            id = "parent_"+id;
            if($(this).prop("checked") == true){
            $("input[name='"+id+"']").prop('checked', $(this).prop("checked"));
            $("input[name='"+id+"']").closest('tr').find(":checkbox").prop('checked', $(this).prop("checked"));  
            }       
        }
        $(this).closest('tr').find(":checkbox").prop('checked', $(this).prop("checked"));
        if($(this).prop("checked") == false){
        $("#selectall").prop('checked', $(this).prop("checked"));
        }
    });

    $(document).on('change', '.checkbox', function() {
        if($(this).prop("checked") == false){
            $(this).closest('tr').find(".rowcheckall").prop('checked', $(this).prop("checked"));
            $("#selectall").prop('checked', $(this).prop("checked"));
        }
    });

    $.ajax({
               'type':'POST',
               'url':"../../getSelectedPermissions",
               data: {
                "roleid": $('#roleid').val()
                },
               success:function(response){
                response = JSON.parse(response);
                for(var i=0;i<response.length;i++){

                    $("#"+response[i]['permission_id']).prop('checked',true);   

                }
                if ($('.checkbox:checked').length == $('.checkbox').length) {
                                $('.checkbox').closest('tr').find(".rowcheckall").prop('checked', true);
                            $("#selectall").prop('checked', true);
                    }

                $('tbody tr').each(function(){

                  if($(this).find(':checkbox:checked').length == $(this).find('.checkbox').length){
                    $(this).find(".rowcheckall").prop('checked', true);
                  }
                });
               }

    });    
});