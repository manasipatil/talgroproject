$(function(){
//testing

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('[data-toggle="popover"]').popover();   

    var column_val=$("#role_listing_hidden_column_array").val();
    console.log(column_val);
    var arrval=column_val.split(',');
    var arrfinal=[];
    for(var i=0;i<arrval.length; i++){
        var col_name=arrval[i].split('-');
        if(jQuery.trim(col_name[0])=="CheckAll")
            arrfinal[i] = {
                data:col_name[0],name:"chk_box",title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all" type="checkbox"><span class="custom-control-indicator"></span></label>',class:col_name[0]
            };
            else
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:col_name[0],class:col_name[0]};
    }
    // var per_response = "";
    // $.ajax({
    //     type: "POST",
    //     url: "check_permissions",
    //     data: {
    //         permission_name:'role_edit'
    //     }, 
    //     success: function(data)
    //     {
    //          per_response = data;
    //     }
    // });
    var col="";
    // if(per_response){
        var targets = [0,3];
    // }else{
    //     var targets = [0];

    // }
    var functions_table=$('.role_list_tbl').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"role_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                // d.dropdown_competency_code = $("#dropdown_emails_code option:selected").val();
                // d.searchtxt=$("#searchtxt").val();
                // d.no_of_learners=$("#no_of_learners").val();
                // d.team_filter_courses_list=$("#team_filter_courses_list").val();
                // d.team_filter_learining_path_list=$("#team_filter_learining_path_list").val();
                // d.Keyword_filter=$("#Keyword_filter").val();
                // d.tabname=$(".tabname").val();
            },
        },
        columns: arrfinal,
        
        "columnDefs": [ {
            "targets": targets, // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    //code for show/hide columns
    var col_array = $('#role_listing_hidden_column_array').val();
    var col_array =col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<col_array.length; i++){
        new_column=col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "users." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
            functions_table.columns( col_class_name ).visible( true );
        }else{
            functions_table.columns( col_class_name ).visible( false );
        }
    }
    //end of code 

    //common action
    $('.click_common_action').click(function(){
        var i=0;
        var arr = [];
        $('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        var action=$(this).attr("to_do");
        if(action=="delete")
            var text="You will not be able to recover this Role!";
        else if(action=="enable")
            var text="You want to enable all Roles";
        else if(action=="disable")
            var text="You want to disable all Roles";

        swal({   
                title: "Are you sure?",   
                text: "" + text + "",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, do it!",   
                cancelButtonText: "No, cancel plx!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "roles_common_action",
                        data: {
                            "action":action,
                            "chk_check_value":arr
                        }, 
                        success: function(data)
                        {
                            console.log(data);
                            if(action=="delete"){
                                swal("Deleted!", "Role has been Deleted.", "success");
                            }
                            else if(action=="enable"){
                                swal("Enable","Role has been Enable ","success");
                            }
                            else if(action=="disable"){
                                swal("Disable","Role has been disable","success"); 
                            }                       
                            functions_table.ajax.reload();
                            $('.select_all').prop('checked',false);
                            $('.role_listing tr').removeClass("selected");
                            $('.common_action').addClass("hide");
                        }
                    });
                } else {     
                  swal("Cancelled", "Your Codes are safe :)", "error");   
                }
            }); 
    });    

    //for delete single role
    $(document).on('click', '.delete_role', function(){
        var delete_id=$(this).attr("delete_id");
            swal({   
                title: "Are you sure?",   
                text: "You want to delete this role?",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                cancelButtonText: "No, cancel it!",   
                closeOnConfirm: false,   
               closeOnCancel: false 
            },function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "delete_role_code",
                        data: {
                            delete_id:delete_id
                        }, 
                        success: function(data)
                        {
                             swal("Deleted!", "Role has been deleted.", "success"); 
                             functions_table.ajax.reload();
                        }
                    });
                } else {     
                  swal("Cancelled", "Role is safe :)", "error");   
                } 
            });
    });  



});