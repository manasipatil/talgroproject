$(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

///code for team listing starts here
    var column_val=$("#team_listing_hidden_column_array").val();
    console.log(column_val);
    var arrval=column_val.split(',');
    var arrfinal=[];
    for(var i=0;i<arrval.length; i++){
        var col_name=arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            arrfinal[i] = {
                data:col_name[0],name:"chk_box",
        title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all" type="checkbox"><span class="custom-control-indicator"></span></label>',
        class:col_name[0]
    };
        else if(col_name[0]=="CourseAssigned")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Course Assigned",class:col_name[0]};
        else if(col_name[0]=="LarningPathAssigned")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Larning Path Assigned",class:col_name[0]};                  
        else
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:col_name[0],class:col_name[0]};
    }

    var col="";
    var functions_table=$('.team_list_tbl').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"learners_ajax_team_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.dropdown_competency_code = $("#dropdown_emails_code option:selected").val();
                d.searchtxt=$("#searchtxt").val();
                d.no_of_learners=$("#no_of_learners").val();
                d.team_filter_courses_list=$("#team_filter_courses_list").val();
                d.team_filter_learining_path_list=$("#team_filter_learining_path_list").val();
                d.Keyword_filter=$("#Keyword_filter").val();
                // d.tabname=$(".tabname").val();
            },
        },
        columns: arrfinal,
        
        "columnDefs": [ {
            "targets": [0,5], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    //code for show/hide columns
    var col_array = $('#team_listing_hidden_column_array').val();
    var col_array =col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<col_array.length; i++){
        new_column=col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "users." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
            functions_table.columns( col_class_name ).visible( true );
        }else{
            functions_table.columns( col_class_name ).visible( false );
        }
    }
    //end of code    
///code for team listing ends here     


    //common action
    $('.click_common_action').click(function(){
        var i=0;
        var arr = [];
        $('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        var action=$(this).attr("to_do");
        if(action=="delete")
            var text="You will not be able to recover this Functions!";
        else if(action=="enable")
            var text="You want to enable all Functions";
        else if(action=="disable")
            var text="You want to disable all Functions";
        else if(action=="assign_learner")
            var text="You want to assign these learners to the team?";
        else if(action=="remove_learner")
            var text="You want to remove these learners from the team?";        
        else if(action=="enable_team")
            var text="You want to enable the teams?";
        else if(action=="disable_team")
            var text="You want disable the teams?";
        else if(action=="delete_team")
            var text="You want to delete the team?";

        swal({   
                title: "Are you sure?",   
                text: "" + text + "",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, do it!",   
                cancelButtonText: "No, cancel plx!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "learners_common_action",
                        data: {
                            "action":action,
                            "chk_check_value":arr
                        }, 
                        success: function(data)
                        {
                            console.log(data);
                            if(action=="delete_team"){
                                swal("Deleted!", "Team has been Deleted.", "success");
                            }
                            else if(action=="enable_team"){
                                swal("Enable","Team has been Enable ","success");
                            }
                            else if(action=="disable_team"){
                                swal("Disable","Team has been disable","success"); 
                            }                       
                            functions_table.ajax.reload();
                            $('.select_all').prop('checked',false);
                            $('.team_list_tbl tr').removeClass("selected");
                            $('.common_action').addClass("hide");
                        }
                    });
                } else {     
                  swal("Cancelled", "Your Codes are safe :)", "error");   
                }
            }); 
    });

      $.ajax({
        'type':'POST',
        'url':"getEnrolledCoursesList",
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Course</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tc_id']+"'>"+response[i]['tc_title']+"</option>";
          }

          $('#team_filter_courses_list').html(html);
        }
      }); 

      $.ajax({
        'type':'POST',
        'url':"getFullLearningPathList",
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Learning Path</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tlp_id']+"'>"+response[i]['tlp_name']+"</option>";
          }

          $('#team_filter_learining_path_list').html(html);
        }
      });    

    $('#filter_btn').click(function(){
        $("#searchtxt").val('searchtxt');
        functions_table.ajax.reload();
        $('.Cus_card-body').slideToggle("slow");
        $('#filter-ck4').removeClass("active");        
    });   

    //for delete single team
    $(document).on('click', '.delete_team', function(){
        var delete_id=$(this).attr("delete_id");
            swal({   
                title: "Are you sure?",   
                text: "You want to delete this team?",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                cancelButtonText: "No, cancel it!",   
                closeOnConfirm: false,   
               closeOnCancel: false 
            },function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "delete_team_code",
                        data: {
                            delete_id:delete_id
                        }, 
                        success: function(data)
                        {
                             swal("Deleted!", "Your Team has been deleted.", "success"); 
                             functions_table.ajax.reload();
                        }
                    });
                } else {     
                  swal("Cancelled", "Your Team is safe :)", "error");   
                } 
            });
    });   

    $(document).on('click', '.edit_button', function(){
        //on click of single edit on team list
        //call ajax to save session values
        var edit_id=$(this).attr("edit_id");
        $.ajax({
            type: "POST",
            url: "set_session_team",
            data: {
                team_id:edit_id,
                btn_type:"edit_team"
            }, 
            success: function(data)
            {
                console.log(data);
                $('.d-ib').html('Edit Team');
                // $('.steps a[href="#tab-5"]').closest('li').addClass('active');
                window.location = "showAddTeam";
            }
        });

    });    

    $(document).on('click', '.view_button', function(){
        //on click of single delete on team list
        //call ajax to save session values        
        var view_id=$(this).attr("view_id");
        $.ajax({
            type: "POST",
            url: "set_session_team",
            data: {
                team_id:view_id,
                btn_type:"view_team"
            }, 
            success: function(data)
            {
                console.log(data);
                $('.d-ib').html('View Team');
                // $('.steps a[href="#tab-1"]').closest('li').addClass('active');
                window.location = "showAddTeam";
            }
        });
    });  

    $(document).on('click', '#go_to_add_team', function(){
        $.ajax({
            type: "POST",
            url: "set_session_team",
            data: {
                btn_type:"add_team"
            }, 
            success: function(data)
            {
                console.log(data);
                window.location = "showAddTeam";
            }
        });
    });               


        

});  