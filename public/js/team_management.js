$(function(){

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	$('#add_team_form').validate({    
        rules: {
          team_title: {
               required: true,
               minlength: 3
           }
        },
        messages: {
          team_title: {
               required: "Title is required",
               minlength: "Title must contain at least {0} characters"
           }
        }, 
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
          $(element).parents('.form-group').addClass('has-success');
        },
        submitHandler: function(form) {
          	var team_title = $('#team_title').val();
          	var team_description = CKEDITOR.instances['team_description'].getData();
            // alert(CKEDITOR.instances['team_description'].getData());
	      	$.ajax({
		           'type':'POST',
		           'url':"addTeam",
		           data: {
		            "team_title": team_title,"team_description":team_description
	              },
	              success:function(response){
	                console.log(response);
	                response = JSON.parse(response);
	                if(response['status'] == "success"){
	                	$('#tab1_success_content').html(response['message']);

	                    $('#tab1_show_success_msg').show();
	                    $('#tab1_show_success_msg').delay(4000).fadeOut();
	                    setTimeout(function () { 
		                	if($('#select_btn').val() == 'saveandshow'){	
								$('.tab-menu a[href="#tab-2"]').tab('show');
								$('.steps a[href="#tab-1"]').closest('li').removeClass('active');
								$('.steps a[href="#tab-2"]').closest('li').addClass('active');
                                $('.steps a[href="#tab-2"]').closest('li').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-2"]').closest('li').attr('data-toggle','tab');
		                	}
	                	}, 1000); 
	                }else{
	                	$('#tab1_error_content').html(response['message']);

	                    $('#tab1_show_error_msg').show();
	                    $('#tab1_show_error_msg').delay(4000).fadeOut();
	                }

	            }
        	});
          	return false;
        }

    });

	$('#cancel_team_btn').click(function(){
		$('#team_title').val('');
		$('#team_description').val('');
	});

	$('#save_and_add').click(function(){
		$('#select_btn').val('saveandshow');
	});

    // $('.get_learners_listing').on('click',function(){
    //     functions_table.ajax.reload();
    // });

    // $('.get_learn_team_list').on('click',function(){
    //     functions_table3.ajax.reload();
    // });    

    var column_val=$("#learners_hidden_column_array").val();
    console.log(column_val);
    var arrval=column_val.split(',');
    var arrfinal=[];
    for(var i=0;i<arrval.length; i++){
        var col_name=arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            arrfinal[i] = {
                data:col_name[0],name:"chk_box",
        title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all" type="checkbox"><span class="custom-control-indicator"></span></label>',
        class:col_name[0]
    };
        else if(col_name[0]=="EmpCode")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Emp Code",class:col_name[0]};
        else if(col_name[0]=="EmpName")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Emp Name",class:col_name[0]};
         
        else if(col_name[0]=="ManagerName")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Manager Name",class:col_name[0]};
        
        else if(col_name[0]=="EnrollCourses")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Enroll Courses",class:col_name[0]};                    
        else
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:col_name[0],class:col_name[0]};
    }
    //end of code
    
    var col="";
    var functions_table=$('.example2').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            // fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"learners_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.dropdown_competency_code = $("#dropdown_emails_code option:selected").val();
                d.searchtxt=$("#searchtxt").val();
                d.department_filter=$("#department_filter_list").val();
                d.manager_filter=$("#manager_filter_list").val();
                d.level_filter=$("#level_filter_list").val();
                d.enrolled_courses_filter=$("#enrolled_courses_filter_list").val();
                d.Keyword_filter=$("#Keyword_filter").val();
                d.tabname=$(".tabname").val();
            },
        },
        columns: arrfinal,
        
        "columnDefs": [ {
            "targets": [0], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });
    

    
    //code for show/hide columns
    var col_array = $('#learners_hidden_column_array').val();
    var col_array =col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<col_array.length; i++){
        new_column=col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "users." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
            functions_table.columns( col_class_name ).visible( true );
        }else{
            functions_table.columns( col_class_name ).visible( false );
        }
    }
    //end of code
    $('#learner_team_id').on('click',function(){

    });
    // learners team tab starts here
    var column_val=$("#learners_team_hidden_column_array").val();
    console.log(column_val);
    var arrval=column_val.split(',');
    var arrfinal=[];
    for(var i=0;i<arrval.length; i++){
        var col_name=arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            arrfinal[i] = {
                data:col_name[0],name:"chk_box",
        title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all" type="checkbox"><span class="custom-control-indicator"></span></label>',
        class:col_name[0]
    };
        else if(col_name[0]=="EmpCode")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Emp Code",class:col_name[0]};
        else if(col_name[0]=="EmpName")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Emp Name",class:col_name[0]};
         
        else if(col_name[0]=="ManagerName")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Manager Name",class:col_name[0]};
        
        else if(col_name[0]=="EnrollCourses")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Enroll Courses",class:col_name[0]};                    
        else
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:col_name[0],class:col_name[0]};
    }
    //end of code
    
    var col="";
    var functions_table3=$('.example3').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            // fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"learners_ajax_listing_team", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.dropdown_competency_code = $("#dropdown_emails_code option:selected").val();
                d.searchtxt=$("#searchtxt").val();
                d.department_filter=$("#department_filter_list_team").val();
                d.manager_filter=$("#manager_filter_list_team").val();
                d.level_filter=$("#level_filter_list_team").val();
                d.enrolled_courses_filter=$("#enrolled_courses_filter_list_team").val();
                d.Keyword_filter=$("#Keyword_filter_team").val();
                d.tabname=$(".tabname").val();
            },
        },
        columns: arrfinal,
        
        "columnDefs": [ {
            "targets": [0], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $('#filter_btn').click(function(){
        $("#searchtxt").val('searchtxt');
        functions_table.ajax.reload();
        $('.ck33').slideToggle("slow");
        $('#filter-ck33').removeClass("active");
        $('.tab-menu a[href="#tab-2"]').tab('show');
        $('.steps a[href="#tab-1"]').closest('li').removeClass('active');
        $('.steps a[href="#tab-2"]').closest('li').addClass('active');
    });    

    $('#filter_btn_team').click(function(){
        $("#searchtxt").val('searchtxt');
        functions_table3.ajax.reload();
        $('.ck3').slideToggle("slow");
        $('#filter-ck3').removeClass("active");
        $('.tab-menu a[href="#tab-3"]').tab('show');
        $('.steps a[href="#tab-1"]').closest('li').removeClass('active');
        $('.steps a[href="#tab-4"]').closest('li').addClass('active');
    });    
    
    //code for show/hide columns
    var col_array = $('#learners_team_hidden_column_array').val();
    var col_array =col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<col_array.length; i++){
        new_column=col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "users." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
            functions_table3.columns( col_class_name ).visible( true );
        }else{
            functions_table3.columns( col_class_name ).visible( false );
        }
    }
    //end of code
    // learners team tab ends here

    //common action
    $('.click_common_action').click(function(){
        var i=0;
        var arr = [];
        $('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        var action=$(this).attr("to_do");
        if(action=="delete")
            var text="You will not be able to recover this Functions!";
        else if(action=="enable")
            var text="You want to enable all Functions";
        else if(action=="disable")
            var text="You want to disable all Functions";
        else if(action=="assign_learner")
            var text="You want to assign these learners to the team?";
        else if(action=="remove_learner")
            var text="You want to remove these learners from the team?";        
        else if(action=="enable_team")
            var text="You want to enable the teams?";
        else if(action=="disable_team")
            var text="You want disable the teams?";
        else if(action=="delete_team")
            var text="You want to delete the team?";

        swal({   
                title: "Are you sure?",   
                text: "" + text + "",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, do it!",   
                cancelButtonText: "No, cancel plx!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "learners_common_action",
                        data: {
                            "action":action,
                            "chk_check_value":arr
                        }, 
                        success: function(data)
                        {
                            console.log(data);
                            if(action=="delete"){
                                swal("Deleted!", "Your Functions has been Deleted.", "success");
                            }
                            else if(action=="enable"){
                                swal("Enable","Your Functions has been Enable ","success");
                            }
                            else if(action=="disable"){
                                swal("Disable","Your Functions has been disable","success"); 
                            }
                            else if(action=="assign_learner"){
                                swal("Assign","Learners has been assigned to the team","success");
                                $('.tab-menu a[href="#tab-3"]').tab('show');
                                $('.steps a[href="#tab-2"]').closest('li').removeClass('active');
                                $('.steps a[href="#tab-5"]').closest('li').addClass('active');
                                $('.steps a[href="#tab-2"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-2"]').attr('data-toggle','tab');
                                $('.steps a[href="#tab-3"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-3"]').attr('data-toggle','tab');
                                $('.steps a[href="#tab-4"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-4"]').attr('data-toggle','tab');
                                $('.steps a[href="#tab-5"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-5"]').attr('data-toggle','tab');                                                                  
                            }else if(action=="remove_learner"){
                                swal("Remove","Learners has been removed from the team","success");
                                $('.tab-menu a[href="#tab-3"]').tab('show');       
                            }                        
                            functions_table.ajax.reload();
                            functions_table3.ajax.reload();
                            $('.select_all').prop('checked',false);
                            $('.example3 tr').removeClass("selected");
                            $('.common_action').addClass("hide");
                        }
                    });
                } else {     
                  swal("Cancelled", "Your Codes are safe :)", "error");   
                }
            }); 
    });

    //bulk upload excel file
    $('.upload_functions_csv').click(function(){

        if(!$("#upload_success_msg").hasClass("hide"))
            $('#upload_success_msg').addClass("hide");
        if(!$("#all_upload_error_msg").hasClass("hide"))
            $('#all_upload_error_msg').addClass("hide");
        if(!$("#blank_error_msg").hasClass("hide"))
            $('#blank_error_msg').addClass("hide");
        $('.error_msg').text("");
        $('.myprogress').css('width', '0');
        $('.msg').text('');

        //validation for file format
        var file_data = $('#upload_functions').prop('files')[0];
        var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
        if(filename=="" || filename=="undefined"){
            $('#blank_error_msg').removeClass("hide");
            $('#blank_error_msg').html("Please Upload Excel file");
            return false;
        }
        var ext = $("#upload_functions").val().split('.').pop();
        var fileExtension = ['XLS', 'XLSX', 'xlsx', 'xlx'];
        
        if ($.inArray(ext, fileExtension) == -1) {
            $('#blank_error_msg').removeClass("hide");
            $('#blank_error_msg').html("Please upload excel file only.formats are allowed : "+fileExtension.join(', '));
            return false;
        }
        var form_data = new FormData();
        form_data.append('file', file_data);
        $('.msg').text('Uploading in progress...');

        $.ajax({
            type: "POST",
            url: "upload_functions_csv",
            dataType    : 'text',           // what to expect back from the PHP script, if anything
            cache       : false,
            contentType : false,
            processData : false,
            data        : form_data,

            // this part is progress bar
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $('.myprogress').text(percentComplete + '%');
                        $('.myprogress').css('width', percentComplete + '%');
                    }
                }, false);
                return xhr;
            },

            success: function(data)
            {
                //console.log(data);
                $('.myprogress').css('width', '0%');
                $('.myprogress').text('0%');
                $('.msg').text('');
                document.getElementById("Frmfunctionscsv").reset();
                var result = JSON.parse(data);
                var newHTML = [];
                if(result['status']=='all_duplicate'){
                    var substr=result['errors'];
                    for (i = 0; i < substr.length; ++i) {
                        newHTML.push(substr[i]+',');

                    }
                    var lastChar = newHTML.join("").slice(-1);
                    if (lastChar == ',') {
                      var strVal = newHTML.join("").slice(0, -1);
                    }
                    var array = strVal.split(',');
                    $.each(array, function(index, value) {
                      $("#failed_record_list").append("<li>" + value + "</li>");
                    });
                    $('#total_record').text(result['total_count_record']);
                    $('#failed_record').text(result['failed_record']);
                    $("#all_upload_error_msg").removeClass("hide");
               }else if (result['status']=='success_n_duplicate') {
                    var substr=result['errors'];
                    for (i = 0; i < substr.length; ++i) {
                        newHTML.push(substr[i]+',');
                    }
                    var lastChar = newHTML.join("").slice(-1);
                    if (lastChar == ',') {
                      var strVal = newHTML.join("").slice(0, -1);
                    }
                    var array = strVal.split(',');
                    $.each(array, function(index, value) {
                      $("#failed_record_list").append("<li>" + value + "</li>");
                    });
                    $('#total_record').text(result['total_count_record']);
                    $('#failed_record').text(result['failed_record']);
                    $("#all_upload_error_msg").removeClass("hide");
                    functions_table.ajax.reload();
               }else if(result['status']=='no_record_error'){
                    $('#blank_error_msg').removeClass("hide");
                    $('#blank_error_msg').text("No record found in excel");
               }else if(result['status']=="success_all_record"){
                    $('#upload_sucess_msg').removeClass("hide");
                    functions_table.ajax.reload();
                    setTimeout(function(){
                        $('#upload_sucess_msg').addClass("hide");
                        $('#upload_functions_modal').modal('hide');
                    },  2000);
               }else if(result['status']=='header_format_error'){
                    $('#blank_error_msg').removeClass("hide");
                    $('#blank_error_msg').text("Please upload proper format.Please see download sample format for referrence");
               }
              
               
            }
        });
    });	

    // ----Start Filter ------
    //filter btn for selected course for the team
    $('#filter-ck1').click(function(e){
    $('.ck1').slideToggle("slow");
    $('#filter-ck1').toggleClass("active");
         e.preventDefault();
    });
    
    $('.close-filter1').click(function(e){
        $('.ck1').slideToggle("slow");
        $('#filter-ck1').removeClass("active");
        e.preventDefault();
    });
    //filter btn for selected course for the team ends here

    //filter btn for selected learning path for the team    
    $('#filter-ck11').click(function(e){
    $('.ck11').slideToggle("slow");
    $('#filter-ck11').toggleClass("active");
         e.preventDefault();
    });
    
    $('.close-filter11').click(function(e){
        $('.ck11').slideToggle("slow");
        $('#filter-ck11').removeClass("active");
        e.preventDefault();
    });    
    //filter btn for selected learning path for the team    

    //filter btn for selected full course
    $('#filter-ck2').click(function(e){
    $('.ck2').slideToggle("slow");
    $('#filter-ck2').toggleClass("active");
         e.preventDefault();
    });
    
    $('.close-filter2').click(function(e){
        $('.ck2').slideToggle("slow");
        $('#filter-ck2').removeClass("active");
        e.preventDefault();
    });
    //filter btn for selected full course ends here

    //filter btn for selected full learning path
    $('#filter-ck22').click(function(e){
    $('.ck22').slideToggle("slow");
    $('#filter-ck22').toggleClass("active");
         e.preventDefault();
    });
    
    $('.close-filter22').click(function(e){
        $('.ck22').slideToggle("slow");
        $('#filter-ck22').removeClass("active");
        e.preventDefault();
    });    
    //filter btn for selected full learning path ends here
    
    $('#filter-ck3').click(function(e){
    $('.ck3').slideToggle("slow");
    $('#filter-ck3').toggleClass("active");
         e.preventDefault();
    });
    
    $('.close-filter3').click(function(e){
        $('.ck3').slideToggle("slow");
        $('#filter-ck3').removeClass("active");
        e.preventDefault();
    });

    $('#filter-ck33').click(function(e){
    $('.ck33').slideToggle("slow");
    $('#filter-ck33').toggleClass("active");
         e.preventDefault();
    });
    
    $('.close-filter33').click(function(e){
        $('.ck33').slideToggle("slow");
        $('#filter-ck33').removeClass("active");
        e.preventDefault();
    });    
    
    
    
    //----End Filter----
    
   //  $('.col').click(function(){
   //      $('.overrlay').show(500); 
   //  });

   // $('.clse').click(function(){
   //      $('.overrlay').hide();  
   //  }); 
      $.ajax({
        'type':'POST',
        'url':"getDepartmentList",
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Department</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tdm_id']+"'>"+response[i]['tdm_department']+"</option>";
          }

          $('#department_filter_list').html(html);
          $('#department_filter_list_team').html(html);
        }
      });  

      $.ajax({
        'type':'POST',
        'url':"getManagerList",
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Manager</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['id']+"'>"+response[i]['user_firstname']+"</option>";
          }

          $('#manager_filter_list').html(html);
          $('#manager_filter_list_team').html(html);
        }
      });   

      $.ajax({
        'type':'POST',
        'url':"getLevelList",
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Level</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tsl_id']+"'>"+response[i]['tsl_seniority']+"</option>";
          }

          $('#level_filter_list').html(html);
          $('#level_filter_list_team').html(html);
          $('#course_team_seniority').html(html);
          $('#course_seniority').html(html);
          $('#learning_path_team_seniority').html(html);
          $('#learning_path_seniority').html(html);     
        }
      });

      $.ajax({
        'type':'POST',
        'url':"getEnrolledCoursesList",
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Course</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tc_id']+"'>"+response[i]['tc_title']+"</option>";
          }

          $('#enrolled_courses_filter_list').html(html);
          $('#enrolled_courses_filter_list_team').html(html);
          $('#learning_path_course').html(html);
          $('#learning_path_team_course').html(html);
          $('#course_filter_course').html(html);
          $('#course_team_filter_course').html(html);    
        }
      });  

      $.ajax({
        'type':'POST',
        'url':"getCategory",
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Category</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tccm_id']+"'>"+response[i]['tccm_category']+"</option>";
          }

          $('#course_team_category').html(html);
          $('#course_category').html(html);
          $('#learning_path_team_category').html(html);
          $('#learning_path_category').html(html);  
        }
      }); 

      $.ajax({
        'type':'POST',
        'url':"getDevelopementArea",
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "<option value=''>Select Developement Area</option>";
          for(var i=0;i<response.length;i++){
              html += "<option value='"+response[i]['tda_id']+"'>"+response[i]['tda_development']+"</option>";
          }

          $('#course_team_dev_area').html(html);
          $('#course_dev_area').html(html);
          $('#learning_path_team_dev_area').html(html);
          $('#learning_path_dev_area').html(html);  
        }
      });       
 

        $("#add_to_team").click(function(){
            var selected_courses = "";
            $.each($("input[name='all_course']:checked"), function(){            
                selected_courses += $(this).val() +',';
            });
            selected_courses = selected_courses.slice(0, -1);

            $.ajax({
               'type':'POST',
               'url':"addCourseToTeam",
               data: {
                "selected_courses": selected_courses
              },
              success:function(response){
                console.log(response);
                if(response == "false"){
                    $('#tab4_error_content').html("Course already added to the team");

                    $('#tab4_show_error_msg').show();
                    $('#tab4_show_error_msg').delay(4000).fadeOut();
                }else{
                    $('#tab4_success_content').html("Course added successfully");

                    $('#tab4_show_success_msg').show();
                    $('#tab4_show_success_msg').delay(4000).fadeOut();
                    // location.reload(true);
                    // $('.steps a[href="#tab-4"]').closest('li').removeClass('active');
                    // $('.steps a[href="#tab-5"]').closest('li').addClass('active');
                    // $('.steps a[href="#tab-5"]').closest('li').attr('data-toggle','tab');
                    // $('.tab-menu a[href="#tab-5"]').closest('li').attr('data-toggle','tab');   
                    // $('.tab-menu a[href="#tab-5"]').tab('show');                     
                }
                getTeamCourseListFunction(); 
                getFullCourseListFunction();
                }

            });            
        }); 

        $("#remove_from_team").click(function(){
            var selected_courses = "";
            $.each($("input[name='all_team_course']:checked"), function(){            
                selected_courses += $(this).val() +',';
            });
            selected_courses = selected_courses.slice(0, -1);

            $.ajax({
               'type':'POST',
               'url':"removeCourseFromTeam",
               data: {
                "selected_courses": selected_courses
              },
              success:function(response){
                console.log(response);
                $('#tab4_success_content').html("Course removed successfully");

                $('#tab4_show_success_msg').show();
                $('#tab4_show_success_msg').delay(4000).fadeOut();
                getTeamCourseListFunction();  
                getFullCourseListFunction();
                }

            });            
        });     

        $("#add_learn_path_to_team").click(function(){
            var selected_courses = "";
            $.each($("input[name='all_path']:checked"), function(){            
                selected_courses += $(this).val() +',';
            });
            selected_courses = selected_courses.slice(0, -1);

            $.ajax({
               'type':'POST',
               'url':"addLearningPathToTeam",
               data: {
                "selected_courses": selected_courses
              },
              success:function(response){
                console.log(response);
                if(response == "false"){
                    $('#tab5_error_content').html("Learning path already added to the team");

                    $('#tab5_show_error_msg').show();
                    $('#tab5_show_error_msg').delay(4000).fadeOut();
                }else{
                    $('#tab5_success_content').html("Learning path added successfully");

                    $('#tab5_show_success_msg').show();
                    $('#tab5_show_success_msg').delay(4000).fadeOut();
                    // location.reload(true);
                    // $('.steps a[href="#tab-4"]').closest('li').removeClass('active');
                    // $('.steps a[href="#tab-5"]').closest('li').addClass('active');
                    // $('.steps a[href="#tab-5"]').closest('li').attr('data-toggle','tab');
                    // $('.tab-menu a[href="#tab-5"]').closest('li').attr('data-toggle','tab');   
                    // $('.tab-menu a[href="#tab-5"]').tab('show');                     
                }
                  getFullLearningPathListFunction();    
                  getTeamLearingPathListFunction();
                }

            });            
        }); 

        $("#remove_learn_path_to_team").click(function(){
            var selected_courses = "";
            $.each($("input[name='all_team_path']:checked"), function(){            
                selected_courses += $(this).val() +',';
            });
            selected_courses = selected_courses.slice(0, -1);

            $.ajax({
               'type':'POST',
               'url':"removeLearningPathFromTeam",
               data: {
                "selected_courses": selected_courses
              },
              success:function(response){
                console.log(response);
                $('#tab5_success_content').html("Learning path removed successfully");

                $('#tab5_show_success_msg').show();
                $('#tab5_show_success_msg').delay(4000).fadeOut();
                  getFullLearningPathListFunction();    
                  getTeamLearingPathListFunction();
                }

            });            
        });

        $.ajax({
           'type':'POST',
           'url':"getTeamDetails",
          success:function(response){
            console.log(response);
            if(response != "false"){
                response = JSON.parse(response);
                $('#team_title').val(response[0]['tt_title']);
                $('#team_description').val(response[0]['tt_description']);      

                if(CKEDITOR.instances['team_description']){
                  CKEDITOR.instances['team_description'].destroy(true);
                }
                CKEDITOR.replace( 'team_description' );                          
            }

            }

        });

      //function to get list of courses added to the team
      getTeamCourseListFunction();     
      //function to get list of all courses
      getFullCourseListFunction();   
      //function to get all list of learning paths
      getFullLearningPathListFunction();    
      //function to get list of learning paths added to the team
      getTeamLearingPathListFunction();  

if($('#btn_type').val() == "edit_team"){
        $('.d-ib').html('Edit Team');
        $('.steps a[href="#tab-1"]').closest('li').removeClass('active');
}else if($('#btn_type').val() == "view_team"){
        $('.d-ib').html('View Team');
        $('.steps a[href="#tab-1"]').closest('li').removeClass('active');
}else if($('#btn_type').val() == "add_team" && $('#team_id').val() != ""){
        $('.d-ib').html('Add Team');
        $('.steps a[href="#tab-1"]').closest('li').removeClass('active');
}else if($('#btn_type').val() == "add_team"){
        $('.d-ib').html('Add Team');
        $('.steps a[href="#tab-2"]').removeAttr('data-toggle');
        $('.tab-menu a[href="#tab-2"]').removeAttr('data-toggle'); 
        $('.steps a[href="#tab-3"]').removeAttr('data-toggle');
        $('.tab-menu a[href="#tab-3"]').removeAttr('data-toggle'); 
        $('.steps a[href="#tab-4"]').removeAttr('data-toggle');
        $('.tab-menu a[href="#tab-4"]').removeAttr('data-toggle'); 
        $('.steps a[href="#tab-5"]').removeAttr('data-toggle');
        $('.tab-menu a[href="#tab-5"]').removeAttr('data-toggle'); 
        $('.steps a[href="#tab-1"]').closest('li').addClass('active');
}    


$('#filter_course_team_btn').on('click',function(e){

    var filer_list = '{"keyword":"'+$('#course_team_keyword').val()+'","course_cat":"'+$('#course_team_category').val()+'","course":"'+$('#course_team_filter_course').val()+'","dev_area":"'+$('#course_team_dev_area').val()+'","seniority":"'+$('#course_team_seniority').val()+'"}';
    getTeamCourseListFunction(filer_list);
        $('.ck1').slideToggle("slow");
        $('#filter-ck1').removeClass("active");
        e.preventDefault();
});

$('#filter_course_all_btn').on('click',function(e){

    var filer_list = '{"keyword":"'+$('#course_keyword').val()+'","course_cat":"'+$('#course_category').val()+'","course":"'+$('#course_filter_course').val()+'","dev_area":"'+$('#course_dev_area').val()+'","seniority":"'+$('#course_seniority').val()+'"}';
    getFullCourseListFunction(filer_list);
        $('.ck2').slideToggle("slow");
        $('#filter-ck2').removeClass("active");
        e.preventDefault();
});

$('#filter_path_all_btn').on('click',function(e){
    var filer_list = '{"keyword":"'+$('#full_path_keyword').val()+'","course_cat":"'+$('#learning_path_category').val()+'","course":"'+$('#learning_path_course').val()+'","dev_area":"'+$('#learning_path_dev_area').val()+'","seniority":"'+$('#learning_path_seniority').val()+'"}';
    getFullLearningPathListFunction(filer_list);    
        $('.ck22').slideToggle("slow");
        $('#filter-ck22').removeClass("active");
        e.preventDefault();
});

$('#filter_path_team_all_btn').on('click',function(e){
    var filer_list = '{"keyword":"'+$('#full_path_team_keyword').val()+'","course_cat":"'+$('#learning_path_team_category').val()+'","course":"'+$('#learning_path_team_course').val()+'","dev_area":"'+$('#learning_path_team_dev_area').val()+'","seniority":"'+$('#learning_path_team_seniority').val()+'"}';
    getTeamLearingPathListFunction(filer_list);    
        $('.ck11').slideToggle("slow");
        $('#filter-ck11').removeClass("active");
        e.preventDefault();
});


           
});	

function getTeamCourseListFunction($filer_list){
    //function to get list of courses added to the team
      $.ajax({
        'type':'POST',
        'url':"getTeamCourseList",
        data:{"filterdta":$filer_list},
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "";
          for(var i=0;i<response.length;i++){
            if((i % 2) == 0 || i == 0){
                html += '<div class="col-xs-12  col-lg-6 padding-right">';
                html += '<div class="course-card card-left">';
                html += '<div class="media">';
                html += '<div class="media-left">';
                html += '<span class="bg-primary circle sq-50">'+response[i]['tc_title'].charAt(0)+'</span>';
                html += '</div>';
                html += '<div class="media-middle media-body">';
                html += "<h5 class='media-heading'>"+response[i]['tc_title']+"</h5>";
                html += "<small>"+response[i]['tc_description']+"</small>";
                html += '</div>';
                html += '</div>';
                html += '<div class="overlay">';
                html += '<label class="custom-control custom-control-primary custom-checkbox">';
                html += '<input class="custom-control-input" name="all_team_course" type="checkbox" value = "'+response[i]['tc_id']+'">';
                html += '<span class="custom-control-indicator"></span>';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }else{
                html += '<div class="col-xs-12  col-lg-6 padding-left">';
                html += '<div class="course-card card-left">';
                html += '<div class="media">';
                html += '<div class="media-left">';
                html += '<span class="bg-primary circle sq-50">'+response[i]['tc_title'].charAt(0)+'</span>';
                html += '</div>';
                html += '<div class="media-middle media-body">';
                html += "<h5 class='media-heading'>"+response[i]['tc_title']+"</h5>";
                html += "<small>"+response[i]['tc_description']+"</small>";
                html += '</div>';
                html += '</div>';
                html += '<div class="overlay">';
                html += '<label class="custom-control custom-control-primary custom-checkbox">';
                html += '<input class="custom-control-input" name="all_team_course" type="checkbox" value = "'+response[i]['tc_id']+'">';
                html += '<span class="custom-control-indicator"></span>';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }

          }

          $('#getcoursesteamlist').html(html);
        }
      });  
}


function getFullCourseListFunction($filer_list){
      $.ajax({
        'type':'POST',
        'url':"getFullCourseList",
        data:{"filterdta":$filer_list},
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "";
          for(var i=0;i<response.length;i++){
            if((i % 2) == 0 || i == 0){
                html += '<div class="col-xs-12  col-lg-6 padding-right">';
                html += '<div class="course-card card-left">';
                html += '<div class="media">';
                html += '<div class="media-left">';
                html += '<span class="bg-primary circle sq-50">'+response[i]['tc_title'].charAt(0)+'</span>';
                html += '</div>';
                html += '<div class="media-middle media-body">';
                html += "<h5 class='media-heading'>"+response[i]['tc_title']+"</h5>";
                html += "<small>"+response[i]['tc_description']+"</small>";
                html += '</div>';
                html += '</div>';
                html += '<div class="overlay">';
                html += '<label class="custom-control custom-control-primary custom-checkbox">';
                html += '<input class="custom-control-input" name="all_course" type="checkbox" value = "'+response[i]['tc_id']+'">';
                html += '<span class="custom-control-indicator"></span>';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }else{
                html += '<div class="col-xs-12  col-lg-6 padding-left">';
                html += '<div class="course-card card-left">';
                html += '<div class="media">';
                html += '<div class="media-left">';
                html += '<span class="bg-primary circle sq-50">'+response[i]['tc_title'].charAt(0)+'</span>';
                html += '</div>';
                html += '<div class="media-middle media-body">';
                html += "<h5 class='media-heading'>"+response[i]['tc_title']+"</h5>";
                html += "<small>"+response[i]['tc_description']+"</small>";
                html += '</div>';
                html += '</div>';
                html += '<div class="overlay">';
                html += '<label class="custom-control custom-control-primary custom-checkbox">';
                html += '<input class="custom-control-input" name="all_course" type="checkbox" value = "'+response[i]['tc_id']+'">';
                html += '<span class="custom-control-indicator"></span>';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }

          }

          $('#getcourseslist').html(html);
        }
      });      
}

function getFullLearningPathListFunction($filer_list){
      $.ajax({
        'type':'POST',
        'url':"getFullLearningPathList",
        data:{"filterdta":$filer_list},
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "";
          for(var i=0;i<response.length;i++){
            if((i % 2) == 0 || i == 0){
                html += '<div class="col-xs-12  col-lg-6 padding-right">';
                html += '<div class="course-card card-left">';
                html += '<div class="media">';
                html += '<div class="media-left">';
                html += '<span class="bg-primary circle sq-50">'+response[i]['tlp_name'].charAt(0)+'</span>';
                html += '</div>';
                html += '<div class="media-middle media-body">';
                html += "<h5 class='media-heading'>"+response[i]['tlp_name']+"</h5>";
                html += "<small>"+response[i]['tlp_description']+"</small>";
                html += '</div>';
                html += '</div>';
                html += '<div class="overlay">';
                html += '<label class="custom-control custom-control-primary custom-checkbox">';
                html += '<input class="custom-control-input" name="all_path" type="checkbox" value = "'+response[i]['tlp_id']+'">';
                html += '<span class="custom-control-indicator"></span>';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }else{
                html += '<div class="col-xs-12  col-lg-6 padding-left">';
                html += '<div class="course-card card-left">';
                html += '<div class="media">';
                html += '<div class="media-left">';
                html += '<span class="bg-primary circle sq-50">'+response[i]['tlp_name'].charAt(0)+'</span>';
                html += '</div>';
                html += '<div class="media-middle media-body">';
                html += "<h5 class='media-heading'>"+response[i]['tlp_name']+"</h5>";
                html += "<small>"+response[i]['tlp_description']+"</small>";
                html += '</div>';
                html += '</div>';
                html += '<div class="overlay">';
                html += '<label class="custom-control custom-control-primary custom-checkbox">';
                html += '<input class="custom-control-input" name="all_path" type="checkbox" value = "'+response[i]['tlp_id']+'">';
                html += '<span class="custom-control-indicator"></span>';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }

          }

          $('#getlearningpathlist').html(html);
        }
      });  
}

function getTeamLearingPathListFunction($filer_list){
      $.ajax({
        'type':'POST',
        'url':"getTeamLearingPathList",
        data:{"filterdta":$filer_list},
        success:function(response){
          console.log(response);
          response = JSON.parse(response);
          var html = "";
          for(var i=0;i<response.length;i++){
            if((i % 2) == 0 || i == 0){
                html += '<div class="col-xs-12  col-lg-6 padding-right">';
                html += '<div class="course-card card-left">';
                html += '<div class="media">';
                html += '<div class="media-left">';
                html += '<span class="bg-primary circle sq-50">'+response[i]['tlp_name'].charAt(0)+'</span>';
                html += '</div>';
                html += '<div class="media-middle media-body">';
                html += "<h5 class='media-heading'>"+response[i]['tlp_name']+"</h5>";
                html += "<small>"+response[i]['tlp_description']+"</small>";
                html += '</div>';
                html += '</div>';
                html += '<div class="overlay">';
                html += '<label class="custom-control custom-control-primary custom-checkbox">';
                html += '<input class="custom-control-input" name="all_team_path" type="checkbox" value = "'+response[i]['tlp_id']+'">';
                html += '<span class="custom-control-indicator"></span>';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }else{
                html += '<div class="col-xs-12  col-lg-6 padding-left">';
                html += '<div class="course-card card-left">';
                html += '<div class="media">';
                html += '<div class="media-left">';
                html += '<span class="bg-primary circle sq-50">'+response[i]['tlp_name'].charAt(0)+'</span>';
                html += '</div>';
                html += '<div class="media-middle media-body">';
                html += "<h5 class='media-heading'>"+response[i]['tlp_name']+"</h5>";
                html += "<small>"+response[i]['tlp_description']+"</small>";
                html += '</div>';
                html += '</div>';
                html += '<div class="overlay">';
                html += '<label class="custom-control custom-control-primary custom-checkbox">';
                html += '<input class="custom-control-input" name="all_team_path" type="checkbox" value = "'+response[i]['tlp_id']+'">';
                html += '<span class="custom-control-indicator"></span>';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            }

          }

          $('#getlearningpathteamlist').html(html);
        }
      });     
}

