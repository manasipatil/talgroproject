$( document ).ready(function() {
    
    $.ajaxSetup({
        headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //code for dynamic column order
    var column_val=$("#user_hidden_column_array").val();
    //console.log(column_val);
    var arrval=column_val.split(',');
    var arrfinal=[];
    for(var i=0;i<arrval.length; i++){
        var col_name=arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            arrfinal[i] = {data:col_name[0],name:"chk_box",
            title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="manager" type="checkbox"><span class="custom-control-indicator"></span></label>',
            class:"manager_sorting_class"};
        else
            arrfinal[i] = {data:col_name[0],title: col_name[0],class:"manager_sorting_class"};
    }
    //end of code
  	
    var col="";
    var table=$('.manager_record').DataTable( {
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"user_manager_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.manager_sorting_column_name=$('#manager_sorting_column_name').val();
                d.dropdown_manager_department_id = $(".manager_department option:selected").val();
                d.dropdown_manager_function_id = $(".manager_function option:selected").val();
                d.dropdown_manager_level_id = $(".manager_level option:selected").val();
                d.dropdown_manager_status_id = $(".manager_status option:selected").val();
                d.searchtxt=$("#searchmanagertxt").val();
            },
        },
        columns: arrfinal,
        "columnDefs": [ {
            "targets": [0,11], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $(document).on('click','.manager_sorting_class',function(){
        var column_name = $(this).text();
        $('#manager_sorting_column_name').val(column_name); 
        $('.manager_record').DataTable();
    });
    
    //code for show/hide columns
    var col_array = $('#user_hidden_column_array').val();
    var col_array =col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<col_array.length; i++){
        new_column=col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
           table.columns( col_class_name ).visible( true );
       }else{
           table.columns( col_class_name ).visible( false );
       }
    }
    //end of code


    //start code for all user code-----------------------------------------------------------------------

    //code for dynamic column order
    var all_column_val=$("#all_user_hidden_column_array").val();
    //console.log(all_column_val);
    var all_arrval=all_column_val.split(',');
    var all_arrfinal=[];
    for(var i=0;i<all_arrval.length; i++){
        var col_name=all_arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            all_arrfinal[i] = {data:col_name[0],name:"chk_box",
            title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="all_user" type="checkbox"><span class="custom-control-indicator"></span></label>',
            class:"all_user_sorting_class"};
        else
            all_arrfinal[i] = {data:col_name[0],title: col_name[0],class:"all_user_sorting_class"};
    }
    //end of code
    
    var col="";
    var all_table=$('.all_record').DataTable( {
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"all_user_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.all_user_sorting_column_name=$('#all_user_sorting_column_name').val();
                d.dropdown_all_department_id = $(".all_department option:selected").val();
                d.dropdown_all_function_id = $(".all_function option:selected").val();
                d.dropdown_all_level_id = $(".all_level option:selected").val();
                d.dropdown_all_status_id = $(".all_status option:selected").val();
                d.searchtxt=$("#searchalltxt").val();
                //d.range=$( "#user_slider" ).slider( "value" );
            },
        },
        columns: all_arrfinal,
        "columnDefs": [ {
            "targets": [0,9,10], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });
    
    //code for show/hide columns
    var all_col_array = $('#all_user_hidden_column_array').val();
    var all_col_array =all_col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<all_col_array.length; i++){
        new_column=all_col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
           all_table.columns( col_class_name ).visible( true );
       }else{
           all_table.columns( col_class_name ).visible( false );
       }
    }

    $(document).on('click','.all_user_sorting_class',function(){
        var column_name = $(this).text();
        $('#all_user_sorting_column_name').val(column_name); 
        $('.all_record').DataTable();
    });
    //end of code
    //end of  all user code


    //start code for expert code---------------------------------------------------------------------

    //code for dynamic column order
    var expert_column_val=$("#expert_hidden_column_array").val();
    //console.log(all_column_val);
    var expert_arrval=expert_column_val.split(',');
    var expert_arrfinal=[];
    for(var i=0;i<expert_arrval.length; i++){
        var col_name=expert_arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            expert_arrfinal[i] = {data:col_name[0],name:"chk_box",
            title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="expert" type="checkbox"><span class="custom-control-indicator"></span></label>',
            class:"expert_sorting_class"};
        else
            expert_arrfinal[i] = {data:col_name[0],title: col_name[0],class:"expert_sorting_class"};
    }
    //end of code
    
    var col="";
    var expert_table=$('.expert_record').DataTable( {
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"expert_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.expert_sorting_column_name=$('#expert_sorting_column_name').val();
                d.dropdown_expert_department_id = $(".expert_department option:selected").val();
                d.dropdown_expert_function_id = $(".expert_function option:selected").val();
                d.dropdown_expert_level_id = $(".expert_level option:selected").val();
                d.dropdown_expert_status_id = $(".expert_status option:selected").val();
                d.dropdown_expert_area_id=$(".expert_area option:selected").val();
                d.dropdown_expert_competency_id=$(".expert_competency option:selected").val();
                d.dropdown_expert_course_id=$(".expert_courses option:selected").val();
                d.searchtxt=$("#searchexperttxt").val();
            },
        },
        columns: expert_arrfinal,
        "columnDefs": [ {
            "targets": [0,11], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $(document).on('click','.expert_sorting_class',function(){
        var column_name = $(this).text();
        $('#expert_sorting_column_name').val(column_name); 
        $('.expert_record').DataTable();
    });
    
    //code for show/hide columns
    var expert_col_array = $('#expert_hidden_column_array').val();
    var expert_col_array =expert_col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<expert_col_array.length; i++){
        new_column=expert_col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
           expert_table.columns( col_class_name ).visible( true );
       }else{
           expert_table.columns( col_class_name ).visible( false );
       }
    }
    //end of code
    //end of expert code-------------------------------------------------------------

    //start code for hr----------------------------------------------------------------
    //code for dynamic column order
    var hr_column_val=$("#hr_hidden_column_array").val();
    //console.log(all_column_val);
    var hr_arrval=hr_column_val.split(',');
    var hr_arrfinal=[];
    for(var i=0;i<hr_arrval.length; i++){
        var col_name=hr_arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            hr_arrfinal[i] = {data:col_name[0],name:"chk_box",
            title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="hr" type="checkbox"><span class="custom-control-indicator"></span></label>',
            class:"hr_sorting_class"};
        else
            hr_arrfinal[i] = {data:col_name[0],title: col_name[0],class:"hr_sorting_class"};
    }
    //end of code
    
    var col="";
    var hr_table=$('.hr_record').DataTable( {
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"hr_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.hr_sorting_column_name=$('#hr_sorting_column_name').val();
                d.dropdown_hr_department_id = $(".hr_department option:selected").val();
                d.dropdown_hr_function_id = $(".hr_function option:selected").val();
                d.dropdown_hr_level_id = $(".hr_level option:selected").val();
                d.dropdown_hr_status_id = $(".hr_status option:selected").val();
                d.searchtxt=$("#searchhrtxt").val();
            },
        },
        columns: hr_arrfinal,
        "columnDefs": [ {
            "targets": [0,11], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $(document).on('click','.hr_sorting_class',function(){
        var column_name = $(this).text();
        $('#hr_sorting_column_name').val(column_name); 
        $('.hr_record').DataTable();
    });
    
    //code for show/hide columns
    var hr_col_array = $('#hr_hidden_column_array').val();
    var hr_col_array =hr_col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<hr_col_array.length; i++){
        new_column=hr_col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
           hr_table.columns( col_class_name ).visible( true );
       }else{
           hr_table.columns( col_class_name ).visible( false );
       }
    }
    //end of code
    //----------------------------------------------------------------------------------

    //start code for learner code---------------------------------------------------------------------

    //code for dynamic column order
    var learner_column_val=$("#learner_hidden_column_array").val();
    //console.log(all_column_val);
    var learner_arrval=learner_column_val.split(',');
    var learner_arrfinal=[];
    for(var i=0;i<learner_arrval.length; i++){
        var col_name=learner_arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            learner_arrfinal[i] = {data:col_name[0],name:"chk_box",
            title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="learner" type="checkbox"><span class="custom-control-indicator"></span></label>',
            class:"learner_sorting_class"};
        else
            learner_arrfinal[i] = {data:col_name[0],title: col_name[0],class:"learner_sorting_class"};
    }
    //end of code
    
    var col="";
    var learner_table=$('.learner_record').DataTable( {
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"learner_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.learner_sorting_column_name=$('#learner_sorting_column_name').val();
                d.dropdown_learner_department_id = $(".learner_department option:selected").val();
                d.dropdown_learner_function_id = $(".learner_function option:selected").val();
                d.dropdown_learner_level_id = $(".learner_level option:selected").val();
                d.dropdown_learner_status_id = $(".learner_status option:selected").val();
                d.searchtxt=$("#searchlearnertxt").val();
            },
        },
        columns: learner_arrfinal,
        "columnDefs": [ {
            "targets": [0,10], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $(document).on('click','.learner_sorting_class',function(){
        var column_name = $(this).text();
        $('#learner_sorting_column_name').val(column_name); 
        $('.learner_record').DataTable();
    });
    
    //code for show/hide columns
    var learner_col_array = $('#learner_hidden_column_array').val();
    var learner_col_array =learner_col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<learner_col_array.length; i++){
        new_column=learner_col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
           learner_table.columns( col_class_name ).visible( true );
       }else{
           learner_table.columns( col_class_name ).visible( false );
       }
    }
    //end of learner code-------------------------------------------------------------

    //start code for instructor code---------------------------------------------------------------------
    //code for dynamic column order
    var instructor_column_val=$("#instructor_hidden_column_array").val();
    //console.log(all_column_val);
    var instructor_arrval=instructor_column_val.split(',');
    var instructor_arrfinal=[];
    for(var i=0;i<instructor_arrval.length; i++){
        var col_name=instructor_arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            instructor_arrfinal[i] = {data:col_name[0],name:"chk_box",
            title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="instructor" type="checkbox"><span class="custom-control-indicator"></span></label>',
            class:"instructor_sorting_class"};
        else
            instructor_arrfinal[i] = {data:col_name[0],title: col_name[0],class:"instructor_sorting_class"};
    }
    //end of code
    
    var col="";
    var instructor_table=$('.instructor_record').DataTable( {
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"instructor_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.instructor_sorting_column_name=$('#instructor_sorting_column_name').val();
                d.dropdown_instructor_department_id = $(".instructor_department option:selected").val();
                d.dropdown_instructor_function_id = $(".instructor_function option:selected").val();
                d.dropdown_instructor_level_id = $(".instructor_level option:selected").val();
                d.dropdown_instructor_status_id = $(".instructor_status option:selected").val();

                d.dropdown_instructor_area_id=$(".instructor_area option:selected").val();
                d.dropdown_instructor_competency_id=$(".instructor_competency option:selected").val();
                d.dropdown_instructor_course_id=$(".instructor_courses option:selected").val();

                d.searchtxt=$("#searchlearnertxt").val();
            },
        },
        columns: instructor_arrfinal,
        "columnDefs": [ {
            "targets": [0,11], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $(document).on('click','.instructor_sorting_class',function(){
        var column_name = $(this).text();
        $('#instructor_sorting_column_name').val(column_name); 
        $('.instructor_record').DataTable();
    });
    
    //code for show/hide columns
    var instructor_col_array = $('#instructor_hidden_column_array').val();
    var instructor_col_array =instructor_col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<instructor_col_array.length; i++){
        new_column=instructor_col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
           instructor_table.columns( col_class_name ).visible( true );
       }else{
           instructor_table.columns( col_class_name ).visible( false );
       }
    }
    //end of code
    //end of learner code-------------------------------------------------------------

    //code for deleting single user
    $('.submit_form').click(function(e){
        var delete_id=$(this).attr("delete_id");
        e.preventDefault();
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this User!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel it!",   
            closeOnConfirm: true,   
           closeOnCancel: true 
        },function(isConfirm) {   
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: "delete_user",
                    data: {
                        delete_id:delete_id
                    }, 
                    success: function(data)
                    {
                        swal("Deleted!", "Your User has been deleted.", "success"); 
                        table.ajax.reload();
                    }
                });
            } else {     
              swal("Cancelled", "Your user is safe :)", "error");   
            } 
        });
    });

    //search filter
    $('#user_searchButton').click(function () {
        table.ajax.reload();
    });

    $('#all_user_searchButton').click(function(){
        all_table.ajax.reload();
    });

    $('#expert_searchButton').click(function(){
        expert_table.ajax.reload();
    });

    $('#hr_searchButton').click(function(){
        hr_table.ajax.reload();
    });

    $('#learner_searchButton').click(function(){
        learner_table.ajax.reload();
    });
    $('#instructor_searchButton').click(function(){
        instructor_table.ajax.reload();
    });

    //for filter show/hide
    $('.refine_mbutton').click(function(){
        $('.Cus_card-body').slideToggle("slow");
        $('.refine_mbutton').toggleClass("active");
    });


    //common action
    $('.click_common_action').click(function(){
        var $common_tab_name=$(this).attr("tab_name");
        var i=0;
        var arr = [];
        $('.'+$common_tab_name).find('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        var action=$(this).attr("to_do");
        if(action=="delete")
            var text="You will not be able to recover this Competency!";
        else if(action=="enable")
            var text="You want to enable all Competency";
        else if(action=="disable")
            var text="You want to disable all Competency";
       
        swal({   
                title: "Are you sure?",   
                text: "" + text + "",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, do it!",   
                cancelButtonText: "No, cancel plx!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "user_common_action",
                        data: {
                            "action":action,
                            "chk_check_value":arr
                        }, 
                        success: function(data)
                        {
                            if(action=="delete")
                                swal("Deleted!", "Your Competency has been Deleted.", "success");
                            else if(action=="enable")
                                swal("Enable","Your Competency has been Enable ","success");
                            else if(action=="disable")
                                swal("Disable","Your Competency has been disable","success");
                            if($common_tab_name=="all_user") 
                                all_record.ajax.reload();
                            else if($common_tab_name=="learner")
                                learner_record.ajax.reload();
                            else if($common_tab_name=="hr")
                                hr_record.ajax.reload();
                            else if($common_tab_name=="expert")
                                expert_record.ajax.reload();
                            else if($common_tab_name=="manager")
                                manager_record.ajax.reload();
                            else if($common_tab_name=="instructor")
                                instructor_record.ajax.reload();
                            $('.'+$common_tab_name).find('.select_all_tab').prop('checked',false);
                            $('.'+$common_tab_name).find('.common_action').addClass("hide");
                        }
                    });
                } else {     
                  swal("Cancelled", "Your Codes are safe :)", "error");   
                }
            }); 
    });

    //for tab checkbox

    //when click on all checkbox
    $(document).on('click','.select_all_tab', function(){
        var $tab_name=$(this).attr("tab_name");
        if($(".select_all_tab").is(':checked')){
            $('.'+$tab_name).find('.all_user_checkbox').prop('checked',true);
            //class to show button when checkbox check
            $('.'+$tab_name).find('.common_action').removeClass("hide");
        }
        else{
            $('.'+$tab_name).find('.all_user_checkbox').prop('checked',false);
            $('.'+$tab_name).find('.common_action').addClass("hide");
            
        }
    });

    //when clcik on single checkbox
    $(document).on('click','.all_user_checkbox', function(){
        var $tab_name=$(this).attr("tab_name");
        if($(".all_user_checkbox").is(':checked')){
            $('.'+$tab_name).find('.common_action').removeClass("hide");
        }
        else{
            
            $('.'+$tab_name).find('.common_action').addClass("hide");
            var select_all=0;
            $('.'+$tab_name).find(".all_user_checkbox").each(function(){
                if(!$(".all_user_checkbox").is(':checked')){
                    select_all=1;
                }
            });
            if(select_all==1){
                $('.'+$tab_name).find('.select_all_tab').prop('checked',false);
            }
        }
    });

    


});//end of document.ready function

