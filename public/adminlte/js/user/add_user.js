$(document).ready(function(){
  //alert("yess");
	$.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
  });

  $("#profile_image").change(function(){
    readURL(this);
  });

  

  if($('#hidden_user_id').val()==0){
	//code for hiding addition employee details
    $('.hide_employee_details').addClass("hide");
    if($('#hidden_user_type').val()=="all" || $('#hidden_user_type').val()=="instructor" || $('#hidden_user_type').val()=="Expert" ){
      //$('.hide_employee_details').removeClass("hide");
    }
  }else{
    if($('#show_extra_details_edit').val()==0){
      if(!$( ".hide_employee_details" ).hasClass( "hide" ))
        $('.hide_employee_details').addClass("hide");
    }else{
      $('.hide_employee_details').removeClass("hide");
    }
  }
    var chk_true= [];
    var chk_false="";
    $('.click_user_type_master').each(function(){
      if($(this).is(':checked')){
        chk_true.push(1);
      }
    }); 

    var chk_unchk=[];

    //console.log(chk_true);

    if(chk_true!=""){
      $("#select_permission").removeClass("hide");
    } 
    //on click of user type
    $('.click_user_type_master').click(function(){
      var data_attr=$(this).attr("data_attr");
      if($(this).is(':checked')){

        $('.click_user_type_master').each(function(){
          if($(this).is(':checked')){
            chk_true.push(1);
          }
        }); 
        if(chk_true!=""){
          $("#select_permission").removeClass("hide");
        } 

        //$("#select_permission").removeClass("hide");
        $('.user_type_'+data_attr).removeClass("hide");
      }else{
        $('.click_user_type_master').each(function(){
          if($(this).is(':checked')){
            chk_unchk.push(1);
          }
        });
        //console.log(chk_unchk); 
        if(chk_unchk!=""){
          //alert("removeClass");
          $("#select_permission").removeClass("hide");
        }else{
          //alert("addClass");
          $("#select_permission").addClass("hide");
        } 
        $('.user_type_'+data_attr).addClass("hide");
      }



    });

  //for showing extra details for expert and instructor tab
  if($('#hidden_user_id').val()>0){
    var true_chk="";
    var false_chk="";
    $('.show_employee_details').each(function(){
        if($(this).is(':checked')){
          var true_chk="true";
          $('.hide_employee_details').removeClass("hide");
        }else{
          var false_chk="false";
        }
    })
    if(true_chk=="true"){
      $('.hide_employee_details').removeClass("hide");
    }
  }

  $('#Frmadduser').validate({
        rules: {
          	employee_code: {
               required: true,
               minlength: 3
           	},
          	user_firstname :{
              lettersonly: true,
               required: true,
               minlength: 3            
          	},
          	user_middlename :{
              lettersonly: true,
               required: true,
               minlength: 3            
          	},
          	user_lastname :{
              lettersonly: true,
               required: true,
               minlength: 3            
          	},
          	gender :{
               required: true,
          	},
          	email :{
               required: true,
               email: true,
               minlength: 3            
          	},
          	mobile_no :{
              required: true,
              minlength:10,
              maxlength:10,
              number: true
          	}

        },
        messages: {
          	employee_code: {
               required: "Employee Code is required",
               minlength: "Employee Code must contain at least 3 characters"
           	},
           	user_firstname: {
              lettersonly: "Please enter characters only",
               required: "User Firstname is required",
               minlength: "User Firstname must contain at least {0} characters"
           	},
          	user_middlename: {
              lettersonly: "Please enter characters only",
               required: "User Middlename is required",
               minlength: "User Middlename must contain at least {0} characters"
           	},
           	user_lastname: {
              lettersonly: "Please enter characters only",
               required: "User Lastname is required",
               minlength: "User Lastname must contain at least {0} characters"
           	},
           	gender:{
           		required:"Please select gender ",
           	},
           	email:{
           		required:"Please enter Email",
           	},
           	mobile_no:{
           		required:"Please Enter Mobile No",
           		
           	}
        }, 
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
          $(element).parents('.form-group').addClass('has-success');
        },
        submitHandler: function (form) {
        	var formData = new FormData($('#Frmadduser')[0]);
          var input = document.getElementById("profile_image");
          file = input.files[0];
          formData.append("profile_image", file);
            if($('#hidden_user_id').val()>0){
                var hidden_user_id=$('#hidden_user_id').val();
                $.ajax({
                    type: "POST",
                    processData: false,  // Important!
        			      contentType: false,
        			      cache: false,
				            data: formData,
                    url: "edit_user_ajax",
                    success: function(data)
                    {
                      //console.log(data);
                      var result = JSON.parse(data);
                      if(result['status']=== 'error'){
                          $('#err_user_type').html(result['errors']['user_type'])
                          $("#err_employee_code").html(result['errors']['employee_code']);
                          $("#err_user_firstname").html(result['errors']['user_firstname']);
                          $("#err_user_lastname").html(result['errors']['user_lastname']);
                          $("#err_user_middlename").html(result['errors']['user_middlename']);
                          $("#err_gender").html(result['errors']['gender']);
                          $("#err_email").html(result['errors']['email']);
                          $('#err_mobile_no').html(result['errors']['mobile_no']);
                          $('#err_permission_type').html(result['errors']['permission_type']);
                          $('#err_image_type').text(result['errors']['profile_image']);
                          return false;
                      }
                      if(result['status']=== 'success'){
                        var last_insert_id=result['last_insert_id'];
                        window.location="../users";
                      }
                  }
                });
                
        }else{
          //alert("save");
                //save record
                $.ajax({
                    type: "POST",
                    processData: false,
				            contentType: false,
				            data: formData,
                    url: "add_user/add_user_ajax",
                    success: function(data)
                    {
                      //alert("hhh");
                      console.log(data);
                      var result = JSON.parse(data);
                      if(result['status']== 'error'){
                          $('#err_user_type').html(result['errors']['user_type'])
                          $("#err_employee_code").html(result['errors']['employee_code']);
                          $("#err_user_firstname").html(result['errors']['user_firstname']);
                          $("#err_user_lastname").html(result['errors']['user_lastname']);
                          $("#err_user_middlename").html(result['errors']['user_middlename']);
                          $("#err_gender").html(result['errors']['gender']);
                          $("#err_email").html(result['errors']['email']);
                          $('#err_mobile_no').html(result['errors']['mobile_no']);
                          $('#err_permission_type').text(result['errors']['permission_type']);
                          $('#err_image_type').text(result['errors']['profile_image']);
                          return false;
                      }
                      if(result['status']== 'success'){
                        var last_insert_id=result['last_insert_id'];
                         window.location = "../users";
                      }
                        
                    }
                });
            }//end of else condition
        }
  });

  //code for deleting single user
  $('.delete_image').click(function(e){
      swal({   
          title: "Are you sure?",   
          text: "You will not be able to recover this Image!",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes, delete it!",   
          cancelButtonText: "No, cancel it!",   
          closeOnConfirm: true,   
          closeOnCancel: true 
      },function(isConfirm) {   
          if (isConfirm) {
             // $('#replace_image').src("")
              $("#replace_image").attr("src","../../public/img/no_user.jpg");
              $('#image_remove').val(1);
              
          } else {     
            swal("Cancelled", "Your user is safe :)", "error");   
          } 
      });
  });


	$('.show_employee_details').click(function(){
		if($(this).is(':checked')){
			$('.hide_employee_details').removeClass("hide");
		}else{
			$('.hide_employee_details').addClass("hide");
		}
	})

  if($('#hidden_user_id').val()>0){
    var test_table=$('.test_listing').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"edit_user/psyometric_test_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.sorting_test_listing_column_name=$('#sorting_test_listing_column_name').val();
                d.hidden_user_id=$("#hidden_user_id").val();
            },
            
        },
        columns: [
                {data: 'Test',class:"sorting_listing_class" },
                { data: 'Status',class:"sorting_listing_class" },
                
        ],
    });

    // $(document).on('click','.sorting_listing_class',function(){
    //     var column_name = $(this).text();
    //     $('#sorting_column_name').val(column_name); 
    //     $('.example').DataTable();
    // });

    var course_table=$('.course_listing').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"edit_user/course_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.hidden_user_id=$("#hidden_user_id").val();
            },
            
        },
        columns: [
                { name: 'Course Name',data:'CourseName'},
                { name: 'Course Type',data:'CourseType' },
                { name: 'Course Categories',data:'CourseCategories' },
                { name: 'Course Competency',data:'CourseCompetency' },
                { name: 'Course Development Area',data:'CourseDevelopmentArea' }
        ],
    });
  }
  $('.change_country').change(function(){
    //alert($(this).val());
        $.ajax({
              'type':'POST',
              'url':"add_user/getstate_list",
              data: {
                "countryid" : $(this).val()
              },
              success:function(response){
                //console.log(response);
                response = JSON.parse(response);
                var html;
                for(var i=0;i<response.length;i++){
                html += "<option value='"+response[i]['id']+"'>"+response[i]['state_name']+"</option>";
                }
                $('.change_state').html(html);
              }
            });         
  });
  
});

function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('.change_image').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
  }
}
    