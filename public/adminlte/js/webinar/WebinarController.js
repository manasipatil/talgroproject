$( document ).ready(function() {
    $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
    });

    //code for dynamic column order
    var column_val=$("#webinar_hidden_column_array").val();
    //console.log(column_val);
    var arrval=column_val.split(',');
    var arrfinal=[];
    for(var i=0;i<arrval.length; i++){
        var col_name=arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            arrfinal[i] = {data:col_name[0],name:"chk_box",
        title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all" type="checkbox"><span class="custom-control-indicator"></span></label>',
        class:col_name[0]};
        else if(col_name[0]=="WebinarName")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Webinar Name",class:"sorting_class"};
        else if(col_name[0]=="CourseName")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Course Name",class:"sorting_class"};
        else if(col_name[0]=="DevelopmentArea")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Development Area",class:"sorting_class"};
        else
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:col_name[0],class:"sorting_class"};
    }
    //end of code
    //console.log(arrfinal);
  	
    var col="";
    var webinar_table=$('.example').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"webinar_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.sorting_column_name=$('#sorting_column_name').val();
                d.dropdown_webinar_course = $(".webinar_course option:selected").val();
                d.dropdown_webnar_instructor = $(".webnar_instructor option:selected").val();
                d.dropdown_webinar_competency = $(".webinar_competency option:selected").val();
                d.dropdown_webinar_development_area = $(".webinar_development_area option:selected").val();
                d.dropdown_webinar_level = $(".webinar_level option:selected").val();
                d.webinar_start_date=$('#webinar_start_date').val();
                d.webinar_end_date=$('#webinar_end_date').val();
                d.searchtxt=$("#searchtxt").val();
            },
        },
        columns: arrfinal,
        
        "columnDefs": [ {
            "targets": [0,12], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });
    
    
    //code for show/hide columns
    var col_array = $('#webinar_hidden_column_array').val();
    var col_array =col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<col_array.length; i++){
        new_column=col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "webinar_table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
            webinar_table.columns( col_class_name ).visible( true );
        }else{
            webinar_table.columns( col_class_name ).visible( false );
        }
    }
    //end of code
    //search filter
    $('#webinar_searchButton').click(function () {
        webinar_table.ajax.reload();
    });
    
    $(document).on('click','.sorting_class',function(){
        var column_name = $(this).text();
        $('#sorting_column_name').val(column_name); 
        $('.example').DataTable();
    });
    

    //for delete single webinar
    $(document).on('click', '.submit_form', function(){
        var delete_id=$(this).attr("delete_id");
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this Webinar!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                cancelButtonText: "No, cancel it!",   
                closeOnConfirm: false,   
               closeOnCancel: false 
            },function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "delete_webinar",
                        data: {
                            delete_id:delete_id
                        }, 
                        success: function(data)
                        {
                             swal("Deleted!", "Your Webinar has been deleted.", "success"); 
                             webinar_table.ajax.reload();
                        }
                    });
                } else {     
                  swal("Cancelled", "Your Webinar is safe :)", "error");   
                } 
            });
    });

    //common action
    $('.click_common_action').click(function(){
        var i=0;
        var arr = [];
        $('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        var action=$(this).attr("to_do");
        if(action=="delete")
            var text="You will not be able to recover this Webinar!";
        else if(action=="enable")
            var text="You want to enable all Webinar";
        else if(action=="disable")
            var text="You want to disable all Webinar";
       
        swal({   
                title: "Are you sure?",   
                text: "" + text + "",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, do it!",   
                cancelButtonText: "No, cancel plx!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "webinar_common_action",
                        data: {
                            "action":action,
                            "chk_check_value":arr
                        }, 
                        success: function(data)
                        {
                            if(action=="delete")
                                swal("Deleted!", "Your Webinar has been Deleted.", "success");
                            else if(action=="enable")
                                swal("Enable","Your Webinar has been Enable ","success");
                            else if(action=="disable")
                                swal("Disable","Your Webinar has been disable","success"); 
                            webinar_table.ajax.reload();
                            $('.select_all').prop('checked',false);
                            $('.example tr').removeClass("selected");
                        }
                    });

                   
                } else {     
                  swal("Cancelled", "Your Webinar are safe :)", "error");   
                }
            }); 
    });
    
    

    
});


