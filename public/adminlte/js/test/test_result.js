$( document ).ready(function() {
    $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
    });
    //code for dynamic column order
    var column_val=$("#test_result_hidden_column_array").val();
    console.log(column_val);
    var arrval=column_val.split(',');
    var arrfinal=[];
    for(var i=0;i<arrval.length; i++){
        var col_name=arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            arrfinal[i] = {
                data:col_name[0],name:"chk_box",
        title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all" type="checkbox"><span class="custom-control-indicator"></span></label>',
        class:col_name[0]
        };
        else if(col_name[0]=="EmpCode")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Employee Code",class:col_name[0]};
        else if(col_name[0]=="UserName")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"User Name",class:col_name[0]};
        else if(col_name[0]=="RecommendedCourses")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Recommended Courses",class:col_name[0]};
        else if(col_name[0]=="LearningPath")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Show Learning Path",class:col_name[0]};
        else if(col_name[0]=="AssocLearningPath")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Associated Learning Path",class:col_name[0]};                    
        else
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:col_name[0],class:col_name[0]};
    }
    //end of code
    
    var col="";
    var test_table=$('.example2').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"test_result_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false, 
            "data": function ( d ) {
                d.dropdown_test_function = $("#dropdown_tte_test_function option:selected").val();
                d.dropdown_tte_course_id = $("#dropdown_tte_course_id option:selected").val();
                d.test_type=$("input[name='test_type']:checked").val();
                d.searchtxt=$("#searchtxt").val();
                d.txt_parameter=$('#txt_parameter').val();
            },
        },
        columns: arrfinal,
        
        "columnDefs": [ {
            "targets": [6], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });
    
    
    //code for show/hide columns
    var col_array = $('#test_result_hidden_column_array').val();
    var col_array =col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<col_array.length; i++){
        new_column=col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "test_table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
            test_table.columns( col_class_name ).visible( true );
        }else{
            test_table.columns( col_class_name ).visible( false );
        }
    }
    //end of code

    //search filter
    // $('#test_searchButton').click(function () {
    //     test_table.ajax.reload();
    // });


    
    
    

    //for delete single id
    $(document).on('click', '.submit_form', function(){
        var delete_id=$(this).attr("delete_id");
        //e.preventDefault();
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this Code!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                cancelButtonText: "No, cancel it!",   
                closeOnConfirm: false,   
               closeOnCancel: false 
            },function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "delete_functions_code",
                        data: {
                            delete_id:delete_id
                        }, 
                        success: function(data)
                        {
                             swal("Deleted!", "Your Code has been deleted.", "success"); 
                             test_table.ajax.reload();
                        }
                    });
                } else {     
                  swal("Cancelled", "Your imaginary file is safe :)", "error");   
                } 
            });
    });

    //common action
    $('.click_common_action').click(function(){
        alert('hi');
        var i=0;
        var arr = [];
        $('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        var action=$(this).attr("to_do");
        if(action=="delete")
            var text="You will not be able to recover this Functions!";
        else if(action=="enable")
            var text="You want to enable all Functions";
        else if(action=="disable")
            var text="You want to disable all Functions";
       
        swal({   
                title: "Are you sure?",   
                text: "" + text + "",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, do it!",   
                cancelButtonText: "No, cancel plx!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "test_result_common_action",
                        data: {
                            "action":action,
                            "chk_check_value":arr
                        }, 
                        success: function(data)
                        {
                            if(action=="delete")
                                swal("Deleted!", "Your Functions has been Deleted.", "success");
                            else if(action=="enable")
                                swal("Enable","Your Functions has been Enable ","success");
                            else if(action=="disable")
                                swal("Disable","Your Functions has been disable","success"); 
                            test_table.ajax.reload();
                            $('.select_all').prop('checked',false);
                            $('.example2 tr').removeClass("selected");
                            $('.common_action').addClass("hide");
                        }
                    });
                } else {     
                  swal("Cancelled", "Your Codes are safe :)", "error");   
                }
            }); 
    });   
});



