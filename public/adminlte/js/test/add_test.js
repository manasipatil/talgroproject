$(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', '#go_to_add_test', function(){
        $.ajax({
            'type':'POST',
            'url':"clearTestSession",
            success:function(response){
                window.location = "addtest";
                return false;
            }
        });

    });


    // copyTest();

    //-----------------------Start Calendar---------------------------
    //  $('#calendar').fullCalendar({
    //   header: {
    //     left: 'title',
    //     center:'agendaDay,agendaWeek,month',
    //     right: 'prev,next today'
    //   },
    //   defaultDate: '2018-10-06',
    //   navLinks: true, // can click day/week names to navigate views
    //   selectable: true,
    //   selectHelper: true,
    //   select: function(start, end) {
    //     var title = prompt('Event Title:');
    //     var eventData;
    //     if (title) {
    //       eventData = {
    //         title: title,
    //         start: start,
    //         end: end
    //       };
    //       $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
    //     }
    //     $('#calendar').fullCalendar('unselect');
    //   },
    //   editable: true,
    //   eventLimit: true, // allow "more" link when too many events
    //   eventRender: function(event, element) {
    //     if (event.SomeBool) {
    //         element.addClass('bg');
    //     }
    // }
    // });

    //-----------------------End Calendar----------------------

    //add and edit tabs starts here
    $(document).on('click','.btn-number',function(e) {
        
        e.preventDefault();
        
        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                
                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                } 
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
       $(this).data('oldValue', $(this).val());
    });
    $(document).on('change','.input-number',function() {
        
        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());
        
        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        
        
    });
    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(document).on('click','.ad-minus',function(e){
        e.preventDefault();
        var get_id = $(this).attr('data-miid');
        $('#param_'+get_id).remove();
    });

    $('.add-parameter').click(function(){
        var param_div_count = $('#param_div_count').val();
        var html = "";
        var i = (parseInt(param_div_count) + 1);
        html += '<div id="param_'+i+'" class="parameter-full m-b-sm clearfix">';
        html += '<div class="col-xs-6 col-md-7 nopadding">';
        html += '<input class="form-control parameter_text" id="paramtext_'+i+'" type="text">';
        html += '</div>';
        html += '<div class="col-xs-5 col-md-4 nopadding">';
        html += '<div class="input-group">';
        html += '<span class="input-group-btn">';
        html += '<button type="button" class="btn btn-gray btn-number" data-type="minus" data-field="quant['+i+']">';
        html += '<span class="icon icon-minus-circle"></span>';
        html += '</button>';
        html += '</span>';
        html += '<input type="text" name="quant['+i+']" style="width: 50px;" class="form-control text-center input-number" value="10" min="1" max="100">';
        html += '<span class="input-group-btn">';
        html += '<button type="button" class="btn btn-gray btn-number" data-type="plus" data-field="quant['+i+']">';
        html += '<span class="icon icon-plus-circle"></span>';
        html += '</button>';
        html += '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-xs-1 col-md-1 nopadding text-center" style="padding-left: 28px;">';
        html += '<a href="Javascript:Void(0);" data-miid="'+i+'" class="ad-minus"><i class="icon icon-minus-circle"></i></a>';
        html += '</div>';
        html += '</div>';
        html += '<div class="clearfix appenddiv"></div>';
        $('#param_div_count').val(i);
        $('.appenddiv:last').append(html);
    });   


    $('#add_test_form').validate({ 
        rules: {
          test_name: {
               required: true,
               minlength: 3
           }
        },
        messages: {
          test_name: {
               required: "Title is required",
               minlength: "Title must contain at least {0} characters"
           }
        }, 
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
          $(element).parents('.form-group').addClass('has-success');
        },
        submitHandler: function(form) {
            var weightage = "";
            var sum = 0;
            $(".input-number").each(function() {
                weightage += $(this).val() +",";
                sum += +$(this).val();
            });
           
            weightage = weightage.slice(0, -1);

            var parameter_list = "";
            $(".parameter_text").each(function() {
                parameter_list += $(this).val() +",";
            });
            parameter_list = parameter_list.slice(0, -1);

            if(parameter_list != ""){
                if(sum != '100'){
                    $('#tab1_error_content').html('Total weightage should be 100 %');
                    $('#tab1_show_error_msg').show();
                    $('#tab1_show_error_msg').delay(4000).fadeOut();
                    return false;
                }                
            }
            var formData = new FormData(this);
            formData.append("test_title", $('#test_title_id').val());
            formData.append("test_function", $('#test_func_id').val());
            formData.append("test_start_date", $('#start_date_id').val());
            formData.append("test_end_date", $('#end_date_id').val());
            formData.append("test_duration", $('#test_duration_id').val());
            formData.append("test_description",CKEDITOR.instances.test_description.getData());
            formData.append("is_internal",$("input[name='is_internal']:checked"). val());
            formData.append("weightage",weightage);
            formData.append("parameter_list",parameter_list);

            get_url = "insertTest";
            $.ajax({
                url: get_url,
                type: 'POST',
                data:formData,
                dataType: 'JSON',
                contentType : false,
                cache : false,
                processData : false,
                success: function(response) {
                    if(response['status'] == "success"){
                        $('#tab1_success_content').html(response['message']);
                        $('#tab1_show_success_msg').show();
                        $('#tab1_show_success_msg').delay(4000).fadeOut();

                        if(response['type'] == "insert"){
                            //after add test active all the tabs
                            $('.steps a[href="#tab-1"]').closest('li').removeClass('active');
                            $('.steps a[href="#tab-2"]').closest('li').addClass('active');
                            $('.steps a[href="#tab-2"]').attr('data-toggle','tab');
                            $('.tab-menu a[href="#tab-2"]').attr('data-toggle','tab'); 
                            $('.steps a[href="#tab-3"]').attr('data-toggle','tab');
                            $('.tab-menu a[href="#tab-3"]').attr('data-toggle','tab'); 
                            $('.steps a[href="#tab-4"]').attr('data-toggle','tab');
                            $('.tab-menu a[href="#tab-4"]').attr('data-toggle','tab'); 
                            $('.steps a[href="#tab-5"]').attr('data-toggle','tab');
                            $('.tab-menu a[href="#tab-5"]').attr('data-toggle','tab');  
                            $('.steps a[href="#tab-6"]').attr('data-toggle','tab');
                            $('.tab-menu a[href="#tab-6"]').attr('data-toggle','tab');
                            $('.steps a[href="#tab-7"]').attr('data-toggle','tab');
                            $('.tab-menu a[href="#tab-7"]').attr('data-toggle','tab');
                            $('.steps a[href="#tab-8"]').attr('data-toggle','tab');
                            $('.tab-menu a[href="#tab-8"]').attr('data-toggle','tab'); 
                        }

                        setTimeout(function () { 
                            if($('#select_btn').val() == 'saveandshow'){    
                                $('.tab-menu a[href="#tab-2"]').tab('show');
                                $('.steps a[href="#tab-1"]').closest('li').removeClass('active');
                                $('.steps a[href="#tab-2"]').closest('li').addClass('active');
                                $('.steps a[href="#tab-2"]').closest('li').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-2"]').closest('li').attr('data-toggle','tab');
                                $('.steps a[href="#tab-3"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-3"]').attr('data-toggle','tab'); 
                                $('.steps a[href="#tab-4"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-4"]').attr('data-toggle','tab'); 
                                $('.steps a[href="#tab-5"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-5"]').attr('data-toggle','tab');  
                                $('.steps a[href="#tab-6"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-6"]').attr('data-toggle','tab');
                                $('.steps a[href="#tab-7"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-7"]').attr('data-toggle','tab');
                                $('.steps a[href="#tab-8"]').attr('data-toggle','tab');
                                $('.tab-menu a[href="#tab-8"]').attr('data-toggle','tab');
                            }
                        }, 1000); 
                    }else{
                        $('#tab1_error_content').html(response['message']);
                        $('#tab1_show_error_msg').show();
                        $('#tab1_show_error_msg').delay(4000).fadeOut();
                    }
                    return false;
                }
            });
        }

    }); 

    $('#save_add_que_btn').click(function(){
        $('#select_btn').val('saveandshow');
    });

    getTestData(); 
    //add and edit test tab ends here

    // settings tab
    $('.e11').show(); 
    $('.e12, .e13, .e14, .e15, .e16').hide(); 
    $('.e1').click(function(){
          $('.e11').show(); 
          $('.e12, .e13, .e14, .e15, .e16').hide(); 
    });
    $('.e2').click(function(){
          $('.e12').show(); 
          $('.e11, .e13, .e14, .e15, .e16').hide(); 
    });
    $('.e3').click(function(){
          $('.e13').show(); 
          $('.e11, .e12, .e14, .e15, .e16').hide(); 
    });
    $('.e4').click(function(){
          $('.e14').show(); 
          $('.e11, .e12, .e13, .e15, .e16').hide(); 
    });
    $('.e5').click(function(){
          $('.e15').show(); 
          $('.e11, .e12, .e13, .e14, .e16').hide(); 
    });
    $('.e6').click(function(){
          $('.e16').show(); 
          $('.e11, .e12, .e13, .e14, .e15').hide(); 
    });


    $('#save_all_settings').click(function(){

        var invite_email_subject = $('#invite_email_subject').val();
        var invite_email_body = CKEDITOR.instances.invite_email_body.getData();

        var congrats_email_subject = $('#congrats_email_subject').val();
        var congrats_email_body = CKEDITOR.instances.congrats_email_body.getData();

        var attempt_allow_email_subject = $('#attempt_allow_email_subject').val();
        var attempt_allow_email_body = CKEDITOR.instances.attempt_allow_email_body.getData();

        var retake_email_subject = $('#retake_email_subject').val();
        var retake_email_body = CKEDITOR.instances.retake_email_body.getData();

        var allow_email_subject = $('#allow_email_subject').val();
        var allow_email_body = CKEDITOR.instances.allow_email_body.getData();

        var result_email_subject = $('#result_email_subject').val();
        var result_email_body = CKEDITOR.instances.result_email_body.getData();

        var data = '[{"setting_name":"INVITE_EMAIL","parameter":"'+$('#invite_param').val()+'","days_hr":"'+$("input[name='invite_options']:checked"). val()+'","email_subject":"'+invite_email_subject+'","setting_type":"general"},{"setting_name":"REMINDER_EMAIL","parameter":"'+$('#reminder_param').val()+'","days_hr":"'+$("input[name='reminder_options']:checked"). val()+'","email_subject":"","setting_type":"general"},{"setting_name":"CONGRATULATIONS_EMAIL","parameter":"0","days_hr":"","email_subject":"'+congrats_email_subject+'","setting_type":"general"},{"setting_name":"ATTEMPT_ALLOWED_EMAIL","parameter":"'+$('#attempts_param').val()+'","days_hr":"attempt","email_subject":"'+attempt_allow_email_subject+'","setting_type":"general"},{"setting_name":"RETAKE_EMAIL","parameter":"0","days_hr":"","email_subject":"'+retake_email_subject+'","setting_type":"general"},{"setting_name":"ALLOW_ANS_EMAIL","parameter":"0","days_hr":"","email_subject":"'+allow_email_subject+'","setting_type":"general"},{"setting_name":"RESULT_EMAIL","parameter":"0","days_hr":"","email_subject":"'+result_email_subject+'","setting_type":"general"},{"setting_name":"RANDOMIZE_Q","parameter":"0","days_hr":"'+$("input[name='randomize_option5']:checked"). val()+'","email_subject":"","setting_type":"section"},{"setting_name":"ALL_Q_MANDATORY","parameter":"0","days_hr":"'+$("input[name='mand_que_option7']:checked"). val()+'","email_subject":"","setting_type":"section"},{"setting_name":"SKIP_SECTION","parameter":"0","days_hr":"'+$("input[name='skip_section_option9']:checked"). val()+'","email_subject":"","setting_type":"section"},{"setting_name":"ATLEAST_ONE_MANDATORY","parameter":"0","days_hr":"'+$("input[name='one_mand_option11']:checked"). val()+'","email_subject":"","setting_type":"section"}]';
        // console.log(data);
        // return false;
        $.ajax({
            'type':'POST',
            'url':"saveTestSettings",
            'dataType':'HTML',
            'data':{'data':data},
            success:function(response){
                console.log(response);
                $('#tab3_success_content').html("Test settings saved successfully");
                $('#tab3_show_success_msg').show();
                $('#tab3_show_success_msg').delay(4000).fadeOut();
                return false;
            }

        });


        saveTestEmailContentFunction(invite_email_subject,invite_email_body,"INVITE_EMAIL");
        saveTestEmailContentFunction(congrats_email_subject,congrats_email_body,"CONGRATULATIONS_EMAIL");
        saveTestEmailContentFunction(attempt_allow_email_subject,attempt_allow_email_body,"ATTEMPT_ALLOWED_EMAIL");
        saveTestEmailContentFunction(retake_email_subject,retake_email_body,"RETAKE_EMAIL");
        saveTestEmailContentFunction(allow_email_subject,allow_email_body,"ALLOW_ANS_EMAIL");
        saveTestEmailContentFunction(result_email_subject,result_email_body,"RESULT_EMAIL");
        $('.steps a[href="#tab-3"]').closest('li').removeClass('active');
        $('.steps a[href="#tab-4"]').closest('li').addClass('active');
        return false;
    });

    $(document).on('click','#invite_single_save',function(){
        //to save invite email content

        appendDynamicFields();
        var invite_email_subject = $('#invite_email_subject').val();
        var invite_email_body = CKEDITOR.instances.invite_email_body.getData();
        saveTestEmailContentFunction(invite_email_subject,invite_email_body,"INVITE_EMAIL");
    });

    $(document).on('click','#congrats_single_save',function(){
        var congrats_email_subject = $('#congrats_email_subject').val();
        var congrats_email_body = CKEDITOR.instances.congrats_email_body.getData();
        saveTestEmailContentFunction(congrats_email_subject,congrats_email_body,"INVITE_EMAIL");
    });  

    $(document).on('click','#attempt_single_save',function(){
        var attempt_allow_email_subject = $('#attempt_allow_email_subject').val();
        var attempt_allow_email_body = CKEDITOR.instances.attempt_allow_email_body.getData();
        saveTestEmailContentFunction(attempt_allow_email_subject,attempt_allow_email_body,"INVITE_EMAIL");
    });  

    $(document).on('click','#retake_single_save',function(){
        //to save invite email content
        var retake_email_subject = $('#retake_email_subject').val();
        var retake_email_body = CKEDITOR.instances.retake_email_body.getData();
        saveTestEmailContentFunction(retake_email_subject,retake_email_body,"INVITE_EMAIL");
    });

    $(document).on('click','#allow_single_save',function(){
        var allow_email_subject = $('#allow_email_subject').val();
        var allow_email_body = CKEDITOR.instances.allow_email_body.getData();
        saveTestEmailContentFunction(allow_email_subject,allow_email_body,"INVITE_EMAIL");
    });  

    $(document).on('click','#result_single_save',function(){
        var result_email_subject = $('#result_email_subject').val();
        var result_email_body = CKEDITOR.instances.result_email_body.getData();
        saveTestEmailContentFunction(result_email_subject,result_email_body,"INVITE_EMAIL");
    });          

    $(document).on('show.bs.modal','#email-preview',function(e){
        // e.preventDefault();
        var settingtype = $(e.relatedTarget).attr('data-settingtype');
        var preview_subject = "";
        var preview_body = "";

        if(settingtype == "invite_email"){

            preview_subject = $('#invite_email_subject').val();
            preview_body = CKEDITOR.instances.invite_email_body.getData();
            $('#preview_subject').html(preview_subject);
            $('#preview_body').html(preview_body);

        }else if(settingtype == "congrats_email"){

            preview_subject = $('#congrats_email_subject').val();
            preview_body = CKEDITOR.instances.congrats_email_body.getData();
            $('#preview_subject').html(preview_subject);
            $('#preview_body').html(preview_body);  

        }else if(settingtype == "attempt_allow_email"){

            preview_subject = $('#attempt_allow_email_subject').val();
            preview_body = CKEDITOR.instances.attempt_allow_email_body.getData();
            $('#preview_subject').html(preview_subject);
            $('#preview_body').html(preview_body);    

        }else if(settingtype == "retake_email"){

            preview_subject = $('#retake_email_subject').val();
            preview_body = CKEDITOR.instances.retake_email_body.getData();
            $('#preview_subject').html(preview_subject);
            $('#preview_body').html(preview_body);  

        }else if(settingtype == "allow_ans_email"){

            preview_subject = $('#allow_email_subject').val();
            preview_body = CKEDITOR.instances.allow_email_body.getData();
            $('#preview_subject').html(preview_subject);
            $('#preview_body').html(preview_body);  

        }else if(settingtype == "result_email"){

            preview_subject = $('#result_email_subject').val();
            preview_body = CKEDITOR.instances.result_email_body.getData();
            $('#preview_subject').html(preview_subject);
            $('#preview_body').html(preview_body);   

        }


    });

    $(document).on('click','#send_sample_email',function(){
        var preview_subject = $('#preview_subject').html();
        var preview_body = $('#preview_body').html();
        var preview_footer = $('#preview_footer').html();
        var sample_email_id  = $('#sample_email_id').val();
        $.ajax({
            'type':'POST',
            'url':"sendSampleEmail",
            'dataType':'HTML',
            'data':{'preview_subject':preview_subject,'preview_body':preview_body,'preview_footer':preview_footer,'sample_email_id':sample_email_id},
            success:function(response){
                console.log(response);
                $('#tab3_success_content').html("Email sent successfully");
                $('#tab3_show_success_msg').show();
                $('#tab3_show_success_msg').delay(4000).fadeOut();
            }

        })
    });

    getTestSettings();
    //settings tab ends here

    var learner_table=$('.test_learner').DataTable({
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"test_learner_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.assign_learner_sorting_column_name=$('#assign_learner_sorting_column_name').val();
                d.dropdown_learner_department_id = $(".learner_department option:selected").val();
                d.dropdown_learner_function_id = $(".learner_function option:selected").val();
                d.dropdown_learner_level_id = $(".learner_level option:selected").val();
                d.dropdown_learner_status_id = $(".learner_status option:selected").val();
                d.searchtxt=$("#searchlearnertxt").val();
            },
        },
        columns: [
                {data:'CheckAll',name:"chk_box",
                title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="assign_learner" type="checkbox"><span class="custom-control-indicator"></span></label>'},
                { name: 'Employee Code',data:'EmployeeCode',title:'Employee Code',class:"learner_sorting_class"},
                { name: 'User Name',data:'UserName' ,title: 'User Name',class:"learner_sorting_class"},
                { name: 'Email Id',data:'EmailId', title: 'Email Id',class:"learner_sorting_class" },
                { name: 'Department',data:'Department',title: 'Department',class:"learner_sorting_class" },
                { name: 'Contact Detail',data:'ContactDetail',title: 'Contact Detail',class:"learner_sorting_class"},
                { name: 'Manager Name',data:'ManagerName',title: 'Manager Name',class:"learner_sorting_class"},
                { name: 'Course Status',data:'CourseStatus',title: 'Course Status',class:"learner_sorting_class" },
                { name: 'Level',data:'Level',title: 'Level',class:"learner_sorting_class" },
                { name: 'Enroll Courses',data:'EnrollCourses', title: 'Enroll Courses',class:"learner_sorting_class"}
            ],
        "columnDefs": [ {
            "targets": [0], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $(document).on('click','.learner_sorting_class',function(){
        var column_name = $(this).text();
        $('#assign_learner_sorting_column_name').val(column_name); 
        $('.test_learner').DataTable();
    });

    var invite_learner_table=$('.invite_test_learner').DataTable({
        //fetch record from server side
        processing: true,
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        "ajax":{
            url :"test_invite_learner_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.invite_learner_sorting_column_name=$('#invite_learner_sorting_column_name').val();
                d.dropdown_learner_department_id = $(".learner_department option:selected").val();
                d.dropdown_learner_function_id = $(".learner_function option:selected").val();
                d.dropdown_learner_level_id = $(".learner_level option:selected").val();
                d.dropdown_learner_status_id = $(".learner_status option:selected").val();
                d.searchtxt=$("#searchlearnertxt").val();
            },
        },
        columns: [
                {data:'CheckAll',name:"chk_box",
                title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all_tab" tab_name="invite_learner" type="checkbox"><span class="custom-control-indicator"></span></label>'},
                { name: 'Employee Code',data:'EmployeeCode',title:'Employee Code',class:"invite_sorting_class"},
                { name: 'User Name',data:'UserName' ,title: 'User Name',class:"invite_sorting_class"},
                { name: 'Email Id',data:'EmailId', title: 'Email Id',class:"invite_sorting_class" },
                { name: 'Department',data:'Department',title: 'Department',class:"invite_sorting_class" },
                { name: 'Contact Detail',data:'ContactDetail',title: 'Contact Detail',class:"invite_sorting_class"},
                { name: 'Manager Name',data:'ManagerName',title: 'Manager Name',class:"invite_sorting_class"},
                { name: 'Course Status',data:'CourseStatus',title: 'Course Status',class:"invite_sorting_class" },
                { name: 'Level',data:'Level',title: 'Level' },
                { name: 'Enroll Courses',data:'EnrollCourses', title: 'Enroll Courses',class:"invite_sorting_class" }
            ],
        "columnDefs": [ {
            "targets": [0], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $(document).on('click','.invite_sorting_class',function(){
        var column_name = $(this).text();
        $('#invite_sorting_column_name').val(column_name); 
        $('.invite_test_learner').DataTable();
    });

    $('#test_learner_searchButton').click(function(){
        learner_table.ajax.reload();
    });

    $('.assign_learner_to_test').click(function(){
        var i=0;
        var arr = [];
        $('.all_user_checkbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        $.ajax({
            type: "POST",
            url: "assign_learner_to_test",
            data: {
                "chk_check_value":arr
            }, 
            success: function(data)
            {
                alert("success");
                learner_table.ajax.reload();
                invite_learner_table.ajax.reload();
            }
        });
    })

    $('.invite_learner_to_test').click(function(){
        var i=0;
        var arr = [];
        $('.all_user_checkbox:checked').each(function () {
               arr[i++] = $(this).val();
               //alert($(this).val());
        });
        // alert(arr)
        $.ajax({
            type: "POST",
            url: "invite_learner_to_test",
            data: {
                "chk_check_value":arr
            }, 
            success: function(data)
            {
                alert("Invited");
                invite_learner_table.ajax.reload();
                //alert("success");
            }
        });
    })

    //for filter show/hide
    $('.refine_mbutton').click(function(){
        $('.Cus_card-body').slideToggle("slow");
        $('.refine_mbutton').toggleClass("active");
    });    

});


function getTestData(){

    $.ajax({
        'type':'POST',
        'url':"getTestData",
        success:function(response){
            console.log(response);
            if(response != "false"){
                response = JSON.parse(response);
                var test_details = response['test_details'];

                $('#test_title_id').val(test_details[0]['tte_test_name']);
                $('#test_func_id').val(test_details[0]['tte_test_function']);
                $('#start_date_id').val(test_details[0]['tte_start_date']);
                $('#end_date_id').val(test_details[0]['tte_end_date']);
                $('#test_duration_id').val(test_details[0]['tte_test_duration']);
                $('#test_description').val(test_details[0]['tte_description']);
                if(CKEDITOR.instances['test_description']){
                  CKEDITOR.instances['test_description'].destroy(true);
                }
                CKEDITOR.replace( 'test_description' );  

                var get_parameter_list = response['get_parameter_list'];
                if(get_parameter_list.length == '1'){
                    $('#paramtext_0').val(get_parameter_list[0]['tp_parameter']);
                    $('#weight_0').val(get_parameter_list[0]['tp_weightage']);
                }else if(get_parameter_list.length > 1){
                    $('#paramtext_0').val(get_parameter_list[0]['tp_parameter']);
                    $('#weight_0').val(get_parameter_list[0]['tp_weightage']);
                    for(var i=1;i<get_parameter_list.length;i++){

                            $('#param_div_count').val(get_parameter_list.length);
                            var html = "";
                            // var i = (parseInt(param_div_count) + 1);
                            html += '<div id="param_'+i+'" class="parameter-full m-b-sm clearfix">';
                            html += '<div class="col-xs-6 col-md-7 nopadding">';
                            html += '<input class="form-control parameter_text" value="'+get_parameter_list[i]['tp_parameter']+'" id="paramtext_'+i+'" type="text">';
                            html += '</div>';
                            html += '<div class="col-xs-5 col-md-4 nopadding">';
                            html += '<div class="input-group">';
                            html += '<span class="input-group-btn">';
                            html += '<button type="button" class="btn btn-gray btn-number" data-type="minus" data-field="quant['+i+']">';
                            html += '<span class="icon icon-minus-circle"></span>';
                            html += '</button>';
                            html += '</span>';
                            html += '<input type="text" name="quant['+i+']" id="weight_'+i+'" value="'+get_parameter_list[i]['tp_weightage']+'" style="width: 50px;" class="form-control text-center input-number" value="10" min="1" max="100">';
                            html += '<span class="input-group-btn">';
                            html += '<button type="button" class="btn btn-gray btn-number" data-type="plus" data-field="quant['+i+']">';
                            html += '<span class="icon icon-plus-circle"></span>';
                            html += '</button>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="col-xs-1 col-md-1 nopadding text-center" style="padding-left: 28px;">';
                            html += '<a href="Javascript:Void(0);" data-miid="'+i+'" class="ad-minus"><i class="icon icon-minus-circle"></i></a>';
                            html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix appenddiv"></div>';
                            // $('#param_div_count').val(i);
                            $('.appenddiv:last').append(html);
                    }
                }   
                $('.steps a[href="#tab-1"]').closest('li').removeClass('active');
                $('.steps a[href="#tab-8"]').closest('li').addClass('active');
                $('.steps a[href="#tab-2"]').attr('data-toggle','tab');
                $('.tab-menu a[href="#tab-2"]').attr('data-toggle','tab'); 
                $('.steps a[href="#tab-3"]').attr('data-toggle','tab');
                $('.tab-menu a[href="#tab-3"]').attr('data-toggle','tab'); 
                $('.steps a[href="#tab-4"]').attr('data-toggle','tab');
                $('.tab-menu a[href="#tab-4"]').attr('data-toggle','tab'); 
                $('.steps a[href="#tab-5"]').attr('data-toggle','tab');
                $('.tab-menu a[href="#tab-5"]').attr('data-toggle','tab');  
                $('.steps a[href="#tab-6"]').attr('data-toggle','tab');
                $('.tab-menu a[href="#tab-6"]').attr('data-toggle','tab');
                $('.steps a[href="#tab-7"]').attr('data-toggle','tab');
                $('.tab-menu a[href="#tab-7"]').attr('data-toggle','tab');
                $('.steps a[href="#tab-8"]').attr('data-toggle','tab');
                $('.tab-menu a[href="#tab-8"]').attr('data-toggle','tab');                                              
            }else{
                $('.steps a[href="#tab-2"]').removeAttr('data-toggle');
                $('.tab-menu a[href="#tab-2"]').removeAttr('data-toggle'); 
                $('.steps a[href="#tab-3"]').removeAttr('data-toggle');
                $('.tab-menu a[href="#tab-3"]').removeAttr('data-toggle'); 
                $('.steps a[href="#tab-4"]').removeAttr('data-toggle');
                $('.tab-menu a[href="#tab-4"]').removeAttr('data-toggle'); 
                $('.steps a[href="#tab-5"]').removeAttr('data-toggle');
                $('.tab-menu a[href="#tab-5"]').removeAttr('data-toggle');  
                $('.steps a[href="#tab-6"]').removeAttr('data-toggle');
                $('.tab-menu a[href="#tab-6"]').removeAttr('data-toggle');
                $('.steps a[href="#tab-7"]').removeAttr('data-toggle');
                $('.tab-menu a[href="#tab-7"]').removeAttr('data-toggle');
                $('.steps a[href="#tab-8"]').removeAttr('data-toggle');
                $('.tab-menu a[href="#tab-8"]').removeAttr('data-toggle');  
                $('.tab-menu a[href="#tab-1"]').closest('li').addClass('active');  
                $('.steps a[href="#tab-1"]').closest('li').addClass('active');                                                          
            }
        }
    });
}


function getTestSettings(){
    $.ajax({
        'type':'POST',
        'url':"getTestSettingsDetails",
        success:function(response){
            if(response != "false"){
                response = JSON.parse(response);
                for(var i=0;i<response.length;i++){
                   if(response[i]['tts_setting_name'] == "INVITE_EMAIL"){

                        $('#invite_param').val(response[i]['tts_parameter']);
                        $("input[name='invite_options'][value='" + response[i]['tts_days_hrs_attemts'] + "']").attr('checked', 'checked');
                        $('#invite_email_subject').val(response[i]['tts_email_subject']);
                        $('#invite_email_body').val(response[i]['tts_email_body']);      

                        if(CKEDITOR.instances['invite_email_body']){
                          CKEDITOR.instances['invite_email_body'].destroy(true);
                        }
                        CKEDITOR.replace( 'invite_email_body' );  

                   }else if(response[i]['tts_setting_name'] == "REMINDER_EMAIL"){

                        $('#reminder_param').val(response[i]['tts_parameter']);
                        $("input[name='reminder_options'][value='" + response[i]['tts_days_hrs_attemts'] + "']").attr('checked', 'checked');

                   }else if(response[i]['tts_setting_name'] == "CONGRATULATIONS_EMAIL"){

                        $('#congrats_email_subject').val(response[i]['tts_email_subject']);
                        $('#congrats_email_body').val(response[i]['tts_email_body']);      

                        if(CKEDITOR.instances['congrats_email_body']){
                          CKEDITOR.instances['congrats_email_body'].destroy(true);
                        }
                        CKEDITOR.replace( 'congrats_email_body' );   

                   }else if(response[i]['tts_setting_name'] == "ATTEMPT_ALLOWED_EMAIL"){

                        $('#attempts_param').val(response[i]['tts_parameter']);
                        $('#attempt_allow_email_subject').val(response[i]['tts_email_subject']);
                        $('#attempt_allow_email_body').val(response[i]['tts_email_body']);      

                        if(CKEDITOR.instances['attempt_allow_email_body']){
                          CKEDITOR.instances['attempt_allow_email_body'].destroy(true);
                        }
                        CKEDITOR.replace( 'attempt_allow_email_body' );                        

                   }else if(response[i]['tts_setting_name'] == "RETAKE_EMAIL"){

                        $('#retake_email_subject').val(response[i]['tts_email_subject']);
                        $('#retake_email_body').val(response[i]['tts_email_body']);      

                        if(CKEDITOR.instances['retake_email_body']){
                          CKEDITOR.instances['retake_email_body'].destroy(true);
                        }
                        CKEDITOR.replace( 'retake_email_body' );

                   }else if(response[i]['tts_setting_name'] == "ALLOW_ANS_EMAIL"){

                        $('#allow_email_subject').val(response[i]['tts_email_subject']);
                        $('#allow_email_body').val(response[i]['tts_email_body']);      

                        if(CKEDITOR.instances['allow_email_body']){
                          CKEDITOR.instances['allow_email_body'].destroy(true);
                        }
                        CKEDITOR.replace( 'allow_email_body' );                    
                    
                   }else if(response[i]['tts_setting_name'] == "RESULT_EMAIL"){

                        $('#result_email_subject').val(response[i]['tts_email_subject']);
                        $('#result_email_body').val(response[i]['tts_email_body']);      

                        if(CKEDITOR.instances['result_email_body']){
                          CKEDITOR.instances['result_email_body'].destroy(true);
                        }
                        CKEDITOR.replace( 'result_email_body' );                    
                    
                   }
                }
                return false;              
            }
        }
    });    
}

function saveTestEmailContentFunction($email_subject,$email_body,$setting_name){
        $.ajax({
            'type':'POST',
            'url':"saveTestEmailContent",
            'dataType':'HTML',
            'data':{'data':$email_body,'setting_name':$setting_name,'email_subject':$email_subject},
            success:function(response){
                console.log(response);
                $('#tab3_success_content').html("Email content saved successfully");
                $('#tab3_show_success_msg').show();
                $('#tab3_show_success_msg').delay(4000).fadeOut();
            }

        });
}

function appendDynamicFields($field_name,$email_type){  

       if($email_type == "invite_email"){
            var get_body = CKEDITOR.instances.invite_email_body.getData();

            get_body = get_body + '['+$field_name+']';  
            $('#invite_email_body').val(get_body);      

            if(CKEDITOR.instances['invite_email_body']){
              CKEDITOR.instances['invite_email_body'].destroy(true);
            }
            CKEDITOR.replace( 'invite_email_body' );  

       }else if($email_type == "congrats_email"){
            var get_body = CKEDITOR.instances.congrats_email_body.getData();

            get_body = get_body + '['+$field_name+']'; 
            $('#congrats_email_body').val(get_body);      

            if(CKEDITOR.instances['congrats_email_body']){
              CKEDITOR.instances['congrats_email_body'].destroy(true);
            }
            CKEDITOR.replace( 'congrats_email_body' );   

       }else if($email_type== "attempt_allow_email"){

            var get_body = CKEDITOR.instances.attempt_allow_email_body.getData();

            get_body = get_body + '['+$field_name+']';
            $('#attempt_allow_email_body').val(get_body);      

            if(CKEDITOR.instances['attempt_allow_email_body']){
              CKEDITOR.instances['attempt_allow_email_body'].destroy(true);
            }
            CKEDITOR.replace( 'attempt_allow_email_body' );                        

       }else if($email_type == "retake_email"){

            var get_body = CKEDITOR.instances.retake_email_body.getData();

            get_body = get_body + '['+$field_name+']';
            $('#retake_email_body').val(get_body);      

            if(CKEDITOR.instances['retake_email_body']){
              CKEDITOR.instances['retake_email_body'].destroy(true);
            }
            CKEDITOR.replace( 'retake_email_body' );

       }else if($email_type == "allow_ans_email"){

            var get_body = CKEDITOR.instances.allow_email_body.getData();

            get_body = get_body + '['+$field_name+']';
            $('#allow_email_body').val(get_body);      

            if(CKEDITOR.instances['allow_email_body']){
              CKEDITOR.instances['allow_email_body'].destroy(true);
            }
            CKEDITOR.replace( 'allow_email_body' );                    
        
       }else if($email_type == "result_email"){

            var get_body = CKEDITOR.instances.result_email_body.getData();

            get_body = get_body + '['+$field_name+']';
            $('#result_email_body').val(get_body);      

            if(CKEDITOR.instances['result_email_body']){
              CKEDITOR.instances['result_email_body'].destroy(true);
            }
            CKEDITOR.replace( 'result_email_body' );                    
        
       }      
}

// function copyTest($testid){
//     var testid = $testid;

//     swal({   
//         title: "Are you sure?",   
//         text: "You want to copy this test details?",   
//         type: "warning",   
//         showCancelButton: true,   
//         confirmButtonColor: "#DD6B55",   
//         confirmButtonText: "Yes, copy it!",   
//         cancelButtonText: "No, cancel it!",   
//         closeOnConfirm: false,   
//        closeOnCancel: false 
//     },function(isConfirm) {   
//         if (isConfirm) {
//             $.ajax({
//                 'type':'POST',
//                 'url':"copyTestFunction",
//                 'data': {"data":testid},
//                 success:function(response){
//                     console.log(response);
//                     window.location = "addtest";
//                     return false;
//                 }
//             }); 
//         } else {     
//           swal("Cancelled", "Your imaginary file is safe :)", "error");   
//         } 
//     });
// }