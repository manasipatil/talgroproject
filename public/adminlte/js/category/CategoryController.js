$( document ).ready(function() {
    $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
    });

    //code for dynamic column order
    var column_val=$("#category_hidden_column_array").val();
    //console.log(column_val);
    var arrval=column_val.split(',');
    var arrfinal=[];
    for(var i=0;i<arrval.length; i++){
        var col_name=arrval[i].split('-');
        if(col_name[0]=="CheckAll")
            arrfinal[i] = {data:col_name[0],name:"chk_box",
        title:'<label class="custom-control custom-control-primary custom-checkbox mobile-col"><input class="custom-control-input select_all" type="checkbox"><span class="custom-control-indicator"></span></label>',
        class:col_name[0]};
        else if(col_name[0]=="CategoryCode")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Category Code",class:"sorting_class"};
        else if(col_name[0]=="CategoryTitle")
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:"Category Title",class:"sorting_class"};
        else
            arrfinal[i] = {data:col_name[0],name:col_name[0],title:col_name[0],class:"sorting_class"};
    }
    //end of code
    //console.log(arrfinal);
  	
    var col="";
    var category_table=$('.example').DataTable( {
        //fetch record from server side
        processing: true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        } ,  
        serverSide: true,
        searching: false,
        //column reorder except first and last
        colReorder: true,
        colReorder: {
            fixedColumnsLeft: 1,
            fixedColumnsRight: 1,
        },
        //save state of column reorder n visibility
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        dom: "<'table-responsive'tr><'row'<'col-sm-6'><'col-sm-6'p>>",
        
        "ajax":{
            url :"category_ajax_listing", // json datasource
            type: "POST",
            datatype:"JSON",
            async: false,
            "data": function ( d ) {
                d.sorting_column_name=$('#sorting_column_name').val();
                d.dropdown_tccm_category_code = $("#dropdown_tccm_category_code option:selected").val();
                d.searchtxt=$("#searchtxt").val();
            },
        },
        columns: arrfinal,
        
        "columnDefs": [ {
            "targets": [0,3], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });
    
    
    //code for show/hide columns
    var col_array = $('#category_hidden_column_array').val();
    var col_array =col_array.split(",");
    var arrcol=[];
    var new_column;
    var col_class_name='';
    for(var i=0;i<col_array.length; i++){
        new_column=col_array[i].split("-");
        arrcol[new_column[0]] = new_column[1];
        var column = "category_table." + new_column[0]; 
        col_class_name='.'+new_column[0];
        if(new_column[1]==1){
            category_table.columns( col_class_name ).visible( true );
        }else{
            category_table.columns( col_class_name ).visible( false );
        }
    }
    //end of code
    //search filter
    $('#category_searchButton').click(function () {
        category_table.ajax.reload();
    });

    $(document).on('click','.sorting_class',function(){
        var column_name = $(this).text();
        $('#sorting_column_name').val(column_name); 
        $('.example').DataTable();
    });
    //showing edit form
    $(document).on('click', '.edit_button', function(){
        var edit_id=$(this).attr("edit_id");
        $.ajax({
            type: "POST",
            url: "show_category_edit_form",
            datatype:"JSON",
            data: {edit_id:edit_id}, 
            success: function(data)
            {
                var result = JSON.parse(data);
                $('#hidden_tccm_id').val(result[0]['tccm_id']);
                $('#tccm_category_code').val(result[0]['tccm_category_code']);
                $('#tccm_category').val(result[0]['tccm_category']);
                $('.dynamic_add_edit_lable').text("Edit");
                $('#err_tccm_category_code').html("");
                $('#err_tccm_category').html("");
                $('#err_code_exist').html("");
            }
        });
        
       $('#category_modal').modal('show');
    });

    
    $('#Frmcategory').validate({
        rules: {
          tccm_category_code: {
               required: true,
               minlength: 3
           },
          tccm_category :{
               required: true,
               minlength: 5            
          }
        },
        messages: {
          tccm_category_code: {
               required: "Code is required",
               minlength: "Code must contain at least {0} characters"
           },
          tccm_category: {
               required: "Category is required",
               minlength: "Category must contain at least {0} characters"
           }
        }, 
        errorClass: "help-inline text-danger",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
          $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
          $(element).parents('.form-group').addClass('has-success');
        },
        submitHandler: function (form) {
            var tccm_category=$('#tccm_category').val();
            var tccm_category_code=$('#tccm_category_code').val();
            if($('#hidden_tccm_id').val()!=""){
                var tccm_id=$('#hidden_tccm_id').val();
               
                $.ajax({
                    type: "POST",
                    url: "update_category_master",
                    data: {
                        tccm_id:tccm_id,
                        tccm_category:tccm_category,
                        tccm_category_code:tccm_category_code
                    }, 
                    success: function(data)
                    {
                        $("#err_tccm_category_code").html("");
                        $("#err_tccm_category").html("");
                        $("#err_code_exist").html("");
                        var result = JSON.parse(data);
                        if(result['status']=== 'error'){
                            $("#err_tccm_category_code").html(result['errors']['tccm_category_code']);
                            $("#err_tccm_category").html(result['errors']['tccm_category']);
                            $("#err_code_exist").html(result['errors']['tccm_category_code_exist']);
                            return false;
                        }
                        if(result['status']==="success"){
                            $('.dynamic_add_edit_lable').text("Add");
                            
                            document.getElementById("Frmcategory").reset();
                            category_table.ajax.reload();
                            $('#add_edit_sucess_msg').removeClass("hide");
                            setTimeout(function(){
                                $('#add_edit_sucess_msg').addClass("hide");
                                $('#category_modal').modal('hide');
                            },2000);

                        }
                    }
                });
                //return false;
            }else{
                //save record
                $.ajax({
                    type: "POST",
                    url: "add_category_form",
                    datatype:"JSON",
                    data: {tccm_category_code:tccm_category_code,
                            tccm_category:tccm_category,
                    }, 
                    success: function(data)
                    {
                        //alert(data);
                        $("#err_tccm_category_code").html("");
                        $("#err_tccm_category").html("");
                        $("#err_code_exist").html("");
                        var result = JSON.parse(data);
                        if(result['status']=== 'error'){
                            $("#err_tccm_category_code").html(result['errors']['tccm_category_code']);
                            $("#err_tccm_category").html(result['errors']['tccm_category']);
                            $("#err_code_exist").html(result['errors']['tccm_category_code_exist']);
                            return false;
                        }
                        if(result['status']==="success"){
                            $('#add_edit_sucess_msg').removeClass("hide");
                            
                            document.getElementById("Frmcategory").reset();
                            category_table.ajax.reload();

                            setTimeout(function(){
                                $('#add_edit_sucess_msg').addClass("hide");
                                $('#category_modal').modal('hide');
                                },2000);
                        }
                    }
                });
                    //return false;
                }//end of else condition
            }
    });

    //for delete single designation
    $(document).on('click', '.submit_form', function(){
        var delete_id=$(this).attr("delete_id");
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this Code!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                cancelButtonText: "No, cancel it!",   
                closeOnConfirm: false,   
               closeOnCancel: false 
            },function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "delete_category_code",
                        data: {
                            delete_id:delete_id
                        }, 
                        success: function(data)
                        {
                             swal("Deleted!", "Your Category has been deleted.", "success"); 
                             category_table.ajax.reload();
                        }
                    });
                } else {     
                  swal("Cancelled", "Your Category is safe :)", "error");   
                } 
            });
    });

    //common action
    $('.click_common_action').click(function(){
        var i=0;
        var arr = [];
        $('.myCheckbox:checked').each(function () {
               arr[i++] = $(this).val();
        });
        var action=$(this).attr("to_do");
        if(action=="delete")
            var text="You will not be able to recover this Category!";
        else if(action=="enable")
            var text="You want to enable all Category";
        else if(action=="disable")
            var text="You want to disable all Category";
       
        swal({   
                title: "Are you sure?",   
                text: "" + text + "",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, do it!",   
                cancelButtonText: "No, cancel plx!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm) {   
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: "category_common_action",
                        data: {
                            "action":action,
                            "chk_check_value":arr
                        }, 
                        success: function(data)
                        {
                            if(action=="delete")
                                swal("Deleted!", "Your Category has been Deleted.", "success");
                            else if(action=="enable")
                                swal("Enable","Your Category has been Enable ","success");
                            else if(action=="disable")
                                swal("Disable","Your Category has been disable","success"); 
                            category_table.ajax.reload();
                            $('.select_all').prop('checked',false);
                            $('.example tr').removeClass("selected");
                        }
                    });

                   
                } else {     
                  swal("Cancelled", "Your Codes are safe :)", "error");   
                }
            }); 
    });
    
    //add or edit competency model box on close event
    $('#category_modal').on('hidden.bs.modal', function (e) {
        $('.dynamic_add_edit_lable').text("Add");
        $('#err_tccm_category_code').html("");
        $('#err_tccm_category').html("");
        $('#err_code_exist').html("");
        $('#hidden_tccm_id').val("");
        document.getElementById("Frmcategory").reset();
        $("#Frmcategory").validate().resetForm();
        $('#category_modal').modal('hide');
    });
    //bulk upload competency model box on close event
    $('#upload_category_modal').on('hidden.bs.modal', function (e) {
        $('#all_upload_error_msg').addClass("hide");
        $('#upload_success_msg').addClass("hide");
        if(!$('#blank_error_msg').hasClass("hide")){
            $('#blank_error_msg').addClass("hide");
        }
        document.getElementById("Frmcategorycsv").reset();
        $('#upload_category_modal').modal('hide');
    });

    //cancel add/edit competency model box
    $('.cancel_edit_form').click(function(){
        $('#err_tccm_category_code').html("");
        $('#err_tccm_category').html("");
        $('#err_code_exist').html("");
        $('#hidden_tccm_id').val("");
        document.getElementById("Frmcategory").reset();
        $("#Frmcategory").validate().resetForm();
        $('#category_modal').modal('hide');
    });

    //bulk upload excel file
    $('.upload_category_csv').click(function(){
        if(!$("#upload_success_msg").hasClass("hide"))
            $('#upload_success_msg').addClass("hide");
        if(!$("#all_upload_error_msg").hasClass("hide"))
            $('#all_upload_error_msg').addClass("hide");
        if(!$("#blank_error_msg").hasClass("hide"))
            $('#blank_error_msg').addClass("hide");
        $('.error_msg').text("");
        $('.myprogress').css('width', '0');
        $('.msg').text('');

        //validation for file format
        var file_data = $('#upload_category').prop('files')[0];
        var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
        if(filename=="" || filename=="undefined"){
            $('#blank_error_msg').removeClass("hide");
            $('#blank_error_msg').html("Please Upload Excel file");
            return false;
        }
        var ext = $("#upload_category").val().split('.').pop();
        var fileExtension = ['XLS', 'XLSX', 'xlsx', 'xlx'];
        
        if ($.inArray(ext, fileExtension) == -1) {
            $('#blank_error_msg').removeClass("hide");
            $('#blank_error_msg').html("Please upload excel file only.formats are allowed : "+fileExtension.join(', '));
            return false;
        }
        var form_data = new FormData();
        form_data.append('file', file_data);
        $('.msg').text('Uploading in progress...');

        $.ajax({
            type: "POST",
            url: "upload_category_csv",
            dataType    : 'text',           // what to expect back from the PHP script, if anything
            cache       : false,
            contentType : false,
            processData : false,
            data        : form_data,

            // this part is progress bar
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $('.myprogress').text(percentComplete + '%');
                        $('.myprogress').css('width', percentComplete + '%');
                    }
                }, false);
                return xhr;
            },
            success: function(data){
                //console.log(data);
                $('.myprogress').css('width', '0%');
                $('.myprogress').text('0%');
                $('.msg').text('');
                document.getElementById("Frmcategorycsv").reset();
                var result = JSON.parse(data);
                var newHTML = [];
                if(result['status']=='all_duplicate'){
                    var substr=result['errors'];
                    for (i = 0; i < substr.length; ++i) {
                        newHTML.push(substr[i]+',');

                    }
                    var lastChar = newHTML.join("").slice(-1);
                    if (lastChar == ',') {
                      var strVal = newHTML.join("").slice(0, -1);
                    }
                    var array = strVal.split(',');
                    $.each(array, function(index, value) {
                      $("#failed_record_list").append("<li>" + value + "</li>");
                    });
                    $('#total_record').text(result['total_count_record']);
                    $('#failed_record').text(result['failed_record']);
                    $("#all_upload_error_msg").removeClass("hide");
               }else if (result['status']=='success_n_duplicate') {
                    var substr=result['errors'];
                    for (i = 0; i < substr.length; ++i) {
                        newHTML.push(substr[i]+',');
                    }
                    var lastChar = newHTML.join("").slice(-1);
                    if (lastChar == ',') {
                      var strVal = newHTML.join("").slice(0, -1);
                    }
                    var array = strVal.split(',');
                    $.each(array, function(index, value) {
                      $("#failed_record_list").append("<li>" + value + "</li>");
                    });
                    $('#total_record').text(result['total_count_record']);
                    $('#failed_record').text(result['failed_record']);
                    $("#all_upload_error_msg").removeClass("hide");
                    category_table.ajax.reload();
               }else if(result['status']=='no_record_error'){
                    $('#blank_error_msg').removeClass("hide");
                    $('#blank_error_msg').text("No record found in excel");
               }else if(result['status']=="success_all_record"){
                    $('#upload_sucess_msg').removeClass("hide");
                    category_table.ajax.reload();
                    setTimeout(function(){
                        $('#upload_sucess_msg').addClass("hide");
                        $('#upload_category_modal').modal('hide');
                    },  2000);
               }else if(result['status']=='header_format_error'){
                    $('#blank_error_msg').removeClass("hide");
                    $('#blank_error_msg').text("Please upload proper format.Please see download sample format for referrence");
               }else if(result['status']=='format_error'){
                    $('#blank_error_msg').removeClass("hide");
                    $('#blank_error_msg').text("Please upload only Excel file");
               }
            }//success function end
        });
    });

    
});


