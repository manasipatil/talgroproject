<?php
Route::get('/', 'Auth\LoginController@showLoginForm')->name('auth.login');
Route::get('course/{slug}', ['uses' => 'CoursesController@show', 'as' => 'courses.show']);
Route::post('course/payment', ['uses' => 'CoursesController@payment', 'as' => 'courses.payment']);
Route::post('course/{course_id}/rating', ['uses' => 'CoursesController@rating', 'as' => 'courses.rating']);

Route::get('lesson/{course_id}/{slug}', ['uses' => 'LessonsController@show', 'as' => 'lessons.show']);
Route::post('lesson/{slug}/test', ['uses' => 'LessonsController@test', 'as' => 'lessons.test']);

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
$this->post('register', 'Auth\RegisterController@register')->name('auth.register');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::resource('home', 'Admin\DashboardController');
    // Route::get('/home', 'Admin\DashboardController@index');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');  
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::any('role_ajax_listing','Admin\RolesController@role_ajax_listing'); 
    Route::any('roles_common_action', 'Admin\RolesController@roles_common_action');   
    Route::any('delete_role_code', 'Admin\RolesController@delete_role_code');    
    Route::any('permissions_list', 'Admin\RolesController@getUserTypeWisePermissionsList');       
    //for exporting pdf file
    Route::any("role_list_pdf","Admin\RolesController@role_download_PDF");      
    //for exporting excel file for team list
    Route::any("role_list_excel","Admin\RolesController@role_download_excel");    
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);


    Route::any('getSelectedPermissions', 'Admin\RolesController@getSelectedPermissions');
    //show content library section
    Route::resource('contentlibrary', 'Admin\ContentLibraryController');
    //for edit content from content library
    Route::post('editArticle','Admin\ContentLibraryController@editArticle');
    //for delete content for content library
    Route::post('deleteArticle','Admin\ContentLibraryController@deleteArticle');
    //update content from content library
    Route::post('updateArticle','Admin\ContentLibraryController@updateArticle');
    //add content in content library
    Route::post('addArticle','Admin\ContentLibraryController@addArticle');
    //update content active/inactive
    Route::post('updateIsActiveContent','Admin\ContentLibraryController@updateIsActiveContent');
    //add content to favourite tab
    Route::post('updateIsFavContent','Admin\ContentLibraryController@updateIsFavContent');
    //get developement area list in dropdown
    Route::post('getDevelopementAreaCL','Admin\ContentLibraryController@getDevelopementArea');
    //get contents in list
    Route::post('getArticles','Admin\ContentLibraryController@getArticles');
    //get content data as per pagination dynamically
    Route::resource('pagination', 'Admin\ContentLibraryController@pagination');
    //on change of pagination pages get data
    Route::post('ajax/pagination','Admin\ContentLibraryController@pagination');
    //get next record in pagination
    Route::get('ajax/getNextArt','Admin\ContentLibraryController@getNextArt');
    //get crop image in content
    Route::post('image-crop', 'Admin\ContentLibraryController@imageCropPost');
    Route::post('image-crop-small', 'Admin\ContentLibraryController@imageCropPostSmall');
    Route::any('openAddArticle','Admin\ContentLibraryController@openAddArticle');
    Route::any('openAddAudVid','Admin\ContentLibraryController@openAddAudVid');
    Route::any('openAddDocument','Admin\ContentLibraryController@openAddDocument');
    Route::any('openAddWebinar','Admin\ContentLibraryController@openAddWebinar');   
    Route::post('set_session_content','Admin\ContentLibraryController@set_session_content');         
    //show team management page
    Route::resource('team', 'Admin\TeamController');  
    //add team 
    Route::any('addTeam','Admin\TeamController@addTeam'); 
    //open add team tab page
    Route::any('showAddTeam','Admin\TeamController@showAddTeam'); 
    //team listing
    Route::post('teamList','Admin\TeamController@viewTeam');    

    Route::any('learners_ajax_listing','Admin\TeamController@learners_ajax_listing');   

    Route::any('learners_ajax_listing_team','Admin\TeamController@learners_ajax_listing_team'); 

    Route::any('learners_ajax_team_listing','Admin\TeamController@learners_ajax_team_listing');    

    Route::any('learners_common_action', 'Admin\TeamController@learners_common_action');    
    //get department filter list in dropdown
    Route::post('getDepartmentList','Admin\TeamController@getDepartmentList');    
    //get getManagerList filter list in dropdown
    Route::post('getManagerList','Admin\TeamController@getManagerList');
    //get getLevelList filter list in dropdown
    Route::post('getLevelList','Admin\TeamController@getLevelList');
    //get getCategory filter list in dropdown
    Route::post('getCategory','Admin\TeamController@getCategory');
    //get getDevelopementArea filter list in dropdown
    Route::post('getDevelopementArea','Admin\TeamController@getDevelopementArea');    

    //get getCourseList filter list in dropdown
    Route::post('getEnrolledCoursesList','Admin\TeamController@getEnrolledCoursesList');    
    //get getCourseList for courses tab in teammanagement
    Route::post('getFullCourseList','Admin\TeamController@getFullCourseList');
    //get getCourseList for courses tab in teammanagement
    Route::post('getTeamCourseList','Admin\TeamController@getTeamCourseList');    
    //add courses to the team in team management
    Route::post('addCourseToTeam','Admin\TeamController@addCourseToTeam');
    //add courses to the team in team management
    Route::post('removeCourseFromTeam','Admin\TeamController@removeCourseFromTeam');
    //add learning path to the team in team management
    Route::post('addLearningPathToTeam','Admin\TeamController@addLearningPathToTeam');
    //add learning path to the team in team management
    Route::post('removeLearningPathFromTeam','Admin\TeamController@removeLearningPathFromTeam');    
    //get team details in add team tab
    Route::post('getTeamDetails','Admin\TeamController@getTeamDetails');
    //get list for learning path tab in teammanagement
    Route::post('getFullLearningPathList','Admin\TeamController@getFullLearningPathList');
    //get list for team learning path tab in teammanagement
    Route::post('getTeamLearingPathList','Admin\TeamController@getTeamLearingPathList');
    //set_session_team
    Route::post('set_session_team','Admin\TeamController@set_session_team');

     //for adding functions form
    Route::post("add_learners_form","Admin\TeamController@save_learners_data");
    //for showing edit form
    Route::post("show_learners_edit_form","Admin\TeamController@show_learners_edit_form");
    //for updating learners master
    Route::post("update_learners_master","Admin\TeamController@update_learners_master");
    //for uploading learners csv 
    Route::any("upload_learners_csv","Admin\TeamController@upload_learners_csv");
    //for deleting seniority code
    Route::post("delete_learners_code","Admin\TeamController@delete_learners_code");
    //for exporting excel file for learners
    Route::any("learners_excel","Admin\TeamController@learners_download_excel");
    //for exporting excel file for learners
    Route::any("learners_team_excel","Admin\TeamController@learners_team_download_excel");    
    //for exporting excel file for team list
    Route::any("team_list_excel","Admin\TeamController@team_list_download_excel");    
    //for exporting pdf file
    Route::any("learners_pdf","Admin\TeamController@learners_download_PDF"); 
    //for exporting pdf file
    Route::any("learners_team_pdf","Admin\TeamController@learners_team_download_PDF");       
      //for exporting pdf file of  team listing
    Route::any("team_list_pdf","Admin\TeamController@team_list_download_PDF");
    //for deleting single team
    Route::post("delete_team_code","Admin\TeamController@delete_team_code");    

    //for downloading sample file
    Route::any("download_sample_learners_excel","Admin\TeamController@download_sample_learners_excel");     

    //for sorting column and saving order in session
    Route::any('drag_n_drop'  , 'Admin\CommonController@save_session_info');
    
    Route::any('check_permissions','Admin\CommonController@checkIfPermissionExist');

    //for showing sidebar based on user role id
    Route::any('save_user_type_for_permission','Admin\CommonController@save_user_type_for_permission');

    // //for user 
    Route::any('user_manager_ajax_listing', 'Admin\UsersController@user_manager_ajax_listing');
    //for all user
    Route::any('all_user_ajax_listing','Admin\UsersController@user_all_ajax_listing');
    //for hr listing
    Route::any('hr_ajax_listing','Admin\UsersController@hr_ajax_listing');
    //for expert listing
    Route::any('expert_ajax_listing','Admin\UsersController@expert_ajax_listing');
    //for learner listing
    Route::any('learner_ajax_listing','Admin\UsersController@learner_ajax_listing');
    //for instructor listing
    Route::any('instructor_ajax_listing','Admin\UsersController@instructor_ajax_listing');
    //for deleting single user
    Route::post("delete_user","Admin\UsersController@delete_user");
    //for user common action
    Route::any('user_common_action', 'Admin\UsersController@user_common_action');
    //for exporting pdf for users
    Route::any("user_pdf/{user_role}","Admin\UsersController@user_download_PDF");
    //for exporting excel file for user
    Route::any("user_excel/{user_role}","Admin\UsersController@user_download_excel");

    //for dispalying competency
    Route::resource('competency','Admin\CompetencyController');
    Route::any('competency_ajax_listing', 'Admin\CompetencyController@competency_ajax_listing');
    //for common action  competency_common_action
    Route::any('competency_common_action', 'Admin\CompetencyController@competency_common_action');
    //for adding competency form
    Route::post("add_competency_form","Admin\CompetencyController@save_competency_data");
    //for showing edit form
    Route::post("show_competency_edit_form","Admin\CompetencyController@show_competency_edit_form");
    //for updating competency master
    Route::post("update_competency_master","Admin\CompetencyController@update_competency_master");
    //for uploading competency csv 
    Route::any("upload_competency_csv","Admin\CompetencyController@upload_competency_csv");
    //for deleting competency code
    Route::post("delete_competency_code","Admin\CompetencyController@delete_competency_code");
    //for exporting excel file for competency
    Route::any("competency_excel","Admin\CompetencyController@competency_download_excel");
    //for exporting pdf file
    Route::any("competency_pdf","Admin\CompetencyController@competency_download_PDF");
    //for downloading sample file
    Route::any("download_sample_competency_excel","Admin\CompetencyController@download_sample_competency_excel");
    

    //for designation//
    Route::resource('designation','Admin\DesignationController');
    //for ajax listing
    Route::any('designation_ajax_listing', 'Admin\DesignationController@designation_ajax_listing');
    //for common action  designation_common_action
    Route::any('designation_common_action', 'Admin\DesignationController@designation_common_action');
     //for adding competency form
    Route::post("add_designation_form","Admin\DesignationController@save_designation_data");
    //for showing edit form
    Route::post("show_designation_edit_form","Admin\DesignationController@show_designation_edit_form");
    //for updating competency master
    Route::post("update_designation_master","Admin\DesignationController@update_designation_master");
    //for uploading competency csv 
    Route::any("upload_designation_csv","Admin\DesignationController@upload_designation_csv");
    //for deleting competency code
    Route::post("delete_designation_code","Admin\DesignationController@delete_designation_code");
    //for exporting excel file for competency
    Route::any("designation_excel","Admin\DesignationController@designation_download_excel");
    //for exporting pdf file
    Route::any("designation_pdf","Admin\DesignationController@designation_download_PDF");
    //for downloading sample file
    Route::any("download_sample_designation_excel","Admin\DesignationController@download_sample_designation_excel");

    //test file
    Route::resource('test', 'Admin\TestController');
    //for ajax listing
    Route::any('test_ajax_listing', 'Admin\TestController@test_ajax_listing');
    //for common action  designation_common_action
    Route::any('test_common_action', 'Admin\TestController@test_common_action');
     //for adding competency form
    Route::post("test_designation_code","Admin\TestController@test_designation_code");
    //for exporting excel file for competency
    Route::any("test_excel","Admin\TestController@test_download_excel");
    //for exporting pdf file
    Route::any("test_pdf","Admin\TestController@test_download_PDF");
    //for downloading sample file
    Route::any("download_sample_test_excel","Admin\TestController@download_sample_test_excel");
    //create test page
    Route::any('createTest','Admin\TestController@createTest');    
    //add and edit test page
    Route::any('addtest','Admin\TestController@addTest'); 
    //add insert test function
    Route::any('insertTest','Admin\TestController@insertTest');    
    //get test details from this function
    Route::any('getTestData','Admin\TestController@getTestData');
    //save test settings
    Route::any('saveTestSettings','Admin\TestController@saveTestSettings');
    //display test settings
    Route::any('getTestSettingsDetails','Admin\TestController@getTestSettingsDetails');
    Route::any('saveTestEmailContent','Admin\TestController@saveTestEmailContent');
    //send sample email from preview
    Route::any('sendSampleEmail','Admin\TestController@sendSampleEmail');
    //for test result  ajax listing
    Route::any('test_result_ajax_listing', 'Admin\TestController@test_result_ajax_listing');
    //for common action  designation_common_action
    Route::any('test_result_common_action', 'Admin\TestController@test_result_common_action');
    //for exporting excel file for competency
    Route::any("test_result_excel","Admin\TestController@test_result_download_excel");
    //for exporting pdf file
    Route::any("test_result_pdf","Admin\TestController@test_result_download_PDF");   
    //function for copy test
    Route::any('copyTestFunction',"Admin\TestController@copyTestFunction"); 
    //listing of learner test
    Route::any('test_learner_ajax_listing','Admin\TestController@test_learner_ajax_listing');
    //assign learner
    Route::any('assign_learner_to_test','Admin\TestController@assign_learner_to_test');
    //invite learner
    Route::any('invite_learner_to_test','Admin\TestController@invite_learner_to_test');
    //listing of invite learner test
    Route::any('test_invite_learner_ajax_listing','Admin\TestController@test_invite_learner_ajax_listing');  
    //remove test id from session
    Route::any('clearTestSession','Admin\TestController@clearTestSession');
    Route::post("delete_test_code","Admin\TestController@delete_test_code");
    Route::any('editTestFunction','Admin\TestController@editTestFunction');


    //for department//
    Route::resource('department','Admin\DepartmentController');
    //for ajax listing
    Route::any('department_ajax_listing', 'Admin\DepartmentController@department_ajax_listing');
    //for common action  designation_common_action
    Route::any('department_common_action', 'Admin\DepartmentController@department_common_action');
     //for adding competency form
    Route::post("add_department_form","Admin\DepartmentController@save_department_data");
    //for showing edit form
    Route::post("show_department_edit_form","Admin\DepartmentController@show_department_edit_form");
    //for updating competency master
    Route::post("update_department_master","Admin\DepartmentController@update_department_master");
    //for uploading competency csv 
    Route::any("upload_department_csv","Admin\DepartmentController@upload_department_csv");
    //for deleting competency code
    Route::post("delete_department_code","Admin\DepartmentController@delete_department_code");
    //for exporting excel file for competency
    Route::any("department_excel","Admin\DepartmentController@department_download_excel");
    //for exporting pdf file
    Route::any("department_pdf","Admin\DepartmentController@department_download_PDF");
    //for downloading sample file
    Route::any("download_sample_department_excel","Admin\DepartmentController@download_sample_department_excel");

    //for development//
    Route::resource('development','Admin\DevelopmentController');
    //for ajax listing
    Route::any('development_ajax_listing', 'Admin\DevelopmentController@development_ajax_listing');
    //for common action  designation_common_action
    Route::any('development_common_action', 'Admin\DevelopmentController@development_common_action');
     //for adding competency form
    Route::post("add_development_form","Admin\DevelopmentController@save_development_data");
    //for showing edit form
    Route::post("show_development_edit_form","Admin\DevelopmentController@show_development_edit_form");
    //for updating competency master
    Route::post("update_development_master","Admin\DevelopmentController@update_development_master");
    //for uploading competency csv 
    Route::any("upload_development_csv","Admin\DevelopmentController@upload_development_csv");
    //for deleting competency code
    Route::post("delete_development_code","Admin\DevelopmentController@delete_development_code");
    //for exporting excel file for competency
    Route::any("development_excel","Admin\DevelopmentController@development_download_excel");
    //for exporting pdf file
    Route::any("development_pdf","Admin\DevelopmentController@development_download_PDF");
    //for downloading sample file
    Route::any("download_sample_development_excel","Admin\DevelopmentController@download_sample_development_excel");

    //for seniority
    Route::resource('seniority','Admin\SeniorityController');
    //for ajax listing
    Route::any('seniority_ajax_listing', 'Admin\SeniorityController@seniority_ajax_listing');
    //for common action  seniority_common_action
    Route::any('seniority_common_action', 'Admin\SeniorityController@seniority_common_action');
     //for adding seniority form
    Route::post("add_seniority_form","Admin\SeniorityController@save_seniority_data");
    //for showing edit form
    Route::post("show_seniority_edit_form","Admin\SeniorityController@show_seniority_edit_form");
    //for updating seniority master
    Route::post("update_seniority_master","Admin\SeniorityController@update_seniority_master");
    //for uploading seniority csv 
    Route::any("upload_seniority_csv","Admin\SeniorityController@upload_seniority_csv");
    //for deleting seniority code
    Route::post("delete_seniority_code","Admin\SeniorityController@delete_seniority_code");
    //for exporting excel file for seniority
    Route::any("seniority_excel","Admin\SeniorityController@seniority_download_excel");
    //for exporting pdf file
    Route::any("seniority_pdf","Admin\SeniorityController@seniority_download_PDF");
    //for downloading sample file
    Route::any("download_sample_seniority_excel","Admin\SeniorityController@download_sample_seniority_excel");

    //for Category
    Route::resource('course_category','Admin\CategoryController');
    //for ajax listing
    Route::any('category_ajax_listing', 'Admin\CategoryController@category_ajax_listing');
    //for common action  category_common_action
    Route::any('category_common_action', 'Admin\CategoryController@category_common_action');
     //for adding category form
    Route::post("add_category_form","Admin\CategoryController@save_category_data");
    //for showing edit form
    Route::post("show_category_edit_form","Admin\CategoryController@show_category_edit_form");
    //for updating category master
    Route::post("update_category_master","Admin\CategoryController@update_category_master");
    //for uploading category csv 
    Route::any("upload_category_csv","Admin\CategoryController@upload_category_csv");
    //for deleting category code
    Route::post("delete_category_code","Admin\CategoryController@delete_category_code");
    //for exporting excel file for category
    Route::any("category_excel","Admin\CategoryController@category_download_excel");
    //for exporting pdf file
    Route::any("category_pdf","Admin\CategoryController@category_download_PDF");
    //for downloading sample file
    Route::any("download_sample_category_excel","Admin\CategoryController@download_sample_category_excel");

    //for functions
    Route::resource('functions','Admin\FunctionsController');
    //for ajax listing
    Route::any('functions_ajax_listing', 'Admin\FunctionsController@functions_ajax_listing');
    //for common action  functions_common_action
    Route::any('functions_common_action', 'Admin\FunctionsController@functions_common_action');
     //for adding functions form
    Route::post("add_functions_form","Admin\FunctionsController@save_functions_data");
    //for showing edit form
    Route::post("show_functions_edit_form","Admin\FunctionsController@show_functions_edit_form");
    //for updating functions master
    Route::post("update_functions_master","Admin\FunctionsController@update_functions_master");
    //for uploading functions csv 
    Route::any("upload_functions_csv","Admin\FunctionsController@upload_functions_csv");
    //for deleting seniority code
    Route::post("delete_functions_code","Admin\FunctionsController@delete_functions_code");
    //for exporting excel file for functions
    Route::any("functions_excel","Admin\FunctionsController@functions_download_excel");
    //for exporting pdf file
    Route::any("functions_pdf","Admin\FunctionsController@functions_download_PDF");
    //for downloading sample file
    Route::any("download_sample_functions_excel","Admin\FunctionsController@download_sample_functions_excel");    



    //for showing add user form
    Route::any("add_user_form/{hidden_user_type}","Admin\UsersController@add_user_form"); 
    //save record of add user
    Route::any("add_user_form/add_user/add_user_ajax","Admin\UsersController@add_user_ajax");  
    //showing edit form of user
    Route::any("user_restore/edit_user_ajax","Admin\UsersController@edit_user_ajax");
    
    Route::any('user_restore/{user_id}', ['uses' => 'Admin\UsersController@show_user_edit_form', 'as' => 'users.restore']);
    Route::any('user_restore/edit_user/psyometric_test_ajax_listing','Admin\UsersController@psyometric_test_ajax_listing');

    Route::any('user_restore/edit_user/course_ajax_listing','Admin\UsersController@course_ajax_listing');

    Route::any("add_user_form/add_user/getstate_list","Admin\UsersController@getUsersStatesList");

    Route::any("user_restore/add_user/getstate_list","Admin\UsersController@getUsersStatesList");

    //for webinar

    Route::resource('webinar','Admin\WebinarController');
    //for ajax listing
    Route::any('webinar_ajax_listing', 'Admin\WebinarController@webinar_ajax_listing');
    //for calender
    Route::any("getMonthlyEventList","Admin\WebinarController@show_data_on_calender");
    //for common action  seniority_common_action
    Route::any('webinar_common_action', 'Admin\WebinarController@webinar_common_action');
    //for deleting seniority code
    Route::post("delete_webinar","Admin\WebinarController@delete_webinar");
    //for exporting excel file for seniority
    Route::any("webinar_excel","Admin\WebinarController@webinar_download_excel");
    //for exporting pdf file
    Route::any("webinar_pdf","Admin\WebinarController@webinar_download_PDF");
    //for showing calender view
    Route::any("webinar_calender","Admin\WebinarController@show_webinar_calender");

    //Route::any("get_user_type_permission","Admin\UsersController@get_user_type_permission");
    
});

Route::any('admin/user_restore/psyometric_test_ajax_listing','Admin\UsersController@psyometric_test_ajax_listing');

//notification on header
Route::post('app/Profile/notification','Profile\NotificationController@getData');

Route::post('readNotification','Profile\NotificationController@readNotification');
//notification ends here
//for profile functionality
Route::get('profiles','Profile\ProfileController@index');

Route::post('uploadimg','Profile\ProfileController@uploadProfilePicture');

Route::post('updateProfile','Profile\ProfileController@updateProfile');
//profile functionality ends here
Route::post('getStates','Profile\ProfileController@getStatesList');







