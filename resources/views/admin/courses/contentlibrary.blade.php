@extends('layouts.app')

@section('content')


<div class="title-bar clear">
			<div class="col-xs-12 col-sm-8 col-md-8">
            <h1 class="title-bar-title">
				<i class="icon icon-file-text"></i>
                <span class="d-ib">{{ Config('messages.contentlibrary.HEADER_NAME') }}</span>
            </h1>
            <p class="title-bar-description">
              <small>{{ Config('messages.contentlibrary.HEADER_DESCRIPTION') }}</small>
            </p>
			</div>
      <div class="col-xs-12 col-sm-5 col-md-4 ad_top">
      <button class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
      </div>			
			
</div>
		<div class="layout-content-body filter-bg nopadding">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="Cus_card collapsed ">
                        <div class="Cus_card-header">
                            <div class="Cus_card-actions">
                                <button type="button" class="Cus_card-actions Cus_card-toggler" title="Filter"></button>
                            </div>
                        </div>
                        <div class="Cus_card-body collapse m-b-md">
							<div class="col-xs-9 col-md-10 col-lg-11">
								<h5>Search by Keyword</h5>
                                <input class="form-control" id="library_keyword_filter" type="text">					
                            </div>
                            <div class="col-xs-3 col-md-2 col-lg-1">
                                <h5>Sort by</h5>
                                 <select class="form-control">
                                 <option value="A">A</option>
                                 <option value="B">B</option>
                                 </select>
                            </div>
                            
                            <div class="col-xs-12 col-md-5">
							  <div class="row">
							  <div class="col-xs-6 col-md-6 category-dropdown">
                              <h5>Category</h5>
                                 <select class="form-control" id="list_category_filter">
                                  <option value="">Select Category</option>
                                  @foreach($categoryList as $categoryFilter)
                                         <option value="{{$categoryFilter['tccm_id']}}">{{$categoryFilter['tccm_category']}}</option>
                                  @endforeach
                                 </select>
                                </div>
								
								<div class="col-xs-6 col-md-6">
                                <h5>Courses</h5>
                                 <select class="form-control" id="list_course_filter">
                                  <option value="">Select Course</option>
                                  @foreach($courseList as $courseFilter)
                                         <option value="{{$courseFilter['tc_id']}}">{{$courseFilter['tc_title']}}</option>
                                  @endforeach
                                 </select>
								</div>
								</div>
								
                            </div>
                            <div class="col-xs-12 col-md-7">
								<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
								<h5>Competency</h5>
                                <select id="list_competency_filter" class="form-control">
                                   <option value="">Select Competency</option>
                                   @foreach($competencyList as $competency)
                                    <option value="{{$competency['tcm_id']}}">{{$competency['tcm_competency_title']}}</option>
                            @endforeach
                                 </select>
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4">
                                <h5>Development Area</h5>
                                <select id="list_dev_area_filter" class="form-control">
                                     <option value="">Select Development Area</option>
                                     @foreach($developementAreaList as $developementArea)
                                     <option value="{{$developementArea['tda_id']}}">{{$developementArea['tda_development']}}</option>
                              @endforeach
                                 </select>
								</div>
								
								<div class="col-xs-12 col-sm-4 col-md-4">
                                <h5>Senority level</h5>
                                <select id="list_senority_level_filter" class="form-control">
                                          <option value="">Select Level</option>
                                         @foreach($levelList as $level)
                                         <option value="{{$level['tsl_id']}}">{{$level['tsl_seniority']}}</option>
                                  @endforeach
                                 </select>
								</div>
								</div>
								
                            </div>
							          <div class="col-xs-9 col-md-10 col-lg-11">
                        <h5></h5>
                        <button class="btn btn-primary" id="con_lib_filter_btn" type="submit">Search</button>
                        <button class="btn btn-default" id="con_lib_filter_close_btn" type="submit">Close</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <div class="layout-content-body">
            <div class="row gutter-xs">
                <div class="col-xs-12">
				<div class="demo-form-wrapper">
				<div class="panel con-lib">
                
					
                                  
@include('flash-message')
				<div class="tab-menu">
				 <ul class="nav nav-tabs m-t-lg">
				 <li class="active"><a href="#tab-1" id="all_tab_id" data-toggle="tab">All</a></li>
                  <li><a href="#tab-2" id="av_tab_id"  data-toggle="tab">Videos/Audio</a></li>
                  <li><a href="#tab-3" id="article_tab_id" data-toggle="tab">Articles</a></li>
				  <li><a href="#tab-4" id="document_tab_id" data-toggle="tab">Documents</a></li>
				  <li><a href="#tab-5" id="webinar_tab_id" data-toggle="tab">Webinar</a></li>
				  <li><a href="#tab-6" id="fav_tab_id" data-toggle="tab">Favourite</a></li>
				 </ul>
				 </div>
                  <div class="tab-content">
                  <div class="tab-pane fade active in" id="tab-1">
                    <button class="btn btn-primary m-b-sm" onClick="addarticle()" type="button">Add Article +</button>
					<button class="btn btn-primary m-b-sm" onClick="addav()" type="button">Add Video/Audio +</button>
					<button class="btn btn-primary m-b-sm" onClick="adddocument()" type="button">Add Document +</button>
					<button class="btn btn-primary m-b-sm" onClick="addwebinar()" type="button">Add Webinar +</button>
				
					<div class="row m-t-md">
					<div class="over-form" style="left: 30%;top: 30%;display: none;" id="show_msg">
			          <div class="alert alert-success upload_success_msg">
			              <i class="icon icon-check-circle-o"></i>
			              <strong>Success!</strong> <span id="your_msg"></span>
			          </div>
			        </div>	
			       <div class="alert alert-danger upload_error_msg" style="display: none;">
                    <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                    <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="all_div_error"></span>
        			</div>
					<!------Start Article------->
					<div id="displayallcontent"></div>
	
					</div>
					  
                  </div>
                  <div class="tab-pane fade" id="tab-2">
					           <button class="btn btn-primary m-b-sm" onClick="addav()" type="button">Add Video/Audio +</button>
                    <div class="row m-t-md">
                      <div class="over-form" style="left: 30%;top: 30%;display: none;" id="show_msg_av">
                      <div class="alert alert-success upload_success_msg">
                          <i class="icon icon-check-circle-o"></i>
                          <strong>Success!</strong> <span id="your_msg_av"></span>
                      </div>
                      </div>  
                      <div class="alert alert-danger upload_error_msg" style="display: none;">
                            <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                            <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="av_div_error"></span>
                      </div>
                      <div id="av_div">
                        
                      </div>
                   </div>
                  </div>
                  <div class="tab-pane fade" id="tab-3">
                  	<button class="btn btn-primary m-b-sm" onClick="addarticle()" type="button">Add Article +</button>
                  	<div class="row m-t-md">
                    	<div class="over-form" style="left: 30%;top: 30%;display: none;" id="show_msg_art">
      			          <div class="alert alert-success upload_success_msg">
      			              <i class="icon icon-check-circle-o"></i>
      			              <strong>Success!</strong> <span id="your_msg_art"></span>
      			          </div>
  			              </div>	
        			        <div class="alert alert-danger upload_error_msg" style="display: none;">
                            <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                            <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="art_div_error"></span>
                			</div>
                      <div id="article_div">
                      	
                      </div>
                	 </div>
                  </div>
				  <div class="tab-pane fade" id="tab-4">
                    <button class="btn btn-primary m-b-sm" onClick="adddocument()" type="button">Add Document +</button>
                    <div class="row m-t-md">
                      <div class="over-form" style="left: 30%;top: 30%;display: none;" id="show_msg_doc">
                      <div class="alert alert-success upload_success_msg">
                          <i class="icon icon-check-circle-o"></i>
                          <strong>Success!</strong> <span id="your_msg_doc"></span>
                      </div>
                      </div>  
                      <div class="alert alert-danger upload_error_msg" style="display: none;">
                            <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                            <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="doc_div_error"></span>
                      </div>
                      <div id="document_div">
                        
                      </div>
                   </div>                    
                  </div>
				  <div class="tab-pane fade" id="tab-5">
            <button class="btn btn-primary m-b-sm" onClick="addwebinar()" type="button">Add Webinar +</button>
                <div class="row m-t-md">
                      <div class="over-form" style="left: 30%;top: 30%;display: none;" id="show_msg_webinar">
                      <div class="alert alert-success upload_success_msg">
                          <i class="icon icon-check-circle-o"></i>
                          <strong>Success!</strong> <span id="your_msg_webinar"></span>
                      </div>
                      </div>  
                      <div class="alert alert-danger upload_error_msg" style="display: none;">
                            <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                            <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="webinar_div_error"></span>
                      </div>
                      <div id="webinar_div">
                        
                      </div>
                   </div> 
          </div>
				  <div class="tab-pane fade" id="tab-6">
                    <div class="row m-t-md">
                      <div class="over-form" style="left: 30%;top: 30%;display: none;" id="show_msg_fav">
                      <div class="alert alert-success upload_success_msg">
                          <i class="icon icon-check-circle-o"></i>
                          <strong>Success!</strong> <span id="your_msg_fav"></span>
                      </div>
                      </div>  
                      <div class="alert alert-danger upload_error_msg" style="display: none;">
                            <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                            <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="fav_div_error"></span>
                      </div>
                      <div id="favourite_div">
                        
                      </div>
                   </div>          
          </div>
					  
				  </div>
				</div>
                
                </div>
                
                </div>
            </div>
        </div>

        <!--Add Article Popup-->
<div id="addarticle" class="modal pop-modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
             <h1 class="title-bar-title">
				<i class="icon icon-newspaper-o"></i>
                <span class="d-ib">Upload Article to Content Library</span>
            </h1>
            <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </small>
            </p>
            <div class="over-form" id="show_loader" style="left: 40%;top: 75%;display: none;">
              <i class="fa fa-spinner fa-spin" style="font-size:50px;color: #ff9d4e;z-index: 9999;"></i>
        	</div>
          </div>
          <div class="alert alert-danger upload_error_msg" style="display: none;">
                    <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                    <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="add_art_error"></span>
        	</div>
          {{Form::open(array('id'=>'add_article_form','method'=>'post','enctype'=>'multipart/form-data'))}}
          <input type="hidden" value="{{ csrf_token() }}" name="_token">
          <div class="media text-center upload-sec" style="padding: 13px 20px;" id="add-article">
					    <div class="media-body p-t-sm">
						 <label>
                          <h4 class="media-heading"><span class="icon icon-camera color-o"></span> Upload cover image</h4>
                          <p class="media-description">
                          <small>Max file size: 5 MB. Accepted file type: .jpg,.png</small>
						  <input class="file-upload-input" type="file" accept="image/x-png,image/jpeg,image/jpg" id="art_upload_img" name="file">
						 <input type="hidden" class="image_src" name="image_src">
						 <input type="hidden" class="check_img_type" value="addimage">
						 <span id="get_crop_img_add" style="display: none;"></span>
                          </p>
						 </label>
              </div>
              <div id="crop_image_div" style="display: none;">
					            <div class="row">
					                <div class="text-center" style="margin: 0px auto;">
					                    <div id="upload_demo"></div>
					                </div>                
					            </div>
					    </div>
		      </div>
				               				@if ($errors->has('file'))
	            	<span class="help-block">
	                	<strong>{{ $errors->first('file') }}</strong>
	            	</span>
	        	@endif
      <div class="modal-body p-t-0">
			<div class="row">
				
				
				<div class="col-md-12 form-group">
					<h5>Title <span class="color-r">*</span></h5>
				 <!-- <input class="form-control" type="text"> -->
				 {!! Form::text('title', '', ['class' => 'form-control','id'=>'title', 'placeholder' => '', 'required' => '']) !!}
				</div>
					
				<div class="col-md-12 form-group">
				 <h5>Description</h5>
				 <!-- <textarea class="form-control" rows="3"></textarea> -->
				 {{Form::textarea('description','',array('class'=>'form-control ckeditor','id'=>'description','rows' => 3))}}
				</div>

				<div class="col-md-12 form-group">
				 <h5>Body<span class="color-r">*</span></h5>
				 <!-- <textarea class="form-control" rows="5"></textarea> -->
				 {{Form::textarea('body','',array('class'=>'form-control ckeditor','id'=>'body','rows' => 5,'required' => ''))}}
				</div>
					
				<div class="col-md-6 form-group">
				<h5>Competency <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.COMPETENCY_HELP') }}">?</span></h5>
                 <select name="competency" id="competency_id" class="form-control">
                 <option value="">Select Competency</option>
                 @foreach($competencyList as $competency)
                  <option value="{{$competency['tcm_id']}}">{{$competency['tcm_competency_title']}}</option>
				  @endforeach
                 </select>					
        </div>
				
				<div class="col-md-6 form-group">
				<h5>Development Area <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.DEVELOPEMENT_AREA_HELP') }}">?</span></h5>
                 <select name="development_area" id="development_area_id" class="form-control">
                 <option value="">Select Development Area</option>
                 @foreach($developementAreaList as $developementArea)
                 <option value="{{$developementArea['tda_id']}}">{{$developementArea['tda_development']}}</option>
				  @endforeach
                 </select>					
        </div>
					
				<div class="col-md-6 form-group">
				<h5>Level<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.LEVEL_HELP') }}">?</span></h5>
                 <select name="level" id="level" class="form-control">
                  <option value="">Select Level</option>
                 @foreach($levelList as $level)
                 <option value="{{$level['tsl_id']}}">{{$level['tsl_seniority']}}</option>
				  @endforeach
                 </select>					
        </div>
					
				<div class="col-md-6 form-group">
				<h5>Keywords<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.KEYWORDS_HELP') }}">?</span></h5>
                 <!-- <input class="form-control" type="text">				 -->
                 {!! Form::text('keywords', '', ['class' => 'form-control','id'=>'keywords','data-role'=>"tagsinput", 'placeholder' => '', 'required' => '']) !!}
                 <small>(Eg. abc,efg)</small>
        </div>
				<div class="col-md-12"></div>	
				<div class="col-md-6 form-group">
				<h5>Author<span class="color-r">*</span></h5>
                 <!-- <input class="form-control" type="text">				 -->
                 {!! Form::text('author', '', ['class' => 'form-control','id'=>'author', 'placeholder' => '', 'required' => '']) !!}
        </div>
				
				<div class="col-md-12">
				     <div class="m-t m-b-md">
                        <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="is_active" name="is_active" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Active</span>
                        </label>
					    <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="is_available" name="is_available" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Available to all Learners</span>
                        </label>
                       
              </div>
				</div>
				
				
					
				<div class="col-md-12 m-t-md">
					{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
				 <!-- <button type="button" class="btn btn-primary">Save</button> -->
                 <button type="button" id="close_add_modal" class="btn btn-default">Cancel</button>
				</div>
					
				</div>     
		  </div>
			{{Form::close()}}
        </div>
      </div>
    </div>	
	
<!--Add Documents Popup-->
<div id="adddocs" class="modal pop-modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
             <h1 class="title-bar-title">
				<i class="icon icon-file-text-o"></i>
                <span class="d-ib">Upload Document to Content Library</span>
            </h1>
            <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </small>
            </p>
          <div class="over-form" id="show_loader_doc" style="left: 40%;top: 75%;display: none;">
              <i class="fa fa-spinner fa-spin" style="font-size:50px;color: #ff9d4e;z-index: 9999;"></i>
          </div>  
          </div>
          <div class="alert alert-danger upload_error_msg" style="display: none;">
                    <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                    <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="update_doc_error"></span>
          </div>
          {{Form::open(array('id'=>'add_document_form','method'=>'post','enctype'=>'multipart/form-data'))}}
          <input type="hidden" value="{{ csrf_token() }}" name="_token">
          <div class="media text-center upload-sec" style="padding: 13px 20px;" id="add-document">
              <div class="media-body p-t-sm">
             <label>
                          <h4 class="media-heading"><span class="icon icon-camera color-o"></span> Upload cover image</h4>
                          <p class="media-description">
                          <small>Max file size: 5 MB. Accepted file type: .jpg,.png</small>
              <input class="file-upload-input" type="file" accept="image/x-png,image/jpeg,image/jpg" id="doc_upload_img" name="file">
             <input type="hidden" class="doc_image_src" name="doc_image_src">
             <input type="hidden" class="check_img_type" value="docaddimage">
             <span id="doc_get_crop_img_add" style="display: none;"></span>
                          </p>
             </label>
              </div>
              <div class="hrtigmw" style="margin-left: -15px; display: none;"></div>
              <div id="doc_crop_image_div" style="display: none;">
                      <div class="row">
                          <div class="text-center" style="margin: 0px auto;">
                              <div id="doc_upload_demo"></div>
                          </div>                
                      </div>
              </div>
          </div>
          <div class="modal-body p-t-0">
			
				
				
				<div class="row">
				
				
				<div class="col-md-12 form-group">
					<h5>Title <span class="color-r">*</span></h5>
				 {!! Form::text('title', '', ['class' => 'form-control','id'=>'doc_title', 'placeholder' => '', 'required' => '']) !!}
				</div>
					
				<div class="col-md-12 form-group">
				 <h5>Description</h5>

				 {{Form::textarea('doc_description','',array('class'=>'form-control ckeditor','id'=>'doc_description','rows' => 3))}}
				</div>
					
				
				<div class="row">
				<div class="col-md-6 padding-r-0">
				<div class="col-md-12 form-group">
				<h5>Competency <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.COMPETENCY_HELP') }}">?</span></h5>
                 <select name="competency" id="doc_competency_id" class="form-control">
                 <option value="">Select Competency</option>
                 @foreach($competencyList as $competency)
                  <option value="{{$competency['tcm_id']}}">{{$competency['tcm_competency_title']}}</option>
          @endforeach
                 </select>					
                </div>
				
				<div class="col-md-12 form-group">
				<h5>Development Area<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.DEVELOPEMENT_AREA_HELP') }}">?</span></h5>
                 <select name="development_area" id="doc_development_area_id" class="form-control">
                 <option value="">Select Development Area</option>
                 @foreach($developementAreaList as $developementArea)
                 <option value="{{$developementArea['tda_id']}}">{{$developementArea['tda_development']}}</option>
          @endforeach
                 </select>					
                </div>
					
				<div class="col-md-12 form-group">
				<h5>Level<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.LEVEL_HELP') }}">?</span></h5>
                 <select name="level" id="doc_level" class="form-control">
                  <option value="">Select Level</option>
                 @foreach($levelList as $level)
                 <option value="{{$level['tsl_id']}}">{{$level['tsl_seniority']}}</option>
          @endforeach
                 </select>					
                </div>
					
				</div>
					
				<div class="col-md-6">
				 <div class="media text-center upload-doc">
                        <div class="media-body">
						 <label>
						  <span class="icon icon-download icon-2x color-o"></span>
                          <h4 class="media-heading"> Upload Document<span class="color-r">*</span></h4>
                          <p class="media-description">
                          <small>Max file size: 5 MB. Accepted file type: .doc, .docx, .ppt, .pptx, .xls, .xlsx, .pdf, .txt </small>
						  <input class="file-upload-input" type="file" name="select_document" id="select_document">
                          </p>
						 </label>
             <label style="display: none;" id="show_doc_name"><b>File Name : </b><span name="document_name" id="document_name" style="font: bold;"></span></label>
                        </div>
				  </div>
				</div>	
					
				</div>
					
				<div class="col-md-6 form-group">
				<h5>Keywords<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.KEYWORDS_HELP') }}">?</span></h5>
                         {!! Form::text('keywords', '', ['class' => 'form-control','id'=>'doc_keywords','data-role'=>"tagsinput", 'placeholder' => '', 'required' => '']) !!}
                 <small>(Eg. abc,efg)</small>				
                </div>
					
				<div class="col-md-6 form-group">
				<h5>Author<span class="color-r">*</span></h5>
        {!! Form::text('author', '', ['class' => 'form-control','id'=>'doc_author', 'placeholder' => '', 'required' => '']) !!}		</div>
				
				<div class="col-md-12">
				     <div class="m-t m-b-md">
                        <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="doc_is_active" name="doc_is_active">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Active</span>
                        </label>
					    <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="doc_is_available" name="doc_is_available">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Available to all Learners</span>
                        </label>
                       
                      </div>
				</div>
        <input type="hidden" id="edit_document_id" name="edit_document_id">				
				
					
				<div class="col-md-12 m-t-md">
          {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
				 <!-- <button type="button" class="btn btn-primary">Save</button> -->
                 <button type="button" id="close_doc_modal"  class="btn btn-default">Cancel</button>
				</div>
					
				</div>
              
           
		  </div>
			{{Form::close()}}
        </div>
      </div>
    </div>

<!--Start Edit Article Popup-->
<div id="editarticle" class="modal pop-modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
             <h1 class="title-bar-title">
				<i class="icon icon-newspaper-o"></i>
                <span class="d-ib">Edit Article</span>
            </h1>
            <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </small>
            </p>
          </div>
          <div class="over-form" id="show_loader_edit" style="left: 40%;top: 75%;display: none;">
              <i class="fa fa-spinner fa-spin" style="font-size:50px;color: #ff9d4e;z-index: 9999;"></i>
        </div>
        <div class="alert alert-danger upload_error_msg" style="display: none;">
                    <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                    <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="update_art_error"></span>
        	</div>
          {{Form::open(array('id'=>'update_article_form','method'=>'post','enctype'=>'multipart/form-data'))}}
          <input type="hidden" value="{{ csrf_token() }}" name="_token">
             <input type="hidden" class="check_img_type" value="editimage">
          <div class="media text-center upload-sec" style="padding: 13px 20px;" id="edit_article_cover">
                        <div class="media-body p-t-sm">
						 <label>
                          <h4 class="media-heading"><span class="icon icon-camera color-o"></span> Upload cover image</h4>
                          <p class="media-description">
                          <small>Max file size: 5 MB. Accepted file type: .jpg,.png</small>
						  <input class="file-upload-input" type="file" id="art_edit_upload_img" name="file">
						 <input type="hidden" class="image_src_edit" name="image_src_edit">
             <input type="hidden" class="image_src_edit_big" name="image_src_edit_big">
             <input type="hidden" class="image_src_edit_small" name="image_src_edit_small">
						 <span id="get_crop_img" style="display: none;"></span>
                          </p>
						 </label>
                        </div>
                        <div class="hrtigm" style="margin-left: -15px;"></div>
                        <div id="crop_image_div_edit" style="display: none;">
					            <div class="row">
					                <div class="text-center" style="margin: 0px auto;">
					                    <div id="upload_demo_edit"></div>
					                </div>                
					            </div>
					    </div>
				  </div>
          <div class="modal-body p-t-0">
			
				
				
				<div class="row">
				
				
				<div class="col-md-12 form-group">
					<h5>Title <span class="color-r">*</span></h5>
				 <!-- <input class="form-control" type="text" value="How to Write a Web Article"> -->
				 {!! Form::text('title', '', ['class' => 'form-control','id'=>'edit_article_title' ,'placeholder' => '', 'required' => '']) !!}	
				</div>
					
				<div class="col-md-12 form-group">
				 <h5>Description</h5>
				{{Form::textarea('edit_article_description','',array('class'=>'form-control','id'=>'edit_article_description','rows' => 3))}}
				</div>

				<div class="col-md-12 form-group">
				 <h5>Body<span class="color-r">*</span></h5>
				 {{Form::textarea('edit_article_body','',array('class'=>'form-control','id'=>'edit_article_body','rows' => 5,'required' => ''))}}
				</div>
					
				<div class="col-md-6 form-group">
				<h5>Competency <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.COMPETENCY_HELP') }}">?</span></h5>
                 <select name="edit_copentency" id="edit_copentency" class="form-control">
                 <option value="">Select Competency</option>
                 @foreach($competencyList as $competency)
                  <option value="{{$competency['tcm_id']}}">{{$competency['tcm_competency_title']}}</option>
				  @endforeach
                 </select>					
                </div>
				
				<div class="col-md-6 form-group">
				<h5>Development Area <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.DEVELOPEMENT_AREA_HELP') }}">?</span></h5>
                 <select id="edit_development_area" name="edit_development_area" class="form-control">

                 </select>					
                </div>
					
				<div class="col-md-6 form-group">
				<h5>Level<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.LEVEL_HELP') }}">?</span></h5>
                 <select id="edit_level" name="edit_level" class="form-control">
                  <option value="">Select Level</option>
                 @foreach($levelList as $level)
                 <option value="{{$level['tsl_id']}}">{{$level['tsl_seniority']}}</option>
				  @endforeach
                 </select>					
                </div>
					
				<div class="col-md-6 form-group">
				<h5>Keywords<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.KEYWORDS_HELP') }}">?</span></h5>
                 {!! Form::text('keywords', '', ['class' => 'form-control','id'=>'edit_article_keywords','data-role'=>"tagsinput", 'placeholder' => '', 'required' => '']) !!}		
                </div>
				<div class="col-md-12 form-group"></div>
				<div class="col-md-6">
				<h5>Author<span class="color-r">*</span></h5>
                 {!! Form::text('author', '', ['class' => 'form-control','id'=>'edit_article_author', 'placeholder' => '', 'required' => '']) !!}				
                </div>
					
				<div class="col-md-12">
				 <div class="panel-group link-course-panel">
                        <div class="panel">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                               <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="icon icon-link m-r-sm"></i>Linked Courses</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" >
                                <div class="panel-body" id="coursearticle">
                                  
                                </div>
                            </div>
                        </div>
                     
                    </div>
				</div>
				
				<div class="col-md-12">
				     <div class="m-t m-b-md">
                        <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="edit_article_active" name="edit_article_active" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Active</span>
                        </label>
					    <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="edit_article_available" name="edit_article_available" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Available to all Learners</span>
                        </label>
                       
                      </div>
				</div>
				<input type="hidden" id="edit_article_id" name="edit_article_id">
				
					
				<div class="col-md-12 m-t-md">
					{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
				 <!-- <button type="button" class="btn btn-primary">Save</button> -->
                 <button type="button" id="close_edit_modal" class="btn btn-default">Cancel</button>
				</div>
					
				</div>
              
           
		  </div>
			{{Form::close()}}
        </div>
      </div>
    </div>	
<!--End Edit Article Popup-->

<!--Start Add Audio/Video Popup-->
<div id="addav" class="modal pop-modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
             <h1 class="title-bar-title">
        <i class="icon icon-video-camera"></i>
                <span class="d-ib">Upload Video/Audio to Content Library</span>
            </h1>
            <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </small>
            </p>
          <div class="over-form" id="show_loader_av" style="left: 40%;top: 75%;display: none;">
              <i class="fa fa-spinner fa-spin" style="font-size:50px;color: #ff9d4e;z-index: 9999;"></i>
          </div>  
          </div>
          <div class="alert alert-danger upload_error_msg" style="display: none;">
                    <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                    <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="update_av_error"></span>
          </div>
          {{Form::open(array('id'=>'add_av_form','method'=>'post','enctype'=>'multipart/form-data'))}}
          <input type="hidden" value="{{ csrf_token() }}" name="_token">
          <div class="media text-center upload-sec" style="padding: 13px 20px;" id="add-av">
              <div class="media-body p-t-sm">
             <label>
                          <h4 class="media-heading"><span class="icon icon-camera color-o"></span> Upload cover image</h4>
                          <p class="media-description">
                          <small>Max file size: 5 MB. Accepted file type: .jpg,.png</small>
              <input class="file-upload-input" type="file" accept="image/x-png,image/jpeg,image/jpg" id="av_upload_img" name="file">
             <input type="hidden" class="av_image_src" name="av_image_src">
             <input type="hidden" class="check_img_type" value="avaddimage">
             <span id="av_get_crop_img_add" style="display: none;"></span>
                          </p>
             </label>
              </div>
              <div class="hrtigav" style="margin-left: -15px; display: none;"></div>
              <div id="av_crop_image_div" style="display: none;">
                      <div class="row">
                          <div class="text-center" style="margin: 0px auto;">
                              <div id="av_upload_demo"></div>
                          </div>                
                      </div>
              </div>
          </div>

          <div class="modal-body p-t-0 demo-form-wrapper">
      
        
        
        <div class="row">
        
        
        <div class="col-md-12 form-group">
          <h5>Title <span class="color-r">*</span></h5>
         {!! Form::text('title', '', ['class' => 'form-control','id'=>'av_title', 'placeholder' => '', 'required' => '']) !!}
        </div>
          
        <div class="col-md-12 form-group">
         <h5>Description</h5>
         {{Form::textarea('av_description','',array('class'=>'form-control ckeditor','id'=>'av_description','rows' => 3))}}
        </div>
          
        
        <div class="row">
        <div class="col-md-6 padding-r-0">
        <div class="col-md-12 form-group">
        <h5>Competency <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.COMPETENCY_HELP') }}">?</span></h5>
         <!-- <div class="cus-select"> -->
                 <select name="competency" id="av_competency_id" class="form-control">
                 <option value="">Select Competency</option>
                 @foreach($competencyList as $competency)
                  <option value="{{$competency['tcm_id']}}">{{$competency['tcm_competency_title']}}</option>
          @endforeach
                 </select>
          <!-- </div> -->
                </div>
        
        <div class="col-md-12 form-group">
        <h5>Development Area<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.DEVELOPEMENT_AREA_HELP') }}">?</span></h5>
        <!-- <div class="cus-select"> -->
                 <select name="development_area" id="av_development_area_id" class="form-control">
                 <option value="">Select Development Area</option>
                 @foreach($developementAreaList as $developementArea)
                 <option value="{{$developementArea['tda_id']}}">{{$developementArea['tda_development']}}</option>
          @endforeach
                 </select>
        <!-- </div> -->
                </div>
          
        <div class="col-md-12 form-group">
        <h5>Level<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.LEVEL_HELP') }}">?</span></h5>
        <!-- <div class="cus-select"> -->
                 <select name="level" id="av_level" class="form-control">
                  <option value="">Select Level</option>
                 @foreach($levelList as $level)
                 <option value="{{$level['tsl_id']}}">{{$level['tsl_seniority']}}</option>
          @endforeach
                 </select>  
        <!-- </div> -->
                </div>
          
        </div>
          
        <div class="col-md-6">
        <div class="upload-vid">
         <div class="tab-menu">
         <ul class="nav nav-tabs">
         <li class="active"><a href="#tab-va" data-toggle="tab">Upload Videos/Audio</a></li>
                  <li><a href="#tab-embed" data-toggle="tab">Embed Video</a></li>
         </ul>
         </div>
                  <div class="tab-content">
                  <div class="tab-pane fade active in" id="tab-va">
             <div class="media ">
                        <div class="media-body text-center">
             <label>
              <span class="icon icon-upload icon-2x color-o"></span>
                          <p class="media-description">
                          <small>Max file size: 5 MB. Accepted file type: .mp3, .mp4</small>
              <input class="file-upload-input" type="file" name="select_video" id="select_video">
                          </p>           
             </label>
              <label style="display: none;" id="show_vid_name"><b>File Name : </b><span name="video_name" id="video_name" style="font: bold;"></span></label>
                        </div>
               </div>
          </div>
           <div class="tab-pane fade" id="tab-embed">
           <textarea class="form-control" id="embed_url" rows="4"></textarea>
           </div>
            
          
          </div>
        </div>
        </div>  
          
        </div>
          
        <div class="col-md-6 form-group">
        <h5>Keywords<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.KEYWORDS_HELP') }}">?</span></h5>
        {!! Form::text('av_keywords', '', ['class' => 'form-control','id'=>'av_keywords','data-role'=>"tagsinput", 'placeholder' => '', 'required' => '']) !!}
                 <small>(Eg. abc,efg)</small>        
                </div>
          
        <div class="col-md-6 form-group">
        <h5>Author<span class="color-r">*</span></h5>
         {!! Form::text('author', '', ['class' => 'form-control','id'=>'av_author', 'placeholder' => '', 'required' => '']) !!}         
                </div>
        
        <div class="col-md-12">
             <div class="m-t m-b-md">
                        <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="av_is_active" name="av_is_active">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Active</span>
                        </label>
              <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="av_is_available" name="av_is_available">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Available to all Learners</span>
                        </label>
                       
                      </div>
        </div>
        
        <input type="hidden" id="edit_av_id" name="edit_av_id">        
          
        <div class="col-md-12 m-t-md">
          {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
         <!-- <button type="button" class="btn btn-primary">Save</button> -->
                 <button type="button" id="close_av_modal" class="btn btn-default">Cancel</button>
        </div>
          
        </div>
              
           
      </div>
            {{Form::close()}}
        </div>
      </div>
    </div>
<!--End Add Audio/Video Popup-->

<!--Add Webinar Popup-->
<div id="addwebinar" class="modal pop-modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
             <h1 class="title-bar-title">
        <i class="icon icon-newspaper-o"></i>
                <span class="d-ib">Upload Webinar to Content Library</span>
            </h1>
            <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </small>
            </p>
            <div class="over-form" id="show_loader_webinar" style="left: 40%;top: 75%;display: none;">
              <i class="fa fa-spinner fa-spin" style="font-size:50px;color: #ff9d4e;z-index: 9999;"></i>
          </div>
          </div>
          <div class="alert alert-danger upload_error_msg" style="display: none;">
                    <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                    <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="add_webinar_error"></span>
          </div>
          {{Form::open(array('id'=>'add_webinar_form','method'=>'post','enctype'=>'multipart/form-data'))}}
          <input type="hidden" value="{{ csrf_token() }}" name="_token">
          <div class="media text-center upload-sec" style="padding: 13px 20px;" id="add-webinar">
              <div class="media-body p-t-sm">
             <label>
                          <h4 class="media-heading"><span class="icon icon-camera color-o"></span> Upload cover image</h4>
                          <p class="media-description">
                          <small>Max file size: 5 MB. Accepted file type: .jpg,.png</small>
              <input class="file-upload-input" type="file" accept="image/x-png,image/jpeg,image/jpg" id="webinar_upload_img" name="file">
             <input type="hidden" class="webinar_image_src" name="webinar_image_src">
             <input type="hidden" class="check_img_type" value="webinaraddimage">
             <span id="get_crop_img_webinar" style="display: none;"></span>
                          </p>
             </label>
              </div>
              <div class="hrtigwebinar" style="margin-left: -15px; display: none;"></div>
              <div id="webinar_crop_image_div" style="display: none;">
                      <div class="row">
                          <div class="text-center" style="margin: 0px auto;">
                              <div id="webinar_upload_demo"></div>
                          </div>                
                      </div>
              </div>
          </div>
                              @if ($errors->has('file'))
                <span class="help-block">
                    <strong>{{ $errors->first('file') }}</strong>
                </span>
            @endif
      <div class="modal-body p-t-0">
      <div class="row">
        
        
        <div class="col-md-12 form-group">
          <h5>Title <span class="color-r">*</span></h5>
         <!-- <input class="form-control" type="text"> -->
         {!! Form::text('title', '', ['class' => 'form-control','id'=>'webinar_title', 'placeholder' => '', 'required' => '']) !!}
        </div>
          
        <div class="col-md-12 form-group">
         <h5>Description</h5>
         <!-- <textarea class="form-control" rows="3"></textarea> -->
         {{Form::textarea('webinar_description','',array('class'=>'form-control ckeditor','id'=>'webinar_description','rows' => 3))}}
        </div>
          
        <div class="col-md-6 form-group">
        <h5>Competency <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.COMPETENCY_HELP') }}">?</span></h5>
                 <select name="competency" id="webinar_competency_id" class="form-control">
                 <option value="">Select Competency</option>
                 @foreach($competencyList as $competency)
                  <option value="{{$competency['tcm_id']}}">{{$competency['tcm_competency_title']}}</option>
          @endforeach
                 </select>          
        </div>
        
        <div class="col-md-6 form-group">
        <h5>Development Area <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.DEVELOPEMENT_AREA_HELP') }}">?</span></h5>
                 <select name="webinar_development_area_id" id="webinar_development_area_id" class="form-control">
                 <option value="">Select Development Area</option>
                 @foreach($developementAreaList as $developementArea)
                 <option value="{{$developementArea['tda_id']}}">{{$developementArea['tda_development']}}</option>
          @endforeach
                 </select>          
        </div>
          
        <div class="col-md-6 form-group">
        <h5>Level<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.LEVEL_HELP') }}">?</span></h5>
                 <select name="level" id="webinar_level" class="form-control">
                  <option value="">Select Level</option>
                 @foreach($levelList as $level)
                 <option value="{{$level['tsl_id']}}">{{$level['tsl_seniority']}}</option>
          @endforeach
                 </select>          
        </div>
          
        <div class="col-md-6 form-group">
        <h5>Keywords<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.KEYWORDS_HELP') }}">?</span></h5>
                 <!-- <input class="form-control" type="text">         -->
                 {!! Form::text('webinar_keywords', '', ['class' => 'form-control','id'=>'webinar_keywords','data-role'=>"tagsinput", 'placeholder' => '', 'required' => '']) !!}
                 <small>(Eg. abc,efg)</small>
        </div>
        <div class="col-md-12"></div> 
        <div class="col-md-6 form-group">
        <h5>Author<span class="color-r">*</span></h5>
                 <!-- <input class="form-control" type="text">         -->
                 {!! Form::text('author', '', ['class' => 'form-control','id'=>'webinar_author', 'placeholder' => '', 'required' => '']) !!}
        </div>
        
        <div class="col-md-12">
             <div class="m-t m-b-md">
                        <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="webinar_is_active" name="webinar_is_active" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Active</span>
                        </label>
              <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="webinar_is_available" name="webinar_is_available" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Available to all Learners</span>
                        </label>
                       
              </div>
        </div>
        
         <input type="hidden" id="edit_webinar_id" name="edit_av_id">        
          
        <div class="col-md-12 m-t-md">
          {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
         <!-- <button type="button" class="btn btn-primary">Save</button> -->
                 <button type="button" id="close_webinar_modal" class="btn btn-default">Cancel</button>
        </div>
          
        </div>     
      </div>
      {{Form::close()}}
        </div>
      </div>
    </div>  
<script src="{{ url('public/js/contentlibrary') }}/content_library.js"></script>
@stop