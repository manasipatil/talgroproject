@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')


  <div class="title-bar clear">
      <div class="col-xs-12 col-sm-7 col-md-8">
            <h1 class="title-bar-title">
        <i class="icon icon-television"></i>
                <span class="d-ib">{{Config::get('messages.webinar.WEBINAR_LIST')}}</span>
            </h1>
            <p class="title-bar-description">
              <small>{{Config::get('messages.webinar.WEBINAR_TITLE_BAR')}}</small>
            </p>
      </div>
      <div class="col-xs-12 col-sm-5 col-md-4 ad_top">
      <button class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
      <button class="btn btn-primary" type="submit">Add Webinar</button>
      </div>
  </div>
    
  <div class="layout-content-body filter-bg nopadding">
          <div class="row gutter-xs">
              <div class="col-xs-12">
                  <div class="Cus_card collapsed ">
                      <div class="Cus_card-header fil">
                         <button type="button" class="Cus_card-actions Cus_card-toggler" title="Filter"></button>
                      </div>
                      <div class="Cus_card-body collapse">
                          <div class="col-xs-12 col-md-4 col-lg-3">  
                              <h5>Course</h5>
                              <div class="cus-select">
                              <select class="webinar_course">
                                <option value="">Select Course</option>
                                @foreach ($course_list as $course)
                                 <option value="{{$course['tc_id']}}"> {{$course['tc_title']}}</option> 
                                @endforeach
                              </select>
                             </div>
                          </div>
                          <div class="col-xs-12 col-md-4 col-lg-3">
                              <h5>Instructor</h5>
                              <div class="cus-select">
                              <select class="webnar_instructor">
                                <option value="">Select Instructor</option>
                                  @foreach ($instructor_list as $instructor)
                                   <option value="{{$instructor['id']}}"> {{$instructor['user_firstname']}} {{$instructor['user_lastname']}}</option> 
                                  @endforeach
                              </select>
                              </div>
                          </div>
                          <div class="col-xs-12 col-md-4 col-lg-3">
                            <div class="row">
                              <div class="col-xs-6 col-md-6">
                                <h5>Learner</h5>
                                <div class="cus-select">
                                  <select>
                                    <option value="c-plus-plus">C++</option>
                                    <option value="css">CSS</option>
                                    <option value="java">Java</option>
                                  </select>
                                </div>
                              </div>
              
                              <div class="col-xs-6 col-md-6">
                                <h5>Start Date</h5>
                                <div class="input-with-icon">
                                  <input class="form-control" type="text" id="webinar_start_date" data-provide="datepicker" data-date-today-btn="linked">
                                  <span class="icon icon-calendar input-icon"></span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-4 col-lg-3">
                            <div class="row">
                              <div class="col-xs-6 col-md-6">
                                <h5>End Date</h5>
                                <div class="input-with-icon">
                                  <input class="form-control" type="text" id="webinar_end_date" data-provide="datepicker" data-date-clear-btn="true">
                                  <span class="icon icon-calendar input-icon"></span>
                                </div>
                              </div>
                              <div class="col-xs-6 col-md-6">
                                <h5>Platform</h5>
                                <div class="cus-select">
                                  <select>
                                    <option value="c-plus-plus">C++</option>
                                    <option value="css">CSS</option> 
                                    <option value="java">Java</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
            
                          <div class="col-xs-12 col-md-4 col-lg-3">
                            <h5>Competency</h5>
                            <div class="cus-select">
                                <select class="webinar_competency">
                                  <option value="">Select Competency</option>
                                    @foreach ($competency_list as $code)
                                       <option value="{{$code['tcm_id']}}"> {{$code['tcm_competency_title']}}</option> 
                                    @endforeach
                                </select>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-4 col-lg-3">
                            <h5>Development Area</h5>
                            <div class="cus-select">
                              <select class="webinar_development_area">
                                 <option value="">Select Development</option>
                                    @foreach ($development_area_list as $code)
                                       <option value="{{$code['tda_id']}}"> {{$code['tda_development']}}</option> 
                                    @endforeach
                              </select>  
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-4 col-lg-3">
                            <h5>Level</h5>
                            <div class="cus-select">
                              <select class="webinar_level">
                                <option value="">Select Level</option>
                                    @foreach ($level_list as $code)
                                       <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                                    @endforeach
                              </select>  
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-12">
                            <h5>Search by Keyword</h5>
                            <input class="form-control" type="text" id="searchtxt">          
                          </div>
                          <div class="col-xs-12 col-md-4">
                            <h5></h5>
                            <button class="btn btn-primary" type="submit" id="webinar_searchButton">Search</button>
                            <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                          </div>
            
                      </div>
                  </div>
              </div>
          </div>
  </div>
    
  <div class="layout-content-body">
    <div class="row gutter-xs">
      <div class="col-xs-12">
        <div class="card no-border">
          <div class="card-body">
            <?php
            $default_col_order_string="CheckAll-1,WebinarName-1,CourseName-1,DevelopmentArea-1,Learners-1,Date-1,Time-1,Competency-1,Level-1,Instructor-1,Platform-1,WebinarStatus-1,Action-1";
            ?>
            <div class="table-content-header">
              <div class="table-toolbar">
                <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                  <div class="btn-group bulk hide common_action">
                    <label>Bulk Action :</label>
                    <button class="btn btn-link link-muted click_common_action" to_do="enable"  type="button"><span>Enable</span></button> 
                    <span class="pipe">|</span>
                  <button class="btn btn-link link-muted click_common_action" to_do="disable" type="button"><span>Disable</span>
                  </button>
                    <span class="pipe">|</span>
                    <button class="btn btn-link link-muted click_common_action" to_do="delete" type="button"><span>Delete</span></button>
                  </div>
                </div>
                <div class="table-toolbar-tools pull-xs-right pull-md-right">
                  <div class="btn-group export">
                    <a href="{{url('/admin/webinar_excel')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-excel-o icon-lg icon-fw"></span>
                      <span class="visible-lg-inline">Export Excel</span>
                    </a>
                 
                    <a href="{{url('/admin/webinar_pdf')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-pdf-o icon-lg icon-fw"></span>
                      <span class="visible-lg-inline">Export PDF</span>
                    </a>
                    <div class="filter-opt1">
                      <button class="btn btn-link col" type="button">
                        <span class="icon icon-cogs icon-fw" title="Column"></span>
                        <span class="hidden-xs">Column</span>
                      </button>
                      <div class="listing-det overrlay">
                        <ul class="list-group sortable_menu">
                              <?php
                              //if session has value display column according or display default array
                              if($request->session()->has('webinar_hidden_column_array')){
                                $col_arr_value = $request->session()->get('webinar_hidden_column_array');
                              }else{
                                $col_arr_value=$default_col_order_string;
                              }
                              $col_arr_value=explode(",",$col_arr_value);
                              
                              foreach ($col_arr_value as $key => $value) {
                                  $value=explode('-', $value);
                                  $class="";
                                  if($value[0]=='CheckAll' || $value[0]=='Action')
                                    $class='non_sortable';
                                  ?>
                                  <li class="list-group-item list-order-item <?=$class?>"  data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                    <span class="pull-right">
                                      <label class="switch switch-primary">
                                        <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                        <span class="switch-track"></span>
                                        <span class="switch-thumb"></span>
                                      </label>
                                    </span>
                                    <span class="icon icon-arrows"></span>
                                    <?php
                                    if ($value[0]=="CheckAll")
                                      echo "Check All";
                                    else if($value[0]=="WebinarName")
                                      echo "Webinar Name";
                                    else if($value[0]=="CourseName")
                                      echo "Course Name";
                                    else if($value[0]=="DevelopmentArea")
                                      echo "Development Area";
                                    else
                                      echo $value[0];
                                    ?>
                                    </li>
                                  
                                  <?php
                                }
                              ?>
                            <button class="btn btn-primary btn-half save_order" form_name="webinar" id="" type="button">Save</button>
                            <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                            </ul>
                      </div>
                    </div>
                    <a class="btn btn-link" href="{{url('/admin/webinar_calender')}}">
                      <span class="icon icon-calendar icon-fw" title="Calendar View"></span>
                      <span class="hidden-xs">Calendar</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="cus-tbl">
              <input type="hidden" name="sorting_column_name" id="sorting_column_name">  
              <table id="" class="table table-hover table-striped dataTable example" cellspacing="0" width="100%">
              </table>
              <?php
                if($request->session()->has('webinar_hidden_column_array')){
                    $webinar_hidden_column_array = $request->session()->get('webinar_hidden_column_array');?>
                    <input type="hidden" name="webinar_hidden_column_array" id="webinar_hidden_column_array" value="<?=$webinar_hidden_column_array?>"><?php
                }else{?>
                    <input type="hidden" name="webinar_hidden_column_array" id="webinar_hidden_column_array" value="<?=$default_col_order_string?>"><?php
                }
                ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<script src="{{url('public/adminlte/js/webinar/WebinarController.js') }}"></script> 
@stop