@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

  <div class="title-bar clear">
  <div class="col-xs-12 col-sm-7 col-md-8">
    <h1 class="title-bar-title">
    <i class="icon icon-television"></i>
        <span class="d-ib">{{Config::get('messages.webinar.WEBINAR_LIST')}}</span>
    </h1>
    <p class="title-bar-description">
      <small>{{Config::get('messages.webinar.WEBINAR_TITLE_BAR')}}</small>
    </p>
  </div>
  <div class="col-xs-12 col-sm-5 col-md-4 ad_top">
  <button class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
  <button class="btn btn-primary" type="submit">Add Webinar</button>
  </div>
  
</div>

<div class="layout-content-body filter-bg nopadding">
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="Cus_card collapsed">
                <div class="Cus_card-header fil">
                   <button type="button" class="Cus_card-actions Cus_card-toggler" title="Filter"></button>
                </div>
                <div class="Cus_card-body collapse">
                    <div class="col-xs-12 col-md-4 col-lg-3">  
                        <h5>Course</h5>
            <div class="cus-select">
                         <select>
                         <option value="c-plus-plus">C++</option>
                         <option value="css">CSS</option>
               <option value="java">Java</option>
                         </select>
            </div>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-3">
                        <h5>Instructor</h5>
            <div class="cus-select">
                        <select>
                         <option value="c-plus-plus">C++</option>
                         <option value="css">CSS</option>
               <option value="java">Java</option>
                         </select>
            </div>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-3">
            <div class="row">
            <div class="col-xs-6 col-md-6">
                        <h5>Learner</h5>
            <div class="cus-select">
                        <select>
                         <option value="c-plus-plus">C++</option>
                         <option value="css">CSS</option>
               <option value="java">Java</option>
                         </select>
            </div>
                        </div>
            
            <div class="col-xs-6 col-md-6">
                        <h5>Start Date</h5>
                        <div class="input-with-icon">
                          <input class="form-control" type="text" data-provide="datepicker" data-date-today-btn="linked">
                          <span class="icon icon-calendar input-icon"></span>
                        </div>
            </div>
            </div>
            
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-3">
            <div class="row">
            <div class="col-xs-6 col-md-6">
            <h5>End Date</h5>
                        <div class="input-with-icon">
                          <input class="form-control" type="text" data-provide="datepicker" data-date-clear-btn="true">
                          <span class="icon icon-calendar input-icon"></span>
                        </div>
            </div>
            
            <div class="col-xs-6 col-md-6">
                        <h5>Platform</h5>
            <div class="cus-select">
                        <select>
                         <option value="c-plus-plus">C++</option>
                         <option value="css">CSS</option>
               <option value="java">Java</option>
                        </select>
            </div>
            </div>
            </div>
            
                    </div>
          
          <div class="col-xs-12 col-md-4 col-lg-3">
            <h5>Competency</h5>
            <div class="cus-select">
                        <select>
                         <option value="c-plus-plus">C++</option>
                         <option value="css">CSS</option>
               <option value="java">Java</option>
                         </select>
            </div>
                    </div>
          <div class="col-xs-12 col-md-4 col-lg-3">
            <h5>Development Area</h5>
            <div class="cus-select">
                        <select>
                         <option value="c-plus-plus">C++</option>
                         <option value="css">CSS</option>
               <option value="java">Java</option>
                         </select>  
            </div>
                    </div>
          <div class="col-xs-12 col-md-4 col-lg-3">
            <h5>Level</h5>
            <div class="cus-select">
                        <select>
                         <option value="c-plus-plus">C++</option>
                         <option value="css">CSS</option>
               <option value="java">Java</option>
                         </select>  
            </div>
                    </div>
          
          <div class="col-xs-12 col-md-12">
            <h5>Search by Keyword</h5>
                        <input class="form-control" type="text">          
                    </div>
          
          <div class="col-xs-12 col-md-4">
                    <h5></h5>
          <button class="btn btn-primary" type="submit">Search</button>
          <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
            </div>
          
                </div>
            </div>
        </div>
    </div>
</div>

<div class="layout-content-body">
    <div class="row gutter-xs">
        <div class="col-xs-12">
            <div class="card no-border">
       <div class="card-body padding-lft padding-rgt">
               <div class="table-content-header">
                      <div class="table-toolbar">
                       <div class="table-toolbar-tools pull-xs-right pull-xs-right">
                  <div class="btn-group export">
                <a class="btn btn-link" href="{{url('/admin/webinar')}}">
                        <span class="icon icon-list icon-fw" title="Listing View"></span>
                <span class="hidden-xs">Listing</span>
                        </a>
                  </div>
                        </div>
                       </div>
                      </div>
        <div id="calendar"></div>
       </div>
       
            </div>
        </div>
    </div>
</div>
  

<script src="{{url('public/adminlte/js/webinar/CalenderController.js') }}"></script> 
@stop