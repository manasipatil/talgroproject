@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
<!-- <p>
        <a href="{{ route('admin.users.create') }}" class="btn btn-success">@lang('global.app_add_new')</a>
        
    </p> -->
<div class="title-bar clear">
  <div class="col-xs-12 col-sm-8 col-md-8">
        <h1 class="title-bar-title">
    <i class="icon icon-th-list"></i>
            <span class="d-ib">{{Config::get('messages.view_user.VIEW_USER_TITLE_BAR')}}</span>
        </h1>
        <p class="title-bar-description">
          <small>{{Config::get('messages.view_user.VIEW_USER_TITLE_BAR_DESCRIPTION')}}.</small>
        </p>
  </div>
  <div class="col-xs-12 col-sm-5 col-md-4 ad_top">
      <button style="right: 1px;" class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
  </div>    
</div>
    
    
        
<div class="layout-content-body">
  <div class="row gutter-xs">
    <div class="col-xs-12">
        <div class="demo-form-wrapper">
          <div class="panel">
            <div class="tab-menu">
              <ul class="nav nav-tabs m-t-lg">
                  <li class="active"><a href="#tab-1" data-toggle="tab">All</a></li>
                        <li><a href="#tab-2" data-toggle="tab">Learner</a></li>
                        <li ><a href="#tab-3" data-toggle="tab">Manager</a></li>
                  <li><a href="#tab-4" data-toggle="tab">Expert</a></li>
                  <li><a href="#tab-5" data-toggle="tab">HR</a></li>
                  <li><a href="#tab-6" data-toggle="tab">Instructor</a></li>
              </ul>
            </div>
            <div class="tab-content combined-list">
              <!--tab 1 start-->
              <div class="tab-pane fade active in all_user" id="tab-1">
                <?php
                  $default_col_order_string="CheckAll-1,EMPCode-1,Name-1,Email-1,Mobile-1,Dept-1,Function-1,Level-1,Manager-1,CourseEnrolled-1,Action-1";
                                  ?>
                <div class="table-content-header">
                  <div class="table-toolbar">
                    <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                      <div class="btn-group bulk common_action hide">
                        <label>Bulk Action :</label>
                        <button class="btn btn-link link-muted click_common_action" tab_name="all_user" to_do="enable" type="button">
                          <span>Enable</span>
                        </button> 
                        <span class="pipe">|</span>
                        <button class="btn btn-link link-muted click_common_action" tab_name="all_user" to_do="disable" type="button">
                          <span>Disable</span>
                        </button>
                        <span class="pipe">|</span>
                        <button class="btn btn-link link-muted click_common_action" tab_name="all_user" to_do="delete" type="button">
                          <span>Delete</span>
                        </button>
                      </div>
                    </div>
                    <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                      <div class="btn-group export">
                        <a href="{{url('/admin/user_excel/all')}}" type="button" class="btn btn-link link-muted">
                          <span class="icon icon-file-excel-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export Excel"></span>
                          <span class="hidden-xs">Export Excel</span>
                        </a>
                        <a href="{{url('/admin/user_pdf/all')}}" type="button" class="btn btn-link link-muted">
                          <span class="icon icon-file-pdf-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export PDF"></span>
                          <span class="hidden-xs">Export PDF</span>
                        </a>
                        <div class="filter-opt1">
                          <button class="btn btn-link col" type="button">
                            <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                            <span class="hidden-xs">Column</span>
                          </button>
                          <div class="listing-det overrlay">
                            <ul class="list-group sortable_menu">
                              <?php
                              //if session has value display column according or display default array
                              if($request->session()->has('all_user_hidden_column_array')){
                                $col_arr_value = $request->session()->get('all_user_hidden_column_array');
                              }else{
                                $col_arr_value=$default_col_order_string;
                              }
                              $col_arr_value=explode(",",$col_arr_value);

                              foreach ($col_arr_value as $key => $value) {
                                  //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                  $value=explode('-', $value);
                                  $class="";
                                  if($value[0]=='CheckAll' || $value[0]=='Action')
                                    $class='non_sortable';
                                  ?>
                                  <li class="list-group-item list-order-item <?=$class?> " data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                    <span class="pull-right">
                                      <label class="switch switch-primary">
                                        <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                        <span class="switch-track"></span>
                                        <span class="switch-thumb"></span>
                                      </label>
                                    </span>
                                    <span class="icon icon-arrows"></span>
                                    <?php
                                    if ($value[0]=="CheckAll")
                                      echo "Check All";
                                    else
                                      echo $value[0];
                                    ?>
                                    </li>
                                  <?php
                                }
                              ?>
                              
                              <button class="btn btn-primary btn-half save_order" form_name="all_user" type="button">Save</button>
                              <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="btn-group">
                        <!-- <a href="{{url('/admin/add_user_form/all')}}" type="button" class="btn btn-primary" style="margin-top:-5px;">
                          <span class="hidden-xs">Add User</span>
                        </a> -->
                        <a href="{{url('/admin/add_user_form/all')}}" type="button" class="btn btn-primary" style="margin-top:-5px;">
                          <span class="hidden-xs">Add User</span>
                        </a>
                      </div>
                      <div class="btn-group">
                        <a href="#" class="refine_mbutton" title="Filter"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="table-content-body"> 
                  <div class="Cus_card filter-bg">
                    <div class="Cus_card-body" style="display: none;">
                      <div class="col-xs-6 col-md-3">
                        <h5>Department</h5>
                        <div class="cus-select">
                          <select class="all_department">
                              <option value="">Select Department</option>
                              @foreach ($array_department as $code)
                                 <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Level</h5>
                        <div class="cus-select">
                          <select class="all_level">
                            <option value="">Select Level</option>
                              @foreach ($array_level as $code)
                                 <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Function</h5>
                        <div class="cus-select">
                          <select class="all_function">
                            <option value="">Select Function</option>
                              @foreach ($array_function as $code)
                                 <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                              @endforeach
                         </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Status</h5>
                        <div class="cus-select">
                          <select class="all_status">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-9">
                        <h5>Search</h5>
                        <input class="form-control" type="text" id="searchalltxt">
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>No of Learners</h5>
                        <div>
                            <div class="slider slider-circle slider-primary" id="user_slider" data-slider="primary" data-min="0" data-max="200" data-start="[1, 500]" data-step="0.5" data-connect="true" data-target='["#from", "#to"]'></div>
                            Range: 
                            <span id="from">1</span> — 
                            <span id="to">500</span>
                        </div>
                      </div>
                      <div class="col-xs-12 col-md-4">
                        <h5></h5>
                        <button class="btn btn-primary" id="all_user_searchButton" type="submit">Search</button>
                        <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="all_user_sorting_column_name" id="all_user_sorting_column_name">
                  <table class="table table-hover table-striped all_record" cellspacing="0" width="100%">
                    <thead></thead>
                    <tbody>
                    </tbody>
                  </table>
                  <?php
                    if($request->session()->has('all_user_hidden_column_array')){
                        $all_user_hidden_column_array = $request->session()->get('all_user_hidden_column_array');?>
                        <input type="hidden" name="all_user_hidden_column_array" id="all_user_hidden_column_array" value="<?=$all_user_hidden_column_array?>"><?php
                    }else{?>
                        <input type="hidden" name="all_user_hidden_column_array" id="all_user_hidden_column_array" value="<?=$default_col_order_string?>"><?php
                    }
                  ?>
                </div>
              </div>
              <!--tab2 start-->
              <div class="tab-pane fade learner" id="tab-2">
                <?php
                  $default_col_order_string="CheckAll-1,EMPCode-1,Name-1,Email-1,Mobile-1,Dept-1,Function-1,Level-1,Manager-1,CourseEnrolled-1,Action-1";
                                  ?>
                <div class="table-content-header">
                  <div class="table-toolbar">
                    <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                      <div class="btn-group bulk common_action hide">
                        <label>Bulk Action :</label>
                        <button class="btn btn-link link-muted click_common_action" tab_name="learner" to_do="enable" type="button">
                          <span>Enable</span>
                        </button> 
                        <span class="pipe">|</span>
                        <button class="btn btn-link link-muted click_common_action" tab_name="learner" to_do="disable" type="button">
                          <span>Disable</span>
                        </button>
                        <span class="pipe">|</span>
                        <button class="btn btn-link link-muted click_common_action" tab_name="learner" to_do="delete" type="button">
                          <span>Delete</span>
                        </button>
                      </div>
                    </div>
                    <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                      <div class="btn-group export">
                        <a href="{{url('/admin/user_excel/learner')}}" type="button" class="btn btn-link link-muted">
                          <span class="icon icon-file-excel-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export Excel"></span>
                          <span class="hidden-xs">Export Excel</span>
                        </a>
                        <a href="{{url('/admin/user_pdf/learner')}}" type="button" class="btn btn-link link-muted">
                          <span class="icon icon-file-pdf-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export PDF"></span>
                          <span class="hidden-xs">Export PDF</span>
                        </a>
                        <div class="filter-opt1">
                          <button class="btn btn-link col" type="button">
                            <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                            <span class="hidden-xs">Column</span>
                          </button>
                          <div class="listing-det overrlay">
                            <ul class="list-group sortable_menu">
                              <?php
                              //if session has value display column according or display default array
                              if($request->session()->has('learner_hidden_column_array')){
                                $col_arr_value = $request->session()->get('learner_hidden_column_array');
                              }else{
                                $col_arr_value=$default_col_order_string;
                              }
                              $col_arr_value=explode(",",$col_arr_value);

                              foreach ($col_arr_value as $key => $value) {
                                  //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                  $value=explode('-', $value);
                                  $class="";
                                  if($value[0]=='CheckAll' || $value[0]=='Action')
                                    $class='non_sortable';
                                  ?>
                                  <li class="list-group-item list-order-item <?=$class?> " data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                    <span class="pull-right">
                                      <label class="switch switch-primary">
                                        <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                        <span class="switch-track"></span>
                                        <span class="switch-thumb"></span>
                                      </label>
                                    </span>
                                    <span class="icon icon-arrows"></span>
                                    <?php
                                    if ($value[0]=="CheckAll")
                                      echo "Check All";
                                    else
                                      echo $value[0];
                                    ?>
                                    </li>
                                  <?php
                                }
                              ?>
                              
                              <button class="btn btn-primary btn-half save_order" form_name="learner" type="button">Save</button>
                              <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="btn-group">
                        <a href="{{url('/admin/add_user_form/Learner')}}" type="button" class="btn btn-primary" style="margin-top:-5px;">
                          <span class="hidden-xs">Add Learner</span>
                        </a>
                      </div>
                      <div class="btn-group">
                        <a href="#" class="refine_mbutton" title="Filter"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="table-content-body"> 
                  <div class="Cus_card filter-bg">
                    <div class="Cus_card-body" style="display: none;">
                      <div class="col-xs-6 col-md-3">
                        <h5>Department</h5>
                        <div class="cus-select">
                          <select class="learner_department">
                              <option value="">Select Department</option>
                              @foreach ($array_department as $code)
                                 <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Level</h5>
                        <div class="cus-select">
                          <select class="learner_level">
                            <option value="">Select Level</option>
                              @foreach ($array_level as $code)
                                 <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Function</h5>
                        <div class="cus-select">
                         <select class="learner_function">
                            <option value="">Select Function</option>
                              @foreach ($array_function as $code)
                                 <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                              @endforeach
                         </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Status</h5>
                        <div class="cus-select">
                          <select class="learner_status">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-9">
                        <h5>Search</h5>
                        <input class="form-control" type="text" id="searchlearnertxt">
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>No of Learners</h5>
                        <div>
                            <div class="slider slider-circle slider-primary" data-slider="primary" data-min="0" data-max="200" data-start="[1, 500]" data-step="0.5" data-connect="true" data-target='["#from", "#to"]'></div>
                            Range: 
                            <span id="from">1</span> — 
                            <span id="to">500</span>
                        </div>
                      </div>
                      <div class="col-xs-12 col-md-4">
                        <h5></h5>
                        <button class="btn btn-primary" id="learner_searchButton" type="submit">Search</button>
                        <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="learner_sorting_column_name" id="learner_sorting_column_name">
                  <table class="table table-hover table-striped learner_record" cellspacing="0" width="100%">
                    <thead></thead>
                    <tbody>
                    </tbody>
                  </table>
                  <?php
                    if($request->session()->has('learner_hidden_column_array')){
                        $learner_hidden_column_array = $request->session()->get('learner_hidden_column_array');?>
                        <input type="hidden" name="learner_hidden_column_array" id="learner_hidden_column_array" value="<?=$learner_hidden_column_array?>"><?php
                    }else{?>
                        <input type="hidden" name="learner_hidden_column_array" id="learner_hidden_column_array" value="<?=$default_col_order_string?>"><?php
                    }
                  ?>
                </div>
              </div>
              <!--tab 3 start-->
              <div class="tab-pane fade manager" id="tab-3">
                <?php
                  $default_col_order_string="CheckAll-1,EMPCode-1,Name-1,Email-1,Mobile-1,Dept-1,Function-1,Level-1,Manager-1,Courses-1,CourseEnrolled-1,Action-1";
                                  ?>
                <div class="table-content-header">
                  <div class="table-toolbar">
                    <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                      <div class="btn-group bulk common_action hide">
                        <label>Bulk Action :</label>
                        <button class="btn btn-link link-muted click_common_action" tab_name="manager" to_do="enable" type="button">
                          <span>Enable</span>
                        </button> 
                        <span class="pipe">|</span>
                        <button class="btn btn-link link-muted click_common_action" tab_name="manager" to_do="disable" type="button">
                          <span>Disable</span>
                        </button>
                        <span class="pipe">|</span>
                        <button class="btn btn-link link-muted click_common_action" tab_name="manager" to_do="delete" type="button">
                          <span>Delete</span>
                        </button>
                      </div>
                    </div>
                    <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                      <div class="btn-group export">
                        <a href="{{url('/admin/user_excel/manager')}}" type="button" class="btn btn-link link-muted">
                          <span class="icon icon-file-excel-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export Excel"></span>
                          <span class="hidden-xs">Export Excel</span>
                        </a>
                        <a href="{{url('/admin/user_pdf/manager')}}" type="button" class="btn btn-link link-muted">
                          <span class="icon icon-file-pdf-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export PDF"></span>
                          <span class="hidden-xs">Export PDF</span>
                        </a>
                        <div class="filter-opt1">
                          <button class="btn btn-link col" type="button">
                            <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                            <span class="hidden-xs">Column</span>
                          </button>
                          <div class="listing-det overrlay">
                            <ul class="list-group sortable_menu">
                              <?php
                              //if session has value display column according or display default array
                              if($request->session()->has('user_hidden_column_array')){
                                $col_arr_value = $request->session()->get('user_hidden_column_array');
                              }else{
                                $col_arr_value=$default_col_order_string;
                              }
                              $col_arr_value=explode(",",$col_arr_value);

                              foreach ($col_arr_value as $key => $value) {
                                  //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                  $value=explode('-', $value);
                                  $class="";
                                  if($value[0]=='CheckAll' || $value[0]=='Action')
                                    $class='non_sortable';
                                  ?>
                                  <li class="list-group-item list-order-item <?=$class?>" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                    <span class="pull-right">
                                      <label class="switch switch-primary">
                                        <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                        <span class="switch-track"></span>
                                        <span class="switch-thumb"></span>
                                      </label>
                                    </span>
                                    <span class="icon icon-arrows"></span>
                                    <?php
                                    if ($value[0]=="CheckAll")
                                      echo "Check All";
                                    else
                                      echo $value[0];
                                    ?>
                                    </li>
                                  <?php
                                }
                              ?>
                              
                              <button class="btn btn-primary btn-half save_order " form_name="manager" type="button">Save</button>
                              <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="btn-group">
                        <a href="{{url('/admin/add_user_form/Manager')}}" type="button" class="btn btn-primary" style="margin-top:-5px;">
                          <span class="hidden-xs">Add Manager</span>
                        </a>
                      </div>
                      <div class="btn-group">
                        <a href="#" class="refine_mbutton" title="Filter"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="table-content-body"> 
                  <div class="Cus_card filter-bg">
                    <div class="Cus_card-body" style="display: none;">
                      <div class="col-xs-6 col-md-3">
                        <h5>Department</h5>
                        <div class="cus-select">
                          <select class="manager_department">
                              <option value="">Select Department</option>
                              @foreach ($array_department as $code)
                                 <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Level</h5>
                        <div class="cus-select">
                          <select class="manager_level">
                            <option value="">Select Level</option>
                              @foreach ($array_level as $code)
                                 <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Function</h5>
                        <div class="cus-select">
                         <select class="manager_function">
                            <option value="">Select Function</option>
                              @foreach ($array_function as $code)
                                 <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                              @endforeach
                         </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Status</h5>
                        <div class="cus-select">
                          <select class="manager_status">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-9">
                        <h5>Search</h5>
                        <input class="form-control" type="text" id="searchmanagertxt">
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>No of Learners</h5>
                        <div>
                            <div class="slider slider-circle slider-primary" data-slider="primary" data-min="0" data-max="200" data-start="[1, 500]" data-step="0.5" data-connect="true" data-target='["#from", "#to"]'></div>
                            Range: 
                            <span id="from">1</span> — 
                            <span id="to">500</span>
                        </div>
                      </div>
                      <div class="col-xs-12 col-md-4">
                        <h5></h5>
                        <button class="btn btn-primary" id="user_searchButton" type="submit">Search</button>
                        <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="manager_sorting_column_name" id="manager_sorting_column_name">
                  <table class="table table-hover table-striped manager_record" cellspacing="0" width="100%">
                    <thead></thead>
                    <tbody>
                    </tbody>
                  </table>
                  <?php
                    if($request->session()->has('user_hidden_column_array')){
                        $user_hidden_column_array = $request->session()->get('user_hidden_column_array');?>
                        <input type="hidden" name="user_hidden_column_array" id="user_hidden_column_array" value="<?=$user_hidden_column_array?>"><?php
                    }else{?>
                        <input type="hidden" name="user_hidden_column_array" id="user_hidden_column_array" value="<?=$default_col_order_string?>"><?php
                    }
                  ?>
                </div>
              </div>
              <!--tab 4 start-->
              <div class="tab-pane fade expert" id="tab-4">
                <?php
                  $default_col_order_string="CheckAll-1,EMPCode-1,Name-1,Email-1,Mobile-1,Dept-1,Function-1,Level-1,Manager-1,Courses-1,CourseEnrolled-1,Action-1";?>
                  <div class="table-content-header">
                    <div class="table-toolbar">
                      <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                        <div class="btn-group bulk common_action hide">
                          <label>Bulk Action :</label>
                          <button class="btn btn-link link-muted click_common_action" tab_name="expert" to_do="enable" type="button">
                            <span>Enable</span>
                          </button> 
                          <span class="pipe">|</span>
                          <button class="btn btn-link link-muted click_common_action" tab_name="expert" to_do="disable" type="button">
                            <span>Disable</span>
                          </button>
                          <span class="pipe">|</span>
                          <button class="btn btn-link link-muted click_common_action" tab_name="expert" to_do="delete" type="button">
                            <span>Delete</span>
                          </button>
                        </div>
                      </div>
                      <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                        <div class="btn-group export">
                          <a href="{{url('/admin/user_excel/expert')}}" type="button" class="btn btn-link link-muted">
                            <span class="icon icon-file-excel-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export Excel"></span>
                            <span class="hidden-xs">Export Excel</span>
                          </a>
                          <a href="{{url('/admin/user_pdf/expert')}}" type="button" class="btn btn-link link-muted">
                            <span class="icon icon-file-pdf-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export PDF"></span>
                            <span class="hidden-xs">Export PDF</span>
                          </a>
                          <div class="filter-opt1">
                            <button class="btn btn-link col" type="button">
                              <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                              <span class="hidden-xs">Column</span>
                            </button>
                            <div class="listing-det overrlay">
                              <ul class="list-group sortable_menu">
                                <?php
                                //if session has value display column according or display default array
                                if($request->session()->has('expert_hidden_column_array')){
                                  $col_arr_value = $request->session()->get('expert_hidden_column_array');
                                }else{
                                  $col_arr_value=$default_col_order_string;
                                }
                                $col_arr_value=explode(",",$col_arr_value);

                                foreach ($col_arr_value as $key => $value) {
                                    //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                    $value=explode('-', $value);
                                    $class="";
                                    if($value[0]=='CheckAll' || $value[0]=='Action')
                                      $class='non_sortable';
                                    ?>
                                    <li class="list-group-item list-order-item <?=$class?>" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                      <span class="pull-right">
                                        <label class="switch switch-primary">
                                          <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                          <span class="switch-track"></span>
                                          <span class="switch-thumb"></span>
                                        </label>
                                      </span>
                                      <span class="icon icon-arrows"></span>
                                      <?php
                                      if ($value[0]=="CheckAll")
                                        echo "Check All";
                                      else
                                        echo $value[0];
                                      ?>
                                      </li>
                                    <?php
                                  }
                                ?>
                                
                                <button class="btn btn-primary btn-half save_order" form_name="expert" type="button">Save</button>
                                <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="btn-group">
                          <a href="{{url('/admin/add_user_form/Expert')}}" type="button" class="btn btn-primary" style="margin-top:-5px;">
                            <span class="hidden-xs">Add Expert</span>
                          </a>
                        </div>
                        <div class="btn-group">
                          <a href="#" class="refine_mbutton" title="Filter"></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="table-content-body"> 
                    <div class="Cus_card filter-bg">
                      <div class="Cus_card-body" style="display: none;">
                        <div class="col-xs-6 col-md-4">
                          <h5>Department</h5>
                            <div class="cus-select">
                              <select class="expert_department" id="dropdown_expert_department_id">
                                <option value="">Select Department</option>
                                @foreach ($array_department as $code)
                                   <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                                @endforeach
                              </select>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Level</h5>
                          <div class="cus-select">
                            <select class="expert_level" id="dropdown_expert_level_id">
                              <option value="">Select Level</option>
                                @foreach ($array_level as $code)
                                   <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Function</h5>
                          <div class="cus-select">
                            <select class="expert_function" id="dropdown_expert_function_id">
                              <option value="">Select Function</option>
                                @foreach ($array_function as $code)
                                   <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                                @endforeach
                           </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Expertise Areas</h5>
                          <div class="cus-select">
                             <select class="expert_area" id="dropdown_expert_area_id">
                              <option value="">Select Expertise Area</option>
                                @foreach ($array_expert_area as $code)
                                   <option value="{{$code['tda_id']}}"> {{$code['tda_development']}}</option> 
                                @endforeach
                           </select>
                           </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Competencies</h5>
                          <div class="cus-select">
                            <select class="expert_competency" id="dropdown_expert_competency_id">
                              <option value="">Select Competencies</option>
                                @foreach ($array_competency as $code)
                                   <option value="{{$code['tcm_id']}}"> {{$code['tcm_competency_title']}}</option>
                                @endforeach
                           </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Courses</h5>
                           <div class="cus-select">
                             <select class="expert_courses" id="dropdown_expert_course_id">
                              <option value="">Select Courses</option>
                                  @foreach ($array_courses as $code)
                                     <option value="{{$code['tc_id']}}"> {{$code['tc_title']}}</option> 
                                  @endforeach
                              </select>
                           </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                          <h5>Status</h5>
                          <div class="cus-select">
                            <select class="expert_status" id="dropdown_expert_status_id">
                              <option value="0">Active</option>
                              <option value="1">Inactive</option>
                          </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                          <h5>Rating</h5>
                          <div class="cus-select">
                                 <select>
                                 <option value="A">A</option>
                                 <option value="B">B</option>
                                 </select>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                          <h5>Search</h5>
                          <input class="form-control" type="text" id="searchexperttxt">
                        </div>


                        <div class="col-xs-12 col-md-4">
                          <h5></h5>
                          <button class="btn btn-primary" id="expert_searchButton" type="submit">Search</button>
                          <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="expert_sorting_column_name" id="expert_sorting_column_name">
                    <table class="table table-hover table-striped expert_record" cellspacing="0" width="100%">
                      <thead></thead>
                      <tbody>
                      </tbody>
                    </table>
                    <?php
                      if($request->session()->has('expert_hidden_column_array')){
                          $expert_hidden_column_array = $request->session()->get('expert_hidden_column_array');?>
                          <input type="hidden" name="expert_hidden_column_array" id="expert_hidden_column_array" value="<?=$expert_hidden_column_array?>"><?php
                      }else{?>
                          <input type="hidden" name="expert_hidden_column_array" id="expert_hidden_column_array" value="<?=$default_col_order_string?>"><?php
                      }
                    ?>
                  </div>
              </div>
              <!--tab 5 start-->
              <div class="tab-pane fade hr" id="tab-5">
                <?php
                  $default_col_order_string="CheckAll-1,EMPCode-1,Name-1,Email-1,Mobile-1,Dept-1,Function-1,Level-1,Manager-1,Courses-1,CourseEnrolled-1,Action-1";?>
                  <div class="table-content-header">
                    <div class="table-toolbar">
                      <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                        <div class="btn-group bulk common_action hide">
                          <label>Bulk Action :</label>
                          <button class="btn btn-link link-muted click_common_action" tab_name="hr" to_do="enable" type="button">
                            <span>Enable</span>
                          </button> 
                          <span class="pipe">|</span>
                          <button class="btn btn-link link-muted click_common_action" tab_name="hr" to_do="disable" type="button">
                            <span>Disable</span>
                          </button>
                          <span class="pipe">|</span>
                          <button class="btn btn-link link-muted click_common_action" tab_name="hr" to_do="delete" type="button">
                            <span>Delete</span>
                          </button>
                        </div>
                      </div>
                      <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                        <div class="btn-group export">
                          <a href="{{url('/admin/user_excel/hr')}}" type="button" class="btn btn-link link-muted">
                            <span class="icon icon-file-excel-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export Excel"></span>
                            <span class="hidden-xs">Export Excel</span>
                          </a>
                          <a href="{{url('/admin/user_pdf/hr')}}" type="button" class="btn btn-link link-muted">
                            <span class="icon icon-file-pdf-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export PDF"></span>
                            <span class="hidden-xs">Export PDF</span>
                          </a>
                          <div class="filter-opt1">
                            <button class="btn btn-link col" type="button">
                              <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                              <span class="hidden-xs">Column</span>
                            </button>
                            <div class="listing-det overrlay">
                              <ul class="list-group sortable_menu">
                                <?php
                                //if session has value display column according or display default array
                                if($request->session()->has('hr_hidden_column_array')){
                                  $col_arr_value = $request->session()->get('hr_hidden_column_array');
                                }else{
                                  $col_arr_value=$default_col_order_string;
                                }
                                $col_arr_value=explode(",",$col_arr_value);

                                foreach ($col_arr_value as $key => $value) {
                                    //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                    $value=explode('-', $value);
                                    $class="";
                                    if($value[0]=='CheckAll' || $value[0]=='Action')
                                      $class='non_sortable';
                                    ?>
                                    <li class="list-group-item list-order-item <?=$class?>" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                      <span class="pull-right">
                                        <label class="switch switch-primary">
                                          <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                          <span class="switch-track"></span>
                                          <span class="switch-thumb"></span>
                                        </label>
                                      </span>
                                      <span class="icon icon-arrows"></span>
                                      <?php
                                      if ($value[0]=="CheckAll")
                                        echo "Check All";
                                      else
                                        echo $value[0];
                                      ?>
                                      </li>
                                    <?php
                                  }
                                ?>
                                
                                <button class="btn btn-primary btn-half save_order " form_name="hr" type="button">Save</button>
                                <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="btn-group">
                          <a href="{{url('/admin/add_user_form/HR')}}" type="button" class="btn btn-primary" style="margin-top:-5px;">
                            <span class="hidden-xs">Add HR</span>
                          </a>
                        </div>
                        <div class="btn-group">
                          <a href="#" class="refine_mbutton" title="Filter"></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="table-content-body"> 
                    <div class="Cus_card filter-bg">
                      <div class="Cus_card-body" style="display: none;">
                        <div class="col-xs-6 col-md-3">
                          <h5>Department</h5>
                          <div class="cus-select">
                            <select class="hr_department">
                                <option value="">Select Department</option>
                                @foreach ($array_department as $code)
                                   <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                          <h5>Level</h5>
                          <div class="cus-select">
                            <select class="hr_level">
                              <option value="">Select Level</option>
                                @foreach ($array_level as $code)
                                   <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                          <h5>Function</h5>
                          <div class="cus-select">
                           <select class="hr_function">
                              <option value="">Select Function</option>
                                @foreach ($array_function as $code)
                                   <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                                @endforeach
                           </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                          <h5>Status</h5>
                          <div class="cus-select">
                            <select class="hr_status">
                              <option value="0">Active</option>
                              <option value="1">Inactive</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-9">
                          <h5>Search</h5>
                          <input class="form-control" type="text" id="searchhrtxt">
                        </div>
                        <div class="col-xs-6 col-md-3">
                          <h5>No of Learners</h5>
                          <div>
                              <div class="slider slider-circle slider-primary" data-slider="primary" data-min="0" data-max="200" data-start="[1, 500]" data-step="0.5" data-connect="true" data-target='["#from", "#to"]'></div>
                              Range: 
                              <span id="from">1</span> — 
                              <span id="to">500</span>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                          <h5></h5>
                          <button class="btn btn-primary" id="hr_searchButton" type="submit">Search</button>
                          <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="hr_sorting_column_name" id="hr_sorting_column_name">
                    <table class="table table-hover table-striped hr_record" cellspacing="0" width="100%">
                      <thead></thead>
                      <tbody>
                      </tbody>
                    </table>
                    <?php
                      if($request->session()->has('hr_hidden_column_array')){
                          $hr_hidden_column_array = $request->session()->get('hr_hidden_column_array');?>
                          <input type="hidden" name="hr_hidden_column_array" id="hr_hidden_column_array" value="<?=$hr_hidden_column_array?>"><?php
                      }else{?>
                          <input type="hidden" name="hr_hidden_column_array" id="hr_hidden_column_array" value="<?=$default_col_order_string?>"><?php
                      }
                    ?>
                  </div>
              </div>
              <!--tab 6 instructor start-->
              <div class="tab-pane fade instructor" id="tab-6">
                <?php
                  $default_col_order_string="CheckAll-1,EMPCode-1,Name-1,Email-1,Mobile-1,Dept-1,Function-1,Level-1,Manager-1,Courses-1,CourseEnrolled-1,Action-1";?>
                  <div class="table-content-header">
                    <div class="table-toolbar">
                      <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                        <div class="btn-group bulk common_action hide">
                          <label>Bulk Action :</label>
                          <button class="btn btn-link link-muted click_common_action" tab_name="instructor" to_do="enable" type="button">
                            <span>Enable</span>
                          </button> 
                          <span class="pipe">|</span>
                          <button class="btn btn-link link-muted click_common_action" tab_name="instructor" to_do="disable" type="button">
                            <span>Disable</span>
                          </button>
                          <span class="pipe">|</span>
                          <button class="btn btn-link link-muted click_common_action" tab_name="instructor" to_do="delete" type="button">
                            <span>Delete</span>
                          </button>
                        </div>
                      </div>
                      <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                        <div class="btn-group export">
                          <a href="{{url('/admin/user_excel/instructor')}}" type="button" class="btn btn-link link-muted">
                            <span class="icon icon-file-excel-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export Excel"></span>
                            <span class="hidden-xs">Export Excel</span>
                          </a>
                          <a href="{{url('/admin/user_pdf/instructor')}}" type="button" class="btn btn-link link-muted">
                            <span class="icon icon-file-pdf-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export PDF"></span>
                            <span class="hidden-xs">Export PDF</span>
                          </a>
                          <div class="filter-opt1">
                            <button class="btn btn-link col" type="button">
                              <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                              <span class="hidden-xs">Column</span>
                            </button>
                            <div class="listing-det overrlay">
                              <ul class="list-group sortable_menu">
                                <?php
                                //if session has value display column according or display default array
                                if($request->session()->has('instructor_hidden_column_array')){
                                  $col_arr_value = $request->session()->get('instructor_hidden_column_array');
                                }else{
                                  $col_arr_value=$default_col_order_string;
                                }
                                $col_arr_value=explode(",",$col_arr_value);

                                foreach ($col_arr_value as $key => $value) {
                                    //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                    $value=explode('-', $value);
                                    $class="";
                                    if($value[0]=='CheckAll' || $value[0]=='Action')
                                      $class='non_sortable';
                                    ?>
                                    <li class="list-group-item list-order-item <?=$class?>" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                      <span class="pull-right">
                                        <label class="switch switch-primary">
                                          <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                          <span class="switch-track"></span>
                                          <span class="switch-thumb"></span>
                                        </label>
                                      </span>
                                      <span class="icon icon-arrows"></span>
                                      <?php
                                      if ($value[0]=="CheckAll")
                                        echo "Check All";
                                      else
                                        echo $value[0];
                                      ?>
                                      </li>
                                    <?php
                                  }
                                ?>
                                
                                <button class="btn btn-primary btn-half save_order " form_name="instructor" type="button">Save</button>
                                <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="btn-group">
                          <a href="{{url('/admin/add_user_form/Instructor')}}" type="button" class="btn btn-primary" style="margin-top:-5px;">
                            <span class="hidden-xs">Add Instructor</span>
                          </a>
                        </div>
                        <div class="btn-group">
                          <a href="#" class="refine_mbutton" title="Filter"></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="table-content-body"> 
                    <div class="Cus_card filter-bg">
                      <div class="Cus_card-body" style="display: none;">
                       <div class="col-xs-6 col-md-4">
                          <h5>Department</h5>
                            <div class="cus-select">
                              <select class="instructor_department" id="">
                                <option value="">Select Department</option>
                                @foreach ($array_department as $code)
                                   <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                                @endforeach
                              </select>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Level</h5>
                          <div class="cus-select">
                            <select class="instructor_level" id="">
                              <option value="">Select Level</option>
                                @foreach ($array_level as $code)
                                   <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Function</h5>
                          <div class="cus-select">
                            <select class="instructor_function" id="">
                              <option value="">Select Function</option>
                                @foreach ($array_function as $code)
                                   <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                                @endforeach
                           </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Expertise Areas</h5>
                          <div class="cus-select">
                             <select class="instructor_area" id="">
                              <option value="">Select Expertise Area</option>
                                @foreach ($array_expert_area as $code)
                                   <option value="{{$code['tda_id']}}"> {{$code['tda_development']}}</option> 
                                @endforeach
                           </select>
                           </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Competencies</h5>
                          <div class="cus-select">
                            <select class="instructor_competency" id="">
                              <option value="">Select Competencies</option>
                                @foreach ($array_competency as $code)
                                   <option value="{{$code['tcm_id']}}"> {{$code['tcm_competency_title']}}</option>
                                @endforeach
                           </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                          <h5>Courses</h5>
                           <div class="cus-select">
                             <select class="instructor_courses" id="">
                              <option value="">Select Courses</option>
                                  @foreach ($array_courses as $code)
                                     <option value="{{$code['tc_id']}}"> {{$code['tc_title']}}</option> 
                                  @endforeach
                              </select>
                           </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                          <h5>Status</h5>
                          <div class="cus-select">
                            <select class="instructor_status" id="dropdown_expert_status_id">
                              <option value="0">Active</option>
                              <option value="1">Inactive</option>
                          </select>
                          </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                          <h5>Rating</h5>
                          <div class="cus-select">
                            <select>
                              <option value="A">A</option>
                              <option value="B">B</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                          <h5>Search</h5>
                          <input class="form-control" type="text" id="searchinstructortxt">
                        </div>
                        <div class="col-xs-12 col-md-4">
                          <h5></h5>
                          <button class="btn btn-primary" id="instructor_searchButton" type="submit">Search</button>
                          <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="instructor_sorting_column_name" id="instructor_sorting_column_name">
                    <table class="table table-hover table-striped instructor_record" cellspacing="0" width="100%">
                      <thead></thead>
                      <tbody>
                      </tbody>
                    </table>
                    <?php
                      if($request->session()->has('instructor_hidden_column_array')){
                          $instructor_hidden_column_array = $request->session()->get('instructor_hidden_column_array');?>
                          <input type="hidden" name="instructor_hidden_column_array" id="instructor_hidden_column_array" value="<?=$instructor_hidden_column_array?>"><?php
                      }else{?>
                          <input type="hidden" name="instructor_hidden_column_array" id="instructor_hidden_column_array" value="<?=$default_col_order_string?>"><?php
                      }
                    ?>
                  </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
    

<script src="{{url('public/adminlte/js/user/UserController.js') }}"></script> 
@stop