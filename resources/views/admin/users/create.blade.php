@extends('layouts.app')

@section('content')
  <div class="title-bar clear">
    <div class="col-xs-12 col-sm-8 col-md-8">
      <h1 class="title-bar-title">
          <i class="icon icon-file-text"></i>
          <span class="d-ib">{{Config::get('messages.add_user.ADD_USER_TITLE_BAR')}}</span>
      </h1>
      <p class="title-bar-description">
        <small>{{Config::get('messages.add_user.ADD_USER_TITLE_BAR_DESCRIPTION')}}</small>
      </p>
    </div>
  <div class="col-xs-12 col-sm-5 col-md-4 ad_top">
      <button style="right: 1px;" class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
  </div>    
  </div>      
  <div class="layout-content-body">
    <div class="row gutter-xs">
      <div class="col-xs-12">
        <div class="demo-form-wrapper">
          <div class="panel con-lib">
            <div class="tab-menu">
              <?php  
                  if(count($array_data)>0){
                    $team_user_list = array_column($team_user_list, 'tlt_team_id');
                    $expertise_user_list = array_column($expertise_user_list, 'tudr_development_id');
                    $role_user_list=array_column($role_user_list, 'tutr_user_type_id');
                    $permission_user_list=array_column($permission_user_list, 'role_id');
                    //print_r($permission_user_list);
                    $hidden_user_id=$array_data[0]['id'];
                    $employee_code=$array_data[0]['employee_code'];
                    $user_lastname=$array_data[0]['user_lastname'];
                    $user_firstname=$array_data[0]['user_firstname'];
                    $user_middlename=$array_data[0]['user_middlename'];
                    $country=$array_data[0]['country'];
                    $state=$array_data[0]['state'];
                    $city=$array_data[0]['city'];
                    $email=$array_data[0]['email'];
                    $user_function=$array_data[0]['user_function'];
                    $user_level=$array_data[0]['user_level'];
                    $user_department=$array_data[0]['user_department'];
                    $user_address_line1=$array_data[0]['user_address_line1'];
                    $user_address_line2=$array_data[0]['user_address_line2'];
                    $user_blog_url=$array_data[0]['user_blog_url'];
                    $user_twitter_url=$array_data[0]['user_twitter_url'];
                    $user_linkedin_url=$array_data[0]['user_linkedin_url'];
                    $user_bio=$array_data[0]['user_bio'];
                    $user_skip_manager=$array_data[0]['user_skip_manager'];
                    $profile_image=$array_data[0]['profile_image'];
                    $pincode=$array_data[0]['pincode'];
                    $website_url=$array_data[0]['website_url'];
                    $gender=$array_data[0]['gender'];
                    $mobile_no=$array_data[0]['mobile_no'];
                    $user_manager=$array_data[0]['user_manager'];
                  }else{
                    $team_user_list=array();
                    $expertise_user_list=array();
                    $permission_user_list=array();
                    $role_user_list=array();
                    $hidden_user_id="0";
                    $employee_code="";
                    $user_lastname="";
                    $user_firstname="";
                    $user_middlename="";
                    $country="";
                    $state="";$city="";
                    $email="";
                    $user_function="";
                    $user_level="";
                    $user_department="";
                    $user_address_line1="";
                    $user_address_line2="";
                    $user_blog_url="";
                    $user_twitter_url="";
                    $user_linkedin_url="";
                    $user_bio="";
                    $user_skip_manager="";
                    $profile_image="";
                    $pincode="";
                    $website_url="";
                    $gender="";
                    $mobile_no="";
                    $user_manager="";
                  }
                  if($hidden_user_id==0){
                    $tab_class="disabled";
                    $tab_name="";
                  }else{
                    $tab_class="";
                    $tab_name="data-toggle='tab'";
                  }
                  ?>
              <ul class="nav nav-tabs m-t-lg" id="demoTabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Basic Details</a></li>
                <li class="<?=$tab_class?>" ><a href="#tab-2" <?=$tab_name?>  >Psychometric Test</a></li>
                <li class="<?=$tab_class?>"><a href="#tab-3" <?=$tab_name?> >Courses</a></li>
                <li class="<?=$tab_class?>"><a href="#tab-4" <?=$tab_name?> >Learning Path</a></li>
             </ul>
            </div>
            <div class="tab-content">

              <div class="tab-pane fade active in" id="tab-1">
                
                <form action="#" method="post" enctype="multipart/form-data" id="Frmadduser">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="hidden_user_id" id="hidden_user_id" value="<?=$hidden_user_id?>">
                  <input type="hidden" name="show_extra_details_edit" id="show_extra_details_edit" value="<?=((in_array(4, $role_user_list))?"1":"0")?>">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="select-usr-type clearfix">
                        <div class="col-sm-3 col-md-3 col-lg-2">Select User Type</div>
                        <?php if($hidden_user_id>0){?>
                          <div class="col-sm-9 col-md-9 col-lg-10">
                            @foreach ($user_type_master as $user)
                              <label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                <input class="custom-control-input  click_user_type_master <?=(($user['tutm_user_type']=="Expert" || $user['tutm_user_type']=="Instructor")?"show_employee_details":"")?>"  type="checkbox" value="{{ $user['tutm_id']}}" data_attr="{{ $user['tutm_id']}}"   name="user_type[]" id="user_type"  <?=((in_array($user['tutm_id'], $role_user_list))?"checked":"")?> >
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">{{$user['tutm_user_type']}} </span>
                              </label>
                            @endforeach

                            
                          </div><?php
                        }else{
                          $hidden_user_type=Request::segment(3);
                          
                          ?>
                          <input type="hidden" name="hidden_user_type" id="hidden_user_type" value="<?=$hidden_user_type?>">
                          <div class="col-sm-9 col-md-9 col-lg-10">
                            @foreach ($user_type_master as $user)
                              <label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                <input class="custom-control-input chk_value click_user_type_master <?=(($user['tutm_user_type']=="Expert" || $user['tutm_user_type']=="Instructor")?"show_employee_details":"")?>" data_attr="{{$user['tutm_id']}}" user_type_val="{{$user['tutm_user_type']}}"  type="checkbox" value="{{ $user['tutm_id']}}"  name="user_type[]" id="user_type"    >
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">{{$user['tutm_user_type']}} </span>
                              </label>
                            @endforeach
                         
                        </div>

                        <?php
                        }?>
                        <div class="col-md-12"><label id="err_user_type" class="error"></label></div>
                      </div>
                    </div>
                    <div class="col-sm-8 col-md-9">
                      <div class="user-detail-heading">
                        <div class="prof-icon">
                          <span class="icon icon-user"></span>
                        </div>
                        <div class="media-body">
                          <h5 class="media-title">Employee Detail</h5>
                        </div>
                      </div>
                      
                      <div class="row m-b-lg">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>Employee Code<span class="color-r">*</span></h5>
                          <input class="form-control" type="text" id="employee_code" name="employee_code" value="<?php echo $employee_code ?>"> 
                          <span id="err_employee_code" class="error"></span> 
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>Function</h5>
                          <div class="cus-select">
                              <select class="all_function" id="user_function" name="user_function">
                                  <option value="">Select Function</option>
                                    @foreach ($array_function as $code)
                                       <option value="{{$code['tfm_id']}}" <?php echo ($user_function == $code['tfm_id'])?"selected":"" ?>> {{$code['tfm_function']}}</option> 
                                    @endforeach
                              </select>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>Department</h5>
                          <div class="cus-select">
                              <select class="" id="user_department" name="user_department">
                                    <option value="">Select Department</option>
                                    @foreach ($array_department as $code)
                                       <option value="{{$code['tdm_id']}}" <?php echo ($user_department == $code['tdm_id'])?"selected":"" ?>> {{$code['tdm_department']}}</option> 
                                    @endforeach
                              </select>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <h5>First Name<span class="color-r">*</span></h5>
                             <input class="form-control" type="text" value="<?=$user_firstname?>" id="user_firstname" name="user_firstname">
                             <span id="err_user_firstname" class="error"></span>    
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <h5>Middle Name<span class="color-r">*</span></h5>
                             <input class="form-control" type="text" value="<?=$user_middlename?>" id="user_middlename" name="user_middlename">
                            <span id="err_user_middlename" class="error"></span>   
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <h5>Last Name<span class="color-r">*</span></h5>
                             <input class="form-control" type="text" value="<?=$user_lastname?>" id="user_lastname" name="user_lastname"> 
                             <span id="err_user_lastname" class="error"></span>   
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>Gender<span class="color-r">*</span></h5>
                           <div class="cus-select">
                            <select id="gender" name="gender">
                              <option value="M" <?php echo ($gender == "M")?"selected":"" ?> >Male</option>
                              <option value="F" <?php echo ($gender == "F")?"selected":"" ?>>Female</option>
                            </select>
                            <span id="err_gender" class="error"></span>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <h5>Email Id<span class="color-r">*</span></h5>
                            <input class="form-control" type="text" id="email" value="<?=$email?>" name="email">
                            <span id="err_email" class="error"></span>   
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <h5>Mobile Number<span class="color-r">*</span></h5>
                             <input class="form-control numericOnly" type="text" id="mobile_no"  value="<?=$mobile_no?>" name="mobile_no">
                             <span id="err_mobile_no" class="error"></span>   
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <h5>Level <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config::get('messages.add_user.USER_LEVEL_TOOLTIP')}}">?</span></h5>
                            <div class="cus-select">
                            <select class="" id="user_level" name="user_level">
                                <option value="">Select Level</option>
                                  @foreach ($array_level as $code)
                                     <option value="{{$code['tsl_id']}}" <?php echo ($user_level == $code['tsl_id'])?"selected":"" ?>> {{$code['tsl_seniority']}}</option> 
                                  @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <h5>Manager</h5>
                            <div class="cus-select">
                                <select class="" id="user_manager" name="user_manager">
                                    <option value="">Select Manager</option>
                                      @foreach ($manager_list as $manager)
                                         <option value="{{$manager['id']}}" <?php echo ($user_manager == $manager['id'])?"selected":"" ?>> {{$manager['user_firstname'].' '.$manager['user_lastname']}}</option> 
                                      @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <h5>Skip Manager</h5>
                            <div class="cus-select">
                            <select class="" id="user_skip_manager" name="user_skip_manager">
                                <option value="">Select Skip Manager</option>
                                  @foreach ($manager_list as $manager)
                                     <option value="{{$manager['id']}}" <?php echo ($user_skip_manager == $manager['id'])?"selected":"" ?>> {{$manager['user_firstname'].' '.$manager['user_lastname']}}</option> 
                                  @endforeach
                            </select>
                             </div>
                        </div>
                      </div> 
                      <div class="user-detail-heading">
                        <div class="prof-icon">
                          <span class="icon icon-user"></span>
                        </div>
                        <div class="media-body">
                          <h5 class="media-title">Address</h5>
                        </div>
                      </div>
                      <div class="row m-b-lg">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>Address Line 1</h5>
                          <input class="form-control" type="text" value="<?=$user_address_line1?>" id="user_address_line1" name="user_address_line1">   
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>Address Line 2</h5>
                          <input class="form-control" type="text" value="<?=$user_address_line2?>"  id="user_address_line2" name="user_address_line2">   
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>City</h5>
                          <input class="form-control" type="text" id="city" value="<?=$city?>" name="city">   
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>State</h5>
                          <div class="cus-select"">
                            <?php $edit_state=trim($state);?>
                            <select id="state" name="state" class="change_state">

                              @foreach ($stateList as $state)
                               <option value="{{$state['id']}}" 
                               <?php echo ( $state['id'] == $edit_state)?"selected='selected'":"" ?>> {{$state['state_name']}}</option> 
                              @endforeach
                              
                            </select>
                          
                          </div>   
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>Country</h5>
                          <div class="cus-select"">
                            <?php $edit_country=trim($country);?>
                            <select class="change_country" id="user_country" name="user_country">
                              <option value="">Select Country</option>
                                @foreach ($countrylist as $country)
                                     <option value="{{$country['id']}}" 
                                     <?php echo ( $country['id'] == $edit_country)?"selected='selected'":"" ?>> {{$country['country_name']}}</option> 
                                @endforeach
                                
                            </select> 
                          </div> 
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                          <h5>Zip Code</h5>
                          <input class="form-control" type="text" value="<?=$pincode?>" id="pincode" name="pincode">   
                        </div>
                      </div>
                      
                        <div class="hide_employee_details">
                          <div class="user-detail-heading">
                           <div class="prof-icon">
                           <span class="icon icon-user"></span>
                           </div>
                           <div class="media-body">
                           <h5 class="media-title">Additional Details</h5>
                           </div>
                              </div>
                              <div class="row">
                                <div class="col-md-8">
                                  <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                      <h5>Instructor / Expert website URL</h5>
                                      <input class="form-control" value="<?=$website_url?>" type="text" id="website_url" name="website_url">
                                      <h5>Instructor / Expert Blog URL</h5>
                                      <input class="form-control" value="<?=$user_blog_url?>" type="text" id="user_blog_url" name="user_blog_url">
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                      <h5>Twitter URL</h5>
                                      <input class="form-control" type="text" value="<?=$user_twitter_url?>" id="user_twitter_url" name="user_twitter_url">
                                      <h5>Linked in URL</h5>
                                      <input class="form-control" type="text" value="<?=$user_linkedin_url?>" id="user_linkedin_url" name="user_linkedin_url">
                                    </div>
                                   </div>
                                </div>
                                <div class="col-md-4">
                                  <h5>Experties Area <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config::get('messages.add_user.EXPERT_AREA_TOOLTIP')}}">?</span></h5>
                                  <div class="cus-select">
                                      <select class="ms" multiple="multiple" name="development_area[]" id="development_area">
                                          @foreach ($expert_area_list as $expert)
                                              <option value="{{$expert['tda_id']}}" <?=((in_array($expert['tda_id'], $expertise_user_list))?"selected":"")?>> 
                                                {{$expert['tda_development']}}
                                              </option> 
                                          @endforeach
                                      </select>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <h5>Add Bio <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config::get('messages.add_user.USER_BIO_TOOLTIP')}}">?</span></h5>
                                  <textarea class="form-control" rows="5" id="user_bio" name="user_bio">
                                    <?=$user_bio?>
                                    
                                  </textarea>
                                </div>
                              </div>
                        </div>    
                     
                      
                    </div>
                    <div class="col-sm-4 col-md-3 clearfix">
                      <div class="user-avatar">
                        <label class="error" id="err_image_type"></label>
                        <?php if($profile_image!=""){?>
                          <label class="user-btn-remove">
                            <span class="icon icon-times delete_image" title="Remove"></span>
                          </label>
                        <?php } ?>
                       <label class="user-avatar-btn">
                        <span class="icon icon-camera"></span>
                        <input class="file-upload-input" type="file" name="file" id="profile_image" name="profile_image">
                       </label>
                       <?php
                       if($hidden_user_id>0){
                        if($profile_image==""){?>
                          <img class="img-responsive change_image" src="../../public/img/no_user.jpg" width="100%" alt="User"><?php
                        }else{?>
                          <img class="img-responsive change_image" id="replace_image" src="../../public/img/<?=$profile_image?>" width="100%" alt="User">
                          <input type="hidden" name="image_remove" id="image_remove">
                          <?php
                        }

                       }else{?>
                          <img class="img-responsive change_image"  src="../../public/img/no_user.jpg" width="100%" alt="User"><?php
                       }?>
                       
                      </div>
                      <?php
                      //print_r($team_user_list);?>
                      <div class="user-detail-heading">
                         <div class="prof-icon">
                         <span class="icon icon-user"></span>
                         </div>
                         <div class="media-body">
                         <h5 class="media-title">Select Team</h5>
                         </div>
                      </div>

                      <div class="cus-select">
                        <select class="ms" multiple="multiple" id="user_team" name="user_team[]">
                            @foreach ($team_list as $team)
                                <option value="{{$team['tt_id']}}"  
                                <?=((in_array($team['tt_id'], $team_user_list))?"selected":"")?> > {{$team['tt_title']}}</option> 
                            @endforeach
                        </select>

                      </div>
                    </div>
                    
                    <div class="row hide" id="select_permission">
                      <div class="col-md-12">
                        <label class="error" id="err_permission_type"></label>
                        <div class="select-usr-type clearfix">
                        <div class="col-sm-3 col-md-3 col-lg-2">Select Permission Type</div>
                          <div class="col-sm-9 col-md-9 col-lg-10">
                            <div class="row">
                            <?php
                            if($hidden_user_id>0){
                                $labelid=0;
                                foreach ($user_type_master as $user) {
                                  $labelid=$user['tutm_id'];
                                  ?>
                                  <div class="user_type_<?=$labelid?> <?=((in_array($user['tutm_id'], $role_user_list))?"":"hide")?>">
                                    <label><?=$user['tutm_user_type']?></label>
                                    <?php
                                    $get_user_type = DB::select( DB::raw("SELECT * FROM roles WHERE user_type=".(int)$user['tutm_id'].""));
                                    $get_user_type=json_decode(json_encode($get_user_type),true);
                                    foreach ($get_user_type as  $type) {?>

                                      <label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                      <input class="custom-control-input"  type="checkbox" value="{{ $type['id']}}" data-attr="{{ $type['id']}}" <?=((in_array($type['id'], $permission_user_list))?"checked":"cdedd")?>  name="permission_type[]" id="permission_type" >
                                      <span class="custom-control-indicator"></span>
                                      <span class="custom-control-label">{{$type['title']}} </span>
                                      </label><?php 
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12"></div><br><?php
                                }
                            }else{
                            $labelid=0;
                            foreach ($user_type_master as $user) {
                              $labelid=$user['tutm_id'];
                              ?>
                              <div class="user_type_<?=$labelid?> hide">
                                <label><?=$user['tutm_user_type']?></label>
                                <?php
                                $get_user_type = DB::select( DB::raw("SELECT * FROM roles WHERE user_type=".(int)$user['tutm_id'].""));
                                $get_user_type=json_decode(json_encode($get_user_type),true);
                                foreach ($get_user_type as  $type) {?>

                                  <label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                  <input class="custom-control-input"  type="checkbox" value="{{ $type['id']}}" data-attr="{{ $type['id']}}"  name="permission_type[]" id="permission_type" >
                                  <span class="custom-control-indicator"></span>
                                  <span class="custom-control-label">{{$type['title']}} </span>
                                  </label><?php 
                                }
                                ?>
                              </div>
                              <div class="col-md-12 col-sm-12 col-xs-12"></div><br><?php
                            }
                          }
                            ?>
                          </div>
                           
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="col-sm-12 col-md-12 m-t-md">
                      <?php if($hidden_user_id>0){ ?>
                        <button type="submit" class="btn btn-primary">Update</button><?php
                      }else{?>
                        <button type="submit" class="btn btn-primary">Add</button><?php
                      }?><button type="button" class="btn btn-default">Cancel</button>
                    </div>
                  </div>
                </form>
              </div>
              
              <div class="tab-pane fade" id="tab-2">
                  <div class="layout-content-body">
                    <div class="row gutter-xs">
                      <div class="col-xs-12">
                        <div class="card">
                            <div class="card-body">
                              <div class="cus-tbl">
                                
                                <table id="example1" class="table table-hover table-striped test_listing" cellspacing="0" width="100%">
                                  <thead>
                                    <tr>
                                        <th>Test</th>
                                        <th>Status</th>
                                    </tr>
                                  </thead>
                                  <tfoot>
                                    <tr>
                                        <th>Test</th>
                                        <th>Status</th>
                                    </tr>
                                  </tfoot>
                                  <tbody>
                                  </tbody> 
                                </table>
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="tab-pane fade" id="tab-3">
                <div class="layout-content-body">
                    <div class="row gutter-xs">
                      <div class="col-xs-12">
                        <div class="card">
                            <div class="card-body">
                              <div class="cus-tbl">
                                <table id="example1" class="table table-hover table-striped course_listing" cellspacing="0" width="100%">
                                  <thead>
                                    <tr>
                                        <th>Course Name</th>
                                        <th>Course Type</th>
                                        <th>Course Categories</th>
                                        <th>Course Competency</th>
                                        <th>Course Development Area</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  </tbody> 
                                </table>
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="tab-pane fade" id="tab-4">
                <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
              </div>
            </div>
          </div>
          </div>
      </div>
    </div>
  </div>
  <script src="{{url('public/adminlte/js/user/add_user.js') }}"></script> 
@stop