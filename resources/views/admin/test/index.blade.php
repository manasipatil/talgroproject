@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')


  <div class="title-bar clear">
      <div class="col-xs-12 col-xs-8 col-md-8">
        <h1 class="title-bar-title">
            <i class="icon icon-list-ul"></i>
            <span class="d-ib">{{Config::get('messages.test.TEST_LIST')}}</span>
        </h1>
        <p class="title-bar-description">
          <small>{{Config::get('messages.test.TEST_TITLE_BAR')}}</small>
        </p>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 ad_top">
        <button class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
        <!-- <button class="btn btn-primary" type="submit">Create Test <i class="icon icon-plus color-o m-l-sm"></i></button> -->
        <a href="{{url('/admin/createTest')}}" type="button" class="btn btn-primary" style="margin-top:-5px;">
                         <span class="hidden-xs">Create Test</span>
                       </a>
        
      </div>
  </div>


  <div class="layout-content-body filter-bg nopadding">
      <div class="row gutter-xs">
          <div class="col-xs-12">
              <div class="Cus_card collapsed ">
                <div class="Cus_card-header fil">
                   <button type="button" class="Cus_card-actions Cus_card-toggler" title="Filter"></button>
                </div>
                <div class="Cus_card-body collapse">
                  <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">  
                    <h5>Test Name</h5>
                    <input class="form-control" type="text" id="searchtxt">
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h5>Parameter</h5>
                    <input class="form-control" type="text" name="txt_parameter" id="txt_parameter">
                    <label class="custom-control custom-control-primary custom-radio">
                      <input class="custom-control-input"  type="radio" name="test_type" id="test_type" value="Internal">
                      <span class="custom-control-indicator"></span>Internal
                    </label>
                    <label class="custom-control custom-control-primary custom-radio">
                      <input class="custom-control-input"  name="test_type" id="test_type"  type="radio" value="External">
                      <span class="custom-control-indicator"></span>External
                    </label>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h5>Associated Course</h5>
                    <div class="cus-select">
                      <select id="dropdown_tte_course_id" name="dropdown_tte_course_id" class="form-control">
                        <option value="">Select Course</option>
                          @foreach ($array_courses as $code)
                             <option value="{{$code['tc_id']}}"> {{$code['tc_title']}}</option> 
                          @endforeach
                      </select>  
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h5>Test Function</h5>
                    <div class="cus-select">
                      <select id="dropdown_tte_test_function" name="dropdown_tte_test_function" class="form-control">
                        <option value="">Select Functions</option>
                          @foreach ($array_code_data as $code)
                             <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                          @endforeach
                      </select>  
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-xs-12 col-md-4">
                    <h5></h5>
                    <button class="btn btn-primary" type="submit" id="test_searchButton">Search</button>
                    <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                  </div>
              
                        </div>
                    </div>
          </div>
      </div>
  </div>
        
  <div class="layout-content-body">
    <div class="row gutter-xs">
      <div class="col-xs-12">
        <div class="card">
            <div class="card-body">
              <?php
              $default_col_order_string="CheckAll-1,TestName-1,Parameters-1,AssociatedCourses-1,TestFunction-1,Date-1,Action-1";
              ?>
              <div class="table-content-header">
                <div class="table-toolbar">
                <div class="table-toolbar-tools pull-sm-left hide_tab">
                  <div class="btn-group bulk hide common_action">
                      <label>Bulk Action :</label>
                      <button class="btn btn-link link-muted click_common_action" to_do="enable" type="button"><span>Enable</span></button> 
                      <span class="pipe">|</span>
                     <button class="btn btn-link link-muted click_common_action"  to_do="disable" type="button"><span>Disable</span></button>
                     <span class="pipe">|</span>
                     <button class="btn btn-link link-muted click_common_action"  to_do="delete" type="button"><span>Delete</span></button>
                  </div>
                </div>
                <div class="table-toolbar-tools pull-lg-right">
                  <div class="btn-group">
                    <a href="{{url('/admin/test_excel')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-excel-o icon-lg icon-fw"></span>
                      <span class="visible-lg-inline">Export Excel</span>
                    </a>
                   
                    <a href="{{url('/admin/test_pdf')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-pdf-o icon-lg icon-fw"></span>
                      <span class="visible-lg-inline">Export PDF</span>
                    </a>
                      <div class="filter-opt1">
                        <button class="btn btn-link link-muted col" type="button">
                        <span class="icon icon-cogs icon-lg icon-fw"></span>
                        <span class="visible-lg-inline">Column</span>
                       </button>
                        <div class="listing-det overrlay">
                            <ul class="list-group sortable_menu">
                                <?php
                                //if session has value display column according or display default array
                                if($request->session()->has('test_hidden_column_array')){
                                  $col_arr_value = $request->session()->get('test_hidden_column_array');
                                }else{
                                  $col_arr_value=$default_col_order_string;
                                }
                                $col_arr_value=explode(",",$col_arr_value);

                                foreach ($col_arr_value as $key => $value) {
                                    //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                    $value=explode('-', $value);
                                    $class="";
                                    if($value[0]=='CheckAll' || $value[0]=='Action')
                                      $class='non_sortable';
                                    ?>
                                    <li class="list-group-item list-order-item <?=$class?>" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                      <span class="pull-right">
                                        <label class="switch switch-primary">
                                          <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                          <span class="switch-track"></span>
                                          <span class="switch-thumb"></span>
                                        </label>
                                      </span>
                                      <span class="icon icon-arrows"></span>
                                      <?php
                                      if ($value[0]=="CheckAll")
                                        echo "Check All";
                                      else if($value[0]=="TestName")
                                        echo "Test Name";
                                      else if($value[0]=="AssociatedCourses")
                                        echo "Associated Courses";
                                      else if($value[0]=="TestFunction")
                                        echo "Test Function";
                                      else
                                        echo $value[0];
                                      ?>
                                      </li>
                                    <?php
                                  }
                                ?>
                              <button class="btn btn-primary btn-half save_order" form_name="test" id="" type="button">Save</button>
                            <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                              </ul>
                        </div>
                     </div>
                    </div>
                  </div>
                 </div>
              </div>
            
              <div class="cus-tbl">
                <table id="example1" class="table table-hover table-striped example" cellspacing="0" width="100%">
                  <tbody>
                  </tbody> 
                </table>
                <?php
                if($request->session()->has('test_hidden_column_array')){
                    $test_hidden_column_array = $request->session()->get('test_hidden_column_array');?>
                    <input type="hidden" name="test_hidden_column_array" id="test_hidden_column_array" value="<?=$test_hidden_column_array?>"><?php
                }else{?>
                    <input type="hidden" name="test_hidden_column_array" id="test_hidden_column_array" value="<?=$default_col_order_string?>"><?php
                }
                ?>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  


<script src="{{url('public/adminlte/js/test/TestController.js') }}"></script> 
@stop