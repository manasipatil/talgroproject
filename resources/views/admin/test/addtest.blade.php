  @inject('request', 'Illuminate\Http\Request')
  @extends('layouts.app')

  @section('content')

<div class="title-bar clear">
	<div class="col-xs-12 col-sm-8 col-md-8">
      <h1 class="title-bar-title">
		<i class="icon icon-file-text"></i>
          <span class="d-ib">Assessment <i class="icon icon-angle-right"></i> Test Details</span>
      </h1>
      <p class="title-bar-description">
        <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
      </p>
	</div>
	
	
  </div>
     
	
			
  <div class="layout-content-body">
      <div class="row gutter-xs">
          <div class="col-xs-12">
		<div class="demo-form-wrapper">
		<div class="panel">
          <div class="select-tp">
		  <h5>Select test type</h5>
            <label class="custom-control custom-control-primary custom-radio">
            <input class="custom-control-input" <?php if (isset($tte_is_internal) && $tte_is_internal=="1") echo "checked";?> name="is_internal" type="radio" value="internal" checked="checked">
            <span class="custom-control-indicator"></span>Internal
          </label>
            <label class="custom-control custom-control-primary custom-radio">
            <input class="custom-control-input" <?php if (isset($tte_is_internal) && $tte_is_internal=="0") echo "checked";?> name="is_internal" value="external" type="radio">
            <span class="custom-control-indicator"></span>External
          </label>
		</div>
			
            <ul class="steps">
              <li class="step col-md-1 active">
                <a class="step-segment" href="#tab-1" data-toggle="tab">
                  <span class="step-icon">1</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Basic Information</strong>
                </div>
              </li>
              <li class="step col-md-1">
                <a class="step-segment" href="#tab-2" data-toggle="tab">
                  <span class="step-icon">2</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Add Questions</strong>
                </div>
              </li>
              <li class="step col-md-1">
                <a class="step-segment" href="#tab-3" data-toggle="tab">
                  <span class="step-icon">3</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Test Settings</strong>
                </div>
              </li>
			<li class="step col-md-1">
                <a class="step-segment" href="#tab-4" data-toggle="tab">
                  <span class="step-icon">4</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">learning Path</strong>
                </div>
              </li>
			<li class="step col-md-1">
                <a class="step-segment" href="#tab-5" data-toggle="tab">
                  <span class="step-icon">5</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Assign Learners</strong>
                </div>
              </li>
			<li class="step col-md-1">
                <a class="step-segment" href="#tab-6" data-toggle="tab">
                  <span class="step-icon">6</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Invite Learners</strong>
                </div>
              </li>
			<li class="step col-md-1">
                <a class="step-segment" href="#tab-7" data-toggle="tab">
                  <span class="step-icon">7</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Results</strong>
                </div>
              </li>
			<li class="step col-md-1">
                <a class="step-segment" href="#tab-8" data-toggle="tab">
                  <span class="step-icon">8</span>
                </a>
                <div class="step-content">
                  <strong class="hidden-xs">Set Learning Path based on test results </strong>
                </div>
              </li>
            </ul>
		<div class="tab-menu">
		 <ul class="nav nav-tabs m-t-lg">
		 <li class="active"><a href="#tab-1" data-toggle="tab">Test Details</a></li>
            <li><a href="#tab-2" data-toggle="tab">Add Questions</a></li>
            <li><a href="#tab-3" data-toggle="tab">Settings</a></li>
		  <li><a href="#tab-4" data-toggle="tab">Create Learning Path</a></li>
		  <li><a href="#tab-5" data-toggle="tab">Assign Learners</a></li>
		  <li><a href="#tab-6" data-toggle="tab">Invite Learners</a></li>
		  <li><a href="#tab-7" data-toggle="tab">Results</a></li>
		  <li><a href="#tab-8" data-toggle="tab">Assign learning path</a></li>
		 </ul>
		 </div>
            <div class="tab-content">
            <div class="tab-pane fade  active in" id="tab-1">
              <div class="over-form" style="left: 40%;top: 45%;display: none;" id="tab1_show_error_msg">
                <div class="alert alert-danger">
                            <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                  <i class="icon icon-times-circle-o"></i><strong>Error!</strong> <span id="tab1_error_content"></span>
                </div>
              </div>
              <div class="over-form" style="left: 40%;top: 45%;display: none;" id="tab1_show_success_msg">
                <div class="alert alert-success">
                  <i class="icon icon-check-circle-o"></i>
                  <strong>Success!</strong> <span id="tab1_success_content"></span>
                </div>
              </div>
			       <div class="row">
			       	{{Form::open(array('id'=>'add_test_form','method'=>'post','enctype'=>'multipart/form-data'))}}
		            <div class="col-xs-12 col-sm-6  col-md-6 col-lg-3">  
                          <h5>Test Name <span class="color-r">*</span></h5>
						<div class="cus-select">
						{!! Form::text('test_name', '', ['class' => 'form-control','id'=>'test_title_id', 'placeholder' => '']) !!}	
						</div>
                      </div>
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                          <h5>Test Function</h5>
						<div class="cus-select">
                          <select class="ms" id="test_func_id" multiple="multiple">
                            @foreach ($testFunctionList as $testFunction)
                                <option value="{{$testFunction['tfm_id']}}" <?=((in_array($testFunction['tfm_id'], $selected_function_list))?"selected":"")?>> 
                                  {{$testFunction['tfm_function']}}
                                </option> 
                            @endforeach
                           </select>
						</div>
                      </div>
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
						<div class="row">
						<div class="col-xs-6 col-md-6">
                          <h5>Start Date</h5>
						<div class="input-with-icon">
                            <input class="form-control" style="padding-left: 1px;" type="text" id="start_date_id" data-provide="datepicker" data-date-today-btn="linked">
                            <span class="icon icon-calendar input-icon"></span>
                          </div>
                          </div>
						
						<div class="col-xs-6 col-md-6">
                          <h5>End Date</h5>
                          <div class="input-with-icon">
                            <input class="form-control" style="padding-left: 1px;" type="text" id="end_date_id" data-provide="datepicker" data-date-today-btn="linked">
                            <span class="icon icon-calendar input-icon"></span>
                          </div>
						</div>
						</div>
                      </div>
			        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">  
                      <h5>Test Duration</h5>
			        <div class="">
			        	<input id="test_duration_id" class="form-control" type="text">
                      <!-- <input id="demo-timepicker-1" class="form-control" type="text">
                      <span class="icon icon-clock-o input-icon"></span> -->
                      </div>
			        </div>
			        <div class="col-xs-12 col-md-12 col-lg-12">  
                      <h5>Description</h5>
			        <!-- <textarea class="form-control" rows="5"></textarea> -->
			        {{Form::textarea('test_description','',array('class'=>'form-control ckeditor','id'=>'test_description','rows' => 5))}}
			        </div>
              <input type="hidden" id="param_div_count" name="param_div_count" value="0">
					<div class="col-xs-12 col-md-6 col-lg-4 sec-parameter"> 
					<div class="parameter-title clearfix">
					<div class="col-xs-7 col-md-7 nopadding">
					<h5>Parameters</h5> 
					</div>
					<div class="col-xs-5 col-md-5 nopadding">
					<h5>Weightage</h5> 
					</div>
					
					</div>
					<div id="param_0" class="parameter-full m-b-sm clearfix">
					<div class="col-xs-6 col-md-7 nopadding">
				    <input class="form-control parameter_text" id="paramtext_0" type="text">
					</div>
					<div class="col-xs-5 col-md-4 nopadding">
					<div class="input-group">
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-gray btn-number" data-type="minus" data-field="quant[0]">
                      <span class="icon icon-minus-circle"></span>
                      </button>
                      </span>
                      <input type="text" name="quant[0]" id="weight_0" style="width: 50px;" class="form-control text-center input-number" value="10" min="1" max="100">
                      <span class="input-group-btn">
                      <button type="button" class="btn btn-gray btn-number" data-type="plus" data-field="quant[0]">
                      <span class="icon icon-plus-circle"></span>
                      </button>
                      </span>
                      </div>
					</div>
<!-- 						<div class="col-xs-1 col-md-1 nopadding text-center" style="padding-left: 28px;">
				    <a href="Javascript:Void(0);" class="ad-minus"><i class="icon icon-minus-circle"></i></a>
			        </div> -->
					</div>
					<div class="clearfix appenddiv"></div>
					<div class="col-md-12 nopadding">
					<a href="Javascript:Void(0);" class="add-parameter"><i class="icon icon-plus"></i> add another parameter</a>
					</div>
					
					<div class="col-xs-11 col-md-11 nopadding m-t-md">
					<div class="total-parameter">
					<label>Total Weightage:<span>100%</span></label>
					</div>
					</div>
					</div>
					
					     <input type="hidden" id="select_btn">
			        <div class="col-sm-12 col-md-12 m-t-md text-right">
                {!! Form::submit('Save', ['class' => 'btn btn-primary','id'=>'save_test_btn']) !!}
                {!! Form::submit('Save &amp; Add Questions', ['class' => 'btn btn-primary','id'=>'save_add_que_btn']) !!}
		            <!-- <button type="button" id="save_test_btn" class="btn btn-primary">Save</button> -->
                      <!-- <button type="button" id="save_add_que_btn" class="btn btn-primary">Save &amp; Add Questions</button> -->
		            </div>
		            {{Form::close()}}
				   </div>
		  
		  </div>
            <div class="tab-pane fade" id="tab-2">
			  <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
            </div>
            <div class="tab-pane fade" id="tab-3">
              <div class="over-form" style="left: 40%;top: 45%;display: none;" id="tab3_show_error_msg">
                <div class="alert alert-danger">
                            <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                  <i class="icon icon-times-circle-o"></i><strong>Error!</strong> <span id="tab3_error_content"></span>
                </div>
              </div>
              <div class="over-form" style="left: 40%;top: 45%;display: none;" id="tab3_show_success_msg">
                <div class="alert alert-success">
                  <i class="icon icon-check-circle-o"></i>
                  <strong>Success!</strong> <span id="tab3_success_content"></span>
                </div>
              </div>
              <?php 
                for($i=0;$i<count($selected_radio_settings);$i++){
                  switch ($selected_radio_settings[$i]['tts_setting_name']) {
                    case 'INVITE_EMAIL':
                      $INVITE_EMAIL = $selected_radio_settings[$i]['tts_days_hrs_attemts'];
                      break;

                    case 'RANDOMIZE_Q':
                      $RANDOMIZE_Q = $selected_radio_settings[$i]['tts_days_hrs_attemts'];
                      break;

                    case 'REMINDER_EMAIL':
                      $REMINDER_EMAIL = $selected_radio_settings[$i]['tts_days_hrs_attemts'];
                      break;

                    case 'ALL_Q_MANDATORY':
                      $ALL_Q_MANDATORY = $selected_radio_settings[$i]['tts_days_hrs_attemts'];
                      break;

                    case 'SKIP_SECTION':
                      $SKIP_SECTION = $selected_radio_settings[$i]['tts_days_hrs_attemts'];
                      break;   
                      
                    case 'ATLEAST_ONE_MANDATORY':
                      $ATLEAST_ONE_MANDATORY = $selected_radio_settings[$i]['tts_days_hrs_attemts'];
                      break;                                                                                                          

                    default:
                      # code...
                      break;
                  }

                }

              ?>
              <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 test-setting">  
                               <h3>General Settings</h3>
                 <ul class="setting-list">
                  <li>
                   <span class="pull-left">“Invite Email” trigger to be sent <input class="form-control" id="invite_param" type="text"> days / hrs before the test</span>
                    <span class="pull-right">
                    <div class="btn-group btn-toggle r2" data-toggle="buttons"> 
                       <label class="btn btn-gray <?php if (isset($INVITE_EMAIL) && $INVITE_EMAIL=="Days") echo "active";?>">
                          <input type="radio" <?php if (isset($INVITE_EMAIL) && $INVITE_EMAIL=="Days") echo "checked";?> class="invite_option" value="Days" name="invite_options" id="option1" autocomplete="off"> Days
                      </label>
                      <label class="btn btn-gray <?php if (isset($INVITE_EMAIL) && $INVITE_EMAIL=="Hrs") echo "active";?>">
                          <input type="radio" <?php if (isset($INVITE_EMAIL) && $INVITE_EMAIL=="Hrs") echo "checked";?> class="invite_option" value="Hrs" name="invite_options" id="option2" autocomplete="off"> Hrs
                      </label>
                    </div>
                    <button type="button" class="btn btn-gray r1 e1">Email Format</button>
                    </span>
                  </li>
                  <li>
                   <span class="pull-left">Reminder Frequency <input class="form-control" id="reminder_param" type="text"> times every days / hrs</span>
                    <span class="pull-right">
                    <div class="btn-group btn-toggle r2" data-toggle="buttons"> 
                     <label class="btn btn-gray  <?php if (isset($REMINDER_EMAIL) && $REMINDER_EMAIL=="Days") echo "active";?>">
                        <input type="radio" <?php if (isset($REMINDER_EMAIL) && $REMINDER_EMAIL=="Days") echo "checked";?> name="reminder_options" id="option3" value="Days" autocomplete="off"> Days
                    </label>
                    <label class="btn btn-gray <?php if (isset($REMINDER_EMAIL) && $REMINDER_EMAIL=="Hrs") echo "active";?>">
                        <input type="radio" <?php if (isset($REMINDER_EMAIL) && $REMINDER_EMAIL=="Hrs") echo "checked";?> name="reminder_options" id="option4" value="Hrs" autocomplete="off"> Hrs
                    </label>
                    </div>
                    </span>
                  </li>
                  <li>
                   <span class="pull-left">“Congratulations Email” on test completion</span>
                    <span class="pull-right">
                    <button type="button" class="btn btn-gray r1 e2">Email Format</button>
                    </span>
                  </li>
                  <li>
                   <span class="pull-left"><input class="form-control" id="attempts_param" type="text"> attempts allowed with the same link</span>
                    <span class="pull-right">
                    <button type="button" class="btn btn-gray r1 e3">Email Format</button>
                    </span>
                  </li>
                  <li>
                   <span class="pull-left">Retake email if problem from TalGro</span>
                    <span class="pull-right">
                    <button type="button" class="btn btn-gray r1 e4">Email Format</button>
                    </span>
                  </li>
                  <li>
                   <span class="pull-left">Allow answers to be changed before completion</span>
                    <span class="pull-right">
                    <button type="button" class="btn btn-gray r1 e5">Email Format</button>
                    </span>
                  </li>
                  <li>
                   <span class="pull-left">”Result Email” Page format</span>
                    <span class="pull-right">
                    <button type="button" class="btn btn-gray r1 e6">Email Format</button>
                    </span>
                  </li>
                 </ul>
                  
                 <h3>Section Settings</h3>
                 <ul class="setting-list">
                  <li>
                   <span class="pull-left"> Randomize Q</span>
                    <span class="pull-right">
                    <div class="btn-group btn-toggle r2" data-toggle="buttons"> 
                     <label class="btn btn-gray <?php if (isset($RANDOMIZE_Q) && $RANDOMIZE_Q=="Yes") echo "active";?>">
                        <input type="radio" <?php if (isset($RANDOMIZE_Q) && $RANDOMIZE_Q=="Yes") echo "checked";?> name="randomize_option5" id="randomize_option5" value="Yes" autocomplete="off"> Yes
                    </label>
                    <label class="btn btn-gray <?php if (isset($RANDOMIZE_Q) && $RANDOMIZE_Q=="No") echo "active";?> ">
                        <input type="radio" <?php if (isset($RANDOMIZE_Q) && $RANDOMIZE_Q=="No") echo "checked";?> name="randomize_option5" id="randomize_option5" value="No" autocomplete="off"> No
                    </label>
                                    </div>
                    </span>
                  </li>
                  <li>
                   <span class="pull-left">All Question Mandatory</span>
                    <span class="pull-right">
                    <div class="btn-group btn-toggle r2" data-toggle="buttons"> 
                     <label class="btn btn-gray <?php if (isset($ALL_Q_MANDATORY) && $ALL_Q_MANDATORY=="Yes") echo "active";?>">
                        <input type="radio" <?php if (isset($ALL_Q_MANDATORY) && $ALL_Q_MANDATORY=="Yes") echo "checked";?> name="mand_que_option7" id="mand_que_option7" value="Yes" autocomplete="off"> Yes
                    </label>
                    <label class="btn btn-gray <?php if (isset($ALL_Q_MANDATORY) && $ALL_Q_MANDATORY=="No") echo "active";?>">
                        <input type="radio" <?php if (isset($ALL_Q_MANDATORY) && $ALL_Q_MANDATORY=="No") echo "checked";?> name="mand_que_option7" id="mand_que_option7" value="No" autocomplete="off"> No
                    </label>
                    </div>
                    </span>
                  </li>
                  <li>
                   <span class="pull-left">Skip section &amp; comback later</span>
                    <span class="pull-right">
                    <div class="btn-group btn-toggle r2" data-toggle="buttons"> 
                     <label class="btn btn-gray <?php if (isset($SKIP_SECTION) && $SKIP_SECTION=="Yes") echo "active";?>">
                        <input type="radio" <?php if (isset($SKIP_SECTION) && $SKIP_SECTION=="Yes") echo "checked";?> name="skip_section_option9" id="skip_section_option9" value="Yes" autocomplete="off"> Yes
                     </label>
                    <label class="btn btn-gray  <?php if (isset($SKIP_SECTION) && $SKIP_SECTION=="No") echo "active";?>">
                        <input type="radio" <?php if (isset($SKIP_SECTION) && $SKIP_SECTION=="No") echo "checked";?>  name="skip_section_option9" id="skip_section_option9" value="No" autocomplete="off"> No
                    </label>
                    </div>
                    </span>
                  </li>
                  <li>
                   <span class="pull-left">Atleast one Q mandary</span>
                    <span class="pull-right">
                    <div class="btn-group btn-toggle r2" data-toggle="buttons"> 
                     <label class="btn btn-gray <?php if (isset($ATLEAST_ONE_MANDATORY) && $ATLEAST_ONE_MANDATORY=="Yes") echo "active";?>">
                        <input type="radio" name="one_mand_option11" id="one_mand_option11" value="Yes" autocomplete="off" <?php if (isset($ATLEAST_ONE_MANDATORY) && $ATLEAST_ONE_MANDATORY=="Yes") echo "checked";?>> Yes
                    </label>
                    <label class="btn btn-gray <?php if (isset($ATLEAST_ONE_MANDATORY) && $ATLEAST_ONE_MANDATORY=="No") echo "active";?>">
                        <input type="radio" <?php if (isset($ATLEAST_ONE_MANDATORY) && $ATLEAST_ONE_MANDATORY=="No") echo "checked";?> name="one_mand_option11" id="one_mand_option11" value="No" autocomplete="off"> No
                    </label>
                    </div>
                    </span>
                  </li>
                 </ul>
                              </div>
                              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  
                  <div class="email-section e11">
                  <div class="email-heading">{{Config::get('messages.test.INVITE_EMAIL_FORMAT_HEADER')}}</div>
                  <div class="email-body">
                  <div class="row">
                          <div class="col-xs-12 col-md-12">  
                                  <h5 class="m-t-0">Email Subject</h5>
                  <input type="text" id="invite_email_subject" class="form-control">
                                  </div>
                  <div class="col-xs-12 col-md-12 m-b-md">  
                                  <h5>Email Body</h5>
                        <!-- <textarea class="form-control" rows="5"></textarea> -->
                        {{Form::textarea('invite_email_body','',array('class'=>'form-control ckeditor','id'=>'invite_email_body','rows' => 5))}}
                        </div>
                  <div class="col-sm-12 col-md-12">
                          <button type="button" class="btn btn-primary" data-settingtype="invite_email" data-toggle="modal" data-target="#email-preview">Preview</button>
                                  <button type="button" id="invite_single_save" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default">Cancel</button>
                          </div>
                  </div>
                  </div>
                  <div class="email-subheading">Dynamic Fields</div>
                  <div class="email-body shortcode">
                    <div class="row">
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('FIRST_NAME','invite_email');" >[FIRST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('LAST_NAME','invite_email');">[LAST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('EMAIL','invite_email');">[EMAIL]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('PHONE','invite_email');">[PHONE]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('COURSE_LINK','invite_email');">[COURSE_LINK]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('DAYS','invite_email');">[DAYS]</a></div>
                    </div>
                  </div>
                              </div>
                  
                  
                  <div class="email-section e12">
                  <div class="email-heading">{{Config::get('messages.test.CONGRATS_EMAIL_FORMAT_HEADER')}}</div>
                  <div class="email-body">
                  <div class="row">
                          <div class="col-xs-12 col-md-12">  
                                  <h5 class="m-t-0">Email Subject</h5>
                  <input type="text" id="congrats_email_subject" class="form-control">
                                  </div>
                  <div class="col-xs-12 col-md-12 m-b-md">  
                                  <h5>Email Body</h5>
                        {{Form::textarea('congrats_email_body','',array('class'=>'form-control ckeditor','id'=>'congrats_email_body','rows' => 5))}}
                        </div>
                  <div class="col-sm-12 col-md-12">
                          <button type="button" class="btn btn-primary" data-settingtype="congrats_email" data-toggle="modal" data-target="#email-preview">Preview</button>
                                  <button type="button" id="congrats_single_save" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default">Cancel</button>
                          </div>
                  </div>
                  </div>
                  <div class="email-subheading">Dynamic Fields</div>
                  <div class="email-body shortcode">
                    <div class="row">
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('FIRST_NAME','congrats_email');">[FIRST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('LAST_NAME','congrats_email');">[LAST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('EMAIL','congrats_email');">[EMAIL]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('PHONE','congrats_email');">[PHONE]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('COURSE_LINK','congrats_email');">[COURSE_LINK]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('DAYS','congrats_email');">[DAYS]</a></div>
                    </div>
                  </div>
                              </div>
                  
                  
                  <div class="email-section e13">
                  <div class="email-heading">{{Config::get('messages.test.ATTEMPT_ALLOW_EMAIL_FORMAT_HEADER')}}</div>
                  <div class="email-body">
                  <div class="row">
                          <div class="col-xs-12 col-md-12">  
                                  <h5 class="m-t-0">Email Subject</h5>
                  <input type="text" id="attempt_allow_email_subject" class="form-control">
                                  </div>
                  <div class="col-xs-12 col-md-12 m-b-md">  
                                  <h5>Email Body</h5>
                        {{Form::textarea('attempt_allow_email_body','',array('class'=>'form-control ckeditor','id'=>'attempt_allow_email_body','rows' => 5))}}
                        </div>
                  <div class="col-sm-12 col-md-12">
                          <button type="button" class="btn btn-primary" data-settingtype="attempt_allow_email" data-toggle="modal" data-target="#email-preview">Preview</button>
                                  <button type="button" id="attempt_single_save" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default">Cancel</button>
                          </div>
                  </div>
                  </div>
                  <div class="email-subheading">Dynamic Fields</div>
                  <div class="email-body shortcode">
                    <div class="row">
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('FIRST_NAME','attempt_allow_email');">[FIRST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('LAST_NAME','attempt_allow_email');">[LAST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('EMAIL','attempt_allow_email');">[EMAIL]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('PHONE','attempt_allow_email');">[PHONE]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('COURSE_LINK','attempt_allow_email');">[COURSE_LINK]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('DAYS','attempt_allow_email');">[DAYS]</a></div>
                    </div>
                  </div>
                              </div>
                  
                  
                  <div class="email-section e14">
                  <div class="email-heading">{{Config::get('messages.test.RETAKE_EMAIL_FORMAT_HEADER')}}</div>
                  <div class="email-body">
                  <div class="row">
                          <div class="col-xs-12 col-md-12">  
                                  <h5 class="m-t-0">Email Subject</h5>
                  <input type="text" id="retake_email_subject" class="form-control">
                                  </div>
                  <div class="col-xs-12 col-md-12 m-b-md">  
                                  <h5>Email Body</h5>
                        {{Form::textarea('retake_email_body','',array('class'=>'form-control ckeditor','id'=>'retake_email_body','rows' => 5))}}
                        </div>
                  <div class="col-sm-12 col-md-12">
                          <button type="button" class="btn btn-primary" data-settingtype="retake_email" data-toggle="modal" data-target="#email-preview">Preview</button>
                                  <button type="button" id="retake_single_save" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default">Cancel</button>
                          </div>
                  </div>
                  </div>
                  <div class="email-subheading">Dynamic Fields</div>
                  <div class="email-body shortcode">
                    <div class="row">
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('FIRST_NAME','retake_email');">[FIRST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('LAST_NAME','retake_email');">[LAST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('EMAIL','retake_email');">[EMAIL]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('PHONE','retake_email');">[PHONE]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('COURSE_LINK','retake_email');">[COURSE_LINK]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('DAYS','retake_email');">[DAYS]</a></div>
                    </div>
                  </div>
                              </div>
                  
                  
                  <div class="email-section e15">
                  <div class="email-heading">{{Config::get('messages.test.ALLOW_ANS_EMAIL_FORMAT_HEADER')}}</div>
                  <div class="email-body">
                  <div class="row">
                          <div class="col-xs-12 col-md-12">  
                                  <h5 class="m-t-0">Email Subject</h5>
                  <input type="text" id="allow_email_subject" class="form-control">
                                  </div>
                  <div class="col-xs-12 col-md-12 m-b-md">  
                                  <h5>Email Body</h5>
                        {{Form::textarea('allow_email_body','',array('class'=>'form-control ckeditor','id'=>'allow_email_body','rows' => 5))}}
                        </div>
                  <div class="col-sm-12 col-md-12">
                          <button type="button" class="btn btn-primary" data-settingtype="allow_ans_email" data-toggle="modal" data-target="#email-preview">Preview</button>
                                  <button type="button" id="allow_single_save" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default">Cancel</button>
                          </div>
                  </div>
                  </div>
                  <div class="email-subheading">Dynamic Fields</div>
                  <div class="email-body shortcode">
                    <div class="row">
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('FIRST_NAME','allow_ans_email');">[FIRST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('LAST_NAME','allow_ans_email');">[LAST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('EMAIL','allow_ans_email');">[EMAIL]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('PHONE','allow_ans_email');">[PHONE]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('COURSE_LINK','allow_ans_email');">[COURSE_LINK]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);" onclick="appendDynamicFields('DAYS','allow_ans_email');">[DAYS]</a></div>
                    </div>
                  </div>
                              </div>
                  
                  
                  <div class="email-section e16">
                  <div class="email-heading">{{Config::get('messages.test.RESULT_EMAIL_FORMAT_HEADER')}}</div>
                  <div class="email-body">
                  <div class="row">
                          <div class="col-xs-12 col-md-12">  
                                  <h5 class="m-t-0">Email Subject</h5>
                  <input type="text" id="result_email_subject" class="form-control">
                                  </div>
                  <div class="col-xs-12 col-md-12 m-b-md">  
                                  <h5>Email Body</h5>
                        {{Form::textarea('result_email_body','',array('class'=>'form-control ckeditor','id'=>'result_email_body','rows' => 5))}}
                        </div>
                  <div class="col-sm-12 col-md-12">
                          <button type="button" class="btn btn-primary" data-settingtype="result_email" data-toggle="modal" data-target="#email-preview">Preview</button>
                                  <button type="button" id="result_single_save" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default">Cancel</button>
                          </div>
                  </div>
                  </div>
                  <div class="email-subheading">Dynamic Fields</div>
                  <div class="email-body shortcode">
                    <div class="row">
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('FIRST_NAME','result_email');">[FIRST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('LAST_NAME','result_email');">[LAST_NAME]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('EMAIL','result_email');">[EMAIL]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('PHONE','result_email');">[PHONE]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('COURSE_LINK','result_email');">[COURSE_LINK]</a></div>
                    <div class="col-md-12"><a href="Javascript:Void(0);"  onclick="appendDynamicFields('DAYS','result_email');">[DAYS]</a></div>
                    </div>
                  </div>
                              </div>
                  
                 </div>
                    <div class="col-sm-12 col-md-12 m-t-md text-right">
                      <button type="button" id="save_all_settings" class="btn btn-primary">Save</button>
                              <button type="button" class="btn btn-primary">Continue to Learning Path</button>
                      </div>
             </div>
            </div>
		  <div class="tab-pane fade" id="tab-4">
              <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
            </div>
<div class="tab-pane fade" id="tab-5">
                <?php
                  $default_col_order_string="CheckAll-1,EmployeeCode-1,UserName-1,EmailId-1,Department-1,ContactDetail-1,ManagerName-1,CourseStatus-1,Level-1,EnrollCourses-1";
                                  ?>
                <div class="table-content-header">
                  <div class="table-toolbar">
                    <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                      
                    </div>
                    <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                      <div class="btn-group export">
                        
                        
                        <div class="filter-opt1">
                          <button class="btn btn-link col" type="button">
                            <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                            <span class="hidden-xs">Column</span>
                          </button>
                          <div class="listing-det overrlay">
                            <ul class="list-group sortable_menu">
                              <?php
                              //if session has value display column according or display default array
                              if($request->session()->has('learner_hidden_column_array')){
                                $col_arr_value = $request->session()->get('learner_hidden_column_array');
                              }else{
                                $col_arr_value=$default_col_order_string;
                              }
                              $col_arr_value=explode(",",$col_arr_value);

                              foreach ($col_arr_value as $key => $value) {
                                  //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                  $value=explode('-', $value);
                                  $class="";
                                  if($value[0]=='CheckAll' || $value[0]=='Action')
                                    $class='non_sortable';
                                  ?>
                                  <li class="list-group-item list-order-item <?=$class?> " data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                    <span class="pull-right">
                                      <label class="switch switch-primary">
                                        <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                        <span class="switch-track"></span>
                                        <span class="switch-thumb"></span>
                                      </label>
                                    </span>
                                    <span class="icon icon-arrows"></span>
                                    <?php
                                    if ($value[0]=="CheckAll"){
                                      echo "Check All";
                                    }elseif ($value[0]=="EmployeeCode") {
                                      echo "Employee Code";
                                    }elseif ($value[0]=="EnrollCourses") {
                                     echo "Enroll Courses";
                                    }else
                                      echo $value[0];
                                    ?>
                                    </li>
                                  <?php
                                }
                              ?>
                              
                              <button class="btn btn-primary btn-half save_order" form_name="test_learner" type="button">Save</button>
                              <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                            </ul>
                          </div>
                        </div>
                      </div>
                      
                      <div class="btn-group">
                        <a href="#" class="refine_mbutton" title="Filter"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="table-content-body"> 
                  <div class="Cus_card filter-bg">
                    <div class="Cus_card-body" style="display: none;">
                      <div class="col-xs-6 col-md-3">
                        <h5>Department</h5>
                        <div class="cus-select">
                          <select class="learner_department">
                              <option value="">Select Department</option>
                              @foreach ($array_department as $code)
                                 <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Level</h5>
                        <div class="cus-select">
                          <select class="learner_level">
                            <option value="">Select Level</option>
                              @foreach ($array_level as $code)
                                 <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Function</h5>
                        <div class="cus-select">
                         <select class="learner_function">
                            <option value="">Select Function</option>
                              @foreach ($array_function as $code)
                                 <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                              @endforeach
                         </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Status</h5>
                        <div class="cus-select">
                          <select class="learner_status">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-9">
                        <h5>Search</h5>
                        <input class="form-control" type="text" id="searchlearnertxt">
                      </div>
                      <div class="col-xs-6 col-md-3">
                        
                      </div>
                      <div class="col-xs-12 col-md-4">
                        <h5></h5>
                        <button class="btn btn-primary" id="test_learner_searchButton" type="submit">Search</button>
                        <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="assign_learner_sorting_column_name" id="assign_learner_sorting_column_name">
                  <table class="table table-hover table-striped test_learner" cellspacing="0" width="100%">
                    
                    <tbody>
                    </tbody>
                  </table>
                    <div class="col-sm-12 col-md-12 m-t-md text-right">
                      <button type="button" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-primary assign_learner_to_test">Assign Learners</button>
                    </div>
                </div>
              </div>
              <div class="tab-pane fade" id="tab-6">
                <?php
                  $default_col_order_string="CheckAll-1,EmployeeCode-1,UserName-1,EmailId-1,Department-1,ContactDetail-1,ManagerName-1,CourseStatus-1,Level-1,EnrollCourses-1";
                                  ?>
                <div class="table-content-header">
                  <div class="table-toolbar">
                    <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                      
                    </div>
                    <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                      <div class="btn-group export">
                        
                        
                        <div class="filter-opt1">
                          <button class="btn btn-link col" type="button">
                            <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                            <span class="hidden-xs">Column</span>
                          </button>
                          <div class="listing-det overrlay">
                            <ul class="list-group sortable_menu">
                              <?php
                              //if session has value display column according or display default array
                              if($request->session()->has('learner_hidden_column_array')){
                                $col_arr_value = $request->session()->get('learner_hidden_column_array');
                              }else{
                                $col_arr_value=$default_col_order_string;
                              }
                              $col_arr_value=explode(",",$col_arr_value);

                              foreach ($col_arr_value as $key => $value) {
                                  //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                  $value=explode('-', $value);
                                  $class="";
                                  if($value[0]=='CheckAll' || $value[0]=='Action')
                                    $class='non_sortable';
                                  ?>
                                  <li class="list-group-item list-order-item <?=$class?> " data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                    <span class="pull-right">
                                      <label class="switch switch-primary">
                                        <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                        <span class="switch-track"></span>
                                        <span class="switch-thumb"></span>
                                      </label>
                                    </span>
                                    <span class="icon icon-arrows"></span>
                                    <?php
                                    if ($value[0]=="CheckAll"){
                                      echo "Check All";
                                    }elseif ($value[0]=="EmployeeCode") {
                                      echo "Employee Code";
                                    }elseif ($value[0]=="EnrollCourses") {
                                     echo "Enroll Courses";
                                    }else
                                      echo $value[0];
                                    ?>
                                    </li>
                                  <?php
                                }
                              ?>
                              
                              <button class="btn btn-primary btn-half save_order" form_name="test_learner" type="button">Save</button>
                              <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                            </ul>
                          </div>
                        </div>
                      </div>
                     
                      <div class="btn-group">
                        <a href="#" class="refine_mbutton" title="Filter"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="table-content-body"> 
                  <div class="Cus_card filter-bg">
                    <!-- <div class="Cus_card-body" style="display: none;">
                      <div class="col-xs-6 col-md-3">
                        <h5>Department</h5>
                        <div class="cus-select">
                          <select class="learner_department">
                              <option value="">Select Department</option>
                              @foreach ($array_department as $code)
                                 <option value="{{$code['tdm_id']}}"> {{$code['tdm_department']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Level</h5>
                        <div class="cus-select">
                          <select class="learner_level">
                            <option value="">Select Level</option>
                              @foreach ($array_level as $code)
                                 <option value="{{$code['tsl_id']}}"> {{$code['tsl_seniority']}}</option> 
                              @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Function</h5>
                        <div class="cus-select">
                         <select class="learner_function">
                            <option value="">Select Function</option>
                              @foreach ($array_function as $code)
                                 <option value="{{$code['tfm_id']}}"> {{$code['tfm_function']}}</option> 
                              @endforeach
                         </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-3">
                        <h5>Status</h5>
                        <div class="cus-select">
                          <select class="learner_status">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6 col-md-9">
                        <h5>Search</h5>
                        <input class="form-control" type="text" id="searchlearnertxt">
                      </div>
                      <div class="col-xs-6 col-md-3">
                        
                      </div>
                      <div class="col-xs-12 col-md-4">
                        <h5></h5>
                        <button class="btn btn-primary" id="test_learner_searchButton" type="submit">Search</button>
                        <button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
                      </div>
                    </div> -->
                  </div>
                  <input type="hidden" name="invite_sorting_column_name" id="invite_sorting_column_name">
                  <table class="table table-hover table-striped invite_test_learner" cellspacing="0" width="100%">
                    
                    <tbody>
                    </tbody>
                  </table>
                    <div class="col-sm-12 col-md-12 m-t-md text-right">
                      <button type="button" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-primary invite_learner_to_test">Invite Learners</button>
                    </div>
                </div>
              </div>
		  <div class="tab-pane fade" id="tab-7">
          <?php
          $default_col_order_string="EmpCode-1,UserName-1,Score-1,RecommendedCourses-1,LearningPath-1,AssocLearningPath-1,Results-1";
          ?>        
          <div class="table-content-header">
            <div class="table-toolbar">
              <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                <div class="btn-group bulk hide common_action">
                  <label>
                  Bulk Action :
                  </label>
                <button class="btn btn-link link-muted click_common_action" to_do="enable" type="button">
                  <span>Enable</span>
                </button> 
                <span class="pipe">|</span>
                <button class="btn btn-link link-muted click_common_action" to_do="disable" type="button">
                  <span>Disable</span>
                </button>
                <span class="pipe">|</span>
                <button class="btn btn-link link-muted click_common_action" to_do="delete" type="button">
                  <span>Delete</span>
                </button>

                </div>
              </div>
            <div class="table-toolbar-tools pull-xs-right pull-sm-right">
              <div class="btn-group export">
              <a href="{{url('/admin/test_result_excel')}}" type="button" class="btn btn-link link-muted">
                <span class="icon icon-file-excel-o icon-fw" title="Export Excel"></span>
                <span class="hidden-xs">Export Excel</span>
              </a>
              <a href="{{url('/admin/test_result_pdf')}}" type="button" class="btn btn-link link-muted">
                <span class="icon icon-file-pdf-o icon-fw" title="Export PDF"></span>
                <span class="hidden-xs">Export PDF</span>
              </a>
              <div class="filter-opt1">
              <button class="btn btn-link col" type="button">
                <span class="icon icon-cogs icon-fw" title="Column"></span>
                <span class="hidden-xs">Column</span>
              </button>
              <div class="listing-det overrlay">
                    <ul class="list-group sortable_menu">
                            <?php
                            //if session has value display column according or display default array
                            if($request->session()->has('test_result_hidden_column_array')){
                              $col_arr_value = $request->session()->get('test_result_hidden_column_array');
                            }else{
                              $col_arr_value=$default_col_order_string;
                            }
                            $col_arr_value=explode(",",$col_arr_value);
                            // echo "<pre>";
                            // print_r($col_arr_value);
                            foreach ($col_arr_value as $key => $value) {
                                //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                $value=explode('-', $value);
                                $class="";
                                if($value[0]=='CheckAll')
                                  $class='non_sortable';
                                ?>
                                <li class="list-group-item list-order-item <?=$class?> savitest" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                  <span class="pull-right">
                                    <label class="switch switch-primary">
                                      <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                      <span class="switch-track"></span>
                                      <span class="switch-thumb"></span>
                                    </label>
                                  </span>
                                  <span class="icon icon-arrows"></span>
                                  <?php
                                  if($value[0]=="Emp Code")
                                    echo "Emp Code";
                                  else if($value[0]=="User Name")
                                    echo "User Name";
                                  else if($value[0]=="Score")
                                    echo "Score";
                                  else if($value[0]=="Recommended Courses")
                                    echo "Recommended Courses";
                                   else if($value[0]=="Learning Path")
                                    echo "Learning Path";
                                  else if($value[0]=="AssocLearning Path")
                                    echo "AssocLearning Path";
                                  else if($value[0]=="Results")
                                    echo "Results";                                              
                                  else
                                    echo $value[0];
                                  ?>
                                  </li>
                                <?php
                              }
                            ?>
                          <button class="btn btn-primary btn-half save_order" form_name="test_result" id="" type="button">Save</button>
                        <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                    </ul>
              </div>
              </div>
              </div>

            </div>
            </div>
          </div>
            
          <div class="cus-tbl">  
              <table id="example2" class="table table-hover table-striped example2" cellspacing="0" width="100%">
                <thead></thead>
                <tbody>
                </tbody> 
              </table>
              <?php
              if($request->session()->has('test_result_hidden_column_array')){
                  $test_result_hidden_column_array = $request->session()->get('test_result_hidden_column_array');?>
                  <input type="hidden" name="test_result_hidden_column_array" id="test_result_hidden_column_array" value="<?=$test_result_hidden_column_array?>"><?php
              }else{?>
                  <input type="hidden" name="test_result_hidden_column_array" id="test_result_hidden_column_array" value="<?=$default_col_order_string?>"><?php
              }
              ?>  
          </div>  

      </div>
		  <div class="tab-pane fade" id="tab-8">
              <p>Raw denim you probably haven't heard of them jean shorts Austin. </p>
            </div>
		  </div>
		</div>
          
          </div>
          
          </div>
      </div>
  </div>

<div id="email-preview" class="modal pop-modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
             <h1 class="title-bar-title">
                <i class="icon icon-envelope-o"></i>
                <span class="d-ib">Email Preview</span>
            </h1>
            
          </div>
          
          <div class="modal-body">
              
              <div class="row">
                <div class="col-md-12">
                <div class="m-b-sm">Subject:<span id="preview_subject"></span></div>
                <span id="preview_body">
                <div class="m-b-lg">Dear [FIRSTNAME],</div>

                  <p>Welcome to the PACE test from PeopleFirst (India). As you may already be aware, this is part of the 360 degree and the Executive Coaching intervention planned by your organization, with PeopleFirst (India) as the development partner.</p>

                  <p>Welcome to the PACE test from PeopleFirst (India). As you may already be aware, this is part of the 360 degree and the Executive Coaching intervention planned by your organization, with PeopleFirst (India) as the development partner.</p>
                            
                  <p>Welcome to the PACE test from PeopleFirst (India). As you may already be aware, this is part of the 360 degree and the Executive Coaching intervention planned by your organization, with PeopleFirst (India) as the development partner.</p>

                   <p>You may now attempt the PACE test by clicking on the following link:</p>
                   <div class="m-y-md">[logo_LINK]</div>
                 </span>
                 <span id="preview_footer">  
                   Regards,<br/>
                   Team TalGro<br/>
                   Team Psychometrics
                 </span>
                </div>
                <div class="col-md-12">
                 <h5>Send Test Email</h5>
                     <div class="input-group">
                                <input class="form-control" id="sample_email_id" type="text" placeholder="abc@abc.com">
                                <span class="input-group-btn">
                                 <button type="button" id="send_sample_email" class="btn btn-primary">Send</button> 
                                </span>
                      </div>
                </div>
              </div>    
          </div>
        </div>
      </div>
</div>


<script src="{{ url('public/adminlte/js/test') }}/add_test.js"></script> 
<script src="{{ url('public/adminlte/js/test') }}/test_result.js"></script> 
  @stop