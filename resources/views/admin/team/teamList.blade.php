@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
        <div class="title-bar clear">
			<div class="col-xs-12 col-sm-7 col-md-8">
            <h1 class="title-bar-title">
				<i class="icon icon-list-ul"></i>
                <span class="d-ib">Team Listing</span>
            </h1>
            <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
            </p>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-4 ad_top">
			<button class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
			<!-- <a class="btn btn-primary" href="{{url('/admin/showAddTeam')}}">Add Team</a> -->
			<button class="btn btn-primary" id="go_to_add_team" type="submit">Add Team</button>
			
			<!--<a href="#" id="panel-fullscreen" role="button" title="Toggle fullscreen"><i class="icon icon-expand"></i></a>-->
			</div>
			
        </div>

		<div class="layout-content-body filter-bg nopadding">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="Cus_card collapsed ">
                        <div class="Cus_card-header fil">
                           <button type="button" id="filter-ck4" class="Cus_card-actions Cus_card-toggler" title="Filter"></button>
                        </div>
                        <div class="Cus_card-body collapse">
                            <div class="col-xs-12 col-md-4 col-lg-3">  
                                <h5>Number Of Learners</h5>
								<div class="cus-select">
                                 <select id="no_of_learners">
                                  <option value="">Select Learners</option>
                                  <option value="0-5">0-5</option>
                                 <option value="5-10">5-10</option>
                                 <option value="10-100">10-100</option>
                                 <option value="100-200">100-200</option>
                                 <option value="200-300">200-300</option>
                                 <option value="400-500">400-500</option>
                                 </select>
								</div>
                            </div>
                            <div class="col-xs-12 col-md-4 col-lg-3">
                                <h5>Course</h5>
								<div class="cus-select">
                                <select id="team_filter_courses_list">
                                 </select>
								</div>
                            </div>
							
							<div class="col-xs-12 col-md-4 col-lg-3">
								<h5>Learning Path</h5>
								<div class="cus-select">
                                <select id="team_filter_learining_path_list">
                                 </select>
								</div>
                            </div>

							<div class="col-xs-12 col-md-4 col-lg-3">
								<h5>Search by Keyword</h5>
								<input class="form-control" id="Keyword_filter" type="text">		
                            </div>   
							
							<div class="col-xs-12 col-md-4">
	                        <h5></h5>
							<button class="btn btn-primary" id="filter_btn" type="submit">Search</button>
							<button class="btn btn-default Cus_card-toggler" type="submit">Close</button>
						    </div>
							
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layout-content-body">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-body">
		          <?php
		          $default_col_order_string="CheckAll-1,Team-1,Learners-1,CourseAssigned-1,LearningPathAssigned-1,Action-1";
		          ?>                          	
							<div class="table-content-header">
              <div class="table-toolbar">
                <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                  <div class="btn-group bulk hide common_action">
					  <label>
					  Bulk Action :
					  </label>
                    <button class="btn btn-link link-muted click_common_action" to_do="enable_team"  type="button">
                      <span>Enable</span>
                    </button> 
					<span class="pipe">|</span>
                    <button class="btn btn-link link-muted click_common_action" to_do="disable_team"  type="button">
                      <span>Disable</span>
                    </button>
					<span class="pipe">|</span>
                    <button class="btn btn-link link-muted click_common_action" to_do="delete_team"  type="button">
                      <span>Delete</span>
                    </button>
                    
                  </div>
                </div>
                <div class="table-toolbar-tools pull-xs-right pull-sm-right">
					<div class="btn-group export">
				    <a href="{{url('/admin/team_list_excel')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-excel-o icon-fw" title="Export Excel"></span>
                      <span class="hidden-xs">Export Excel</span>
                    </a>
					<a href="{{url('/admin/team_list_pdf')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-pdf-o icon-fw" title="Export PDF"></span>
                      <span class="hidden-xs">Export PDF</span>
                    </a>
					<div class="filter-opt1">
					<button class="btn btn-link col" type="button">
                      <span class="icon icon-cogs icon-fw" title="Column"></span>
                      <span class="hidden-xs">Column</span>
          </button>
					<div class="listing-det overrlay">
			            <ul class="list-group sortable_menu">
                            <?php
                            //if session has value display column according or display default array
                            if($request->session()->has('team_listing_hidden_column_array')){
                              $col_arr_value = $request->session()->get('team_listing_hidden_column_array');
                            }else{
                              $col_arr_value=$default_col_order_string;
                            }
                            $col_arr_value=explode(",",$col_arr_value);
                            // echo "<pre>";
                            // print_r($col_arr_value);
                            foreach ($col_arr_value as $key => $value) {
                                //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                $value=explode('-', $value);
                                $class="";
                                if($value[0]=='CheckAll')
                                  $class='non_sortable';
                                ?>
                                <li class="list-group-item list-order-item <?=$class?> savitest" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                  <span class="pull-right">
                                    <label class="switch switch-primary">
                                      <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                      <span class="switch-track"></span>
                                      <span class="switch-thumb"></span>
                                    </label>
                                  </span>
                                  <span class="icon icon-arrows"></span>
                                  <?php
                                  if ($value[0]=="CheckAll")
                                    echo "Check All";
                                  else if($value[0]=="Team")
                                    echo "Team";
                                  else if($value[0]=="Learners")
                                    echo "Learners";
                                  else if($value[0]=="CourseAssigned")
                                    echo "CourseAssigned";
                                  else if($value[0]=="LearningPathAssigned")
                                    echo "LearningPathAssigned";
                                   else if($value[0]=="Action")
                                    echo "Action";                                              
                                  else
                                    echo $value[0];
                                  ?>
                                  </li>
                                <?php
                              }
                            ?>
                          <button class="btn btn-primary btn-half save_order" form_name="team_listing" id="" type="button">Save</button>
                        <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                		</ul>
                </div>
					</div>
					</div>
                </div>
              </div>
            </div>
			<div class="cus-tbl">	 
              <table id="team_list_tbl" class="table table-hover table-striped team_list_tbl" cellspacing="0" width="100%">
                <thead></thead>
                <tbody>
                </tbody> 
              </table>
              <?php
              if($request->session()->has('team_listing_hidden_column_array')){
                  $team_listing_hidden_column_array = $request->session()->get('team_listing_hidden_column_array');?>
                  <input type="hidden" name="team_listing_hidden_column_array" id="team_listing_hidden_column_array" value="<?=$team_listing_hidden_column_array?>"><?php
              }else{?>
                  <input type="hidden" name="team_listing_hidden_column_array" id="team_listing_hidden_column_array" value="<?=$default_col_order_string?>"><?php
              }
              ?>  
			</div>
<!-- 						<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							    <div class="show-count">
								<label>
						         Show 
								 <div class="cus-select">
								 <select>
                                 <option value="1">1</option>
                                 <option value="2">2</option>
							     <option value="3">3</option>
                                 </select>
								 </div>
								 entries of <span>596</span>
								</label>
								 </div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 pagin">
							     <nav>
                                 <ul class="pagination">
                                   <li class="page-item">
                                   <a class="page-link" href="#" aria-label="Previous">
                                   <span aria-hidden="true">&laquo;</span>
                                   <span class="sr-only">Previous</span>
                                   </a>
                                   </li>
                                   <li class="page-item"><a class="page-link" href="#">1</a></li>
                                   <li class="page-item"><a class="page-link" href="#">2</a></li>
                                   <li class="page-item"><a class="page-link" href="#">3</a></li>
                                   <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                    </a>
                                   </li>
                                  </ul>
                             </nav>
						</div>
						</div>	 -->
							
                        </div>
						
				
						
                    </div>
                </div>
            </div>
        </div>
    </div>


         <script src="{{ url('public/js/team') }}/team_listing.js"></script>
@stop