    @inject('request', 'Illuminate\Http\Request')
    @extends('layouts.app')

    @section('content')

    	<div class="title-bar clear">
    		<div class="col-xs-12 col-sm-8 col-md-8">
    	    <h1 class="title-bar-title">
    			<i class="icon icon-users"></i>
    	        <span class="d-ib">Add Team</span>
    	    </h1>
    	    <p class="title-bar-description">
    	      <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
    	    </p>
    		</div>
    		
    		
    	</div>

            <div class="layout-content-body">
                <div class="row gutter-xs">
                    <div class="col-xs-12">
    				<div class="demo-form-wrapper">
    				<div class="panel">
    					
                      <ul class="steps">
                        <li class="step col-md-5 active">
                          <a class="step-segment" href="#tab-1" data-toggle="tab">
                            <span class="step-icon">1</span>
                          </a>
                          <div class="step-content">
                            <strong class="hidden-xs">Basic Details</strong>
                          </div>
                        </li>
                        <li class="step col-md-5">
                          <a class="step-segment get_learners_listing" href="#tab-2" data-toggle="tab">
                            <span class="step-icon">2</span>
                          </a>
                          <div class="step-content">
                            <strong class="hidden-xs">Assign Learners</strong>
                          </div>
                        </li>
                        <li class="step col-md-5">
                          <a class="step-segment get_learn_team_list" href="#tab-3" data-toggle="tab">
                            <span class="step-icon">3</span>
                          </a>
                          <div class="step-content">
                            <strong class="hidden-xs">Learner in Team</strong>
                          </div>
                        </li>
    					<li class="step col-md-5">
                          <a class="step-segment" href="#tab-4" data-toggle="tab">
                            <span class="step-icon">4</span>
                          </a>
                          <div class="step-content">
                            <strong class="hidden-xs">Courses</strong>
                          </div>
                        </li>
    					<li class="step col-md-5">
                          <a class="step-segment" href="#tab-5" data-toggle="tab">
                            <span class="step-icon">5</span>
                          </a>
                          <div class="step-content">
                            <strong class="hidden-xs">Learning Paths</strong>
                          </div>
                        </li>
    					
    					
                      </ul>
    				<div class="tab-menu">
    				 <ul class="nav nav-tabs m-t-lg">
    				 <li class="active"><a href="#tab-1" data-toggle="tab">Basic Details</a></li>
                      <li><a href="#tab-2" class="get_learners_listing" data-toggle="tab">Assign Learners</a></li>
                      <li><a href="#tab-3" class="get_learn_team_list" id="learner_team_id" data-toggle="tab">Learners in Team</a></li>
    				  <li><a href="#tab-4" data-toggle="tab">Courses</a></li>
    				  <li><a href="#tab-5" data-toggle="tab">Learning Paths</a></li>
    				 </ul>
    				 </div>
                      <div class="tab-content">
                      <div class="tab-pane fade active in" id="tab-1">
                      	<div class="over-form" style="left: 40%;top: 30%;display: none;" id="tab1_show_error_msg">
    		                <div class="alert alert-danger">
    		                    <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
    		                    <i class="icon icon-times-circle-o"></i><strong>Error!</strong> <span id="tab1_error_content"></span>
    		          		</div>
              			</div>
              			<div class="over-form" style="left: 40%;top: 30%;display: none;" id="tab1_show_success_msg">
                          <div class="alert alert-success">
                              <i class="icon icon-check-circle-o"></i>
                              <strong>Success!</strong> <span id="tab1_success_content"></span>
                          </div>
                        </div>
                       <div class="row">
    					        <div class="col-xs-12 col-sm-12 col-md-6">
    					        {{Form::open(array('id'=>'add_team_form','method'=>'post','enctype'=>'multipart/form-data'))}}
                      <?php $team_id = $request->session()->get('team_id');
                            $btn_type = $request->session()->get('btn_type');
                       ?>
                      <input type="hidden" id="team_id" value="{{ $team_id }}">
                      <input type="hidden" id="btn_type" value="{{ $btn_type }}">
    							<div class="col-xs-12 col-sm-12 col-md-12 form-group">
                                    <h5>Title<span class="color-r">*</span></h5>
                                    {!! Form::text('team_title', '', ['class' => 'form-control','id'=>'team_title', 'placeholder' => '', 'required' => '']) !!}	
                                </div>
    					        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                                    <h5>Description</h5>
                                    {{Form::textarea('team_description','',array('class'=>'form-control ckeditor','id'=>'team_description','rows' => 3))}}
                                </div>
                                <input type="hidden" id="select_btn">
    							 <div class="col-xs-12 col-sm-12 col-md-12 m-t-md">
    					         {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    					         {!! Form::submit('Save and Add Learners', ['class' => 'btn btn-primary','id'=>'save_and_add']) !!}
    					         <!-- <button type="button" id="save_and_add" class="btn btn-primary">Save and Add Learners</button> -->
                                 <button type="button" id="cancel_team_btn" class="btn btn-default">Cancel</button>
    					         </div>
    					         {{Form::close()}}
    					         </div>
    					   <div class="col-xs-12 col-sm-12 col-md-6"></div>
    					   
    					   
    				   </div>
                      </div>
          <div class="tab-pane fade" id="tab-2">
          <input type="hidden" class="tabname" value="tab-2" >
          <?php
          $default_col_order_string="CheckAll-1,EmpCode-1,EmpName-1,Department-1,ManagerName-1,Level-1,Role-1,EnrollCourses-1";
          ?>         
          <div class="table-content-header">
            <div class="table-toolbar">
              <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                <div class="btn-group bulk hide common_action">
                    <label>
                    Bulk Action :
                    </label>
                    <button class="btn btn-link link-muted click_common_action" to_do="enable" type="button">
                      <span>Enable</span>
                    </button> 
                    <span class="pipe">|</span>
                    <button class="btn btn-link link-muted click_common_action" to_do="disable" type="button">
                      <span>Disable</span>
                    </button>
                    <span class="pipe">|</span>
                    <button class="btn btn-link link-muted click_common_action" to_do="delete" type="button">
                      <span>Delete</span>
                    </button>
                    
                  </div>
              </div>
                <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                    <div class="btn-group export">
                      <a href="{{url('/admin/learners_excel')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-excel-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export Excel"></span>
                      <span class="hidden-xs">Export Excel</span>
                      </a>
                      <a href="{{url('/admin/learners_pdf')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-pdf-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export PDF"></span>
                      <span class="hidden-xs">Export PDF</span>
                      </a>
                    <div class="filter-opt1">
                    <button class="btn btn-link col" type="button">
                      <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                      <span class="hidden-xs">Column</span>
                    </button>
                    <div class="listing-det overrlay">
                    <ul class="list-group sortable_menu">
                            <?php
                            //if session has value display column according or display default array
                            if($request->session()->has('learners_hidden_column_array')){
                              $col_arr_value = $request->session()->get('learners_hidden_column_array');
                            }else{
                              $col_arr_value=$default_col_order_string;
                            }
                            $col_arr_value=explode(",",$col_arr_value);
                            // echo "<pre>";
                            // print_r($col_arr_value);
                            foreach ($col_arr_value as $key => $value) {
                                //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                $value=explode('-', $value);
                                $class="";
                                if($value[0]=='CheckAll')
                                  $class='non_sortable';
                                ?>
                                <li class="list-group-item list-order-item <?=$class?> savitest" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                  <span class="pull-right">
                                    <label class="switch switch-primary">
                                      <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                      <span class="switch-track"></span>
                                      <span class="switch-thumb"></span>
                                    </label>
                                  </span>
                                  <span class="icon icon-arrows"></span>
                                  <?php
                                  if ($value[0]=="CheckAll")
                                    echo "Check All";
                                  else if($value[0]=="Emp Code")
                                    echo "Emp Code";
                                  else if($value[0]=="Emp Name")
                                    echo "Emp Name";
                                  else if($value[0]=="Department")
                                    echo "Department";
                                  else if($value[0]=="Manager Name")
                                    echo "Manager Name";
                                   else if($value[0]=="Level")
                                    echo "Level";
                                  else if($value[0]=="Role")
                                    echo "Role";
                                  else if($value[0]=="Enroll Courses")
                                    echo "Enroll Courses";                                              
                                  else
                                    echo $value[0];
                                  ?>
                                  </li>
                                <?php
                              }
                            ?>
                          <button class="btn btn-primary btn-half save_order" form_name="learners_team" id="" type="button">Save</button>
                        <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                    </ul>
                </div>
          </div>
             
          </div>
            <div class="btn-group">
             <a href="#" id="filter-ck33" class="refine_mbutton" title="Filter"></a>
            </div>
                 
                </div>
              </div>
            </div>
            
           <div class="Cus_card filter-bg">
                        <div class="Cus_card-body ck33" style="display: none;">

              <div class="col-xs-6 col-md-3">
                                <h5>Department</h5>
                 <div class="cus-select">
                                 <select id="department_filter_list">
                                 </select>
                </div>
                             </div>
               <div class="col-xs-6 col-md-3">
                                <h5>Manager</h5>
                 <div class="cus-select">
                                 <select id="manager_filter_list">
                                 </select>
                 </div>
                             </div>
              
               <div class="col-xs-6 col-md-3">
                                <h5>Level</h5>
                                 <div class="cus-select">
                                 <select id="level_filter_list">
                                 </select>
                 </div>
                 </div>
              <div class="col-xs-6 col-md-3">
                                <h5>Enroll Courses</h5>
                                 <div class="cus-select">
                                 <select id="enrolled_courses_filter_list">
                                 </select>
                 </div>
                 </div>
               
                             <div class="col-xs-12 col-md-12">
                                <h5>Keyword</h5>
                                <input class="form-control" id="Keyword_filter" type="text">
                             </div>
                            <div class="col-xs-12 col-md-4">
                          <h5></h5>
              <button class="btn btn-primary" id="filter_btn" type="submit">Search</button>
              <button class="btn btn-default close-filter33" type="submit">Close</button>
                </div>
              
                        </div>
                      </div> 
          <div class="cus-tbl">  
              <table id="example2" class="table table-hover table-striped example2" cellspacing="0" width="100%">
                <thead></thead>
                <tbody>
                </tbody> 
              </table>
              <?php
              if($request->session()->has('learners_hidden_column_array')){
                  $learners_hidden_column_array = $request->session()->get('learners_hidden_column_array');?>
                  <input type="hidden" name="learners_hidden_column_array" id="learners_hidden_column_array" value="<?=$learners_hidden_column_array?>"><?php
              }else{?>
                  <input type="hidden" name="learners_hidden_column_array" id="learners_hidden_column_array" value="<?=$default_col_order_string?>"><?php
              }
              ?>              
          </div>
            <button class="btn btn-primary click_common_action" to_do="assign_learner" type="button">Assign Learners to Team</button>
            <button class="btn btn-default" type="button">Cancel</button>

        </div>
        <div class="tab-pane fade" id="tab-3">
          <input type="hidden" class="tabname" value="tab-3" >
          <?php
          $default_col_order_string="CheckAll-1,EmpCode-1,EmpName-1,Department-1,ManagerName-1,Level-1,Role-1,EnrollCourses-1";
          ?>         
          <div class="table-content-header">
            <div class="table-toolbar">
              <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                <div class="btn-group bulk hide common_action">
                    <label>
                    Bulk Action :
                    </label>
                    <button class="btn btn-link link-muted click_common_action" to_do="enable" type="button">
                      <span>Enable</span>
                    </button> 
                    <span class="pipe">|</span>
                    <button class="btn btn-link link-muted click_common_action" to_do="disable" type="button">
                      <span>Disable</span>
                    </button>
                    <span class="pipe">|</span>
                    <button class="btn btn-link link-muted click_common_action" to_do="delete" type="button">
                      <span>Delete</span>
                    </button>
                    
                  </div>
              </div>
                <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                    <div class="btn-group export">
                      <a href="{{url('/admin/learners_team_excel')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-excel-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export Excel"></span>
                      <span class="hidden-xs">Export Excel</span>
                      </a>
                      <a href="{{url('/admin/learners_team_pdf')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-pdf-o icon-fw" data-toggle="tooltip" data-placement="top" title="Export PDF"></span>
                      <span class="hidden-xs">Export PDF</span>
                      </a>
                    <div class="filter-opt1">
                    <button class="btn btn-link col" type="button">
                      <span class="icon icon-cogs icon-fw" data-toggle="tooltip" data-placement="top" title="Column"></span>
                      <span class="hidden-xs">Column</span>
                    </button>
                    <div class="listing-det overrlay">
                    <ul class="list-group sortable_menu">
                            <?php
                            //if session has value display column according or display default array
                            if($request->session()->has('learners_team_hidden_column_array')){
                              $col_arr_value = $request->session()->get('learners_team_hidden_column_array');
                            }else{
                              $col_arr_value=$default_col_order_string;
                            }
                            $col_arr_value=explode(",",$col_arr_value);
                            // echo "<pre>";
                            // print_r($col_arr_value);
                            foreach ($col_arr_value as $key => $value) {
                                //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                $value=explode('-', $value);
                                $class="";
                                if($value[0]=='CheckAll')
                                  $class='non_sortable';
                                ?>
                                <li class="list-group-item list-order-item <?=$class?> savitest" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                  <span class="pull-right">
                                    <label class="switch switch-primary">
                                      <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                      <span class="switch-track"></span>
                                      <span class="switch-thumb"></span>
                                    </label>
                                  </span>
                                  <span class="icon icon-arrows"></span>
                                  <?php
                                  if ($value[0]=="CheckAll")
                                    echo "Check All";
                                  else if($value[0]=="Emp Code")
                                    echo "Emp Code";
                                  else if($value[0]=="Emp Name")
                                    echo "Emp Name";
                                  else if($value[0]=="Department")
                                    echo "Department";
                                  else if($value[0]=="Manager Name")
                                    echo "Manager Name";
                                   else if($value[0]=="Level")
                                    echo "Level";
                                  else if($value[0]=="Role")
                                    echo "Role";
                                  else if($value[0]=="Enroll Courses")
                                    echo "Enroll Courses";                                              
                                  else
                                    echo $value[0];
                                  ?>
                                  </li>
                                <?php
                              }
                            ?>
                          <button class="btn btn-primary btn-half save_order" form_name="learners" id="" type="button">Save</button>
                        <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                    </ul>
                </div>
          </div>
             
          </div>
            <div class="btn-group">
             <a href="#" id="filter-ck3" class="refine_mbutton" title="Filter"></a>
            </div>
                 
                </div>
              </div>
            </div>
            
           <div class="Cus_card filter-bg">
                        <div class="Cus_card-body ck3" style="display: none;">

              <div class="col-xs-6 col-md-3">
                                <h5>Department</h5>
                 <div class="cus-select">
                                 <select id="department_filter_list_team">
                                 </select>
                </div>
                             </div>
               <div class="col-xs-6 col-md-3">
                                <h5>Manager</h5>
                 <div class="cus-select">
                                 <select id="manager_filter_list_team">
                                 </select>
                 </div>
                             </div>
              
               <div class="col-xs-6 col-md-3">
                                <h5>Level</h5>
                                 <div class="cus-select">
                                 <select id="level_filter_list_team">
                                 </select>
                 </div>
                 </div>
              <div class="col-xs-6 col-md-3">
                                <h5>Enroll Courses</h5>
                                 <div class="cus-select">
                                 <select id="enrolled_courses_filter_list_team">
                                 </select>
                 </div>
                 </div>
               
                             <div class="col-xs-12 col-md-12">
                                <h5>Keyword</h5>
                                <input class="form-control" id="Keyword_filter_team" type="text">
                             </div>
                            <div class="col-xs-12 col-md-4">
                          <h5></h5>
              <button class="btn btn-primary" id="filter_btn_team" type="submit">Search</button>
              <button class="btn btn-default close-filter3" type="submit">Close</button>
                </div>
              
                        </div>
                      </div> 
          <div class="cus-tbl">  
              <table id="example3" class="table table-hover table-striped example3" cellspacing="0" width="100%">
                <thead></thead>
                <tbody>
                </tbody> 
              </table>
              <?php
              if($request->session()->has('learners_team_hidden_column_array')){
                  $learners_team_hidden_column_array = $request->session()->get('learners_team_hidden_column_array');?>
                  <input type="hidden" name="learners_team_hidden_column_array" id="learners_team_hidden_column_array" value="<?=$learners_team_hidden_column_array?>"><?php
              }else{?>
                  <input type="hidden" name="learners_team_hidden_column_array" id="learners_team_hidden_column_array" value="<?=$default_col_order_string?>"><?php
              }
              ?>              
          </div>
            <button class="btn btn-primary click_common_action" to_do="remove_learner" type="button">Remove Learners from Team</button>
            <button class="btn btn-default" type="button">Cancel</button>
        </div>
    				  <div class="tab-pane fade" id="tab-4">
                <div class="over-form" style="left: 40%;top: 30%;display: none;" id="tab4_show_success_msg">
                          <div class="alert alert-success">
                              <i class="icon icon-check-circle-o"></i>
                              <strong>Success!</strong> <span id="tab4_success_content"></span>
                          </div>
                </div>
                <div class="over-form" style="left: 40%;top: 30%;display: none;" id="tab4_show_error_msg">
                    <div class="alert alert-danger">
                        <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                        <i class="icon icon-times-circle-o"></i><strong>Error!</strong> <span id="tab4_error_content"></span>
                  </div>
                </div>            
                <div class="row">
      				  <div class="col-xs-12 col-sm-5 col-md-5 course-col">
      				  <div class="heading">
      				  <h5>Selected Course for the Team</h5>
      				  <div class="pull-xs-right"> <a href="#" class="refine_mbutton filter-ck1" id="filter-ck1" title="Filter"></a></div>
      				  </div>
      				  
      				  <div class="Cus_card filter-bg">
                              <div class="Cus_card-body ck1" style="display: none;">
      							
                                  <div class="col-xs-12 col-md-6">
                                      <h5>Category</h5>
      								 <div class="cus-select">
                                       <select id="course_team_category">
                                       <option value="A">A</option>
                                       <option value="B">B</option>
                                       </select>
      								 </div>
                                  </div>
      							<div class="col-xs-12 col-md-6">
                                      <h5>Courses</h5>
      								 <div class="cus-select">
                                       <select id="course_team_filter_course">
                                       <option value="A">A</option>
                                       <option value="B">B</option>
                                       </select>
      								 </div>
                                  </div>
      							<div class="col-xs-12 col-md-6">
                                      <h5>Development Area</h5>
      								 <div class="cus-select">
                                       <select id="course_team_dev_area">
                                       <option value="A">A</option>
                                       <option value="B">B</option>
                                       </select>
      								</div>
                                   </div>
      							<div class="col-xs-12 col-md-6">
                                      <h5>Seniority Level</h5>
      								 <div class="cus-select">
                                       <select id="course_team_seniority">
                                       <option value="A">A</option>
                                       <option value="B">B</option>
                                       </select>
      								 </div>
                                   </div>
      							<div class="col-xs-12 col-md-12">
                                      <h5>Keyword</h5>
                                      <input class="form-control" id="course_team_keyword" type="text">
                                   </div>
      							<div class="col-xs-12 col-md-12">
      	                        <h5></h5>
      							<button class="btn btn-primary" id="filter_course_team_btn" type="submit">Search</button>
      							<button class="btn btn-default close-filter1" type="submit" title="Close">Close</button>
      						    </div>
      								 
      						   
                                  
                                  
      							
                              </div>
                            </div>
      				  
      				  <div class="row" id="getcoursesteamlist">
      					</div>
      				  </div>
      				  <div class="col-xs-12 col-sm-2 col-md-2">
      				  <div class="btn-fn hidden-xs">
      				  <button id="add_to_team" class="btn btn-default"><i class="icon icon-angle-double-left"></i> Add </button>
      				  <button id="remove_from_team" class="btn btn-default"> Remove <i class="icon icon-angle-double-right"></i> </button>
      				  </div>
      				  
      				  <div class="mob-btn-fn hidden-sm hidden-md hidden-lg">
      				  <button class="btn btn-default"><i class="icon icon-angle-double-up"></i> Add</button>
      				  <button class="btn btn-default"> Remove <i class="icon icon-angle-double-down"></i></button>
      				  </div>
      					  
      				  </div>
      				  <div class="col-xs-12 col-sm-5  col-md-5 course-col">
      				  <div class="heading">
      				  <h5>Full Course List</h5>
      				  <div class="pull-xs-right"> <a href="#" class="refine_mbutton filter-ck2" id="filter-ck2"  title="Filter"></a></div>
      				  </div>
      				  
      				  <div class="Cus_card filter-bg">
                        <div class="Cus_card-body ck2" style="display: none;">
      							
                                  <div class="col-xs-12 col-md-6">
                                      <h5>Category</h5>
      								 <div class="cus-select">
                                       <select  id="course_category">
                                       <option value="A">A</option>
                                       <option value="B">B</option>
                                       </select>
      								 </div>
                                  </div>
      							<div class="col-xs-12 col-md-6">
                                      <h5>Courses</h5>
      								 <div class="cus-select">
                                       <select id="course_filter_course">
                                       <option value="A">A</option>
                                       <option value="B">B</option>
                                       </select>
      								 </div>
                                  </div>
      							<div class="col-xs-12 col-md-6">
                                      <h5>Development Area</h5>
      								 <div class="cus-select">
                                       <select  id="course_dev_area">
                                       <option value="A">A</option>
                                       <option value="B">B</option>
                                       </select>
      								</div>
                                   </div>
      							<div class="col-xs-12 col-md-6">
                                      <h5>Seniority Level</h5>
      								 <div class="cus-select">
                                       <select id="course_seniority">
                                       <option value="A">A</option>
                                       <option value="B">B</option>
                                       </select>
      								 </div>
                                   </div>
      							<div class="col-xs-12 col-md-12">
                                      <h5>Keyword</h5>
                                      <input class="form-control" id="course_keyword" type="text">
                                   </div>
      							<div class="col-xs-12 col-md-12">
      	                        <h5></h5>
      							<button class="btn btn-primary" id="filter_course_all_btn" type="submit">Search</button>
      							<button class="btn btn-default close-filter2" type="submit" title="Close">Close</button>
      						    </div>
      								 
      						   
                                  
                                  
      							
                              </div>
                        </div>
      					 
      				  <div class="row" id="getcourseslist">
      					</div>  
      					  
      				  </div>
      				   
      				  </div>
              </div>
    				  <div class="tab-pane fade" id="tab-5">
                <div class="over-form" style="left: 40%;top: 30%;display: none;" id="tab5_show_success_msg">
                          <div class="alert alert-success">
                              <i class="icon icon-check-circle-o"></i>
                              <strong>Success!</strong> <span id="tab5_success_content"></span>
                          </div>
                </div>
                <div class="over-form" style="left: 40%;top: 30%;display: none;" id="tab5_show_error_msg">
                    <div class="alert alert-danger">
                        <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                        <i class="icon icon-times-circle-o"></i><strong>Error!</strong> <span id="tab5_error_content"></span>
                  </div>
                </div>                  
              <div class="row">
              <div class="col-xs-12 col-sm-5 col-md-5 course-col">
              <div class="heading">
              <h5>Selected Learning Paths for the Team</h5>
              <a href="#" id="filter-ck11" class="refine_mbutton" title="Filter"></a>
              </div>
              
              <div class="Cus_card filter-bg">
                            <div class="Cus_card-body ck11" style="display: none;">
                  
                                <div class="col-xs-12 col-md-6">
                                    <h5>Category</h5>
                     <div class="cus-select">
                                     <select  id="learning_path_team_category">
                                     <option value="A">A</option>
                                     <option value="B">B</option>
                                     </select>
                     </div>
                                </div>
                  <div class="col-xs-12 col-md-6">
                                    <h5>Courses</h5>
                     <div class="cus-select">
                                     <select id="learning_path_team_course">
                                     <option value="A">A</option>
                                     <option value="B">B</option>
                                     </select>
                     </div>
                                </div>
                  <div class="col-xs-12 col-md-6">
                                    <h5>Development Area</h5>
                     <div class="cus-select">
                                     <select id="learning_path_team_dev_area">
                                     <option value="A">A</option>
                                     <option value="B">B</option>
                                     </select>
                    </div>
                                 </div>
                  <div class="col-xs-12 col-md-6">
                                    <h5>Seniority Level</h5>
                     <div class="cus-select">
                                     <select id="learning_path_team_seniority">
                                     <option value="A">A</option>
                                     <option value="B">B</option>
                                     </select>
                     </div>
                                 </div>
                  <div class="col-xs-12 col-md-12">
                                    <h5>Keyword</h5>
                                    <input class="form-control" id="full_path_team_keyword" type="text">
                                 </div>
                  <div class="col-xs-12 col-md-12">
                              <h5></h5>
                  <button class="btn btn-primary" id="filter_path_team_all_btn" type="submit">Search</button>
                  <button class="btn btn-default close-filter11" type="submit" title="Close">Close</button>
                    </div>
                     
                   
                                
                                
                  
                            </div>
                          </div>
              
              <div class="row" id="getlearningpathteamlist">
              <div class="col-xs-12  col-lg-6 padding-right">
               <div class="course-card">
               <div class="media">
                          <div class="media-left">
                          <span class="bg-primary circle sq-50">T</span>
                          </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Time Management Lorum ipsum ipsum dolor ipsum dolor</h5>
                        <small>Lorum ipsum ipsum dolor ipsum dolor Lorum ipsum ipsum dolor ipsum dolor</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-left">
               <div class="course-card">
               <div class="media">
                      <div class="media-left">
                        <span class=" bg-info circle sq-50">L</span>
                      </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Leadership</h5>
                        <small>28 days course, Advance learner</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                 
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-right">
               <div class="course-card">
               <div class="media">
                          <div class="media-left">
                          <span class="bg-warning circle sq-50">P</span>
                          </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Public Speaking</h5>
                        <small>Lorum ipsum ipsum dolor ipsum dolor</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                 
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-left">
               <div class="course-card">
               <div class="media">
                      <div class="media-left">
                        <span class="bg-danger circle sq-50">C</span>
                      </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Communication</h5>
                        <small>28 days course, Advance learner</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-right">
               <div class="course-card">
               <div class="media">
                          <div class="media-left">
                          <span class="bg-violet circle sq-50">M</span>
                          </div>
                          <div class="media-middle media-body">
                          <h5 class="media-heading">Management Skill</h5>
                           <small>Lorum ipsum ipsum dolor ipsum dolor</small>
                          </div>
                          
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-left">
               <div class="course-card">
               <div class="media">
                       <div class="media-left">
                        <span class="bg-danger circle sq-50">C</span>
                      </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Communication</h5>
                        <small>28 days course, Advance learner</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                 
               </div>
               </div>
              </div>
              </div>
              </div>
              <div class="col-xs-12 col-sm-2 col-md-2">
              <div class="btn-fn hidden-xs">
              <button class="btn btn-default" id="add_learn_path_to_team"><i class="icon icon-angle-double-left"></i> Add </button>
              <button class="btn btn-default" id="remove_learn_path_to_team"> Remove <i class="icon icon-angle-double-right"></i> </button>
              </div>
              
              <div class="mob-btn-fn hidden-sm hidden-md hidden-lg">
              <button class="btn btn-default"><i class="icon icon-angle-double-up"></i> Add</button>
              <button class="btn btn-default"> Remove <i class="icon icon-angle-double-down"></i></button>
              </div>
                
              </div>
              <div class="col-xs-12 col-sm-5  col-md-5 course-col">
              <div class="heading">
              <h5>Full Learning Paths List</h5>
              <a href="#" id="filter-ck22" class="refine_mbutton"  title="Filter"></a>
              </div>
              
              <div class="Cus_card filter-bg">
                      <div class="Cus_card-body ck22" style="display: none;">
                  
                                <div class="col-xs-12 col-md-6">
                                    <h5>Category</h5>
                     <div class="cus-select">
                                     <select  id="learning_path_category">
                                     <option value="A">A</option>
                                     <option value="B">B</option>
                                     </select>
                     </div>
                                </div>
                  <div class="col-xs-12 col-md-6">
                                    <h5>Courses</h5>
                     <div class="cus-select">
                                     <select id="learning_path_course">
                                     <option value="A">A</option>
                                     <option value="B">B</option>
                                     </select>
                     </div>
                                </div>
                  <div class="col-xs-12 col-md-6">
                                    <h5>Development Area</h5>
                     <div class="cus-select">
                                     <select id="learning_path_dev_area">
                                     <option value="A">A</option>
                                     <option value="B">B</option>
                                     </select>
                    </div>
                                 </div>
                  <div class="col-xs-12 col-md-6">
                                    <h5>Seniority Level</h5>
                     <div class="cus-select">
                                     <select  id="learning_path_seniority">
                                     <option value="A">A</option>
                                     <option value="B">B</option>
                                     </select>
                     </div>
                                 </div>
                  <div class="col-xs-12 col-md-12">
                                    <h5>Keyword</h5>
                                    <input class="form-control" id="full_path_keyword" type="text">
                                 </div>
                  <div class="col-xs-12 col-md-12">
                              <h5></h5>
                  <button class="btn btn-primary" id="filter_path_all_btn" type="submit">Search</button>
                  <button class="btn btn-default close-filter22" type="submit" title="Close">Close</button>
                    </div>
            
                  
                            </div>
                      </div>
               
              <div class="row" id="getlearningpathlist">
              <div class="col-xs-12  col-lg-6 padding-right">
               <div class="course-card">
               <div class="media">
                          <div class="media-left">
                          <span class="bg-primary circle sq-50">T</span>
                          </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Time Management Lorum ipsum ipsum dolor ipsum dolor</h5>
                        <small>Lorum ipsum ipsum dolor ipsum dolor Lorum ipsum ipsum dolor ipsum dolor</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-left">
               <div class="course-card">
               <div class="media">
                      <div class="media-left">
                        <span class=" bg-info circle sq-50">L</span>
                      </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Leadership</h5>
                        <small>28 days course, Advance learner</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                 
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-right">
               <div class="course-card">
               <div class="media">
                          <div class="media-left">
                          <span class="bg-warning circle sq-50">P</span>
                          </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Public Speaking</h5>
                        <small>Lorum ipsum ipsum dolor ipsum dolor</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                 
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-left">
               <div class="course-card">
               <div class="media">
                      <div class="media-left">
                        <span class="bg-danger circle sq-50">C</span>
                      </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Communication</h5>
                        <small>28 days course, Advance learner</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-right">
               <div class="course-card">
               <div class="media">
                          <div class="media-left">
                          <span class="bg-violet circle sq-50">M</span>
                          </div>
                          <div class="media-middle media-body">
                          <h5 class="media-heading">Management Skill</h5>
                           <small>Lorum ipsum ipsum dolor ipsum dolor</small>
                          </div>
                          
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                
               </div>
               </div>
              </div>
              <div class="col-xs-12  col-lg-6 padding-left">
               <div class="course-card">
               <div class="media">
                       <div class="media-left">
                        <span class="bg-danger circle sq-50">C</span>
                      </div>
                       <div class="media-middle media-body">
                        <h5 class="media-heading">Communication</h5>
                        <small>28 days course, Advance learner</small>
                       </div>
                      
                    </div>
               <div class="overlay">
                   <label class="custom-control custom-control-primary custom-checkbox">
                                <input class="custom-control-input" type="checkbox">
                                <span class="custom-control-indicator"></span>
                              </label>
                 
               </div>
               </div>
              </div>
              </div>  
                
              </div>
               
              </div>
              </div>
    				  
    				  </div>
    				</div>
                    
                    </div>
                    
                    </div>
                </div>
            </div>	

         <script src="{{ url('public/js') }}/team_management.js"></script>
    @stop