@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

        
<div class="title-bar clear">
	<div class="col-xs-12 col-sm-8 col-md-8">
    <h1 class="title-bar-title">
		<i class="icon icon-file-text"></i>
        <span class="d-ib article_heading">Upload Article to Content Library</span>
    </h1>
    <p class="title-bar-description">
      <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
    </p>
	</div>
</div>

<div class="layout-content-body">
    <div class="row gutter-xs">
        <div class="col-xs-12">
		<div class="demo-form-wrapper">
		<div class="panel add-webinar">
			<div class="over-form" id="show_loader" style="left: 40%;top: 75%;display: none;">
              <i class="fa fa-spinner fa-spin" style="font-size:50px;color: #ff9d4e;z-index: 9999;"></i>
        	</div>
					<div class="over-form" style="left: 30%;top: 30%;display: none;" id="show_msg">
			          <div class="alert alert-success upload_success_msg">
			              <i class="icon icon-check-circle-o"></i>
			              <strong>Success!</strong> <span id="your_msg"></span>
			          </div>
			        </div>	        	
			<div class="alert alert-danger upload_error_msg" style="display: none;">
                    <!--<a href="#" class="close" data-dismiss="alert">&times;</a>-->
                    <i class="icon icon-times-circle-o"></i><strong>Error!</strong><span id="add_art_error"></span>
        	</div>
			{{Form::open(array('id'=>'add_article_form','method'=>'post','enctype'=>'multipart/form-data'))}}
          <input type="hidden" value="{{ csrf_token() }}" name="_token">
		 <div class="minus-margin">
		    <div class="media text-center upload-sec" style="padding: 13px 20px;" id="add-article">
              <div class="media-body p-t-sm">
             <label>
                          <h4 class="media-heading"><span class="icon icon-camera color-o"></span> Upload cover image</h4>
                          <p class="media-description">
                          <small>Max file size: 5 MB. Accepted file type: .jpg,.png</small>
              <input class="file-upload-input" type="file" accept="image/x-png,image/jpeg,image/jpg" id="art_upload_img" name="file">
             <input type="hidden" class="image_src" name="image_src">
             <input type="hidden" class="check_img_type" value="addimage">
             <span id="get_crop_img_add" style="display: none;"></span>
                          </p>
             </label>
              </div>
              <div class="hrtigm" style="margin-left: -15px; display: none;"></div>
              <div id="crop_image_div" style="display: none;">
                      <div class="row">
                          <div class="text-center" style="margin: 0px auto;">
                              <div id="upload_demo"></div>
                          </div>                
                      </div>
              </div>
          </div>
		</div>	
		<div class="row fshfhsdbfhsb">
				  <div class="col-sm-8 col-md-9">
				    
					<div class="col-xs-12 col-sm-12 col-md-12">
                        <h5>Title<span class="color-r">*</span></h5>
                        {!! Form::text('title', '', ['class' => 'form-control','id'=>'set_title', 'placeholder' => '', 'required' => '']) !!}
						<h5>Description</h5>
                        {{Form::textarea('description','',array('class'=>'form-control ckeditor','id'=>'description','rows' => 5))}}
                    	<h5>Body<span class="color-r">*</span></h5>
                        {{Form::textarea('body','',array('class'=>'form-control ckeditor','id'=>'body','rows' => 5,'required' => ''))}}
                    </div>
					<div class="col-xs-12 col-sm-6 col-md-4">
                      <h5>Competency <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.COMPETENCY_HELP') }}">?</span></h5>
	                 <select name="competency" id="competency_id" class="form-control">
	                 <option value="">Select Competency</option>
	                 @foreach($competencyList as $competency)
	                  <option value="{{$competency['tcm_id']}}">{{$competency['tcm_competency_title']}}</option>
					  @endforeach
	                 </select>
                    </div>
					<div class="col-xs-12 col-sm-6 col-md-4">
                        <h5>Development Area <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.DEVELOPEMENT_AREA_HELP') }}">?</span></h5>
	                 <select name="development_area" id="development_area_id" class="form-control">
	                 <option value="">Select Development Area</option>
	                 @foreach($developementAreaList as $developementArea)
	                 <option value="{{$developementArea['tda_id']}}">{{$developementArea['tda_development']}}</option>
					  @endforeach
	                 </select>
                    </div>
					<div class="col-xs-12 col-sm-6 col-md-4">
                        <h5>Level<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.LEVEL_HELP') }}">?</span></h5>
	                 <select name="level" id="level" class="form-control">
	                  <option value="">Select Level</option>
	                 @foreach($levelList as $level)
	                 <option value="{{$level['tsl_id']}}">{{$level['tsl_seniority']}}</option>
					  @endforeach
	                 </select>
                    </div>
					<div class="col-xs-12 col-sm-6 col-md-4">
                     <h5>Keywords<span class="color-r">*</span> <span class="badge1 badge-default" data-toggle="tooltip" data-placement="top" title="{{ Config('messages.contentlibrary.KEYWORDS_HELP') }}">?</span></h5>
					 {!! Form::text('keywords', '', ['class' => 'form-control','id'=>'keywords','data-role'=>"tagsinput", 'placeholder' => '', 'required' => '']) !!}
                 	<small>(Eg. abc,efg)</small>
                    </div>
					<div class="col-xs-12 col-sm-6 col-md-4">
                     <h5>Author<span class="color-r">*</span></h5>
					 {!! Form::text('author', '', ['class' => 'form-control','id'=>'author', 'placeholder' => '', 'required' => '']) !!}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                    	<div class="panel-group link-course-panel" style="display: none;">
                        <div class="panel">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                               <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="icon icon-link m-r-sm"></i>Linked Courses</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" >
                                <div class="panel-body" id="coursearticle">
                                  
                                </div>
                            </div>
                        </div>
                     
                    </div>
                    </div>
					  <div class="clearfix"></div>
					 <div class="col-md-12 m-t-md">
                        <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="is_active" name="is_active" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Active</span>
                        </label>
					    <label class="custom-control custom-control-default custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="is_available" name="is_available" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">Available to all Learners</span>
                        </label>                     
					 </div>
					  
					
				 
					  
				  </div>
				  
				  <div class="col-sm-4 col-md-3 padding">
				  <div class="user-avatar">
				   <label class="user-btn-remove">
                   <span class="icon icon-times" title="Remove"></span>
                   </label>
                   <label class="user-avatar-btn">
                   <span class="icon icon-camera"></span>
                   <input class="file-upload-input"  id="art_upload_img_small" type="file" name="file">
                    <input type="hidden" class="image_src_small" name="image_src_small">
		            <input type="hidden" class="check_img_type_small" value="addimage">
		            <span id="get_crop_img_add_small" style="display: none;"></span>
                   </label>
                   <img class="img-responsive artnoimg" width="100%" src="../public/adminlte/img/no-img.jpg" alt="">
                   <div class="hrtigm_small" style="display: none;"></div>
                   </div>

                    <div id="crop_image_div_small" style="display: none;">
                      <div class="row">
                          <div class="text-center" style="margin: 0px auto;">
                              <div id="upload_demo_small"></div>
                          </div>                
                      </div>
              		</div>
				     <div class="img-detail">
					   <h4>Upload article cover image</h4>
					   <p>Recommended dimensions: 450x253px. <br/>
                          Accepted File Types: .png, .gif, .jpeg
                       </p>
				     </div>
				  </div>
			  	<input type="hidden" id="edit_article_id" name="edit_article_id">
				   <div class="col-sm-12 col-md-12 m-t-md m-b-md m-l-md">
		            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    <button type="button" id="close_add_modal" class="btn btn-default">Cancel</button>
		            </div>
					
				
			</div>
			  
          
		  {{Form::close()}}
		  </div>
		</div>
        
        </div>
        
        </div>
    </div>

<script src="{{ url('public/js/contentlibrary') }}/content_library.js"></script>
@stop    