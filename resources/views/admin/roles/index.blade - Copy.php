@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
        <div class="title-bar clear">
            <div class="col-xs-12 col-sm-7 col-md-8">
            <h1 class="title-bar-title">
                <i class="icon icon-list-ul"></i>
                <span class="d-ib">@lang('global.roles.title')</span>
            </h1>
            <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
            </p>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 ad_top">
            <button style="right: 1px;" class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
            <!-- <a class="btn btn-primary" href="{{url('/admin/showAddTeam')}}">Add Team</a> -->
            @can('role_create')
            <a href="{{ route('admin.roles.create') }}" style="right: 1px;" class="btn btn-primary">@lang('global.app_add_new')</a>
            @endcan
            
            <!--<a href="#" id="panel-fullscreen" role="button" title="Toggle fullscreen"><i class="icon icon-expand"></i></a>-->
            </div>
            
        </div>    

    

    <div class="panel panel-default">

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($roles) > 0 ? 'datatable' : '' }} @can('role_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('role_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('global.roles.fields.title')</th>
                        <th>@lang('global.roles.fields.permission')</th>
                                                <th>&nbsp;</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($roles) > 0)
                        @foreach ($roles as $role)
                            <tr data-entry-id="{{ $role->id }}">
                                @can('role_delete')
                                    <td></td>
                                @endcan

                                <td>{{ $role->title }}</td>
                                <td>
                                    @foreach ($role->permission as $singlePermission)
                                        @php $permission_title = str_replace("_"," ",$singlePermission->title);
                                           $permission_title = ucwords($permission_title);
                                        @endphp
                                        <span class="label label-info label-many">{{ $permission_title }}</span>
                                    @endforeach
                                </td>
                                                                <td>
                                    @can('role_view')
                                    <a href="{{ route('admin.roles.show',[$role->id]) }}" class="btn btn-xs btn-primary">@lang('global.app_view')</a>
                                    @endcan
                                    @can('role_edit')
                                    <a href="{{ route('admin.roles.edit',[$role->id]) }}" class="btn btn-xs btn-info edit_role">@lang('global.app_edit')</a>
                                    @endcan
                                    @can('role_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.roles.destroy', $role->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('role_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.roles.mass_destroy') }}';
        @endcan

    </script>
@endsection