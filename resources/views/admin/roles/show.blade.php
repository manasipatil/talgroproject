@extends('layouts.app')

@section('content')
        <div class="title-bar clear">
            <div class="col-xs-12 col-sm-7 col-md-8">
            <h1 class="title-bar-title">
                <i class="icon icon-list-ul"></i>
                <span class="d-ib">@lang('global.roles.title')</span>
            </h1>
            <p class="title-bar-description">
              <small>View Role</small>
            </p>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 ad_top">
            <button style="right: 1px;" class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
            <!-- <a class="btn btn-primary" href="{{url('/admin/showAddTeam')}}">Add Team</a> -->
            @can('role_create')
            <a href="{{ route('admin.roles.create') }}" style="right: 1px;" class="btn btn-primary">@lang('global.app_add_new')</a>
            @endcan
            
            <!--<a href="#" id="panel-fullscreen" role="button" title="Toggle fullscreen"><i class="icon icon-expand"></i></a>-->
            </div>
            
        </div>     

    <div class="panel panel-default">

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('global.roles.fields.title')</th>
                            <td>{{ $role->title }}</td>
                        </tr>
                        <tr>
                            <th>User Type</th>
                            <?php if(empty($user_type)){?>
                            <td> - </td>
                            <?php }else{ ?>
                            <td>{{ $user_type[0]['user_type'] }}</td>
                            <?php } ?>
                        </tr>                        
                        <tr>
                            <th>@lang('global.roles.fields.permission')</th>
                            <td>
                                @foreach ($role->permission as $singlePermission)
                                        @php $permission_title = str_replace("_"," ",$singlePermission->title);
                                           $permission_title = ucwords($permission_title);
                                        @endphp
                                    <span class="label label-info label-many">{{ $permission_title }}</span>
                                @endforeach
                            </td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->



            <p>&nbsp;</p>

        <!-- <a href="{{ route('admin.roles.index') }}" class="btn btn-default">@lang('global.app_back_to_list')</a> -->
        </div>
    </div>
    <script src="{{ url('public/js/roles') }}/role_management.js"></script>
@stop