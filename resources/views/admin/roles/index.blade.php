@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
        <div class="title-bar clear">
            <div class="col-xs-12 col-sm-7 col-md-8">
            <h1 class="title-bar-title">
                <i class="icon icon-list-ul"></i>
                <span class="d-ib">@lang('global.roles.title')</span>
            </h1>
            <p class="title-bar-description">
              <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small>
            </p>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 ad_top">
            <button style="right: 1px;" class="btn btn-primary fullscreen" id="panel-fullscreen" type="button"><i class="icon icon-expand"></i></button>
            <!-- <a class="btn btn-primary" href="{{url('/admin/showAddTeam')}}">Add Team</a> -->
            @can('role_create')
            <a href="{{ route('admin.roles.create') }}" style="right: 1px;" class="btn btn-primary">@lang('global.app_add_new')</a>
            @endcan
            
            <!--<a href="#" id="panel-fullscreen" role="button" title="Toggle fullscreen"><i class="icon icon-expand"></i></a>-->
            </div>
            
        </div>    

        <div class="layout-content-body">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-body">
                  <?php
                  if($request->session()->has('get_permission_list')){
                      $get_permission_list = $request->session()->get('get_permission_list');
                      $get_permission_list=explode(",",$get_permission_list[0]['title']);
                      // dd(in_array('role_view', $get_permission_list));die();exit();
                  }else{
                      $get_permission_list = "false";
                  }
                  if($get_permission_list != "false"){
                      if (!in_array('role_edit', $get_permission_list) && !in_array('role_view', $get_permission_list) && !in_array('role_delete', $get_permission_list)) {
                          $default_col_order_string="CheckAll-1,Title-1,Permissions-1";
                      }else{
                          $default_col_order_string="CheckAll-1,Title-1,Permissions-1,Action-1";
                      }   
                  }else{
                      $default_col_order_string="CheckAll-1,Title-1,Permissions-1,Action-1";
                  }
                  // dd($default_col_order_string);
                  ?>                        
              <div class="table-content-header">
              <div class="table-toolbar">
                <div class="table-toolbar-tools pull-xs-left pull-sm-left">
                  <div class="btn-group bulk hide common_action">
                      <label>
                      Bulk Action :
                      </label>
                    <button class="btn btn-link link-muted click_common_action" to_do="enable"  type="button">
                      <span>Enable</span>
                    </button> 
                    <span class="pipe">|</span>
                    <button class="btn btn-link link-muted click_common_action" to_do="disable"  type="button">
                      <span>Disable</span>
                    </button>
                    <span class="pipe">|</span>
                    <button class="btn btn-link link-muted click_common_action" to_do="delete"  type="button">
                      <span>Delete</span>
                    </button>
                    
                  </div>
                </div>
                <div class="table-toolbar-tools pull-xs-right pull-sm-right">
                    <div class="btn-group export">
                    <a href="{{url('/admin/role_list_excel')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-excel-o icon-fw" title="Export Excel"></span>
                      <span class="hidden-xs">Export Excel</span>
                    </a>
                    <a href="{{url('/admin/role_list_pdf')}}" type="button" class="btn btn-link link-muted">
                      <span class="icon icon-file-pdf-o icon-fw" title="Export PDF"></span>
                      <span class="hidden-xs">Export PDF</span>
                    </a>
                    <div class="filter-opt1">
                    <button class="btn btn-link col" type="button">
                      <span class="icon icon-cogs icon-fw" title="Column"></span>
                      <span class="hidden-xs">Column</span>
                    </button>
                    <div class="listing-det overrlay">
                        <ul class="list-group sortable_menu">
                            <?php
                            //if session has value display column according or display default array
                            if($request->session()->has('role_listing_hidden_column_array')){
                              $col_arr_value = $request->session()->get('role_listing_hidden_column_array');
                            }else{
                              $col_arr_value=$default_col_order_string;
                            }
                            $col_arr_value=explode(",",$col_arr_value);
                            // echo "<pre>";
                            // print_r($col_arr_value);
                            foreach ($col_arr_value as $key => $value) {
                                //explicit string into array so 0th array would be column name and 1th array would be column visibility
                                $value=explode('-', $value);
                                $class="";
                                if($value[0]=='CheckAll')
                                  $class='non_sortable';
                                ?>
                                <li class="list-group-item list-order-item <?=$class?> savitest" data-item="<?=$value[0]?>" data-visible="<?=$value[1]?>">
                                  <span class="pull-right">
                                    <label class="switch switch-primary">
                                      <input class="switch-input chk_col_visible" type="checkbox" name="layout-header-fixed"  data-sync="true" <?php if($value[1] === '1') echo 'checked="checked"';?>>
                                      <span class="switch-track"></span>
                                      <span class="switch-thumb"></span>
                                    </label>
                                  </span>
                                  <span class="icon icon-arrows"></span>
                                  <?php
                                  if ($value[0]=="CheckAll")
                                    echo "Check All";
                                  else if($value[0]=="Title")
                                    echo "Title";
                                  else if($value[0]=="Permissions")
                                    echo "Permissions";
                                   else if($value[0]=="Action")
                                    echo "Action";                                              
                                  else
                                    echo $value[0];
                                  ?>
                                  </li>
                                <?php
                              }
                            ?>
                          <button class="btn btn-primary btn-half save_order" form_name="role_listing" id="" type="button">Save</button>
                        <button class="btn btn-default btn-half clse" type="button">Cancel</button>
                        </ul>
                </div>
                    </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="cus-tbl">    
              <table id="role_list_tbl" class="table table-hover table-striped role_list_tbl" cellspacing="0" width="100%">
                <thead></thead>
                <tbody>
                </tbody> 
              </table>
              <?php
              if($request->session()->has('role_listing_hidden_column_array')){
                  $role_listing_hidden_column_array = $request->session()->get('role_listing_hidden_column_array');?>
                  <input type="hidden" name="role_listing_hidden_column_array" id="role_listing_hidden_column_array" value="<?=$role_listing_hidden_column_array?>"><?php
              }else{?>
                  <input type="hidden" name="role_listing_hidden_column_array" id="role_listing_hidden_column_array" value="<?=$default_col_order_string?>"><?php
              }
              ?>  
            </div>
                            
            </div>   
        </div>
    </div>
    </div>
</div>
<script src="{{ url('public/js/roles') }}/role_listing.js"></script>
@stop

@section('javascript') 
    <script>
        @can('role_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.roles.mass_destroy') }}';
        @endcan

    </script>
@endsection