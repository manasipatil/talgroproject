@extends('layouts.app')

@section('content')
        <div class="title-bar clear">
            <div class="col-xs-12 col-sm-8 col-md-8">
            <h1 class="title-bar-title">
                <i class="icon icon-file-text"></i>
                <span class="d-ib">@lang('global.roles.title')</span>
            </h1>
            <p class="title-bar-description">
              <small>Create Roles</small>
            </p>
            </div>
            
            
        </div>

  <div class="layout-content-body">    
    {!! Form::open(['method' => 'POST', 'route' => ['admin.roles.store']]) !!}

    <div class="panel panel-default">
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('title', 'Title*', ['class' => 'control-label']) !!}
                                        @if($errors->has('title'))
                        <small class="text-danger" style="color: red;">{{ $errors->first('title') }}</small>
                    @endif
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}


                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('user_type', 'User Type*', ['class' => 'control-label']) !!}
                    <select id="user_type" name="user_type" class='form-control' required="required">
                        <option value="">Select User Type</option>
                        @foreach($user_type as $user_type_list)
                        <option value="{{ $user_type_list['tutm_id'] }}">{{ $user_type_list['tutm_user_type'] }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('user_type'))
                        <small class="text-danger" style="color: red;">{{ $errors->first('user_type') }}</small>
                    @endif
                </div>
            </div>            
            <div class="row">
                <div class="col-xs-12 form-group">
                    @php  @endphp
                    {!! Form::label('permission', 'Permissions*', ['class' => 'control-label']) !!}
                    @if($errors->has('permission'))
                        <small class="text-danger" style="color: red;">{{ $errors->first('permission') }}</small>
                    @endif
                    <table class="table table-bordered table-striped">
                         <tr>
                            <th>{{ Form::checkbox('selectall',"selectall","", array('id'=>'selectall')) }}</th>
                             <th>Permissions</th>
                             <th>Access</th>
                             <th>Create</th>
                             <th>Edit</th>
                             <th>View</th>
                             <th>Delete</th>
                         </tr>  
                         <tbody id="display_permissions">
                         @php $title = "";$i=0;$j=0;$check_id=0; @endphp
                         <tr>
                         @foreach($permissions as $permission)
                            
                         @php 
                            if($permission['parent_id'] == 0){
                                $permission['parent_id'] = "parent_".$permission['moduleid'];
                            }else{
                                $permission['parent_id'] = "child_".$permission['parent_id'];
                            }

                            if($check_id == 0 || $check_id == ""){
                             $check_id = $permission['header_id'];
                            }

                            if($permission['header_id'] == $check_id || $check_id == 0){

                            if($i==0){
                            @endphp
                            <td>{{ Form::checkbox($permission['parent_id'],"selectall","", array('class'=>'rowcheckall')) }}</td>
                                <td>{{ $permission['header_name'] }}</td>
                            @php } @endphp
                            <td>{{ Form::checkbox('permission[]',$permission['id'],"", array('id'=>$permission['id'],'class'=>'checkbox')) }}</td>
                            @php
                            $check_id = $permission['header_id'];
                      
                            $i++;
                            }else{
                                $check_id = "";
                              
                                 @endphp
                                </tr>
                                @php 
                               @endphp
                               <td>{{ Form::checkbox($permission['parent_id'],"selectall","", array('class'=>'rowcheckall')) }}</td>
                               <td>{{ $permission['header_name'] }}</td>
                                 <td>{{ Form::checkbox('permission[]',$permission['id'],"", array('id'=>$permission['id'],'class'=>'checkbox')) }}</td>
                                @php
                               $check_id = $permission['header_id'];
                            }

                         @endphp
                         @endforeach 
                     </tr>
                     </tbody>
                    </table>
                </div>
            </div>
            
    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
        </div>
        
    </div>
</div>
<script src="{{ url('public/js/roles') }}/role_management.js"></script>
@stop

