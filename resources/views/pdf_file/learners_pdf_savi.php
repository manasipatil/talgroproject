<html>
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>
<body>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Code</th>
				<th>Title</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($competency as $key => $value)
			<tr>
				<td>{{ $value->tcm_competency_code }}</td>
				<td>{{ $value->tcm_competency_title }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
<html>