<html>
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>
<body>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<table class="table table-bordered"  style="table-layout: fixed">
		<thead>
			<tr>
				<th>Title</th>
				<th>Permissions</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($roles as $key => $value)
			<?php 
				$value['permissions'] = str_replace(",",", ",$value['permissions']);
				$value['permissions'] = str_replace("_"," ",$value['permissions']);
                $value['permissions'] = ucwords($value['permissions']);
           	?>
			<tr style="word-wrap: break-word">
				<td>{{ $value['title'] }}</td>
				<td>{{ $value['permissions'] }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
<html>