<html>
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>
<body>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<table class="table table-bordered" style="table-layout: fixed">
		<thead>
			<tr>
				<th>Test Name</th>
				<th>Parameters</th>
				<th>Associated Courses</th>
				<th>Test Function</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody><?php
			foreach ($test as $key => $value){ 
				//code for test function
	           if(($value->tte_test_function!="") AND ($value->tte_test_function!="null")){
	                $arr_function=explode(",",$value->tte_test_function);
	                $array_function = DB::table('tbl_functions_master')
	                    ->select('tfm_function')
	                    ->whereIn('tfm_id', $arr_function)
	                    ->get();
	                $fun_name="";
	                $array_function=json_decode(json_encode($array_function),true);
	                foreach ($array_function as $fun) {
	                   $fun_name=$fun_name.','.$fun['tfm_function'];
	                }
	                $fun_name = trim($fun_name, ',');
	                $TestFunction=ucwords($fun_name);
	            }else{
	                $TestFunction="-";
	            }
	            //for date
	            if($value->tte_start_date!=""){
	            	$test_date=date(Config::get('myconstants.display_date_format'),strtotime($value->tte_start_date));
	            }else{
	            	$test_date="-";
	            }
				?>
				<tr>
					<td>{{ $value->tte_test_name }}</td>
					<td>{{ $value->tte_parameters }}</td>
					<td>{{ $value->tc_title}}</td>
					<td>{{ $TestFunction }}</td>
					<td>{{ $test_date }}</td>
				</tr>
			<?php
		}?>
		</tbody>
	</table>
</body>
<html>