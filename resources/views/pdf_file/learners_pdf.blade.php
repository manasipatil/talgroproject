<html>
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>
<body>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Emp Code</th>
				<th>Emp Name</th>
				<th>Department</th>
				<th>Manager Name</th>
				<th>Level</th>
				<th>Role</th>
				<th>Enroll Courses</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($learners as $key => $value)
			<tr>
				<td>{{ $value['employee_code'] }}</td>
				<td>{{ $value['user_firstname'] }}</td>
				<td>{{ $value['tdm_department'] }}</td>
				<td>{{ $value['user_manager'] }}</td>
				<td>{{ $value['user_level'] }}</td>
				<td>{{ $value['title'] }}</td>
				<td>{{ $value['user_firstname'] }}</td>												
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
<html>