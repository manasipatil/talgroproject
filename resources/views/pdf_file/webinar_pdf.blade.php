<html>
<style type="text/css">
   
</style>
<body>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<table class="table table-bordered" style="table-layout: fixed">
		<thead>
			<tr>
				<th>Webinar Name</th>
				<th>Course Name</th>
				<th>Development Area</th>
				<th>Learners</th>
				<th>Date</th>
				<th>Time</th>
				<th>Competency</th>
				<th>Level</th>
				<th>Instructor</th>
				<th>Platform</th>
				<th>Webinar Status</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($webinar as $key => $value){

				$course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
            from tbl_content_library tcl inner join tbl_article_course tac on tcl.tcl_id=tac.tac_article_id
            inner join tbl_courses tc on tc.tc_id=tac.tac_course_id 
            where tac_article_id=".$value['tcl_id']." group by tc_title") );
            $course=json_decode(json_encode($course),true);
            if($course){
               $CourseName=$course[0]['course_title'];
            }else{
                $CourseName="No Course Enrolled";
            }
            $status=(($value['tcl_is_active']=="1")?"Active":"InActive");

            $webinar_date=date(Config::get('myconstants.display_date_format'),strtotime($value['tcl_webinar_date']));

				?>
			<tr style="word-wrap: break-word">
				<td>{{ $value['tcl_title']}}</td>
				<td>{{ $CourseName}}</td>
				<td>{{ $value['tda_development'] }}</td>
				<td>No Learner</td>
				<td>{{ $webinar_date }}</td>
				<td>{{ $value['tcl_webinar_from_time'] }} {{ $value['tcl_webinar_to_time']}}</td>
				<td>{{ $value['tcm_competency_title'] }}</td>
				<td>{{ $value['tsl_seniority'] }}</td>
				<td>{{ $value['instructor_name'] }}</td>
				<td>No Platform</td>
				<td>{{ $status}}</td>
			</tr>
		<?php }?>
			
		</tbody>
	</table>
</body>
<html>