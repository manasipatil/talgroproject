<html>
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>
<body>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<table class="table table-bordered" style="table-layout: fixed">
		<thead>
			<tr>
				<th>Employee Code</th>
				<th>User Name</th>
				<th>Score</th>
				<th>Recommended Courses</th>
				<th>Show Learning Path</th>
				<th>Associated Learning Path</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($test as $key => $value)
			<tr>
				<td>{{ $value->employee_code }}</td>
				<td>{{ $value->user_firstname }} {{ $value->user_lastname }}</td>
				<td>{{ $value->ttlr_test_score}}</td>
				<td>{{ $value->tte_course_id }}</td>
				<td>{{ $value->employee_code }}</td>
				<td>{{ $value->employee_code }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
<html>