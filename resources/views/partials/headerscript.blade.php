<script>
$(document).ready(function(){

		$("#photo").change(function() {
	        $("#photo_form").submit();
	    });

        $.ajax({
               'type':'POST',
               'url':"{{url('/app/Profile/notification')}}",
               data: {
		        "_token": "{{ csrf_token() }}"
		        },
               success:function(data){

               	$('.notifcounttop').html(data.length);
               	$('.notifcount').html(data.length);
               	var html  = '';
               	var bold  = '';
               	var bgcolor="";
				var GivenDate = "";
				var CurrentDate = "";
				var notiftime = "";



				for(var i=0;i<data.length;i++){

					if(data[i]['is_read'] == '0'){
			            bold = "font-weight:bold";
			            bgcolor="background-color: #80808038;";
			        }else{
			        	bold = "";
			        	bgcolor="";
			        }

			        notiftime = data[i]['created_date'];

					html += '<a class="list-group-item" id="list-group-item'+data[i]['id']+'" onclick="updateReadNotif('+data[i]['id']+')" href="#" style="'+bgcolor+'">';
                    html +=    '<div class="notification">';
                    html +=      '<div class="notification-media">';
                    html +=        '<span class="icon icon-exclamation-triangle bg-warning rounded sq-40"></span>';
                    html +=      '</div>';
                    html +=     '<div class="notification-content">';
                    html +=        '<small class="notification-timestamp">'+notiftime+'</small>';
                    html +=        '<h5 class="notification-heading" id="notification-heading'+data[i]['id']+'" style="'+bold+'">'+data[i]['notification']+'</h5>';
                    html +=        '<p class="notification-text" id="notification-text'+data[i]['id']+'" style="'+bold+'">';
                    html +=          '<small class="truncate">'+data[i]['message']+'</small>';
                    html +=        '</p>';
                    html +=      '</div>';
                    html +=   ' </div>';
                    html +=  '</a>';
               }
               $('.notifdata').html(html);
           	}
        });

        $('#mark_all_read').click(function(){
        		$.ajax({
	               'type':'POST',
	               'url':"{{url('/readNotification')}}",
	               data: {
			        "_token": "{{ csrf_token() }}"
			        },
	               success:function(data){
	               		$(".list-group-item").removeAttr("style");
	               		$(".notification-heading").removeAttr("style");
	               		$(".notification-text").removeAttr("style");
	               		$(".dropdown-toggle").attr("aria-expanded","true");
	               		$("#dropid").addClass('open');
	               }
             	});
        });

        $('#country').change(function(){
        		$.ajax({
	               'type':'POST',
	               'url':"getStates",
	               data: {
			        "_token": "{{ csrf_token() }}",
			        "countryid" : $(this).val()
			        },
	               success:function(response){
	               	console.log(response);
	               	response = JSON.parse(response);
	               	var html;
	               	for(var i=0;i<response.length;i++){
	               		html += "<option value='"+response[i]['id']+"'>"+response[i]['state_name']+"</option>";
	               	}

	               	$('#stateselect').html(html);
	               }
             	});        	
        });

});

function updateReadNotif($id){
	        $.ajax({
               'type':'POST',
               'url':"{{url('/readNotification')}}",
               data: {
		        "_token": "{{ csrf_token() }}",
		        "notifid" : $id
		        },
               success:function(data){
               		    $("#list-group-item"+$id).removeAttr("style");
	               		$("#notification-heading"+$id).removeAttr("style");
	               		$("#notification-text"+$id).removeAttr("style");
	               		$(".dropdown-toggle").attr("aria-expanded","true");
	               		$("#dropid").addClass('open')
               }

             });
}
</script>