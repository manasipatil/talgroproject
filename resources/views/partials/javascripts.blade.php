<script src="{{ url('public/adminlte/js') }}/main.js"></script>
<script src="{{ url('public/adminlte/js') }}/TalGro.min.js"></script>
<script src="{{ url('public/adminlte/js') }}/dropdowntree.js"></script>
<script src="{{ url('public/adminlte/js') }}/bootstrap.js"></script> 
<script src="{{ url('public/adminlte/js') }}/moment.min.js"></script>
<script src="{{ url('public/adminlte/js') }}/fullcalendar.js"></script>
<script src="{{ url('public/adminlte/js') }}/application.min.js"></script>
<script src="{{ url('public/adminlte/js') }}/demo.min.js"></script>
<script src="{{ url('public/adminlte/js') }}/multiple-select.js"></script>
<script src="{{asset('public/js/croppie.js')}}"></script>
<link rel="stylesheet" href="{{asset('public/css/croppie.css')}}">
    <script src="{{ url('public/adminlte/plugins/jqueryvalidation/jquery.validate.js') }}"></script>
    <script src="{{url('public/adminlte/js/controller/commoncontroller.js') }}"></script> 
    <script src="{{ url('public/js') }}/common.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    
<script src="{{ url('public/adminlte/js/app.min.js') }}"></script>

<script>
    window._token = '{{ csrf_token() }}';
</script>

@include('partials.headerscript')

@yield('javascript')