@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<!-- <aside class="main-sidebar"> -->
    <!-- sidebar: style can be found in sidebar.less -->

      <div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">
              <ul class="sidenav">
                <li class="sidenav-search hidden-md hidden-lg">
                  <form class="sidenav-form" action="http://demo.madebytilde.com/">
                    <div class="form-group form-group-sm">
                      <div class="input-with-icon">
                        <input class="form-control" type="text" placeholder="Search…">
                        <span class="icon icon-search input-icon"></span>
                      </div>
                    </div>
                  </form>
                </li>
                <li class="sidenav-heading">Navigation</li>
                @php $parentid = "";$has_subnav=""; $i=0;$redirect_link = ""; @endphp
                @foreach($getHeaders as $getHeader)
                @php $has_subnav = "";
                @endphp
                @if($getHeader['parent_id'] == 0 )
                    @if($i != 0) </li> @endif
                    @php if($getHeader['redirect_link'] == '#'){ $has_subnav = "has-subnav";} @endphp
                <li class="sidenav-item {{$has_subnav}}">
                    @if($getHeader['redirect_link'] == "#")
                  <a href="#" aria-haspopup="true">
                    @else
                    @php echo '<a href="'.route($getHeader['redirect_link']).'" aria-haspopup="true">'; @endphp
                    @endif
                    <span class="sidenav-icon icon {{$getHeader['icon_class']}}"></span>
                    <span class="sidenav-label">{{$getHeader['header_name']}}</span>
                  </a>
                  @php $parentid = $getHeader['id']; $i++; @endphp
                @endif   
                @if($getHeader['parent_id'] != 0 )
                  @if($parentid == $getHeader['parent_id'])
                    <ul class="sidenav-subnav collapse">  
                    <li class="{{ $request->segment(2) == $getHeader['active_segments'] ? 'active active-sub' : '' }}">
                      @php echo '<a href="'.route($getHeader['redirect_link']).'" aria-haspopup="true">'; @endphp
                            <span class="sidenav-icon icon {{$getHeader['icon_class']}}"></span>
                            <span class="title">
                               {{$getHeader['header_name']}}
                            </span>
                        </a>
                    </li>
                    </ul>
                  @endif
                @endif             
                @endforeach
                <li class="sidenav-item">
                  <a href="#logout" onclick="$('#logout').submit();">
                    <span class="sidenav-icon icon icon-arrow-left"></span>
                    <span class="sidenav-label">@lang('global.app_logout')</span>
                  </a>
                </li>    
              </ul>
            </nav>
          </div>
        </div>
      </div>
<!-- </aside> -->
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('global.logout')</button>
{!! Form::close() !!}
