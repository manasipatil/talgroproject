<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>
    {{ trans('global.global_title') }}
</title>
<link rel="apple-touch-icon" sizes="180x180" href="{{url('public/adminlte/img/apple-touch-icon.png')}}">
<link rel="icon" type="image/png" href="{{url('public/adminlte/img/favicon-32x32.png')}}" sizes="32x32">
<link rel="icon" type="image/png" href="{{url('public/adminlte/img/favicon-16x16.png')}}" sizes="16x16">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-type" content="text/html; charset=utf-8">

<link rel="manifest" href="manifest.json">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#d9230f">
<meta name="theme-color" content="#ffffff">

<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->



<!-- <link rel="stylesheet"
      href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
	<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Old+Standard+TT:400,400i,700">

<link rel="stylesheet" href="{{ url('public/adminlte/css') }}/vendor.min.css">
<link rel="stylesheet" href="{{ url('public/adminlte/css') }}/TalGro.min.css">
<link rel="stylesheet" href="{{ url('public/adminlte/css') }}/application.min.css">
<link rel="stylesheet" href="{{ url('public/adminlte/css') }}/fullcalendar.css">
<link rel="stylesheet" href="{{ url('public/adminlte/css') }}/demo.min.css">
<link rel="stylesheet" href="{{ url('public/adminlte/css') }}/multiple-select.css">
<link rel="stylesheet" href="{{ url('public/adminlte/css') }}/nasruddin.css">
 

<!--  -->
  <link href="{{ url('public/css/common.css') }}" rel="stylesheet">
      <script src="{{ url('public/adminlte/js') }}/vendor.min.js"></script>
      <link rel="stylesheet" href="{{ url('public/adminlte/css') }}/bootstrap-tagsinput.css">
      <script type="text/javascript" src="{{ url('public/adminlte/js') }}/bootstrap-tagsinput.js"></script>
<script src="{{ url('public/adminlte/plugins/ckeditor/ckeditor.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
