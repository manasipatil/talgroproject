@if ($message = Session::get('success'))
<div class="alert alert-success alert-block notifymsg">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong id="errormsg">{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block notifymsg">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block notifymsg">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('info'))
<div class="alert alert-info alert-block notifymsg">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($errors->any())
<div class="alert alert-danger notifymsg">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	Please check the form below for errors
</div>
@endif

<div class="alert alert-success alert-block notifymsg" style="display: none;">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <span  id="errormsg"><strong></strong></span>
</div>