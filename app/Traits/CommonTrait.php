<?php
namespace App\Traits;

use DB;
use Session;
trait CommonTrait {

    public function get_role_list() {
        $role_data=\DB::table('roles')
                ->select()
                ->where('is_deleted', '=', 0)
                ->get();
        $role_data=json_decode(json_encode($role_data),true);
       return $role_data;
    }

    public function get_code_list() {
        $code_data=\DB::table('tbl_competency_master')
                ->select('tcm_id','tcm_competency_code','tcm_competency_title')
                ->where('tcm_is_deleted', '=', 0)
                ->get();
        $code_data=json_decode(json_encode($code_data),true);
       return $code_data;
    }

    public function get_designation_code_list() {
        $code_data=\DB::table('tbl_designation_master')
                ->select('tdm_id','tdm_designation_code')
                ->where('tdm_is_deleted', '=', 0)
                ->get();
        $code_data=json_decode(json_encode($code_data),true);
        return $code_data;
    }

    public function get_department_code_list() {
        $code_data=\DB::table('tbl_department_master')
                ->select('tdm_id','tdm_department_code','tdm_department')
                ->where('tdm_is_deleted', '=', 0)
                ->get();
        $code_data=json_decode(json_encode($code_data),true);

        //print_r($code_data);exit;
        return $code_data;
    }

    public function get_development_code_list(){
        $code_data=\DB::table('tbl_development_area')
                ->select('tda_id','tda_development_code','tda_development')
                ->where('tda_is_deleted', '=', 0)
                ->get();
        $code_data=json_decode(json_encode($code_data),true);
        return $code_data;
    }

    public function get_functions_code_list(){
        $code_data=\DB::table('tbl_functions_master')
                ->select('tfm_id','tfm_function_code','tfm_function')
                ->where('tfm_is_deleted', '=', 0)
                ->get();
        $code_data=json_decode(json_encode($code_data),true);
        return $code_data;
    }
    public function get_seniority_code_list(){
        $code_data=\DB::table('tbl_seniority_level')
                ->select('tsl_id','tsl_seniority_code','tsl_seniority')
                ->where('tsl_is_deleted', '=', 0)
                ->get();
        $code_data=json_decode(json_encode($code_data),true);
        return $code_data;
    }

    public function get_manager_list(){
        $manager_list = DB::table('users')
            ->select('user_firstname','user_lastname','user_middlename','id')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('role_id', '2')
            ->where('role_user.is_deleted','0')
            ->get();
        $manager_list=json_decode(json_encode($manager_list),true);
        return $manager_list;
    }
    public function get_team_list(){
        $team_list = DB::table('tbl_team')
            ->select('tt_id','tt_title')
            ->where('tt_is_deleted', '0')
            ->get();
        $team_list=json_decode(json_encode($team_list),true);
        return $team_list;
    }
    // public function get_expertise_area_list(){
    //     $expert_area_list = DB::table('tbl_expertise_area')
    //         ->select('tea_id','tea_title')
    //         ->where('tea_is_deleted', '0')
    //         ->get();
    //     $expert_area_list=json_decode(json_encode($expert_area_list),true);
    //     return $expert_area_list;
    // }

    public function get_category_code_list(){
        $category_code_list = DB::table('tbl_course_category_master')
            ->select('tccm_id','tccm_category_code')
            ->where('tccm_is_deleted', '0')
            ->get();
        $category_code_list=json_decode(json_encode($category_code_list),true);
        return $category_code_list;
    }
    public function getCountryList(){
        $countryList = DB::table('countries')->get();
        $countryList = json_decode(json_encode($countryList),true);
        return $countryList;
    }

    public function get_courses(){
        $course_list = DB::table('tbl_courses')
            ->select('tc_id','tc_title')
            ->where('tc_is_deleted', '0')
            ->get();
        $course_list=json_decode(json_encode($course_list),true);
        return $course_list;
    }

    public function get_user_type_master() {
        $user_type=\DB::table('tbl_user_type_master')
                ->select('tutm_id','tutm_user_type')
                ->where('tutm_is_deleted', '=', 0)
                ->get();
        $user_type=json_decode(json_encode($user_type),true);
        return $user_type;
    }

    public function get_instructor(){
        $instructor_list = DB::table('users')
            ->select('user_firstname','user_lastname','user_middlename','id')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('role_id', '6')
            ->where('role_user.is_deleted','0')
            ->get();
        $instructor_list=json_decode(json_encode($instructor_list),true);
        return $instructor_list;
    }
    

    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    public function download_file($file){
        if(isset($file)){
            // Get parameters
            $file = urldecode($file); // Decode URL-encoded string
            $filepath=public_path("uploads\competency_csv\\".$file);

            // Process download
            if(file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                exit;
            }
        }
    }

    public function checkIfPermissionExist($permission_name){

        if(Session::has('get_permission_list')){
            $get_permission_list = Session::get('get_permission_list');
            $get_permission_list=explode(",",$get_permission_list[0]['title']);
        }else{
            $get_permission_list = "false";
        }

        if($get_permission_list != "false"){
           $if_exists =  in_array($permission_name, $get_permission_list);
           if($if_exists){
                return true;
           }else{
                return false;
           }
        }else{
            return false;
        }
    }    
}
?>