<?php

namespace App\Http\Controllers\Admin;

use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
use File;
use PDF;

class CategoryController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $array_code_data=$this->get_category_code_list();
        return view('admin.category.index', compact('array_code_data'));
        
    }
    //listing of competency
    public function category_ajax_listing(Request $request){
        
        //$columns=array(0=>'chk_box',1=>'tccm_category',2=>'tccm_category_code');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
       // $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_tccm_category_code=$request->input('dropdown_tccm_category_code');
        $search=$request->input('searchtxt');

        
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" (LOWER(tccm_category) LIKE '".$search."%' ";
            $where .=" OR LOWER(tccm_category_code) LIKE '".$search."%' )";
        }
        if($dropdown_tccm_category_code!=""){
            $where .=" AND ";
            $where .=" tccm_id ='".$dropdown_tccm_category_code."' ";

        }

        // if($order=='chk_box' || $order=="")
        //     $order_by=" ORDER BY tccm_id DESC";
        // else
        //     $order_by="ORDER BY ". $order." ".$dir;

        $sorting_column_name=$request->input('sorting_column_name');
        if($sorting_column_name=="Category Code"){
             $order_by=" ORDER BY tccm_category_code ".$dir;
        }elseif ($sorting_column_name=="Category Title") {
             $order_by=" ORDER BY tccm_category ".$dir;
        }else{
            $order_by=" ORDER BY tccm_id DESC";
        }
        
        $total_record = DB::select( DB::raw("SELECT tccm_id,tccm_category_code,tccm_category FROM tbl_course_category_master WHERE tccm_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        

        $array_data = DB::select( DB::raw("SELECT tccm_id,tccm_category_code,tccm_category FROM tbl_course_category_master WHERE tccm_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('category_hidden_column_array')){
                $col_arr_value = $request->session()->get('category_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tccm_id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="CategoryCode"){
                            $nesteddata['CategoryCode']=ucwords($row['tccm_category_code']);   
                        }
                        if($value[0]=="CategoryTitle"){
                            $nesteddata['CategoryTitle']=ucwords($row['tccm_category']);
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tccm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void(0)" delete_id="'.$row['tccm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tccm_id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['CategoryCode']=ucwords($row['tccm_category_code']);
                    $nesteddata['CategoryTitle']=ucwords($row['tccm_category']);
                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tccm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        <a href="javascript:void()" delete_id="'.$row['tccm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    //competency common action enable,disable and delete
    public function category_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_course_category_master')
                    ->whereIn('tccm_id', $arr_value)
                    ->update(['tccm_is_active' => 0]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_course_category_master')
                    ->whereIn('tccm_id', $arr_value)
                    ->update(['tccm_is_active' => 1]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_course_category_master')
                    ->whereIn('tccm_id', $arr_value)
                    ->update(['tccm_is_deleted' => 1]);
           
        }
    }
    //save category
    function save_category_data(Request $request){
        $errors = array();
        $input = $request->all();
        if($input['tccm_category_code']==""){
            $errors['tccm_category_code'] =Config::get('messages.category.CATEGORY_CODE_ERROR');
        }
        if($input['tccm_category']==""){
            $errors['tccm_category'] = Config::get('messages.category.CATEGORY_TITLE_ERROR');
        }

        $users_code = DB::table('tbl_course_category_master')
        ->where('tccm_category_code', $input['tccm_category_code'])
        ->where('tccm_is_deleted','0')
        ->get();
        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tccm_category_code_exist'] = Config::get('messages.category.CATEGORY_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            $date=date('Y-m-d H:i:s');
            DB::table('tbl_course_category_master')->insert([
            [  
                'tccm_category_code' => $input['tccm_category_code'], 
                'tccm_category' => $input['tccm_category'],
                'tccm_created_at'=>$date,
                'tccm_updated_at'=>$date
                ]
            ]);
            echo json_encode(array('status' => 'success', 'errors' => $errors));
        }
    }
    //return data for edit category form
    public function show_category_edit_form(Request $request){
        $input=$request->all();
        $edit_id=$input['edit_id'];
        $edit_data = DB::table('tbl_course_category_master')->where('tccm_id', '=', $edit_id)->get();
        if(count($edit_data)>0){
            echo json_encode($edit_data);
        }
    }
    //update category master
    public function update_category_master(Request $request){
        $input=$request->all();
        $tccm_id=$input['tccm_id'];
        $errors=array();
        if($input['tccm_category_code']==""){
            $errors['tccm_category_code'] =Config::get('messages.category.CATEGORY_CODE_ERROR');
        }
        if($input['tccm_category']==""){
            $errors['tccm_category'] = Config::get('messages.category.CATEGORY_TITLE_ERROR');
        }
        $users_code =DB::table('tbl_course_category_master')
                ->where([
                    ['tccm_category_code', '=', $input['tccm_category_code']],
                    ['tccm_id', '!=', $tccm_id],
                    ['tccm_is_deleted', '=', 0]
                ])->get();

        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tccm_category_code_exist'] = Config::get('messages.category.CATEGORY_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            if($tccm_id>0){
                DB::table('tbl_course_category_master')
                ->where('tccm_id', $tccm_id)
                ->update(['tccm_category_code' => $input['tccm_category_code'],'tccm_category'=>$input['tccm_category']]);
               echo json_encode(array('status' => 'success','errors' => $errors));

            }
        }
    }
    //upload excel file for bulk category
    public function upload_category_csv(Request $request){

        if($request->hasFile('file')) {
            $file = $request->file('file');
            $detectedType=$file->getClientOriginalExtension();
            $allowedTypes = array('XLSX','XLX','xlsx','xlx');
            $error_msg=array();
            if(!in_array($detectedType, $allowedTypes)){
                
                $error_msg=Config::get('messages.category.CATEGORY_BULK_VALID_FORMAT');
                echo json_encode(array('status' => 'format_error', 'errors' => $error_msg));
            }else{
                $total_count_record=0;
                $count_insert_record=0;
                $count_failed_record=0;
                //get file path and read excel file
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                //convert the object data into array
                $array_data=json_decode(json_encode($data),true);
                $key_array=array();
                $key_array=$array_data[0];
                $key_value=array_keys($key_array[0]);
                
                if($key_value[0]!="category_code" || $key_value[1]!="category"){

                    echo json_encode(array('status' => 'header_format_error','errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    exit;

                }
                $date=date('Y-m-d H:i:s');
                if(!empty($array_data[0])){
                   $i=0;
                   
                   $error_row_array=array();
                    foreach ($array_data as  $value) {
                        if($i<=0){
                            foreach ($value as $value1) {

                                $code_data=\DB::table('tbl_course_category_master')
                                    ->select('tccm_id','tccm_category_code')
                                    ->where('tccm_category_code', '=', $value1['category_code'] )
                                    ->where('tccm_is_deleted','=',0)
                                    ->get();
                                $code_data=json_decode(json_encode($code_data),true);
                                if(count($code_data)==0){
                                   $insert[] = [
                                    'tccm_category_code' => $value1['category_code'],
                                    'tccm_category' => $value1['category'],
                                    'tccm_created_at'=>$date,
                                    'tccm_updated_at'=>$date
                                    ]; 
                                    $count_insert_record++;
                                }else{
                                    $count_failed_record++;
                                    $error_row_array[]=$value1['category_code'];
                                }
                                $total_count_record++;
                            }
                            $i++;
                        }
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('tbl_course_category_master')->insert($insert);
                        if ($insertData) {
                            //if duplicate record found
                            if(count($error_row_array)>0){
                               echo json_encode(
                                array('status' => 'success_n_duplicate', 'errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }else{
                                //if no duplicate record
                                 echo json_encode(array('status' => 'success_all_record', 'errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }
                             
                        }
                    }else{
                        //if all records are duplicate
                        echo json_encode(array('status' => 'all_duplicate','errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    }
                }else{
                    $error_msg=Config::get('messages.category.CATEGORY_INSERT_ERROR');
                    echo json_encode(array('status' => 'no_record_error', 'errors' => $error_msg,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                }
                
            }
        }
    }
    //delete category code
    public function delete_category_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_course_category_master')
                ->where('tccm_id', $delete_id)
                ->update(['tccm_is_deleted' => 1]);
        }
    }
    //download data into excel format
    function category_download_excel(){
         $customer_data = DB::table('tbl_course_category_master')
                        ->where('tccm_is_deleted', 0)
                        ->get()
                        ->toArray();
         $customer_array[] = array('Category Code', 'Category Title');
         foreach($customer_data as $customer){
          $customer_array[] = array(
           'Category Code'  => $customer->tccm_category_code,
           'Category Title'   => $customer->tccm_category,
          );
        }
        return Excel::create('category', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }
    //download data into pdf format
    public function category_download_PDF(){
        $category = DB::table("tbl_course_category_master")
                ->where('tccm_is_deleted', 0)
                ->get();
        view()->share('category',$category);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.category_pdf');
        return $pdf->download('categorypdfview.pdf');
    }
    //download sample format for excel
    public function download_sample_category_excel(){
        $file= public_path('uploads\test_csv\category.XLSX');
        header("Content-Description: File Transfer"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Disposition: attachment; filename='" . basename($file) . "'"); 
        readfile ($file);
        exit(); 
    }
}
