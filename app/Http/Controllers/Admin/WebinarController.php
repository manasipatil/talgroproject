<?php

namespace App\Http\Controllers\Admin;

use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
use File;
use PDF;

class WebinarController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $course_list=$this->get_courses();
        $instructor_list=$this->get_instructor();
        $competency_list=$this->get_code_list();
        $development_area_list=$this->get_development_code_list();
        $level_list=$this->get_seniority_code_list();
        return view('admin.webinar.index',compact('course_list','instructor_list','competency_list','development_area_list','level_list'));
        
    }
    //listing of competency
    public function webinar_ajax_listing(Request $request){
        
        //$columns=array(0=>'chk_box',1=>'tcl_title',2=>'tccm_category_code');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        //$order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_webinar_course=$request->input('dropdown_webinar_course');
        $dropdown_webnar_instructor=$request->input('dropdown_webnar_instructor');
        $dropdown_webinar_competency=$request->input('dropdown_webinar_competency');
        $dropdown_webinar_development_area=$request->input('dropdown_webinar_development_area');
        $dropdown_webinar_level=$request->input('dropdown_webinar_level');
        $webinar_start_date=$request->input('webinar_start_date');
        $webinar_end_date=$request->input('webinar_end_date');
        $search=$request->input('searchtxt');
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" (LOWER(tcl_title) LIKE '".$search."%' ";
            $where .=" OR LOWER(tcl_keywords) LIKE '".$search."%' )";
        }
        if($dropdown_webinar_course!=""){
            $list_course= DB::select( DB::raw("SELECT tac_article_id FROM tbl_article_course WHERE tac_course_id = '".$dropdown_webinar_course."' "));
            $list_course=json_decode(json_encode($list_course),true);
            $ids = array_column($list_course, 'tac_article_id');
            $course_area_string=implode(",",$ids);
            $where .=" AND ";
            $where .=" tcl_id IN ('".$course_area_string."') ";
        }
        if($dropdown_webnar_instructor!=""){
            $where .=" AND ";
            $where .=" tcl_webinar_instructor ='".$dropdown_webnar_instructor."' ";
        }
        if($dropdown_webinar_competency!=""){
            $where .=" AND ";
            $where .=" tcl_competency ='".$dropdown_webinar_competency."' ";
        }
        if($dropdown_webinar_development_area!=""){
            $where .=" AND ";
            $where .=" tcl_developement_area ='".$dropdown_webinar_development_area."' ";
        }
        if($dropdown_webinar_level!=""){
            $where .=" AND ";
            $where .=" tcl_level ='".$dropdown_webinar_level."' ";
        }

        if($webinar_start_date!="" && $webinar_end_date!=""){
            $where .=" AND ";
            $where .=" date(tcl_webinar_date) between '".$webinar_start_date."' AND '".$webinar_end_date."' ";
        }elseif ($webinar_start_date!="") {
            $where .=" AND ";
            $where .=" tcl_webinar_date >= '".$webinar_start_date."' ";
        }elseif ($webinar_end_date!="") {
            $where .=" AND ";
            $where .=" tcl_webinar_date = '".$webinar_end_date."' ";
        }

        // if($order=='chk_box' || $order=="")
        //     $order_by=" ORDER BY tcl_id DESC";
        // else
        //     $order_by="ORDER BY ". $order." ".$dir;

        $sorting_column_name=$request->input('sorting_column_name');
        if($sorting_column_name=="Webinar Name"){
             $order_by=" ORDER BY tcl_title ".$dir;
        }elseif ($sorting_column_name=="Development Area") {
             $order_by=" ORDER BY tda_development ".$dir;
        }elseif ($sorting_column_name=="Date") {
             $order_by=" ORDER BY tcl_webinar_date ".$dir;
        }elseif ($sorting_column_name=="Competency") {
             $order_by=" ORDER BY tcm_competency_title ".$dir;
        }elseif ($sorting_column_name=="Level") {
             $order_by=" ORDER BY tsl_seniority ".$dir;
        }elseif ($sorting_column_name=="Instructor") {
             $order_by=" ORDER BY instructor_name ".$dir;
        }elseif ($sorting_column_name=="WebinarStatus") {
             $order_by=" ORDER BY tcl_is_active ".$dir;
        }else{
            $order_by=" ORDER BY tcl_id DESC";
        }

        // echo "SELECT tcl_id,tcl_type,tcl_title,tcl_competency,tcl_developement_area,tcl_level,tcl_author,tda_development,
        //     tcm_competency_title,tsl_seniority,tcl_webinar_date,tcl_webinar_from_time,tcl_webinar_to_time,tcl_webinar_instructor,tcl_is_active,CONCAT(u.user_firstname,u.user_lastname) as instructor_name

        //  FROM tbl_content_library tcl left join tbl_development_area tda on tcl.tcl_developement_area=tda.tda_id
        //  left join tbl_competency_master tcm on tcl.tcl_competency=tcm.tcm_id
        //  LEFT JOIN tbl_seniority_level tsl on tcl.tcl_level =tsl.tsl_id
        //  LEFT JOIN users u on tcl.tcl_webinar_instructor=u.id
        //   WHERE tcl_is_deleted='0' ".$where.$order_by;exit;
        
        $total_record = DB::select( DB::raw("SELECT tcl_id,tcl_type,tcl_title,tcl_competency,tcl_developement_area,tcl_level,tcl_author,tda_development,
            tcm_competency_title,tsl_seniority,tcl_webinar_date,tcl_webinar_from_time,tcl_webinar_to_time,tcl_webinar_instructor,tcl_is_active,CONCAT(u.user_firstname,u.user_lastname) as instructor_name

         FROM tbl_content_library tcl left join tbl_development_area tda on tcl.tcl_developement_area=tda.tda_id
         left join tbl_competency_master tcm on tcl.tcl_competency=tcm.tcm_id
         LEFT JOIN tbl_seniority_level tsl on tcl.tcl_level =tsl.tsl_id
         LEFT JOIN users u on tcl.tcl_webinar_instructor=u.id
          WHERE tcl_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        

        $array_data = DB::select( DB::raw("SELECT tcl_id,tcl_type,tcl_title,tcl_competency,tcl_developement_area,tcl_level,tcl_author,tda_development,
            tcm_competency_title,tsl_seniority,tcl_webinar_date,tcl_webinar_from_time,tcl_webinar_to_time,tcl_webinar_instructor,tcl_is_active,CONCAT(u.user_firstname,u.user_lastname) as instructor_name

         FROM tbl_content_library tcl left join tbl_development_area tda on tcl.tcl_developement_area=tda.tda_id
         left join tbl_competency_master tcm on tcl.tcl_competency=tcm.tcm_id
         LEFT JOIN tbl_seniority_level tsl on tcl.tcl_level =tsl.tsl_id
         LEFT JOIN users u on tcl.tcl_webinar_instructor=u.id
          WHERE tcl_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){
            if($request->session()->has('webinar_hidden_column_array')){
                $col_arr_value = $request->session()->get('webinar_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                foreach ($array_data as $row) {
                    foreach ($col_arr_value as $key => $value) {
                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tcl_id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="WebinarName"){
                            $nesteddata['WebinarName']=ucwords($row['tcl_title']);   
                        }
                        if($value[0]=="CourseName"){

                            $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                                from tbl_content_library tcl inner join tbl_article_course tac on tcl.tcl_id=tac.tac_article_id
                                inner join tbl_courses tc on tc.tc_id=tac.tac_course_id 
                                where tac_article_id=".$row['tcl_id']." group by tc_title") );
                                $course=json_decode(json_encode($course),true);
                                if($course){
                                   $nesteddata['CourseName']=$course[0]['course_title'];
                                }else{
                                    $nesteddata['CourseName']="No Course Enrolled";
                                }
                            
                        }
                        if($value[0]=="DevelopmentArea"){
                            $nesteddata['DevelopmentArea']=$row['tda_development'];
                        }
                        if($value[0]=="Learners"){
                            $nesteddata['Learners']="No Learner";
                        }
                        if($value[0]=="Date"){
                            $nesteddata['Date']=date(Config::get('myconstants.display_date_format'),strtotime($row['tcl_webinar_date']));
                        }
                        if($value[0]=="Time"){
                            $nesteddata['Time']=$row['tcl_webinar_from_time']."-".$row['tcl_webinar_to_time'];
                        }

                        if($value[0]=="Competency"){
                            $nesteddata['Competency']=$row['tcm_competency_title'];
                        }
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="Instructor"){
                            $nesteddata['Instructor']=$row['instructor_name'];
                        }
                        if($value[0]=="Platform"){
                            $nesteddata['Platform']="No Pltfrm";
                        }
                        if($value[0]=="WebinarStatus"){
                            $nesteddata['WebinarStatus']=(($row['tcl_is_active']=="1")?"Active":"InActive");
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tcl_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void(0)" delete_id="'.$row['tcl_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tcl_id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['WebinarName']=ucwords($row['tcl_title']);
                    $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
                        from tbl_content_library tcl inner join tbl_article_course tac on tcl.tcl_id=tac.tac_article_id
                        inner join tbl_courses tc on tc.tc_id=tac.tac_course_id 
                        where tac_article_id=".$row['tcl_id']." group by tc_title") );
                        $course=json_decode(json_encode($course),true);
                        if($course){
                           $nesteddata['CourseName']=$course[0]['course_title'];
                        }else{
                            $nesteddata['CourseName']="No Course Enrolled";
                        }
                    $nesteddata['DevelopmentArea']=$row['tda_development'];
                    $nesteddata['Learners']="No Learner";
                    $nesteddata['Date']=date(Config::get('myconstants.display_date_format'),strtotime($row['tcl_webinar_date']));
                    $nesteddata['Time']=$row['tcl_webinar_from_time']."-".$row['tcl_webinar_to_time'];
                    $nesteddata['Competency']=$row['tcm_competency_title'];
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $nesteddata['Instructor']=$row['instructor_name'];
                    $nesteddata['Platform']="No Pltfrm";
                    $nesteddata['WebinarStatus']=(($row['tcl_is_active']=="1")?"Active":"InActive");

                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tcl_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        <a href="javascript:void()" delete_id="'.$row['tcl_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    //competency common action enable,disable and delete
    public function webinar_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_content_library')
                    ->whereIn('tcl_id', $arr_value)
                    ->update(['tcl_is_active' => 1]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_content_library')
                    ->whereIn('tcl_id', $arr_value)
                    ->update(['tcl_is_active' => 0]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_content_library')
                    ->whereIn('tcl_id', $arr_value)
                    ->update(['tcl_is_deleted' => 1]);
           
        }
    }
    
    
   
    
    //delete category code
    public function delete_webinar_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_content_library')
                ->where('tcl_id', $delete_id)
                ->update(['tcl_is_deleted' => 1]);
        }
    }
    //download data into excel format
    function webinar_download_excel(){
        $array_data = DB::select( DB::raw("SELECT tcl_id,tcl_type,tcl_title,tcl_competency,tcl_developement_area,tcl_level,tcl_author,tda_development,
            tcm_competency_title,tsl_seniority,tcl_webinar_date,tcl_webinar_from_time,tcl_webinar_to_time,tcl_webinar_instructor,tcl_is_active,CONCAT(u.user_firstname,u.user_lastname) as instructor_name

         FROM tbl_content_library tcl left join tbl_development_area tda on tcl.tcl_developement_area=tda.tda_id
         left join tbl_competency_master tcm on tcl.tcl_competency=tcm.tcm_id
         LEFT JOIN tbl_seniority_level tsl on tcl.tcl_level =tsl.tsl_id
         LEFT JOIN users u on tcl.tcl_webinar_instructor=u.id
          WHERE tcl_is_deleted='0'") );
        $array_data=json_decode(json_encode($array_data),true);
         $customer_array[] = array('Webinar Name', 'Course Name','Development Area','Learners','Date','Time','Competency','Level','Instructor','Platform','Webinar Status');
         foreach($array_data as $arr){
            $course = DB::select( DB::raw("SELECT array_to_string(array_agg(tc_title), ',') as course_title
            from tbl_content_library tcl inner join tbl_article_course tac on tcl.tcl_id=tac.tac_article_id
            inner join tbl_courses tc on tc.tc_id=tac.tac_course_id 
            where tac_article_id=".$arr['tcl_id']." group by tc_title") );
            $course=json_decode(json_encode($course),true);
            if($course){
               $CourseName=$course[0]['course_title'];
            }else{
                $CourseName="No Course Enrolled";
            }
          $customer_array[] = array(
           'Webinar Name'  => $arr['tcl_title'],
           'Course Name'   => $CourseName,
           'Development Area'   => $arr['tda_development'],
           'Learners'   => "No Learner",
           'Date'   => date(Config::get('myconstants.display_date_format'),strtotime($arr['tcl_webinar_date'])),
           'Time'   => $arr['tcl_webinar_from_time']."-".$arr['tcl_webinar_to_time'],
           'Competency'   => $arr['tcm_competency_title'],
           'Level'   => $arr['tsl_seniority'],
           'Instructor'   => $arr['instructor_name'],
           'Platform'   => "No Pltfrm",
           'Webinar Status'   => (($arr['tcl_is_active']=="1")?"Active":"InActive")
          );
        }
        return Excel::create('webinar', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }
    //download data into pdf format
    public function webinar_download_PDF(){
       $array_data = DB::select( DB::raw("SELECT tcl_id,tcl_type,tcl_title,tcl_competency,tcl_developement_area,tcl_level,tcl_author,tda_development,
            tcm_competency_title,tsl_seniority,tcl_webinar_date,tcl_webinar_from_time,tcl_webinar_to_time,tcl_webinar_instructor,tcl_is_active,CONCAT(u.user_firstname,u.user_lastname) as instructor_name

         FROM tbl_content_library tcl left join tbl_development_area tda on tcl.tcl_developement_area=tda.tda_id
         left join tbl_competency_master tcm on tcl.tcl_competency=tcm.tcm_id
         LEFT JOIN tbl_seniority_level tsl on tcl.tcl_level =tsl.tsl_id
         LEFT JOIN users u on tcl.tcl_webinar_instructor=u.id
          WHERE tcl_is_deleted='0'") );
        $array_data=json_decode(json_encode($array_data),true);
        view()->share('webinar',$array_data);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.webinar_pdf');
        return $pdf->download('webinarpdfview.pdf');
    }

    public function show_webinar_calender(){
        return view('admin.webinar.calender');
    }

    public function show_data_on_calender(){
        $array_data = DB::select( DB::raw("SELECT tcl_id,tcl_type,tcl_title,tcl_competency,tcl_developement_area,tcl_level,tcl_author,tcl_webinar_date,tcl_webinar_from_time,tcl_webinar_to_time,tcl_webinar_instructor,tcl_is_active,CONCAT(u.user_firstname,u.user_lastname) as instructor_name

         FROM tbl_content_library tcl 
         LEFT JOIN users u on tcl.tcl_webinar_instructor=u.id
          WHERE tcl_is_deleted='0'") );
        $array_data=json_decode(json_encode($array_data),true);
        $event_array=array();
        foreach ($array_data as $record) {
            $event_array[] = array(
                    'id' => $record['tcl_id'],
                    'title' => $record['tcl_title'],
                    'start' => $record['tcl_webinar_date'],
                    //'end'=>"2018-10-27 13:00:00"
                    
                );
            }
        echo json_encode($event_array);
    }
    
}
