<?php

namespace App\Http\Controllers\Admin;

use App\Designation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
//use Maatwebsite\Excel\Facades\Excel;
use File;
use PDF;

class DesignationController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $array_code_data=$this->get_designation_code_list();
        return view('admin.designation.index', compact('array_code_data'));
    }

    //listing of designation
    public function designation_ajax_listing(Request $request){
        // echo "<pre>";
        // print_r($request->all());
        //$columns=array(0=>'chk_box',1=>'tdm_designation_code',2=>'tdm_designation');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        //$order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_tdm_desination=$request->input('dropdown_tdm_desination');
        $search=$request->input('searchtxt');
        
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tdm_designation) LIKE '".$search."%' ";
            $where .=" OR LOWER(tdm_designation_code) LIKE '".$search."%' )";
        }
        if($dropdown_tdm_desination!=""){
            $where .=" AND ";
            $where .=" tdm_id ='".$dropdown_tdm_desination."' ";
        }
        
        $sorting_column_name=$request->input('sorting_column_name');
        if($sorting_column_name=="Designation Code"){
             $order_by=" ORDER BY tdm_designation_code ".$dir;
        }elseif ($sorting_column_name=="Designation") {
             $order_by=" ORDER BY tdm_designation ".$dir;
        }else{
            $order_by=" ORDER BY tdm_id DESC";
        }

        $total_record = DB::select( DB::raw("SELECT tdm_id,tdm_designation_code,tdm_designation FROM tbl_designation_master WHERE tdm_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        $array_data = DB::select( DB::raw("SELECT tdm_id,INITCAP(tdm_designation_code) as tdm_designation_code ,INITCAP(tdm_designation) as tdm_designation FROM tbl_designation_master WHERE tdm_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('designation_hidden_column_array')){
                $col_arr_value = $request->session()->get('designation_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tdm_id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';      
                        }
                        if($value[0]=="DesignationCode"){
                            $nesteddata['DesignationCode']=ucwords($row['tdm_designation_code']);   
                        }
                        if($value[0]=="Designation"){
                            $nesteddata['Designation']=ucwords($row['tdm_designation']);
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tdm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void(0)" delete_id="'.$row['tdm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tdm_id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                    $nesteddata['DesignationCode']=ucwords($row['tdm_designation_code']);
                    $nesteddata['Designation']=ucwords($row['tdm_designation']);
                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tdm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        <a href="javascript:void()" delete_id="'.$row['tdm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        
        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }
    //for common action enable,disable,delete
    public function designation_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_designation_master')
                    ->whereIn('tdm_id', $arr_value)
                    ->update(['tdm_is_disabled' => 0]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_designation_master')
                    ->whereIn('tdm_id', $arr_value)
                    ->update(['tdm_is_disabled' => 1]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_designation_master')
                    ->whereIn('tdm_id', $arr_value)
                    ->update(['tdm_is_deleted' => 1]);
           
        }
    }
    //save designation
    function save_designation_data(Request $request){
        $errors = array();
        $input = $request->all();
        if($input['tdm_designation_code']==""){
            $errors['tdm_designation_code'] =Config::get('messages.designation.DESIGNATION_CODE_ERROR');
        }
        if($input['tdm_designation']==""){
            $errors['tdm_designation'] = Config::get('messages.designation.DESIGNATION_TITLE_ERROR');
        }

        $users_code = DB::table('tbl_designation_master')
        ->where('tdm_designation_code', $input['tdm_designation_code'])
        ->where('tdm_is_deleted',0)
        ->get();
        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tdm_code_exist'] = Config::get('messages.designation.DESIGNATION_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            $date=date('Y-m-d H:i:s');
            DB::table('tbl_designation_master')->insert([
            [  
                'tdm_designation_code' => $input['tdm_designation_code'], 
                'tdm_designation' => $input['tdm_designation'],
                'tdm_created_at'=>$date,
                'tdm_updated_at'=>$date
                ]
            ]);
            echo json_encode(array('status' => 'success', 'errors' => $errors));
        }
    }
    //return edit designation form data
    public function show_designation_edit_form(Request $request){
        $input=$request->all();
        $edit_id=$input['edit_id'];
        $edit_data = DB::table('tbl_designation_master')->where('tdm_id', '=', $edit_id)->get();
        if(count($edit_data)>0){
            echo json_encode($edit_data);
        }
    }
    //update designation master
    public function update_designation_master(Request $request){
        $input=$request->all();
        $tdm_id=$input['tdm_id'];
        $errors=array();
        if($input['tdm_designation_code']==""){
            $errors['tdm_designation_code'] =Config::get('messages.designation.DESIGNATION_CODE_ERROR');
        }
        if($input['tdm_designation']==""){
            $errors['tdm_designation'] = Config::get('messages.designation.DESIGNATION_TITLE_ERROR');
        }
        $users_code =DB::table('tbl_designation_master')
                ->where([
                    ['tdm_designation_code', '=', $input['tdm_designation_code']],
                    ['tdm_id', '!=', $tdm_id],
                    ['tdm_is_deleted','=',0]
                ])->get();

        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tdm_code_exist'] = Config::get('messages.designation.DESIGNATION_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            if($tdm_id>0){
                DB::table('tbl_designation_master')
                ->where('tdm_id', $tdm_id)
                ->update(['tdm_designation_code' => $input['tdm_designation_code'],'tdm_designation'=>$input['tdm_designation']]);
               echo json_encode(array('status' => 'success','errors' => $errors));

            }
        }
    }
    //bulk upload designation
    public function upload_designation_csv(Request $request){
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $detectedType=$file->getClientOriginalExtension();
            $allowedTypes = array('XLSX','XLX','xlsx','xlx');
            $error_msg=array();
            if(!in_array($detectedType, $allowedTypes)){
                //echo "0#".Config::get('messages.competency.COMPETENCY_BULK_VALID_FORMAT');
                
                $error_msg=Config::get('messages.designation.DESIGNATION_BULK_VALID_FORMAT');
                echo json_encode(array('status' => 'format_error', 'errors' => $error_msg));
            }else{
                $total_count_record=0;
                $count_insert_record=0;
                $count_failed_record=0;
                //get file path and read excel file
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                $array_data=json_decode(json_encode($data),true);
                $key_array=array();
                $key_array=$array_data[0];
                $key_value=array_keys($key_array[0]);
                if($key_value[0]!="designation_code" || $key_value[1]!="designation"){

                    echo json_encode(array('status' => 'header_format_error','errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    exit;

                }
                
                $date=date('Y-m-d H:i:s');
                
                if(!empty($array_data[0])){
                   $i=0;
                   $error_row_array=array();
                    foreach ($array_data as  $value) {
                        if($i<=0){
                            foreach ($value as $value1) {

                                $code_data=\DB::table('tbl_designation_master')
                                    ->select('tdm_id','tdm_designation_code')
                                    ->where('tdm_designation_code', '=', $value1['designation_code'] )
                                    ->where('tdm_is_deleted','=',0)
                                    ->get();
                                $code_data=json_decode(json_encode($code_data),true);
                                if(count($code_data)==0){
                                   $insert[] = [
                                    'tdm_designation_code' => $value1['designation_code'],
                                    'tdm_designation' => $value1['designation'],
                                    'tdm_created_at'=>$date,
                                    'tdm_updated_at'=>$date
                                    ]; 
                                    $count_insert_record++;
                                }else{
                                    $count_failed_record++;
                                    $error_row_array[]=$value1['designation_code'];
                                }
                                $total_count_record++;
                            }
                            $i++;
                        }
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('tbl_designation_master')->insert($insert);
                        if ($insertData) {
                            //if duplicate record found
                            if(count($error_row_array)>0){
                               echo json_encode(
                                array('status' => 'success_n_duplicate', 'errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }else{
                                //if no duplicate record
                                 echo json_encode(array('status' => 'success_all_record', 'errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }
                             
                        }
                    }else{
                        //if all records are duplicate
                        echo json_encode(array('status' => 'all_duplicate','errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    }
                }else{
                    $error_msg=Config::get('messages.designation.DESIGNATION_INSERT_ERROR');
                    echo json_encode(array('status' => 'no_record_error', 'errors' => $error_msg,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                }
                
            }
        }
    }

    //when user click on delete button listing
    public function delete_designation_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_designation_master')
                ->where('tdm_id', $delete_id)
                ->update(['tdm_is_deleted' => 1]);
        }
    }
    //export data into excel format
    function designation_download_excel(){
         $customer_data = DB::table('tbl_designation_master')
                        ->where('tdm_is_deleted', 0)
                        ->get()
                        ->toArray();
         $customer_array[] = array('Designation Code', 'Designation');
         foreach($customer_data as $customer)
         {
          $customer_array[] = array(
           'Designation Code'  => $customer->tdm_designation_code,
           'Designation'   => $customer->tdm_designation,
          );
        }
        return Excel::create('designation', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }
    //export data into pdf format
    public function designation_download_PDF(){
        $designation = DB::table("tbl_designation_master")
                ->where('tdm_is_deleted', 0)
                ->get();
        view()->share('designation',$designation);


        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.designation_pdf');
       
        return $pdf->download('designationpdfview.pdf');
        //return view('competencypdfview');
    }
    //download sample csv format file
    public function download_sample_designation_excel(){
        $file= public_path('uploads\test_csv\designation.XLSX');
        header("Content-Description: File Transfer"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Disposition: attachment; filename='" . basename($file) . "'"); 
        readfile ($file);
        exit(); 
    }
}
