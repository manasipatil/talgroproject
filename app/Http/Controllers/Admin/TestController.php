<?php

namespace App\Http\Controllers\Admin;

use App\Functions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
use File;
use PDF;
use Mail;

class TestController extends Controller
{
    use CommonTrait;
    
    public function index()
    {
        $array_code_data=$this->get_functions_code_list();
        $array_courses=$this->get_courses();
        return view('admin.test.index', compact('array_code_data','array_courses'));
        
    }
    public function createTest(){
        return view('admin.test.create_test');
    }
    public function addTest(Request $request){
        $array_department=$this->get_department_code_list();
        $array_function=$this->get_functions_code_list();
        $array_level=$this->get_seniority_code_list();
        $array_expert_area=$this->get_development_code_list();
        $array_competency=$this->get_code_list();
        $array_courses=$this->get_courses();

        $total_record = DB::select( DB::raw("SELECT tfm_id,tfm_function FROM tbl_functions_master WHERE tfm_is_deleted='0' AND tfm_is_active = '1' "));
        $testFunctionList=json_decode(json_encode($total_record),true);

        $selected_function_list=array();
        $selected_radio_settings = array();
        $tte_is_internal = "";
        if(!empty($request->session()->get('test_id'))){
        $test_id = $request->session()->get('test_id');
        $selected_function_list = DB::table('tbl_test')->select('tte_test_function','tte_is_internal')->where(['tte_id'=>$test_id,'tte_is_active'=>1,'tte_is_deleted'=>0])->get();

        $selected_function_list = json_decode(json_encode($selected_function_list),true);
        $tte_is_internal = $selected_function_list[0]['tte_is_internal'];
        $selected_function_list = explode(",",$selected_function_list[0]['tte_test_function']);

        $selected_radio_settings = DB::table('tbl_test_settings')->select('tts_setting_name','tts_days_hrs_attemts')->where(['tts_test_id'=>$test_id,'tts_is_active'=>1,'tts_is_deleted'=>0])->get();

        $selected_radio_settings = json_decode(json_encode($selected_radio_settings),true);     
        }
        // return view('admin.test.addtest',['testFunctionList'=>$total_record,'selected_function_list'=>$selected_function_list,'tte_is_internal'=>$tte_is_internal,'selected_radio_settings'=>$selected_radio_settings]);

       return view('admin.test.addtest', compact('testFunctionList','array_department','array_function','array_level','array_expert_area','array_competency','array_courses','selected_function_list','tte_is_internal','selected_radio_settings'));        
    }
    
//listing of test
    public function test_ajax_listing(Request $request){
        
       // $columns=array(0=>'chk_box',1=>'tte_test_name',2=>'tte_test_function',3=>'tte_test_name',4=>'tte_test_name',5=>'tte_test_name');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
       // $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_functions_code=$request->input('dropdown_functions_code');
        $dropdown_tte_course_id=$request->input('dropdown_tte_course_id');
        $test_type=$request->input('test_type');
        $txt_parameter=$request->input('txt_parameter');
        $search=$request->input('searchtxt');

        
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tte_test_name) LIKE '".$search."%') ";
            // $where .=" OR LOWER(tte_test_function) LIKE '".$search."%' )";
        }
        if($dropdown_functions_code!=""){
            $where .=" AND ";
            $where .=" tte_test_function IN('".$dropdown_functions_code."' )";
        }

        if($dropdown_tte_course_id!=""){
            $where .=" AND ";
            $where .=" tte_course_id ='".$dropdown_tte_course_id."' ";
        }
        if($txt_parameter!=""){
            $where .=" AND ";
            $where .=" tte_parameters IN('".$txt_parameter."' )";
        }

        $sorting_column_name=$request->input('sorting_column_name');
        if($sorting_column_name=="Test Name"){
             $order_by=" ORDER BY tte_test_name ".$dir;
        }elseif ($sorting_column_name=="Date") {
             $order_by=" ORDER BY tte_start_date ".$dir;
        }else{
            $order_by=" ORDER BY tte_id DESC";
        }


        // if($order=='chk_box' || $order=="")
        //     $order_by=" ORDER BY tte_id DESC";
        // else
        //     $order_by="ORDER BY ". $order." ".$dir;
        
        $total_record = DB::select( DB::raw("SELECT tte_id,tte_test_function,tte_test_name,tte_start_date,tte_parameters,tte_course_id,tc_title FROM tbl_test tt left join tbl_courses tc on tt.tte_course_id=tc.tc_id WHERE tte_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        

        $array_data = DB::select( DB::raw("SELECT tte_id,tte_test_function,tte_test_name,tte_start_date,tte_parameters,tte_course_id,tc_title FROM tbl_test tt left join tbl_courses tc on tt.tte_course_id=tc.tc_id WHERE tte_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('test_hidden_column_array')){
                $col_arr_value = $request->session()->get('test_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tte_id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="TestName"){
                            $nesteddata['TestName']=ucwords($row['tte_test_name']);   
                        }
                        if($value[0]=="Parameters"){
                            $nesteddata['Parameters']=ucwords($row['tte_parameters']);
                        }
                        if($value[0]=="AssociatedCourses"){
                            $nesteddata['AssociatedCourses']=$row['tc_title'];
                        }
                        if($value[0]=="TestFunction"){
                            $arr_function=explode(",",$row['tte_test_function']);
                            if($row['tte_test_function']!="" AND $row['tte_test_function']!="null"){
                                $array_function = DB::table('tbl_functions_master')
                                    ->select('tfm_function')
                                    ->whereIn('tfm_id', $arr_function)
                                    ->get();
                                $fun_name="";
                                $array_function=json_decode(json_encode($array_function),true);
                                foreach ($array_function as $fun) {
                                   $fun_name=$fun_name.','.$fun['tfm_function'];
                                }
                                $fun_name = trim($fun_name, ',');
                                $nesteddata['TestFunction']=ucwords($fun_name);
                            }else{
                                $nesteddata['TestFunction']="-";
                            }
                            
                        }
                        if($value[0]=="Date"){
                            if($row['tte_start_date']!=""){
                            $nesteddata['Date']=date(Config::get('myconstants.display_date_format'),strtotime($row['tte_start_date']));
                            }else
                            $nesteddata['Date']="-";
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" onclick="editTest('.$row['tte_id'].')" edit_id="'.$row['tte_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>
                                <a class="i-size" href="javascript:void(0)" 
                               onClick="copyTest('.$row['tte_id'].')"><i class="fas fa-copy"></i></a>
                                <a href="javascript:void(0)" delete_id="'.$row['tte_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tte_id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['TestName']=ucwords($row['tte_test_name']);
                    $nesteddata['Parameters']=ucwords($row['tte_parameters']);
                    $nesteddata['AssociatedCourses']=ucwords($row['tc_title']);

                    $arr_function=explode(",",$row['tte_test_function']);
                    if($row['tte_test_function']!="" AND $row['tte_test_function']!="null"){
                        $array_function = DB::table('tbl_functions_master')
                                        ->select('tfm_function')
                                        ->whereIn('tfm_id', $arr_function)
                                        ->get();
                        $fun_name="";
                        $array_function=json_decode(json_encode($array_function),true);
                        foreach ($array_function as $fun) {
                           $fun_name=$fun_name.','.$fun['tfm_function'];
                        }
                        $fun_name = trim($fun_name, ',');
                        $nesteddata['TestFunction']=ucwords($fun_name);
                    }else{
                        $nesteddata['TestFunction']="-";
                    }

                    if($row['tte_start_date']!=""){
                        $nesteddata['Date']=date(Config::get('myconstants.display_date_format'),strtotime($row['tte_start_date']));
                    }else{
                         $nesteddata['Date']="-";
                    }
                   
                    $nesteddata['Action']='<a href="javascript:void(0)" onclick="editTest('.$row['tte_id'].')" edit_id="'.$row['tte_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>
                        <a class="i-size" href="javascript:void(0)" 
                               onClick="copyTest('.$row['tte_id'].')"><i class="fas fa-copy"></i></a>
                        <a href="javascript:void()" delete_id="'.$row['tte_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    //competency common action enable,disable and delete
    public function test_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_test')
                    ->whereIn('tte_id', $arr_value)
                    ->update(['tte_is_active' => 1]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_test')
                    ->whereIn('tte_id', $arr_value)
                    ->update(['tte_is_active' => 0]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_test')
                    ->whereIn('tte_id', $arr_value)
                    ->update(['tte_is_deleted' => 1]);
           
        }
    }
    
    
    //delete functions code
    public function delete_test_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_test')
                ->where('tte_id', $delete_id)
                ->update(['tte_is_deleted' => 1]);
        }
    }
    //download data into excel format
    public function test_download_excel(){
        $array_data = DB::select( DB::raw("SELECT tte_id,tte_test_function,tte_test_name,tte_start_date,tte_parameters,tte_course_id,tc_title FROM tbl_test tt left join tbl_courses tc on tt.tte_course_id=tc.tc_id WHERE tte_is_deleted='0'") );
        $array_data=json_decode(json_encode($array_data));
        $customer_array[] = array('Test Name', 'Parameters','Associated Courses','Test Function','Date');
         foreach($array_data as $arr){

            //code for test function
           if(($arr->tte_test_function!="") AND ($arr->tte_test_function!="null")){
                $arr_function=explode(",",$arr->tte_test_function);
                $array_function = DB::table('tbl_functions_master')
                    ->select('tfm_function')
                    ->whereIn('tfm_id', $arr_function)
                    ->get();
                $fun_name="";
                $array_function=json_decode(json_encode($array_function),true);
                foreach ($array_function as $fun) {
                   $fun_name=$fun_name.','.$fun['tfm_function'];
                }
                $fun_name = trim($fun_name, ',');
                $TestFunction=ucwords($fun_name);
            }else{
                $TestFunction="-";
            }

            if($arr->tte_start_date!="")
                $test_date=date(Config::get('myconstants.display_date_format'),strtotime($arr->tte_start_date));
            else
                $test_date="-";
            //end of code
            $customer_array[] = array(
           'Test Name'  => $arr->tte_test_name,
           'Parameters'   => $arr->tte_parameters,
           'Associated Courses'=>$arr->tc_title,
           'Test Function'=>$TestFunction,
           'Date'=>$test_date
          );
        }
        return Excel::create('Test', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               
            });
        })->download('xlsx');
    }
    //download data into pdf format
    public function test_download_PDF(){
        $test_data = DB::select( DB::raw("SELECT tte_id,tte_test_function,tte_test_name,tte_start_date,tte_parameters,tte_course_id,tc_title FROM tbl_test tt left join tbl_courses tc on tt.tte_course_id=tc.tc_id WHERE tte_is_deleted='0'") );
        $test_data=json_decode(json_encode($test_data));
        view()->share('test',$test_data);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.test_pdf');
        return $pdf->download('testpdfview.pdf');
    }

    public function test_learner_ajax_listing(Request $request){
        $edit_test_id=$request->session()->get('test_id');
        // $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');

        $dropdown_learner_department_id=$request->input('dropdown_learner_department_id');
        $dropdown_learner_function_id=$request->input('dropdown_learner_function_id');
        $dropdown_learner_level_id=$request->input('dropdown_learner_level_id');
        $dropdown_learner_status_id=$request->input('dropdown_learner_status_id');

        
        $limit=$request->input('length');
        $start=$request->input('start');
        // $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .=" OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_learner_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_learner_department_id."' ";
        }
        if($dropdown_learner_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_learner_function_id."' ";
        }
        if($dropdown_learner_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_learner_level_id."' ";
        }
        if($dropdown_learner_status_id!=""){
            $where .=" AND ";
            if($dropdown_learner_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }

        // if($order=='chk_box' || $order=="")
        //     $order_by=" ORDER BY u.id DESC";
        // else
        //     $order_by="ORDER BY ". $order." ".$dir;

        $sorting_column_name=$request->input('assign_learner_sorting_column_name');
        if($sorting_column_name=="Employee Code"){
             $order_by=" ORDER BY employee_code ".$dir;
        }elseif ($sorting_column_name=="User Name") {
             $order_by=" ORDER BY user_firstname ".$dir;
        }elseif ($sorting_column_name=="Email Id") {
             $order_by=" ORDER BY email ".$dir;
        }elseif ($sorting_column_name=="Contact Detail") {
             $order_by=" ORDER BY mobile_no ".$dir;
        }elseif ($sorting_column_name=="Department") {
             $order_by=" ORDER BY tdm_department ".$dir;
        }elseif ($sorting_column_name=="Function") {
             $order_by=" ORDER BY tfm_function ".$dir;
        }elseif ($sorting_column_name=="Level") {
             $order_by=" ORDER BY tsl_seniority ".$dir;
        }elseif ($sorting_column_name=="Manager Name") {
             $order_by=" ORDER BY manager_name ".$dir;
        }else{
            $order_by=" ORDER BY u.id DESC";
        }


        $actual_posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.id,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id

        where u.id NOT IN (SELECT ttlr_user_id 
     FROM tbl_test_learner_relation WHERE ttlr_test_id=".(int)$edit_test_id." and ttlr_is_deleted=0 ) AND u.is_deleted=0 AND role_id='15' and ru.is_deleted=0 ".$where."".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


            $posts = DB::select( DB::raw("SELECT u.user_firstname,u.user_middlename,u.id,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.id NOT IN (SELECT ttlr_user_id 
     FROM tbl_test_learner_relation WHERE ttlr_test_id=".(int)$edit_test_id." and ttlr_is_deleted=0 ) AND u.is_deleted=0 AND role_id='5' and ru.is_deleted=0 ".$where."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
            $posts=json_decode(json_encode($posts),true);
            
        $data=array();
        if($posts){

            if($request->session()->has('test_learner_hidden_column_array')){
                $col_arr_value = $request->session()->get('test_learner_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {
                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="assign_learner" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EmployeeCode"){
                            $nesteddata['EmployeeCode']=$row['employee_code'];
                        }
                        if($value[0]=="UserName"){
                            $nesteddata['UserName']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="EmailId"){
                            $nesteddata['EmailId']=$row['email'];
                        }
                        if($value[0]=="Department"){
                            $nesteddata['Department']=$row['tdm_department'];
                        }
                        if($value[0]=="ContactDetail"){
                            $nesteddata['ContactDetail']=$row['mobile_no'];
                        }
                        if($value[0]=="ManagerName"){
                            $nesteddata['ManagerName']=$row['manager_name'];
                        }
                        if($value[0]=="CourseStatus"){
                            $nesteddata['CourseStatus']="Ongoing";
                        }
                        
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="EnrollCourses"){
                            $learner = DB::select( DB::raw("SELECT count(tlcr_id) as no_of_courses from users u INNER JOIN tbl_learner_course_releation tl on u.id=tl.tlcr_learner_id where tl.tlcr_learner_id=".$row['id']." "));
                                $learner=json_decode(json_encode($learner),true);
                                $nesteddata['EnrollCourses']=$learner[0]['no_of_courses'];
                        }
                        
                        
                        
                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($posts as $row) {
                    $learner = DB::select( DB::raw("SELECT count(tlcr_id) as no_of_courses from users u INNER JOIN tbl_learner_course_releation tl on u.id=tl.tlcr_learner_id where tl.tlcr_learner_id=".$row['id']." "));
                                $learner=json_decode(json_encode($learner),true);
                                
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" tab_name="assign_learner" type="checkbox"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EmployeeCode']=$row['employee_code'];
                    $nesteddata['UserName']=$row['user_firstname']."".$row['user_lastname'];
                    $nesteddata['EmailId']=$row['email'];
                    $nesteddata['Department']=$row['tdm_department'];
                    $nesteddata['ContactDetail']=$row['mobile_no'];
                    $nesteddata['ManagerName']=$row['manager_name'];
                    $nesteddata['CourseStatus']="Ongoing";
                    $nesteddata['Level']=$row['tsl_seniority'];
                    //$nesteddata['EnrollCourses']="4";
                    $nesteddata['EnrollCourses']=$learner[0]['no_of_courses'];
                    
                    
                    
                    $data[]=$nesteddata;
                }
            }
        }

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    public function assign_learner_to_test(Request $request){
        $test_id=$request->session()->get('test_id');
        // $test_id=1;
        $arr_req=$request->all();
        $arr_value= $arr_req['chk_check_value'];
        $date=date('Y-m-d H:i:s');
        foreach ($arr_value as  $value) {
           DB::table('tbl_test_learner_relation')->insert([
            [  
                'ttlr_test_id' => $test_id,
                'ttlr_user_id' => $value,
                'ttlr_created_at'=>$date
                ]
            ]);
        }
        $last_insert_id= DB::getPdo()->lastInsertId();
    }

    public function test_invite_learner_ajax_listing(Request $request){
        $edit_test_id=$request->session()->get('test_id');
        // // $edit_test_id=1;
        // $columns=array(0=>'chk_box',1=>'employee_code',2=>'user_firstname',3=>'email','4'=>'mobile_no','5'=>'tdm_department','6'=>'tfm_function','7'=>'tsl_seniority','8'=>'manager_name');

        $dropdown_learner_department_id=$request->input('dropdown_learner_department_id');
        $dropdown_learner_function_id=$request->input('dropdown_learner_function_id');
        $dropdown_learner_level_id=$request->input('dropdown_learner_level_id');
        $dropdown_learner_status_id=$request->input('dropdown_learner_status_id');

        
        $limit=$request->input('length');
        $start=$request->input('start');
        //$order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        $where="";
        $order_by="";
        
        $search=$request->input('searchtxt');
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(u.user_firstname) LIKE '".$search."%' ";  
            $where .=" OR LOWER(u.user_middlename) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.user_lastname) LIKE '".$search."%' ";   
            $where .=" OR LOWER(u.email) LIKE '".$search."%' ";
            $where .=" OR LOWER(u.employee_code) LIKE '".$search."%' )";
        }
        if($dropdown_learner_department_id!=""){
            $where .=" AND ";
            $where .=" u.user_department ='".$dropdown_learner_department_id."' ";
        }
        if($dropdown_learner_function_id!=""){
            $where .=" AND ";
            $where .=" u.user_function ='".$dropdown_learner_function_id."' ";
        }
        if($dropdown_learner_level_id!=""){
            $where .=" AND ";
            $where .=" u.user_level ='".$dropdown_learner_level_id."' ";
        }
        if($dropdown_learner_status_id!=""){
            $where .=" AND ";
            if($dropdown_learner_status_id=="0")
                $where .=" u.is_disabled =0";
            else
                $where.="u.is_disabled=1";
        }

        // if($order=='chk_box' || $order=="")
        //     $order_by=" ORDER BY u.id DESC";
        // else
        //     $order_by="ORDER BY ". $order." ".$dir;

        $sorting_column_name=$request->input('invite_sorting_column_name');
        if($sorting_column_name=="Employee Code"){
             $order_by=" ORDER BY employee_code ".$dir;
        }elseif ($sorting_column_name=="User Name") {
             $order_by=" ORDER BY user_firstname ".$dir;
        }elseif ($sorting_column_name=="Email Id") {
             $order_by=" ORDER BY email ".$dir;
        }elseif ($sorting_column_name=="Contact Detail") {
             $order_by=" ORDER BY mobile_no ".$dir;
        }elseif ($sorting_column_name=="Department") {
             $order_by=" ORDER BY tdm_department ".$dir;
        }elseif ($sorting_column_name=="Function") {
             $order_by=" ORDER BY tfm_function ".$dir;
        }elseif ($sorting_column_name=="Level") {
             $order_by=" ORDER BY tsl_seniority ".$dir;
        }elseif ($sorting_column_name=="Manager Name") {
             $order_by=" ORDER BY manager_name ".$dir;
        }else{
            $order_by=" ORDER BY u.id DESC";
        }
    
        $actual_posts = DB::select( DB::raw("SELECT u.id,u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u 
        
        left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id

        where u.id IN (SELECT ttlr_user_id 
     FROM tbl_test_learner_relation WHERE ttlr_test_id=".(int)$edit_test_id." and ttlr_is_deleted=0 AND ttlr_has_learner_invite=0 ) AND u.is_deleted=0 AND role_id='5' and ru.is_deleted=0 ".$where."".$order_by));
            $actual_posts=json_decode(json_encode($actual_posts),true);
            $totalFiltered=count($actual_posts);


        $invite_posts = DB::select( DB::raw("SELECT u.id,u.user_firstname,u.user_middlename,u.user_lastname,u.employee_code,u.email,u.mobile_no,tsl.tsl_seniority,tfm.tfm_function,u.user_manager,tdm.tdm_department,u.id,role_id,u.user_department,u.user_level,u.user_function,u.is_disabled,CONCAT(ua.user_firstname,ua.user_lastname) as manager_name
        from users u 
       
        left join role_user ru on u.id=ru.user_id 
        left join roles r on ru.role_id=r.id 
        left join tbl_department_master tdm on u.user_department=tdm.tdm_id
        left join tbl_seniority_level tsl on u.user_level=tsl.tsl_id
        left join tbl_functions_master tfm on u.user_function=tfm.tfm_id
        left join users ua on ua.user_manager=u.id
        where u.id IN (SELECT ttlr_user_id 
     FROM tbl_test_learner_relation WHERE ttlr_test_id=".(int)$edit_test_id." and ttlr_is_deleted=0 AND ttlr_has_learner_invite=0 ) AND u.is_deleted=0 AND role_id='5' and ru.is_deleted=0 ".$where."".$order_by." LIMIT ".$limit." OFFSET ".$start."") );
            $invite_posts=json_decode(json_encode($invite_posts),true);
            
        $data=array();
        if($invite_posts){

            if($request->session()->has('test_invite_learner_hidden_column_array')){
                $col_arr_value = $request->session()->get('test_invite_learner_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                 foreach ($invite_posts as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {
                        $value=explode('-', $value);
                        
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input all_user_checkbox" tab_name="invite_learner" type="checkbox"  value="'.$row['id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="EmployeeCode"){
                            $nesteddata['EmployeeCode']=$row['employee_code'];
                        }
                        if($value[0]=="UserName"){
                            $nesteddata['UserName']=$row['user_firstname']." ".$row['user_lastname'];   
                        }
                        if($value[0]=="EmailId"){
                            $nesteddata['EmailId']=$row['email'];
                        }
                        if($value[0]=="Department"){
                            $nesteddata['Department']=$row['tdm_department'];
                        }
                        if($value[0]=="ContactDetail"){
                            $nesteddata['ContactDetail']=$row['mobile_no'];
                        }
                        if($value[0]=="ManagerName"){
                            $nesteddata['ManagerName']=$row['manager_name'];
                        }
                        if($value[0]=="CourseStatus"){
                            $nesteddata['CourseStatus']="Ongoing";
                        }
                        
                        if($value[0]=="Level"){
                            $nesteddata['Level']=$row['tsl_seniority'];
                        }
                        if($value[0]=="EnrollCourses"){
                            $learner = DB::select( DB::raw("SELECT count(tlcr_id) as no_of_courses from users u INNER JOIN tbl_learner_course_releation tl on u.id=tl.tlcr_learner_id where tl.tlcr_learner_id=".$row['id']." "));
                                $learner=json_decode(json_encode($learner),true);
                            $nesteddata['EnrollCourses']=$learner[0]['no_of_courses'];
                        }
                        
                        
                        
                    }
                    $data[]=$nesteddata;
                 }
            }else{

                foreach ($invite_posts as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input all_user_checkbox" tab_name="invite_learner" type="checkbox"  value="'.$row['id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['EmployeeCode']=$row['employee_code'];
                    $nesteddata['UserName']=$row['user_firstname']."".$row['user_lastname'];
                    $nesteddata['EmailId']=$row['email'];
                    $nesteddata['Department']=$row['tdm_department'];
                    $nesteddata['ContactDetail']=$row['mobile_no'];
                    $nesteddata['ManagerName']=$row['manager_name'];
                    $nesteddata['CourseStatus']="Ongoing";
                    $nesteddata['Level']=$row['tsl_seniority'];
                    $learner = DB::select( DB::raw("SELECT count(tlcr_id) as no_of_courses from users u INNER JOIN tbl_learner_course_releation tl on u.id=tl.tlcr_learner_id where tl.tlcr_learner_id=".$row['id']." "));
                                $learner=json_decode(json_encode($learner),true);
                    $nesteddata['EnrollCourses']=$learner[0]['no_of_courses'];
                    //$nesteddata['EnrollCourses']="4";
                    
                    
                    
                    $data[]=$nesteddata;
                }
            }
        }

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    public function invite_learner_to_test(Request $request){
        $test_id=$request->session()->get('test_id');
        $arr_req=$request->all();
        $arr_value= $arr_req['chk_check_value'];
        $date=date('Y-m-d H:i:s');
       
        //test details
        $test_details = DB::table('tbl_test')
            ->where('tte_id',$test_id)
            ->where('tte_is_deleted',0)
            ->get();
        $test_details=json_decode(json_encode($test_details),true);
        //send mail to user
        $email_template = DB::table('tbl_test_settings')
            ->where('tts_test_id',$test_id)
            ->where('tts_is_deleted',0)
            ->where('tts_setting_name','INVITE_EMAIL')
            ->get();
        $email_template=json_decode(json_encode($email_template),true);
        $email_body=$email_template[0]['tts_email_body'];
        $email_subject=$email_template[0]['tts_email_subject'];
        $data=[];
        foreach ($arr_value as  $value) {
            $users = DB::table('users')
                        ->where('id',$value)
                        ->where('is_deleted',0)
                        ->get();
            $users=json_decode(json_encode($users),true);
            $email_body= str_replace("[FIRST_NAME]",$users[0]['user_firstname'],$email_body);
            $email_body= str_replace("[MIDDLE_NAME]",$users[0]['user_middlename'],$email_body);
            $email_body= str_replace("[LAST_NAME]",$users[0]['user_lastname'],$email_body);
            $email_body= str_replace("[EMAIL]",$users[0]['email'],$email_body);
            $email_body= str_replace("[MOBILENO]",$users[0]['mobile_no'],$email_body);
            $email_body= str_replace("[TEST_NAME]",$test_details[0]['tte_test_name'],$email_body);
            $email_body= str_replace("[TESTSTARTDATE]",$test_details[0]['tte_start_date'],$email_body);
            $email_body= str_replace("[TESTENDDATE]",$test_details[0]['tte_start_date'],$email_body);
            $email_body= str_replace("[TESTTIME]",$test_details[0]['tte_test_duration'],$email_body);
            
           
           Mail::send([], $data, function ($message) use ($email_body,$email_subject,$users)
           { 
               $message->from(Config::get('myconstants.FROM_EMAIL'))->subject($email_subject);

               $message->to($users[0]['email']);
               $message->setBody($email_body,'text/html');
           });



          DB::table('tbl_test_learner_relation')
            ->where('ttlr_test_id', $test_id)
            ->where('ttlr_user_id',$value)
            ->update(['ttlr_has_learner_invite' => 1]);
        }
    }    

    //add testfunctionality starts here

    public function insertTest(Request $request){
        $test_title = $request->test_title;
        $test_function = $request->test_function;
        if($request->test_start_date != ""){
        $test_start_date = date(Config::get('myconstants.save_datetime_format'),strtotime($request->test_start_date));
        }else{
            $test_start_date = "";
        }
        if($request->test_start_date != ""){
        $test_end_date = date(Config::get('myconstants.save_datetime_format'),strtotime($request->test_end_date));
        }else{
            $test_end_date = "";
        }
        $test_duration = $request->test_duration;
        $test_description = $request->test_description;
        $tte_created_at = Config::get('myconstants.current_date'); //get current date which stored in config/myconstants.php
        $tte_updated_at = Config::get('myconstants.current_date');
        $is_internal = $request->is_internal;
        if($is_internal == "internal"){
            $is_internal = 1;
        }else{
            $is_internal = 0;
        }
        $weightage = $request->weightage;
        $parameter_list = $request->parameter_list;

        


        if(!empty($request->session()->get('test_id'))){
            $check_test_title = $this->checkIfTestNameExist($test_title,$request->session()->get('test_id'));
            if($check_test_title != "false"){
                return '{"status":"failed","type":"update","message":"'.Config::get('messages.test.TEST_NAME_EXIST').'"}';
            }
            $test_id = $request->session()->get('test_id');
            $result = DB::table('tbl_test')->where('tte_id',$test_id)->update(['tte_test_name'=>$test_title,'tte_test_function'=>$test_function,'tte_test_duration'=>$test_duration,'tte_description'=>$test_description,'tte_is_active'=>'1','tte_is_deleted'=>'0','tte_updated_at'=>$tte_updated_at,'tte_updated_by'=>auth()->user()->id,'tte_is_internal'=>$is_internal,'tte_parameters'=>$parameter_list]);
            if($test_start_date != "" || $test_end_date != ""){
                DB::table('tbl_test')->where('tte_id',$test_id)->update(['tte_start_date'=>$test_start_date,'tte_end_date'=>$test_end_date]);
            }

            $parameter_array = explode(",", $parameter_list);
            $weightage_array = explode(",", $weightage);
            $temp_array = array();
            $existing_list = DB::select(DB::raw("SELECT array_to_string(array_agg(tp_parameter),',') as tp_parameter FROM tbl_parameters WHERE tp_test_id = ".$test_id." AND tp_is_active = 1 AND tp_is_deleted = 0"));
            $existing_list = json_decode(json_encode($existing_list),true);
            if(isset($existing_list[0]['tp_parameter'])){
            $temp_array = explode(",", $existing_list[0]['tp_parameter']);
            }
            for($i=0;$i<count($parameter_array);$i++){

                if(in_array($parameter_array[$i], $temp_array)){
                    DB::table('tbl_parameters')->where(['tp_test_id'=>$test_id,'tp_parameter'=>$parameter_array[$i]])->update(['tp_weightage'=>$weightage_array[$i],'tp_is_active'=>1,'tp_is_deleted'=>0,'tp_updated_at'=>$tte_updated_at,'tp_updated_by'=>auth()->user()->id]);
                    $pos = array_search($parameter_array[$i], $temp_array);
                    unset($temp_array[$pos]);
                }else{
                    DB::table('tbl_parameters')->insert(['tp_parameter'=>$parameter_array[$i],'tp_test_id'=>$test_id,'tp_weightage'=>$weightage_array[$i],'tp_is_active'=>1,'tp_is_deleted'=>0,'tp_created_at'=>$tte_created_at,'tp_created_by'=>auth()->user()->id,'tp_updated_at'=>$tte_updated_at,'tp_updated_by'=>auth()->user()->id]);
                }

            }
            $temp_array = implode("','", $temp_array);
            $res = DB::select(DB::raw("UPDATE tbl_parameters SET tp_is_deleted = 1 WHERE tp_parameter IN ('".$temp_array."')"));
    
            if($result){

                return '{"status":"success","type":"update","message":"'.Config::get('messages.test.UPDATE_TEST_SUCCESS_MESSAGE').'"}';
            }else{

                return '{"status":"failed","type":"update","message":"'.Config::get('messages.test.UPDATE_TEST_FAILURE_MESSAGE').'"}';            
            }
        }else{
            $check_test_title = $this->checkIfTestNameExist($test_title,'');
            if($check_test_title != "false"){
                return '{"status":"failed","type":"insert","message":"'.Config::get('messages.test.TEST_NAME_EXIST').'"}';
            }            
            $result = DB::table('tbl_test')->insert(['tte_test_name'=>$test_title,'tte_test_function'=>$test_function,'tte_test_duration'=>$test_duration,'tte_description'=>$test_description,'tte_is_active'=>'1','tte_is_deleted'=>'0','tte_created_at'=>$tte_created_at,'tte_created_by'=>auth()->user()->id,'tte_updated_at'=>$tte_updated_at,'tte_updated_by'=>auth()->user()->id,'tte_is_internal'=>$is_internal,'tte_parameters'=>$parameter_list]);
            $result= DB::getPdo()->lastInsertId();
            if($test_start_date != "" || $test_end_date != ""){
                DB::table('tbl_test')->where('tte_id',$result)->update(['tte_start_date'=>$test_start_date,'tte_end_date'=>$test_end_date]);
            }
            $parameter_array = explode(",", $parameter_list);
            $weightage_array = explode(",", $weightage);

            for($i=0;$i<count($parameter_array);$i++){
                DB::table('tbl_parameters')->insert(['tp_parameter'=>$parameter_array[$i],'tp_test_id'=>$result,'tp_weightage'=>$weightage_array[$i],'tp_is_active'=>1,'tp_is_deleted'=>0,'tp_created_at'=>$tte_created_at,'tp_created_by'=>auth()->user()->id,'tp_updated_at'=>$tte_updated_at,'tp_updated_by'=>auth()->user()->id]);
            }


            if($result > 0){
                session(['test_id' => $result]);

                return '{"status":"success","type":"insert","message":"'.Config::get('messages.test.ADD_TEST_SUCCESS_MESSAGE').'"}';
            }else{

                return '{"status":"failed","type":"insert","message":"'.Config::get('messages.test.ADD_TEST_FAILURE_MESSAGE').'"}';            
            }    
        }


    }    

    public function getTestData(Request $request){
        if(!empty($request->session()->get('test_id'))){
        $test_id = $request->session()->get('test_id');
        $test_details = DB::table('tbl_test')->where(['tte_id'=>$test_id,'tte_is_active'=>1,'tte_is_deleted'=>0])->get();
        $test_details = json_encode($test_details);

        $get_values_of_test_details = json_decode($test_details,true);
        for($i=0;$i<count($get_values_of_test_details);$i++){
            if(isset($get_values_of_test_details[$i]['tte_start_date'])){
            $get_values_of_test_details[$i]['tte_start_date'] = date(Config::get('myconstants.display_calender_date_format'),strtotime($get_values_of_test_details[$i]['tte_start_date']));
            }
            if(isset($get_values_of_test_details[$i]['tte_end_date'])){
            $get_values_of_test_details[$i]['tte_end_date'] = date(Config::get('myconstants.display_calender_date_format'),strtotime($get_values_of_test_details[$i]['tte_end_date']));
            }
        }
        $test_details = json_encode($get_values_of_test_details);

        $get_parameter_list = DB::table('tbl_parameters')->select('tp_parameter','tp_weightage')->where(['tp_test_id'=>$test_id,'tp_is_active'=>1,'tp_is_deleted'=>0])->get();
        $get_parameter_list = json_encode($get_parameter_list);
            return '{"test_details":'.$test_details.',"get_parameter_list":'.$get_parameter_list.'}';
        }else{
            return "false";
        }        
    }

    public function saveTestSettings(Request $request){
        //function to insert or update test settings
        if(!empty($request->session()->get('test_id'))){
            $test_id = $request->session()->get('test_id');
            $tts_created_at = Config::get('myconstants.current_date'); //get current date which stored in config/myconstants.php
            $tts_updated_at = Config::get('myconstants.current_date');
            $setting_data =  $request->data;
            $setting_data = json_decode($setting_data,true);
            for($i=0;$i<count($setting_data);$i++){
                $get_count = DB::table('tbl_test_settings')->where(['tts_test_id'=>$test_id,'tts_setting_name'=>$setting_data[$i]['setting_name'],'tts_is_deleted'=>0,'tts_is_active'=>1])->get()->count();
                if($get_count == 0){
                    //insert
                     DB::table('tbl_test_settings')->insert(['tts_test_id'=>$test_id,'tts_setting_name'=>$setting_data[$i]['setting_name'],'tts_parameter'=>$setting_data[$i]['parameter'],'tts_days_hrs_attemts'=>$setting_data[$i]['days_hr'],'tts_setting_type'=>$setting_data[$i]['setting_type'],'tts_setting_type'=>$setting_data[$i]['setting_type'],'tts_email_subject'=>$setting_data[$i]['email_subject'],'tts_is_deleted'=>0,'tts_is_active'=>1,'tts_created_at'=>$tts_created_at,'tts_created_by'=>auth()->user()->id,'tts_updated_at'=>$tts_updated_at,'tts_updated_by'=>auth()->user()->id]);
                }else{
                    // update
                    DB::table('tbl_test_settings')->where(['tts_test_id'=>$test_id,'tts_setting_name'=>$setting_data[$i]['setting_name'],'tts_is_deleted'=>0,'tts_is_active'=>1])->update(['tts_setting_name'=>$setting_data[$i]['setting_name'],'tts_parameter'=>$setting_data[$i]['parameter'],'tts_days_hrs_attemts'=>$setting_data[$i]['days_hr'],'tts_setting_type'=>$setting_data[$i]['setting_type'],'tts_setting_type'=>$setting_data[$i]['setting_type'],'tts_email_subject'=>$setting_data[$i]['email_subject'],'tts_created_at'=>$tts_created_at,'tts_created_by'=>auth()->user()->id,'tts_updated_at'=>$tts_updated_at,'tts_updated_by'=>auth()->user()->id]);
                }
            }
            return "true";
        }else{
            return false;
        }
    }

    public function saveTestEmailContent(Request $request){
        //function to insert or update email body into test settings
        if(!empty($request->session()->get('test_id'))){
            $test_id = $request->session()->get('test_id');
            $tts_created_at = Config::get('myconstants.current_date'); //get current date which stored in config/myconstants.php
            $tts_updated_at = Config::get('myconstants.current_date');
            $email_body = htmlspecialchars_decode($request->data);
            $get_count = DB::table('tbl_test_settings')->where(['tts_test_id'=>$test_id,'tts_setting_name'=>$request->setting_name,'tts_is_deleted'=>0,'tts_is_active'=>1])->get()->count();
            if($get_count == 0){
                DB::table('tbl_test_settings')->insert(['tts_email_subject'=>$request->email_subject,'tts_email_body'=>$email_body,'tts_setting_name'=>$request->setting_name,'tts_test_id'=>$test_id,'tts_is_deleted'=>0,'tts_is_active'=>1,'tts_created_at'=>$tts_created_at,'tts_created_by'=>auth()->user()->id,'tts_updated_at'=>$tts_updated_at,'tts_updated_by'=>auth()->user()->id]);
            }else{
                DB::table('tbl_test_settings')->where(['tts_setting_name'=>$request->setting_name,'tts_test_id'=>$test_id,'tts_is_deleted'=>0,'tts_is_active'=>1])->update(['tts_email_subject'=>$request->email_subject,'tts_email_body'=>$email_body]);
                return "true";
            }
        }       
    }

    public function getTestSettingsDetails(Request $request){
        //get all details for test settings tab
        if(!empty($request->session()->get('test_id'))){
            $test_id = $request->session()->get('test_id');

            $setting_details = DB::table('tbl_test_settings')->where(['tts_test_id'=>$test_id,'tts_is_deleted'=>0,'tts_is_active'=>1])->get();
            $setting_details = json_encode($setting_details);


            return $setting_details;
        }else{
            return "false";
        }          
    }


    public function sendSampleEmail(Request $request){
            $preview_subject = $request->preview_subject;
            $preview_body = $request->preview_body;
            $preview_footer = $request->preview_footer;
            $sample_email_id = $request->sample_email_id;
            $data=[];

            $template = $preview_body .'<br>'.$preview_footer;
           
           Mail::send([], $data, function ($message) use ($template,$preview_subject,$sample_email_id)
           { 
               $message->from('manasi.w3i@gmail.com')->subject($preview_subject);

               $message->to($sample_email_id);
               $message->setBody($template,'text/html');

           });
           return "true";
    }

    //test result tab code starts here

    //listing of test result
    public function test_result_ajax_listing(Request $request){
        $columns=array(0=>'employee_code',1=>'user_firstname',2=>'ttlr_test_score',3=>'tte_course_id',4=>'LearningPath',5=>'AssocLearningPath',6=>'Results');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');


        
        $where="";
        $order_by="";

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY tte_id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

        $test_id = 0;
        if($request->session()->has('test_id')){
            $test_id = $request->session()->get('test_id');
        }
        
        $total_record = DB::select( DB::raw("SELECT tt.tte_id,u.employee_code,u.user_firstname,u.user_lastname,ttlr.ttlr_test_score,tt.tte_course_id FROM tbl_test tt inner join tbl_test_learner_relation ttlr on tt.tte_id=ttlr.ttlr_test_id 
            inner join users u on ttlr.ttlr_user_id = u.id
            WHERE ttlr.ttlr_is_deleted='0' AND tt.tte_is_deleted = 0 AND tt.tte_id = ".$test_id." ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        

        $array_data = DB::select( DB::raw("SELECT tt.tte_id,u.employee_code,u.user_firstname,u.user_lastname,ttlr.ttlr_test_score,tt.tte_course_id FROM tbl_test tt inner join tbl_test_learner_relation ttlr on tt.tte_id=ttlr.ttlr_test_id 
            inner join users u on ttlr.ttlr_user_id = u.id
            WHERE ttlr.ttlr_is_deleted='0' AND tt.tte_is_deleted = 0 AND tt.tte_id = ".$test_id." ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('test_result_hidden_column_array')){
                $col_arr_value = $request->session()->get('test_result_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tte_id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }        
                        if($value[0]=="EmpCode"){
                            $nesteddata['EmpCode']=$row['employee_code'];   
                        }
                        if($value[0]=="UserName"){
                            $nesteddata['UserName']=ucwords($row['user_firstname'])." ".ucwords($row['user_lastname']);
                        }
                        if($value[0]=="Score"){
                            $nesteddata['Score']=$row['ttlr_test_score'] ."%";
                        }
                        if($value[0]=="RecommendedCourses"){
                            $nesteddata['RecommendedCourses']=$row['tte_course_id'];
                        }
                        if($value[0]=="LearningPath"){
                            $nesteddata['LearningPath']=ucwords($row['user_firstname']);
                        }
                        if($value[0]=="AssocLearningPath"){
                            $nesteddata['AssocLearningPath']=ucwords($row['user_firstname']);
                        }
                        if($value[0]=="Results"){
                            $nesteddata['Results']='<a href="#"><i class="icon icon-file"></i> Show Result</a>';
                        }
                        
                        // if($value[0]=="Action"){
                        //     $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tte_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        //         <a href="javascript:void(0)" delete_id="'.$row['tte_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        // }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tte_id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>';
                    $nesteddata['EmpCode']=$row['employee_code'];
                    $nesteddata['UserName']=ucwords($row['user_firstname'])." ".ucwords($row['user_lastname']);
                    $nesteddata['Score']=$row['ttlr_test_score'] ."%";
                    $nesteddata['RecommendedCourses']=$row['tte_course_id'];
                    $nesteddata['LearningPath']=ucwords($row['user_firstname']);
                    $nesteddata['AssocLearningPath']=ucwords($row['user_firstname']);
                    $nesteddata['Results']='<a href="#"><i class="icon icon-file"></i> Show Result</a>';
                    // $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tte_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                    //     <a href="javascript:void()" delete_id="'.$row['tte_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                    //     ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }

    //competency common action enable,disable and delete
    public function test_result_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_test_learner_relation')
                    ->whereIn('ttlr_id', $arr_value)
                    ->update(['ttlr_is_active' => 1]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_test_learner_relation')
                    ->whereIn('ttlr_id', $arr_value)
                    ->update(['ttlr_is_active' => 0]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_test_learner_relation')
                    ->whereIn('ttlr_id', $arr_value)
                    ->update(['ttlr_is_deleted' => 1]);
           
        }
    }
    
    //download data into excel format
    function test_result_download_excel(){
        $array_data = DB::select( DB::raw("SELECT tt.tte_id,u.employee_code,u.user_firstname,u.user_lastname,ttlr.ttlr_test_score,tt.tte_course_id FROM tbl_test tt inner join tbl_test_learner_relation ttlr on tt.tte_id=ttlr.ttlr_test_id 
            inner join users u on ttlr.ttlr_user_id = u.id
            WHERE ttlr.ttlr_is_deleted='0' ") );
        $array_data=json_decode(json_encode($array_data));
        $customer_array[] = array('Employee Code', 'User Name','Score','Recommended Courses','Show Learning Path','Associated Learning Path');
         foreach($array_data as $arr){
          $customer_array[] = array(
           'Employee Code'  => $arr->employee_code,
           'User Name'   => $arr->user_firstname." ".$arr->user_lastname,
           'Score'=>$arr->ttlr_test_score,
           'Recommended Courses'=>$arr->tte_course_id,
           'Show Learning Path'=>$arr->employee_code,
           'Associated Learning Path'=>$arr->employee_code
          );
        }
        return Excel::create('TestResult', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               
            });
        })->download('xlsx');
    }
    //download data into pdf format
    public function test_result_download_PDF(){
        $test_data = DB::select( DB::raw("SELECT tt.tte_id,u.employee_code,u.user_firstname,u.user_lastname,ttlr.ttlr_test_score,tt.tte_course_id FROM tbl_test tt inner join tbl_test_learner_relation ttlr on tt.tte_id=ttlr.ttlr_test_id 
            inner join users u on ttlr.ttlr_user_id = u.id
            WHERE ttlr.ttlr_is_deleted='0' ") );
        $test_data=json_decode(json_encode($test_data));
        view()->share('test',$test_data);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.test_result_pdf');
        return $pdf->download('testresultpdfview.pdf');
    }
    //test result tab code ends here
    function checkIfTestNameExist($test_name,$test_id){
        $check_test_name = DB::table('tbl_test')->where(['tte_is_active'=>1,'tte_is_deleted'=>0,'tte_test_name'=>$test_name])
                                                ->when($test_id, function ($query, $test_id) {
                                                    return $query->where('tte_id','<>', $test_id);
                                                })
                                                ->get();
        $check_test_name = json_decode(json_encode($check_test_name),true);

        if(!empty($check_test_name)){
            return $check_test_name;
        }else{
            return "false";
        }
    } 

    function copyTestFunction(Request $request){
        $old_test_id = $request->data;
        $get_data_to_copy = DB::table('tbl_test')->where(['tte_is_active'=>1,'tte_is_deleted'=>0,'tte_id'=>$old_test_id])->get();
        $get_data_to_copy = json_decode(json_encode($get_data_to_copy),true);
        $test_name = $get_data_to_copy[0]['tte_test_name'];
        $new_test_name = $this->checkCopyTestNameExist($test_name);
        // $get_data_to_copy = $this->checkIfTestNameExist($test_name,'');
        // $old_test_id = $get_data_to_copy[0]['tte_id'];
        DB::select(DB::raw("INSERT INTO tbl_test (tte_test_function, tte_test_duration,tte_description,tte_is_active,tte_is_deleted,tte_is_internal,tte_start_date,tte_end_date,tte_parameters) SELECT tte_test_function, tte_test_duration,tte_description, tte_is_active,tte_is_deleted,tte_is_internal,tte_start_date,tte_end_date,tte_parameters FROM tbl_test WHERE tte_id = '".$old_test_id."'"));
        $new_test_id= DB::getPdo()->lastInsertId();
        session(['test_id' => $new_test_id]);

        DB::table('tbl_test')->where(['tte_id'=>$new_test_id])->update(['tte_test_name'=>$new_test_name]);

        DB::select(DB::raw("INSERT INTO tbl_parameters (tp_test_id,tp_parameter,tp_weightage,tp_is_active,tp_is_deleted) 
                            SELECT ".$new_test_id.",tp_parameter, tp_weightage, tp_is_active,tp_is_deleted
                            FROM tbl_parameters WHERE tp_test_id =".$old_test_id.""));

        // return DB::table('tbl_parameters')->where(['tp_test_id'=>$old_test_id])->update(['tp_test_id'=>$new_test_id]);

        DB::select(DB::raw("INSERT INTO tbl_test_settings (tts_test_id,tts_setting_name,tts_setting_details,tts_parameter,tts_days_hrs_attemts,tts_setting_type,tts_email_subject,tts_email_body,tts_created_at,tts_created_by,tts_updated_at,tts_updated_by,tts_is_deleted,tts_is_active) SELECT ".$new_test_id.",tts_setting_name,tts_setting_details, tts_parameter,tts_days_hrs_attemts,tts_setting_type,tts_email_subject,tts_email_body,tts_created_at,tts_created_by,tts_updated_at,tts_updated_by,tts_is_deleted,tts_is_active FROM tbl_test_settings WHERE tts_test_id =".$old_test_id.""));

        DB::select(DB::raw("INSERT INTO tbl_test_learner_relation (ttlr_test_id,ttlr_user_id,ttlr_is_deleted,ttlr_created_at,ttlr_updated_at,ttlr_has_learner_invite,ttlr_test_score,ttlr_is_active) SELECT ".$new_test_id.",ttlr_user_id,ttlr_is_deleted, ttlr_created_at,ttlr_updated_at,ttlr_has_learner_invite,ttlr_test_score,ttlr_is_active FROM tbl_test_learner_relation WHERE ttlr_test_id =".$old_test_id.""));        

        // DB::table('tbl_test_settings')->where(['tts_test_id'=>$old_test_id])->update(['tts_test_id'=>$new_test_id]);
        //SELECT * FROM public.tbl_test_learner_relation

        return "true";
    }

    function checkCopyTestNameExist($test_name){
        $test_name = $test_name;
        $check_name_exists = $this->checkIfTestNameExist($test_name,'');
        if($check_name_exists != "false"){
            $test_name = "copy_".$test_name;
            $final_result = $this->checkCopyTestNameExist($test_name);
          
        }else{
            $final_result =  $test_name;
        }   
        return $final_result;     
    } 

    function clearTestSession(Request $request){
        if($request->session()->has('test_id')){
            $request->session()->forget('test_id');
        }
        return "true";
    }


    function editTestFunction(Request $request){
        session(['test_id' => $request->data]);
        return "true";        
    }
}