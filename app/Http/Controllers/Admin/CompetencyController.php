<?php

namespace App\Http\Controllers\Admin;

use App\Competency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
use File;
use PDF;

class CompetencyController extends Controller
{
    use CommonTrait;
    public function index()
    {
        if (! Gate::allows('competency_access')) {
            return abort(401);
        }
        $array_code_data=$this->get_code_list();
        return view('admin.competency.index', compact('array_code_data'));
        
    }
    //listing of competency
    public function competency_ajax_listing(Request $request){
        // echo "<pre>";
        // print_r($request->all());exit;
       // $columns=array(0=>'chk_box',1=>'tcm_competency_title',2=>'tcm_competency_code');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        //$order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_competency_code=$request->input('dropdown_competency_code');
        $search=$request->input('searchtxt');

        $sorting_column_name=$request->input('sorting_column_name');

        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" (LOWER(tcm_competency_title) LIKE '".$search."%' ";
            $where .=" OR LOWER(tcm_competency_code) LIKE '".$search."%' )";
        }
        if($dropdown_competency_code!=""){
            $where .=" AND ";
            $where .=" tcm_id ='".$dropdown_competency_code."' ";

        }
        if($sorting_column_name=="Competency Code"){
             $order_by=" ORDER BY tcm_competency_code ".$dir;
        }elseif ($sorting_column_name=="Competency Title") {
             $order_by=" ORDER BY tcm_competency_title ".$dir;
        }else{
            $order_by=" ORDER BY tcm_id DESC";
        }
        
        $total_record = DB::select( DB::raw("SELECT tcm_id,tcm_competency_code,tcm_competency_title FROM tbl_competency_master WHERE tcm_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        

        $array_data = DB::select( DB::raw("SELECT tcm_id,INITCAP(tcm_competency_code) as tcm_competency_code,INITCAP(tcm_competency_title) as tcm_competency_title  FROM tbl_competency_master WHERE tcm_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('competency_hidden_column_array')){
                $col_arr_value = $request->session()->get('competency_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            /*$nesteddata['CheckAll']="<input type='checkbox' class='myCheckbox'
                             value='".$row['tcm_id']."'>"; */
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tcm_id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';   
                        }
                        if($value[0]=="CompetencyCode"){
                            $nesteddata['CompetencyCode']=ucwords($row['tcm_competency_code']);   
                        }
                        if($value[0]=="CompetencyTitle"){
                            $nesteddata['CompetencyTitle']=ucwords($row['tcm_competency_title']);
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tcm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void(0)" delete_id="'.$row['tcm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                           <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tcm_id'].'">
                           <span class="custom-control-indicator"></span>
                           </label>';
                    $nesteddata['CompetencyCode']=ucwords($row['tcm_competency_code']);
                    $nesteddata['CompetencyTitle']=ucwords($row['tcm_competency_title']);
                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tcm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        <a href="javascript:void()" delete_id="'.$row['tcm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data

        );
        echo json_encode($json_data);
    }

    //competency common action enable,disable and delete
    public function competency_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_competency_master')
                    ->whereIn('tcm_id', $arr_value)
                    ->update(['tcm_is_disabled' => 0]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_competency_master')
                    ->whereIn('tcm_id', $arr_value)
                    ->update(['tcm_is_disabled' => 1]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_competency_master')
                    ->whereIn('tcm_id', $arr_value)
                    ->update(['tcm_is_deleted' => 1]);
           
        }
    }
    //save competency
    function save_competency_data(Request $request){
        $errors = array();
        $input = $request->all();
        if($input['tcm_competency_code']==""){
            $errors['tcm_competency_code'] =Config::get('messages.competency.COMPETENCY_CODE_ERROR');
        }
        if($input['tcm_competency_title']==""){
            $errors['tcm_competency_title'] = Config::get('messages.competency.COMPETENCY_TITLE_ERROR');
        }

        $users_code = DB::table('tbl_competency_master')
        ->where('tcm_competency_code', $input['tcm_competency_code'])
        ->where('tcm_is_deleted','0')
        ->get();

       
        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tcm_code_exist'] = Config::get('messages.competency.COMPETENCY_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            $date=date('Y-m-d H:i:s');
            DB::table('tbl_competency_master')->insert([
            [  
                'tcm_competency_code' => $input['tcm_competency_code'], 
                'tcm_competency_title' => $input['tcm_competency_title'],
                'tcm_created_at'=>$date,
                'tcm_updated_at'=>$date
                ]
            ]);
            echo json_encode(array('status' => 'success', 'errors' => $errors));
        }
    }
    //return data for edit competency form
    public function show_competency_edit_form(Request $request){
        $input=$request->all();
        $edit_id=$input['edit_id'];
        $edit_data = DB::table('tbl_competency_master')->where('tcm_id', '=', $edit_id)->get();
        if(count($edit_data)>0){
            echo json_encode($edit_data);
        }
    }
    //update compatency master
    public function update_competency_master(Request $request){
        $input=$request->all();
        $tcm_id=$input['tcm_id'];
        $errors=array();
        if($input['tcm_competency_code']==""){
            $errors['tcm_competency_code'] =Config::get('messages.competency.COMPETENCY_CODE_ERROR');
        }
        if($input['tcm_competency_title']==""){
            $errors['tcm_competency_title'] = Config::get('messages.competency.COMPETENCY_TITLE_ERROR');
        }
        $users_code =DB::table('tbl_competency_master')
                ->where([
                    ['tcm_competency_code', '=', $input['tcm_competency_code']],
                    ['tcm_id', '!=', $tcm_id],
                    ['tcm_is_deleted','=',0]
                ])->get();

        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tcm_code_exist'] = Config::get('messages.competency.COMPETENCY_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            if($tcm_id>0){
                DB::table('tbl_competency_master')
                ->where('tcm_id', $tcm_id)
                ->update(['tcm_competency_code' => $input['tcm_competency_code'],'tcm_competency_title'=>$input['tcm_competency_title']]);
               echo json_encode(array('status' => 'success','errors' => $errors));

            }
        }
    }
    //upload excel file for bulk competency
    public function upload_competency_csv(Request $request){

        if($request->hasFile('file')) {
            $file = $request->file('file');
            $detectedType=$file->getClientOriginalExtension();
            $allowedTypes = array('XLSX','XLX','xlsx','xlx');
            $error_msg=array();
            if(!in_array($detectedType, $allowedTypes)){
                //echo "0#".Config::get('messages.competency.COMPETENCY_BULK_VALID_FORMAT');
                
                $error_msg=Config::get('messages.competency.COMPETENCY_BULK_VALID_FORMAT');
                echo json_encode(array('status' => 'format_error', 'errors' => $error_msg));
            }else{
                $total_count_record=0;
                $count_insert_record=0;
                $count_failed_record=0;
                //get file path and read excel file
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                // echo "<pre>";
                // print_r($data);
                //convert the object data into array
                $array_data=json_decode(json_encode($data),true);
                $key_array=array();
                $key_array=$array_data[0];
                $key_value=array_keys($key_array[0]);
                if($key_value[0]!="competency_code" || $key_value[1]!="competency_title"){

                    echo json_encode(array('status' => 'header_format_error','errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    exit;

                }
                $date=date('Y-m-d H:i:s');
                if(!empty($array_data[0])){
                   $i=0;
                   
                   $error_row_array=array();
                    foreach ($array_data as  $value) {
                        if($i<=0){
                            foreach ($value as $value1) {

                                $code_data=\DB::table('tbl_competency_master')
                                    ->select('tcm_id','tcm_competency_code')
                                    ->where('tcm_competency_code', '=', $value1['competency_code'] )
                                    ->where('tcm_is_deleted','=',0)
                                    ->get();
                                $code_data=json_decode(json_encode($code_data),true);
                                if(count($code_data)==0){
                                   $insert[] = [
                                    'tcm_competency_code' => $value1['competency_code'],
                                    'tcm_competency_title' => $value1['competency_title'],
                                    'tcm_created_at'=>$date,
                                    'tcm_updated_at'=>$date
                                    ]; 
                                    $count_insert_record++;
                                }else{
                                    $count_failed_record++;
                                    $error_row_array[]=$value1['competency_code'];
                                }
                                $total_count_record++;
                            }
                            $i++;
                        }
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('tbl_competency_master')->insert($insert);
                        if ($insertData) {
                            //if duplicate record found
                            if(count($error_row_array)>0){
                               echo json_encode(
                                array('status' => 'success_n_duplicate', 'errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }else{
                                //if no duplicate record
                                 echo json_encode(array('status' => 'success_all_record', 'errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }
                             
                        }
                    }else{
                        //if all records are duplicate
                        echo json_encode(array('status' => 'all_duplicate','errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    }
                }else{
                    $error_msg=Config::get('messages.competency.COMPETENCY_INSERT_ERROR');
                    echo json_encode(array('status' => 'no_record_error', 'errors' => $error_msg,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                }
                
            }
        }
    }
    //delete competency code
    public function delete_competency_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_competency_master')
                ->where('tcm_id', $delete_id)
                ->update(['tcm_is_deleted' => 1]);
        }
    }
    //download data into excel format
    function competency_download_excel(){
         $customer_data = DB::table('tbl_competency_master')
                        ->where('tcm_is_deleted', 0)
                        ->get()
                        ->toArray();
         $customer_array[] = array('Competency Code', 'Competency Title');
         foreach($customer_data as $customer){
          $customer_array[] = array(
           'Competency Code'  => $customer->tcm_competency_code,
           'Competency Title'   => $customer->tcm_competency_title,
          );
        }
        return Excel::create('competency', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }
    //download data into pdf format
    public function competency_download_PDF(){
        $competency = DB::table("tbl_competency_master")
                ->where('tcm_is_deleted', 0)
                ->get();
        view()->share('competency',$competency);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.competency_pdf');
        return $pdf->download('competencypdfview.pdf');
    }
    //download sample format for excel
    public function download_sample_competency_excel(){
        $file= public_path('uploads\test_csv\competency.XLSX');
        header("Content-Description: File Transfer"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Disposition: attachment; filename='" . basename($file) . "'"); 
        readfile ($file);
        exit(); 
    }
}
