<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
use File;
use PDF;

class DepartmentController extends Controller
{
    use CommonTrait;
    public function index()
    {
        if (! Gate::allows('department_access')) {
            return abort(401);
        }
        $array_code_data=$this->get_department_code_list();
        return view('admin.department.index', compact('array_code_data'));
        
    }
    //listing of department
    public function department_ajax_listing(Request $request){
        $columns=array(0=>'chk_box',1=>'tdm_department',2=>'tdm_department_code');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
       // $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');

        //search filter
        $dropdown_department_code=$request->input('dropdown_department_code');
        $search=$request->input('searchtxt');
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tdm_department) LIKE '".$search."%' ";
            $where .=" OR LOWER(tdm_department_code) LIKE '".$search."%' )";
        }
        if($dropdown_department_code!=""){
            $where .=" AND ";
            $where .=" tdm_id ='".$dropdown_department_code."' ";
        }
        $sorting_column_name=$request->input('sorting_column_name');
        if($sorting_column_name=="Department Code"){
             $order_by=" ORDER BY tdm_department_code ".$dir;
        }elseif ($sorting_column_name=="Department") {
             $order_by=" ORDER BY tdm_department ".$dir;
        }else{
            $order_by=" ORDER BY tdm_id DESC";
        }

        $total_record = DB::select( DB::raw("SELECT tdm_id,tdm_department_code,tdm_department FROM tbl_department_master WHERE tdm_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        $array_data = DB::select( DB::raw("SELECT tdm_id,INITCAP(tdm_department_code) as tdm_department_code,INITCAP(tdm_department) as tdm_department FROM tbl_department_master WHERE tdm_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('department_hidden_column_array')){
                $col_arr_value = $request->session()->get('department_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);
                foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            //$nesteddata['CheckAll']="<input type='checkbox' class='myCheckbox' value='".$row['tdm_id']."'>"; 
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                                   <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tdm_id'].'">
                                   <span class="custom-control-indicator"></span>
                                   </label>';     
                        }
                        if($value[0]=="DepartmentCode"){
                            $nesteddata['DepartmentCode']=ucwords($row['tdm_department_code']);   
                        }
                        if($value[0]=="Department"){
                            $nesteddata['Department']=ucwords($row['tdm_department']);
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tdm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void(0)" delete_id="'.$row['tdm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                   // $nesteddata['CheckAll']="<input type='checkbox' class='myCheckbox' value='".$row['tdm_id']."'>";
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tdm_id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>';   
                    $nesteddata['DepartmentCode']=ucwords($row['tdm_department_code']);
                    $nesteddata['Department']=ucwords($row['tdm_department']);
                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tdm_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        <a href="javascript:void()" delete_id="'.$row['tdm_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }
    //for common action enable,disable and delete
    public function department_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_department_master')
                    ->whereIn('tdm_id', $arr_value)
                    ->update(['tdm_is_disabled' => 0]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_department_master')
                    ->whereIn('tdm_id', $arr_value)
                    ->update(['tdm_is_disabled' => 1]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_department_master')
                    ->whereIn('tdm_id', $arr_value)
                    ->update(['tdm_is_deleted' => 1]);
           
        }
    }
    //save department data
    function save_department_data(Request $request){
        $errors = array();
        $input = $request->all();
        if($input['tdm_department_code']==""){
            $errors['tdm_department_code'] =Config::get('messages.department.DEPARTMENT_CODE_ERROR');
        }
        if($input['tdm_department']==""){
            $errors['tdm_department'] = Config::get('messages.department.DEPARTMENT_TITLE_ERROR');
        }

        $users_code = DB::table('tbl_department_master')
        ->where('tdm_department_code', $input['tdm_department_code'])
        ->where('tdm_is_deleted',0)
        ->get();
        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tdm_code_exist'] = Config::get('messages.department.DEPARTMENT_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            $date=date('Y-m-d H:i:s');
            DB::table('tbl_department_master')->insert([
            [  
                'tdm_department_code' => $input['tdm_department_code'], 
                'tdm_department' => $input['tdm_department'],
                'tdm_created_at'=>$date,
                'tdm_updated_at'=>$date
                ]
            ]);
            echo json_encode(array('status' => 'success', 'errors' => $errors));
        }
    }
    //show edit department form
    public function show_department_edit_form(Request $request){
        $input=$request->all();
        $edit_id=$input['edit_id'];
        $edit_data = DB::table('tbl_department_master')->where('tdm_id', '=', $edit_id)->get();
        if(count($edit_data)>0){
            echo json_encode($edit_data);
        }
    }
    //update department master
    public function update_department_master(Request $request){
        $input=$request->all();
        $tdm_id=$input['tdm_id'];
        $errors=array();
        if($input['tdm_department_code']==""){
            $errors['tdm_department_code'] =Config::get('messages.department.DEPARTMENT_CODE_ERROR');
        }
        if($input['tdm_department']==""){
            $errors['tdm_department'] = Config::get('messages.department.DEPARTMENT_TITLE_ERROR');
        }
        $department_code =DB::table('tbl_department_master')
                ->where([
                    ['tdm_department_code', '=', $input['tdm_department_code']],
                    ['tdm_id', '!=', $tdm_id],
                    ['tdm_is_deleted','=',0]
                ])->get();
           // exit;

        $department_code=json_decode(json_encode($department_code),true);

        if(count($department_code)>0){
            $errors['tdm_code_exist'] = Config::get('messages.department.DEPARTMENT_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            if($tdm_id>0){
                DB::table('tbl_department_master')
                ->where('tdm_id', $tdm_id)
                ->update(['tdm_department_code' => $input['tdm_department_code'],'tdm_department'=>$input['tdm_department']]);
               echo json_encode(array('status' => 'success','errors' => $errors));

            }
        }
    }
    //bulu upload department
    public function upload_department_csv(Request $request){
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $detectedType=$file->getClientOriginalExtension();
            $allowedTypes = array('XLSX','XLX','xlsx','xlx');
            $error_msg=array();
            if(!in_array($detectedType, $allowedTypes)){
                $error_msg=Config::get('messages.department.DEPARTMENT_BULK_VALID_FORMAT');
                echo json_encode(array('status' => 'format_error', 'errors' => $error_msg));
            }else{
                $total_count_record=0;
                $count_insert_record=0;
                $count_failed_record=0;
                //get file path and read excel file
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                //convert the object data into array
                $array_data=json_decode(json_encode($data),true);
                $key_array=array();
                $key_array=$array_data[0];
                $key_value=array_keys($key_array[0]);
                if($key_value[0]!="department_code" || $key_value[1]!="department"){

                    echo json_encode(array('status' => 'header_format_error','errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    exit;

                }
                $date=date('Y-m-d H:i:s');
                if(!empty($array_data[0])){
                   $i=0;
                   
                   $error_row_array=array();
                    foreach ($array_data as  $value) {
                        if($i<=0){
                            foreach ($value as $value1) {

                                $code_data=\DB::table('tbl_department_master')
                                    ->select('tdm_id','tdm_department_code')
                                    ->where('tdm_department_code', '=', $value1['department_code'] )
                                    ->where('tdm_is_deleted','=',0)
                                    ->get();
                                $code_data=json_decode(json_encode($code_data),true);
                                if(count($code_data)==0){
                                   $insert[] = [
                                    'tdm_department_code' => $value1['department_code'],
                                    'tdm_department' => $value1['department'],
                                    'tdm_created_at'=>$date,
                                    'tdm_updated_at'=>$date
                                    ]; 
                                    $count_insert_record++;
                                }else{
                                    $count_failed_record++;
                                    $error_row_array[]=$value1['department_code'];
                                }
                                $total_count_record++;
                            }
                            $i++;
                        }
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('tbl_department_master')->insert($insert);
                        if ($insertData) {
                            //if duplicate record found
                            if(count($error_row_array)>0){
                               echo json_encode(
                                array('status' => 'success_n_duplicate', 'errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }else{
                                //if no duplicate record
                                 echo json_encode(array('status' => 'success_all_record', 'errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }
                             
                        }
                    }else{
                        //if all records are duplicate
                        echo json_encode(array('status' => 'all_duplicate','errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    }
                }else{
                    $error_msg=Config::get('messages.department.DEPARTMENT_INSERT_ERROR');
                    echo json_encode(array('status' => 'no_record_error', 'errors' => $error_msg,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                }
                
            }
        }
    }
    //delete department
    public function delete_department_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_department_master')
                ->where('tdm_id', $delete_id)
                ->update(['tdm_is_deleted' => 1]);
        }
    }
    //export data into excel format
    function department_download_excel(){
         $customer_data = DB::table('tbl_department_master')
                        ->where('tdm_is_deleted', 0)
                        ->get()
                        ->toArray();
         $customer_array[] = array('Department Code', 'Department');
         foreach($customer_data as $customer){
          $customer_array[] = array(
           'Department Code'  => $customer->tdm_department_code,
           'Department'   => $customer->tdm_department,
          );
        }
        return Excel::create('department', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array){
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }
    //export data into pdf format
    public function department_download_PDF(){
        $department = DB::table("tbl_department_master")
                ->where('tdm_is_deleted', 0)
                ->get();
        view()->share('department',$department);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.department_pdf');
       
        return $pdf->download('departmentpdfview.pdf');
    }
    //download sample format of department
    public function download_sample_department_excel(){
        $file= public_path('uploads\test_csv\department.XLSX');
        header("Content-Description: File Transfer"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Disposition: attachment; filename='" . basename($file) . "'"); 
        readfile ($file);
        exit(); 
    }
}
