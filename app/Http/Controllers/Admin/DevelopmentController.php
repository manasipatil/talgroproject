<?php

namespace App\Http\Controllers\Admin;

use App\Development;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DB;
use App\Traits\CommonTrait;
use Response;
use Config;
use Session;
use Excel;
//use Maatwebsite\Excel\Facades\Excel;
use File;
use PDF;

class DevelopmentController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $array_competency_code_data=$this->get_code_list();
        $array_code_data=$this->get_development_code_list();
        //return view('admin.development.index', compact('array_code_data'));
        return View('admin.development.index')->with(['array_code_data'=>$array_code_data,'array_competency_code_data'=>$array_competency_code_data]);
    }

    //listing of development
    public function development_ajax_listing(Request $request){
        // echo "<pre>";
        // print_r($request->all());
        //$columns=array(0=>'chk_box',1=>'tda_development_code',2=>'tda_development');
        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
       // $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        $dropdown_tda_development=$request->input('dropdown_tda_development');
        $search=$request->input('searchtxt');
        $where="";
        $order_by="";
        if(!empty($search)){
            $where .=" AND ";
            $where .=" ( LOWER(tda_development) LIKE '".$search."%' ";
            $where .=" OR LOWER(tda_development_code) LIKE '".$search."%' )";
        }
        if($dropdown_tda_development!=""){
            $where .=" AND ";
            $where .=" tda_id ='".$dropdown_tda_development."' ";
        }
        // if($order=='chk_box' || $order=="")
        //     $order_by=" ORDER BY tda_id DESC";
        // else
        //     $order_by="ORDER BY ". $order." ".$dir;

        $sorting_column_name=$request->input('sorting_column_name');
        if($sorting_column_name=="Development Code"){
             $order_by=" ORDER BY tda_development_code ".$dir;
        }elseif ($sorting_column_name=="Development") {
             $order_by=" ORDER BY tda_development ".$dir;
        }else{
            $order_by=" ORDER BY tda_id DESC";
        }
        
        $total_record = DB::select( DB::raw("SELECT tda_id,tda_development_code,tda_development FROM tbl_development_area WHERE tda_is_deleted='0' ".$where.$order_by));
        $total_record=json_decode(json_encode($total_record),true);
        $totalFiltered=count($total_record);

        $array_data = DB::select( DB::raw("SELECT tda_id,INITCAP(tda_development_code) as tda_development_code ,INITCAP(tda_development) as tda_development FROM tbl_development_area WHERE tda_is_deleted='0' ".$where.$order_by." LIMIT ".$limit." OFFSET ".$start."") );
        $array_data=json_decode(json_encode($array_data),true);
        
        $data=array();
        if($array_data){

            if($request->session()->has('development_hidden_column_array')){
                $col_arr_value = $request->session()->get('development_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                foreach ($array_data as $row) {
                    
                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);

                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tda_id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="DevelopmentCode"){
                            $nesteddata['DevelopmentCode']=ucwords($row['tda_development_code']);   
                        }
                        if($value[0]=="Development"){
                            $nesteddata['Development']=ucwords($row['tda_development']);
                        }
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tda_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                                <a href="javascript:void(0)" delete_id="'.$row['tda_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>';
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['tda_id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['DevelopmentCode']=ucwords($row['tda_development_code']);
                    $nesteddata['Development']=ucwords($row['tda_development']);
                    $nesteddata['Action']='<a href="javascript:void(0)" edit_id="'.$row['tda_id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>

                        <a href="javascript:void()" delete_id="'.$row['tda_id'].'" class="i-size submit_form"><i class="icon icon-trash"></i></a>
                        ';
                    $data[]=$nesteddata;
                }
            }
        }

        
        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }
    //for common action enable,disable,delete
    public function development_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
       
        if($action=="enable"){
            $users = DB::table('tbl_development_area')
                    ->whereIn('tda_id', $arr_value)
                    ->update(['tda_is_disabled' => 0]);
            
        }elseif ($action=="disable") {
            $users = DB::table('tbl_development_area')
                    ->whereIn('tda_id', $arr_value)
                    ->update(['tda_is_disabled' => 1]);
        }
        elseif ($action=="delete") {
            $users = DB::table('tbl_development_area')
                    ->whereIn('tda_id', $arr_value)
                    ->update(['tda_is_deleted' => 1]);
           
        }
    }
    //save development
    function save_development_data(Request $request){
        $errors = array();
        $input = $request->all();
        if($input['tda_development_code']==""){
            $errors['tda_development_code'] =Config::get('messages.development.DEVELOPMENT_CODE_ERROR');
        }
        if($input['tda_development']==""){
            $errors['tda_development'] = Config::get('messages.development.DEVELOPMENT_TITLE_ERROR');
        }
        if($input['dropdown_tda_competency_id']==""){
            $errors['dropdown_tda_competency_id'] = Config::get('messages.development.DEVELOPMENT_COMPETENCY_ERROR');
        }

        $users_code = DB::table('tbl_development_area')
        ->where('tda_development_code', $input['tda_development_code'])
        ->where('tda_is_deleted', 0)
        ->get();
        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tda_code_exist'] = Config::get('messages.development.DEVELOPMENT_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            $date=date('Y-m-d H:i:s');
            DB::table('tbl_development_area')->insert([
            [  
                'tda_development_code' => $input['tda_development_code'], 
                'tda_development' => $input['tda_development'],
                'tda_competency_id' => $input['dropdown_tda_competency_id'],
                'tda_created_at'=>$date,
                'tda_updated_at'=>$date
                ]
            ]);
            echo json_encode(array('status' => 'success', 'errors' => $errors));
        }
    }
    //return edit development form data
    public function show_development_edit_form(Request $request){
        $input=$request->all();
        $edit_id=$input['edit_id'];
        $edit_data = DB::table('tbl_development_area')->where('tda_id', '=', $edit_id)->get();
        if(count($edit_data)>0){
            echo json_encode($edit_data);
        }
    }
    //update development master
    public function update_development_master(Request $request){
        $input=$request->all();
        $tda_id=$input['tda_id'];
        $errors=array();
        if($input['tda_development_code']==""){
            $errors['tda_development_code'] =Config::get('messages.development.DEVELOPMENT_CODE_ERROR');
        }
        if($input['tda_development']==""){
            $errors['tda_development'] = Config::get('messages.development.DEVELOPMENT_TITLE_ERROR');
        }
        if($input['dropdown_tda_competency_id']==""){
            $errors['dropdown_tda_competency_id'] = Config::get('messages.development.DEVELOPMENT_COMPETENCY_ERROR');
        }
        //check code exist
        $users_code =DB::table('tbl_development_area')
                ->where([
                    ['tda_development_code', '=', $input['tda_development_code']],
                    ['tda_id', '!=', $tda_id],
                    ['tda_is_deleted', '=', 0]
                ])->get();

        $users_code=json_decode(json_encode($users_code),true);
        if(count($users_code)>0){
            $errors['tda_code_exist'] = Config::get('messages.development.DEVELOPMENT_CODE_EXIST');
        }

        if(count($errors) > 0){
            echo json_encode(array('status' => 'error', 'errors' => $errors));
        }else{
            if($tda_id>0){
                DB::table('tbl_development_area')
                ->where('tda_id', $tda_id)
                ->update(['tda_development_code' => $input['tda_development_code'],'tda_development'=>$input['tda_development'],'tda_competency_id'=> $input['dropdown_tda_competency_id']]);
               echo json_encode(array('status' => 'success','errors' => $errors));

            }
        }
    }
    //bulk upload development
    public function upload_development_csv(Request $request){
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $detectedType=$file->getClientOriginalExtension();
            $allowedTypes = array('XLSX','XLX','xlsx','xlx');
            $error_msg=array();
            if(!in_array($detectedType, $allowedTypes)){
                //echo "0#".Config::get('messages.competency.COMPETENCY_BULK_VALID_FORMAT');
                
                $error_msg=Config::get('messages.development.DEVELOPMENT_BULK_VALID_FORMAT');
                echo json_encode(array('status' => 'format_error', 'errors' => $error_msg));
            }else{
                $total_count_record=0;
                $count_insert_record=0;
                $count_failed_record=0;
                //get file path and read excel file
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                $array_data=json_decode(json_encode($data),true);
                $key_array=array();
                $key_array=$array_data[0];
                //for getting key of array
                $key_value=array_keys($key_array[0]);
                if($key_value[0]!="development_code" || $key_value[1]!="development" || $key_value[2]!="competency_title"){

                    echo json_encode(array('status' => 'header_format_error','errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    exit;

                }
                
                $date=date('Y-m-d H:i:s');
                
                if(!empty($array_data[0])){
                   $i=0;
                   $error_row_array=array();
                    foreach ($array_data as  $value) {
                        if($i<=0){
                            foreach ($value as $value1) {
                                //check development code exist
                                $code_data=\DB::table('tbl_development_area')
                                    ->select('tda_id','tda_development_code')
                                    ->where('tda_development_code', '=', $value1['development_code'] )
                                    ->where('tda_is_deleted','=',0)
                                    ->get();
                                $code_data=json_decode(json_encode($code_data),true);
                                if(count($code_data)==0){
                                    //check competency Code exist
                                    $chk_competency_code=\DB::table('tbl_competency_master')
                                    ->select('tcm_id','tcm_competency_code')
                                    ->where('tcm_competency_title', 'ilike', $value1['competency_title'] )
                                    ->where('tcm_is_deleted','=',0)
                                    ->get();
                                
                                $chk_competency_code=json_decode(json_encode($chk_competency_code),true);
                                if(count($chk_competency_code)>0){
                                    $competency_id=$chk_competency_code[0]['tcm_id'];
                                }
                                else{
                                    $date=date('Y-m-d H:i:s');
                                    $com_code = preg_replace("/[^a-zA-Z]+/", "", $value1['competency_title']);
                                    $dd=DB::table('tbl_competency_master')->insert(
                                        [
                                        'tcm_competency_title' => $value1['competency_title'], 
                                        'tcm_competency_code'=>$com_code,
                                        'tcm_created_at' => $date,
                                        'tcm_updated_at'=>$date
                                        ]
                                    );
                                   $competency_id= DB::getPdo()->lastInsertId();
                                }

                                   $insert[] = [
                                    'tda_development_code' => $value1['development_code'],
                                    'tda_development' => $value1['development'],
                                    'tda_competency_id' => $competency_id,
                                    'tda_created_at'=>$date,
                                    'tda_updated_at'=>$date
                                    ]; 
                                    $count_insert_record++;
                                }else{
                                    $count_failed_record++;
                                    $error_row_array[]=$value1['development_code'];
                                }
                                $total_count_record++;
                            }
                            $i++;
                        }
                    }
                    if(!empty($insert)){
                        $insertData = DB::table('tbl_development_area')->insert($insert);
                        if ($insertData) {
                            //if duplicate record found
                            if(count($error_row_array)>0){
                               echo json_encode(
                                array('status' => 'success_n_duplicate', 'errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }else{
                                //if no duplicate record
                                 echo json_encode(array('status' => 'success_all_record', 'errors' => '','total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                            }
                             
                        }
                    }else{
                        //if all records are duplicate
                        echo json_encode(array('status' => 'all_duplicate','errors' => $error_row_array,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                    }
                }else{
                    $error_msg=Config::get('messages.development.DEVELOPMENT_INSERT_ERROR');
                    echo json_encode(array('status' => 'no_record_error', 'errors' => $error_msg,'total_count_record'=>$total_count_record,'failed_record'=>$count_failed_record));
                }
                
            }
        }
    }

    //when user click on delete button listing
    public function delete_development_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('tbl_development_area')
                ->where('tda_id', $delete_id)
                ->update(['tda_is_deleted' => 1]);
        }
    }
    //export data into excel format
    function development_download_excel(){
         $customer_data = DB::table('tbl_development_area')
                        ->where('tda_is_deleted', 0)
                        ->get()
                        ->toArray();
         $customer_array[] = array('Development Code', 'Development');
         foreach($customer_data as $customer)
         {
          $customer_array[] = array(
           'Development Code'  => $customer->tda_development_code,
           'Development'   => $customer->tda_development,
          );
        }
        return Excel::create('development', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }
    //export data into pdf format
    public function development_download_PDF(){
        $development = DB::table("tbl_development_area")
                ->where('tda_is_deleted', 0)
                ->get();
        view()->share('development',$development);


        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.development_pdf');
       
        return $pdf->download('developmentpdfview.pdf');
    }
    //download sample csv format file
    public function download_sample_development_excel(){
        $file= public_path('uploads\test_csv\development.XLSX');
        header("Content-Description: File Transfer"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Disposition: attachment; filename='" . basename($file) . "'"); 
        readfile ($file);
        exit(); 
    }
}
