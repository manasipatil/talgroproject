<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRolesRequest;
use App\Http\Requests\Admin\UpdateRolesRequest;
use App\Traits\CommonTrait;
use DB;
use Config;
use Excel;
use PDF;
use Session;

class RolesController extends Controller
{
     use CommonTrait;
    /**
     * Display a listing of Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('role_access')) {
            return abort(401);
        }


                $roles = Role::all();

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('role_create')) {
            return abort(401);
        }
        $permissions = DB::table('permissions')->join('module_headers','permissions.header_id','=','module_headers.id')
                                               ->select(array('module_headers.parent_id','permissions.id','permissions.title','permissions.header_id','module_headers.id as moduleid','module_headers.header_name'))
                                               ->where('module_headers.is_active','=','1')
                                               ->orderBy('permissions.header_id')
                                               ->orderBy('permissions.id')
                                               ->get();
        $permissions = json_decode(json_encode($permissions),true);
        // $permissions = \App\Permission::get()->pluck('title', 'id');

        $user_type = DB::table('tbl_user_type_master')->where(['tutm_is_active'=>1,'tutm_is_deleted'=>0])->get();
        $user_type = json_decode(json_encode($user_type),true);

        return view('admin.roles.create', compact('permissions','user_type'));
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param  \App\Http\Requests\StoreRolesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRolesRequest $request)
    {
        if (! Gate::allows('role_create')) {
            return abort(401);
        }
        // dd($request->input('permistionid'));
        // foreach($request->input('permistionid') as $id){
        // echo "<br>$id was checked! ";
        // }
        // $role = Role::create($request->all());
        // $role->permission()->sync(array_filter((array)$request->input('permission')));
        $role = Role::create($request->all());
        $role_id = $role->id;

        $permistionidlist = $request->input('permission');
        foreach($permistionidlist as $permissionid){
            $array[] = array('permission_id'=>$permissionid,'role_id'=>$role_id);
        }
        DB::table('permission_role')->insert($array);

        return redirect()->route('admin.roles.index');
    }


    /**
     * Show the form for editing Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('role_edit')) {
            return abort(401);
        }


        $user_type_selected = DB::table('roles')->where(['is_deleted'=>0,'id'=>$id])->get();
        $user_type_selected = json_decode(json_encode($user_type_selected),true);  
        $user_type_id =   $user_type_selected[0]['user_type'];      
        // $permissions = \App\Permission::get()->pluck('title', 'id');
        $permissions = DB::table('permissions')->join('module_headers','permissions.header_id','=','module_headers.id')
                                               ->select(array('module_headers.parent_id','permissions.id','permissions.title','permissions.header_id','module_headers.id as moduleid','module_headers.header_name'))
                                               ->where(['module_headers.is_active'=>'1','module_headers.user_type'=>$user_type_id])
                                               ->orderBy('permissions.header_id')
                                               ->orderBy('permissions.id')
                                               ->get();
        $permissions = json_decode(json_encode($permissions),true);

        $role = Role::findOrFail($id);

        $user_type = DB::table('tbl_user_type_master')->where(['tutm_is_active'=>1,'tutm_is_deleted'=>0])->get();
        $user_type = json_decode(json_encode($user_type),true);    
  

        return view('admin.roles.edit', compact('role', 'permissions','user_type','user_type_selected'));
    }

    /**
     * Update Role in storage.
     *
     * @param  \App\Http\Requests\UpdateRolesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRolesRequest $request, $id)
    {
        if (! Gate::allows('role_edit')) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->update($request->all());
        $role->permission()->sync(array_filter((array)$request->input('permission')));



        return redirect()->route('admin.roles.index');
    }


    /**
     * Display Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('role_view')) {
            return abort(401);
        }
        $permissions = \App\Permission::get()->pluck('title', 'id');$users = \App\User::whereHas('role',
                    function ($query) use ($id) {
                        $query->where('id', $id);
                    })->get();

        $role = Role::findOrFail($id);

        $user_type = DB::table('tbl_user_type_master')->join('roles','tbl_user_type_master.tutm_id','=','roles.user_type')
                                                      ->select(array('roles.id as id','roles.title as title','tbl_user_type_master.tutm_user_type as user_type'))
                                                      ->where(['roles.is_active'=>1,'roles.id'=>$id])
                                                      ->get();
        $user_type = json_decode(json_encode($user_type),true);        

        return view('admin.roles.show', compact('role', 'users','user_type'));
    }


    /**
     * Remove Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('role_delete')) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('admin.roles.index');
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('role_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Role::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    public function getSelectedPermissions(Request $request){
        $permissions = DB::table('permission_role')->select('permission_id')->where('role_id','=',$request->roleid)->get();
        $permissions = json_encode($permissions); 
        return $permissions;    
    }

    public function role_ajax_listing(Request $request){
        
        if (!$this->checkIfPermissionExist('role_edit') && !$this->checkIfPermissionExist('role_view') && !$this->checkIfPermissionExist('role_delete')) {
            $columns=array(0=>'chk_box',1=>'title',2=>'permissions');
        }else{
            $columns=array(0=>'chk_box',1=>'title',2=>'permissions',3=>'action');
        }

        //for sort order and for limit
        $limit=$request->input('length');
        $start=$request->input('start');
        $order=$columns[$request->input('order.0.column')];
        $dir=$request->input('order.0.dir');
        //search filter
        // $dropdown_name=$request->input('dropdown_name');
        // $search=$request->input('Keyword_filter');
        // $no_of_learners = $request->input('no_of_learners');
        // $team_filter_courses_list = $request->input('team_filter_courses_list');
        // $team_filter_learining_path_list = $request->input('team_filter_learining_path_list');
        // $tabname = $request->input('tabname');

        
         

        if($order=='chk_box' || $order=="")
            $order_by=" ORDER BY id DESC";
        else
            $order_by="ORDER BY ". $order." ".$dir;

            $total_record = DB::select( DB::raw("SELECT id,title, (SELECT array_to_string(array_agg(permissions.title),',') as learning_path FROM permissions INNER JOIN permission_role ON permissions.id = permission_role.permission_id WHERE permission_role.role_id = roles.id GROUP BY permission_role.role_id) as permissions FROM roles WHERE roles.is_deleted = 0".$order_by));    

            $total_record=json_decode(json_encode($total_record),true);
            $totalFiltered=count($total_record);
            

            $array_data = DB::select( DB::raw("SELECT id,title, (SELECT array_to_string(array_agg(permissions.title),',') as learning_path FROM permissions INNER JOIN permission_role ON permissions.id = permission_role.permission_id WHERE permission_role.role_id = roles.id GROUP BY permission_role.role_id) as permissions FROM roles WHERE roles.is_deleted = 0 ".$order_by." LIMIT ".$limit." OFFSET ".$start."") );        
            //.$where.$order_by." LIMIT ".$limit." OFFSET ".$start.""
            $array_data=json_decode(json_encode($array_data),true);
        

        
        $data=array();
        if($array_data){

            if($request->session()->has('role_listing_hidden_column_array')){
                $col_arr_value = $request->session()->get('role_listing_hidden_column_array');
                $col_arr_value=explode(",",$col_arr_value);

                // print_r($col_arr_value);
                // exit;
                
                 foreach ($array_data as $row) {

                    $permissions = $row['permissions'];
                    $permissions = explode(",", $permissions);

                    foreach ($col_arr_value as $key => $value) {

                        $value=explode('-', $value);
                        if($value[0]=="CheckAll"){
                            $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                               <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['id'].'">
                               <span class="custom-control-indicator"></span>
                               </label>';   
                        }
                        if($value[0]=="Title"){
                            $nesteddata['Title']=ucwords($row['title']);   
                        }
                        if($value[0]=="Permissions"){
                            $data_content = "<ul>";
                                    foreach ($permissions as $keyp => $valuep) {
                                        $valuep = str_replace("_"," ",$valuep);
                                        $valuep = ucwords($valuep);
                                        $data_content .= '<li>'.$valuep.'</li>';
                                    }
                                    $data_content .= "</ul>";
                                        $row['permissions'] = str_replace(",",", ",$row['permissions']);
                                        $row['permissions'] = str_replace("_"," ",$row['permissions']);
                                        $row['permissions'] = ucwords($row['permissions']);
                                $nesteddata['Permissions'] ='<a href="#" title="Permissions" data-trigger="focus" data-toggle="popover" data-html="true" data-content="'.$data_content.'">'.substr($row['permissions'], 0, 30).'</a>';
                        }                                         
                        
                        if($value[0]=="Action"){
                            $nesteddata['Action'] = "";
                            if ($this->checkIfPermissionExist('role_edit')) {
    
                            $nesteddata['Action'].='<a href="'.route('admin.roles.edit',$row['id']).'" edit_id="'.$row['id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>';
                            
                            }else if(!Session::has('get_permission_list')){
                            $nesteddata['Action'].='<a href="'.route('admin.roles.edit',$row['id']).'" edit_id="'.$row['id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>';
                            }

                            if ($this->checkIfPermissionExist('role_view')) {
                            $nesteddata['Action'] .= '<a href="'.route('admin.roles.show',$row['id']).'" view_id="'.$row['id'].'" class="i-size view_button"><i class="icon icon-eye"></i></a>';
                            }else if(!Session::has('get_permission_list')){
                            $nesteddata['Action'] .= '<a href="'.route('admin.roles.show',$row['id']).'" view_id="'.$row['id'].'" class="i-size view_button"><i class="icon icon-eye"></i></a>';
                            }

                            if ($this->checkIfPermissionExist('role_delete')) {
                            $nesteddata['Action'] .= '<a href="javascript:void(0)" delete_id="'.$row['id'].'" class="i-size delete_role"><i class="icon icon-trash"></i></a>';
                            }else if(!Session::has('get_permission_list')){
                            $nesteddata['Action'] .= '<a href="javascript:void(0)" delete_id="'.$row['id'].'" class="i-size delete_role"><i class="icon icon-trash"></i></a>';
                            }
                        }
                    }
                    $data[]=$nesteddata;
                 }
            }else{
                foreach ($array_data as $row) {
                    $permissions = $row['permissions'];
                    $permissions = explode(",", $permissions);

                    $nesteddata['CheckAll']='<label class="custom-control custom-control-primary custom-checkbox mobile-col">
                       <input class="custom-control-input myCheckbox" type="checkbox"  value="'.$row['id'].'">
                       <span class="custom-control-indicator"></span>
                       </label>'; 
                    $nesteddata['Title']=ucwords($row['title']);
                    $data_content = "<ul>";
                        foreach ($permissions as $key => $value) {
                            $value = str_replace("_"," ",$value);
                            $value = ucwords($value);
                            $data_content .= '<li>'.$value.'</li>';
                        }
                        $data_content .= "</ul>";
                                $row['permissions'] = str_replace(",",", ",$row['permissions']);
                                $row['permissions'] = str_replace("_"," ",$row['permissions']);
                                $row['permissions'] = ucwords($row['permissions']);
                        $nesteddata['Permissions'] ='<a href="#" title="Permissions" data-trigger="focus" data-toggle="popover" data-html="true" data-content="'.$data_content.'">'.substr($row['permissions'], 0, 30).'</a>';
                        $nesteddata['Action'] = "";
                        if ($this->checkIfPermissionExist('role_edit')) {
                        $nesteddata['Action'] .='<a href="'.route('admin.roles.edit',$row['id']).'"  edit_id="'.$row['id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>';
                        }else if(!Session::has('get_permission_list')){
                        $nesteddata['Action'] .='<a href="'.route('admin.roles.edit',$row['id']).'"  edit_id="'.$row['id'].'" class="i-size edit_button"><i class="icon icon-edit"></i></a>';
                        }

                        if ($this->checkIfPermissionExist('role_view')) {
                        $nesteddata['Action'] .= '<a href="'.route('admin.roles.show',$row['id']).'" view_id="'.$row['id'].'" class="i-size view_button"><i class="icon icon-eye"></i></a>';
                        }else if(!Session::has('get_permission_list')){
                        $nesteddata['Action'] .= '<a href="'.route('admin.roles.show',$row['id']).'" view_id="'.$row['id'].'" class="i-size view_button"><i class="icon icon-eye"></i></a>';                          
                        }

                        if ($this->checkIfPermissionExist('role_delete')) {
                        $nesteddata['Action'] .= '<a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size delete_role"><i class="icon icon-trash"></i></a>';
                        }else if(!Session::has('get_permission_list')){
                        $nesteddata['Action'] .= '<a href="javascript:void()" delete_id="'.$row['id'].'" class="i-size delete_role"><i class="icon icon-trash"></i></a>';
                        }

                    $data[]=$nesteddata;
                }
            }
        }

        // echo "<pre>";
        // print_r($data);
        // exit;

        $json_data=array(
            "draw"             =>       intval($request->input('draw')),
            "recordsTotal"     =>         intval($totalFiltered),
            "recordsFiltered"  =>       intval($totalFiltered),
            "data"=>         $data
            
        );
        echo json_encode($json_data);
    }  

    public function roles_common_action(Request $request){
        $arr_req=$request->all();
        $action=$arr_req['action'];
        $arr_value= $arr_req['chk_check_value'];
        if($action=="enable"){
            $users = DB::table('roles')
                    ->whereIn('id', $arr_value)
                    ->update(['is_active' => 1]);
            
        }elseif ($action=="disable") {
            $users = DB::table('roles')
                    ->whereIn('id', $arr_value)
                    ->update(['is_active' => 0]);
        }
        elseif ($action=="delete") {
            $users = DB::table('roles')
                    ->whereIn('id', $arr_value)
                    ->update(['is_deleted' => 1]);
           
        }     
    }    

    public function delete_role_code(Request $request){
        $delete_id=$request->delete_id;
        if($delete_id!=""){
            DB::table('roles')
                ->where('id', $delete_id)
                ->update(['is_deleted' => 1]);
        }        
    }  

    //download data into pdf format
    public function role_download_PDF(){
        // $learners = DB::table("users")
        //         ->where('is_deleted', 0)
        //         ->get();
        $customer_data = DB::select( DB::raw("SELECT id,title, (SELECT array_to_string(array_agg(permissions.title),',') as learning_path FROM permissions INNER JOIN permission_role ON permissions.id = permission_role.permission_id WHERE permission_role.role_id = roles.id GROUP BY permission_role.role_id) as permissions FROM roles WHERE roles.is_deleted = 0"));        
        $learners=json_decode(json_encode($customer_data),true);     

        view()->share('roles',$learners);

        PDF::setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('pdf_file.role_pdf');

        return $pdf->download('rolespdfview.pdf');
    } 

    //download data into excel format
    function role_download_excel(Request $request){
        $customer_data = DB::select( DB::raw("SELECT id,title, (SELECT array_to_string(array_agg(permissions.title),',') as learning_path FROM permissions INNER JOIN permission_role ON permissions.id = permission_role.permission_id WHERE permission_role.role_id = roles.id GROUP BY permission_role.role_id) as permissions FROM roles WHERE roles.is_deleted = 0")); 

        $customer_data=json_decode(json_encode($customer_data),true);        
         $customer_array[] = array('Title', 'Permissions');
         foreach($customer_data as $customer){     
            $customer['permissions'] = str_replace(",",", ",$customer['permissions']);
            $customer['permissions'] = str_replace("_"," ",$customer['permissions']);
            $customer['permissions'] = ucwords($customer['permissions']);
       
          $customer_array[] = array(
           'Title'  => $customer['title'],
           'Permissions'   => $customer['permissions'],                              
          );
        }
        return Excel::create('roles', function($excel) use ($customer_array){
            $excel->sheet('mySheet', function($sheet) use ($customer_array)
            {
               $sheet->fromArray($customer_array);
               //print_r($customer_array);
            });
        })->download('xlsx');
    }  


    public function getUserTypeWisePermissionsList(Request $request){
        $user_type_id =  $request->user_type;
        $permissions_list = DB::table('permissions')->join('module_headers','permissions.header_id','=','module_headers.id')
                                               ->select(array('module_headers.parent_id','permissions.id','permissions.title','permissions.header_id','module_headers.id as moduleid','module_headers.header_name'))
                                               ->where(['module_headers.is_active'=>'1','module_headers.user_type'=>$user_type_id])
                                               ->orderBy('permissions.header_id')
                                               ->orderBy('permissions.id')
                                               ->get();
        return json_encode($permissions_list);                                       
        return $permissions_list = json_decode(json_encode($permissions_list),true);
        // $permissions = \App\Permission::get()->pluck('title', 'id');

        // return view('admin.roles.create', compact('permissions','user_type'));        
    }           
}
