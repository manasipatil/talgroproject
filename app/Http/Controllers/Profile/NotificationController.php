<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Config;

class NotificationController extends Controller
{
    public function index(){
    	 //      $msg = "This is a simple message.";
      // return response()->json(array('msg'=> $msg));
    }

    public function getData(){
    	$users = DB::table('notification')->where('to_id',auth()->user()->id)->orderBy('created_date','desc')->get()->toArray();
      	$users = json_decode(json_encode($users),true);

       	for($i=0;$i<count($users);$i++){

       		// $users[$i]['created_date'] = date('Y-m-d',strtotime($users[$i]['created_date']));

		$start  = date_create($users[$i]['created_date']);
		$end 	= date_create(); // Current time and date
		$diff  	= date_diff( $start, $end );
			// if($diff->d == 0){
			// 	$users[$i]['created_date'] = $diff->i ." min";
			// }else{

				$users[$i]['created_date'] = date(Config::get('myconstants.display_date_format'),strtotime($users[$i]['created_date']));
			// }
		
		 }

  		// echo "<pre>";
  		// return view('partials.topbar')->with('users',$users);
  		return $users;
    }

    public function readNotification(Request $Request){
      // $Request = Request::all();
      $notifID = $Request->notifid;
      if($notifID == ""){
        DB::table('notification')->where('to_id',auth()->user()->id)->update(['is_read'=>1]);
      }else{

        DB::table('notification')->where('id',$notifID)->update(['is_read'=>1]);

      }

    }
}
