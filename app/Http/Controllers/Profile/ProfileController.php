<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ProfileController extends Controller
{
    public function index(){

        $getdata = DB::table('roles')->join('role_user','role_user.role_id','=','roles.id')
                          ->select('roles.title')
                          ->where('role_user.user_id','=',auth()->user()->id)->get();  
        $getdata = json_decode(json_encode($getdata),true);


        $countryList = DB::table('countries')->orderBy('country_name', 'asc')->get();
        $countryList = json_decode(json_encode($countryList),true);

        $stateList = DB::table('states')->orderBy('state_name', 'asc')->get();
        $stateList = json_decode(json_encode($stateList),true);
    	return view('profile',['getdata' => $getdata,'countryList'=>$countryList,'stateList'=>$stateList]);
    }

    public function uploadProfilePicture(Request $request){
    	$this->validate($request, [
	    	'photo' => 'mimes:jpeg,bmp,png', //only allow this type extension file.
		]);
    	$file = $request->file('photo');
    	$file_name = $file->getClientOriginalName();
    	$file->move('public/img/profileimg', $file->getClientOriginalName()); 
    	$file_name = "profileimg/".$file_name;
    	DB::table('users')->where('id',auth()->user()->id)->update(['profile_image'=>$file_name]);
    	return redirect()->to('/profiles');
    }

    public function updateProfile(Request $request){
    	$data = $request->all();

    	$this->validate($request,[
    		'fproname' => 'bail|required',
    		'lproname' => 'bail|required',
    		'prousername' => 'bail|required|email',
            'mobileno' => 'bail|nullable|numeric'
    	],[
    		'fproname.required'=>'Please enter first name',
    		'lproname.required'=>'Please enter last name',
    		'prousername.required'=>'Please enter username',
    		'prousername.email'=>'Please enter valid email',
            'mobileno.required'=>'Please enter mobile number',
            'mobileno.numeric'=>'Please enter valid mobile number'
    	]);

    	$username = $data['prousername'];

    	DB::table('users')->where('id',auth()->user()->id)->update(['user_firstname'=>$data['fproname'],'user_lastname'=>$data['lproname'],'email'=>$username,'gender'=>$data['gender'],'mobile_no'=>$data['mobileno'],'user_address_line1'=>$data['address1'],'user_address_line2'=>$data['address2'],'country'=>$data['country'],'state'=>$data['state'],'city'=>$data['city'],'pincode'=>$data['pincode']]);

    	             $request->session()->flash('message','User Updated Successfully');
              $request->session()->flash('alert-class','alert-success');
    	return redirect()->to('/profiles');	

    }

    public function getCountryList(){
        $countryList = DB::table('countries')->get();
        $countryList = json_decode(json_encode($countryList),true);
        return view('profile')->with('countryList',$countryList);
    }

    public function getStatesList(Request $request){

        $stateList = DB::table('states')->where(['country_id'=>$request->countryid])->orderBy('state_name', 'asc')->get();
        $stateList = json_encode($stateList);  
        return $stateList;
    }
}
