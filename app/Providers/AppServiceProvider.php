<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('partials.sidebar',function($view){
            // dd(session('get_permission_list'));exit;
            $variableName = session('sidebar_role_id');
            if($variableName==""){
                $select_user_type = DB::select(DB::raw("select roles.user_type,roles.id from role_user inner join roles on role_user.role_id = roles.id inner join tbl_user_type_master on tbl_user_type_master.tutm_id = roles.user_type where role_user.user_id = '".auth()->user()->id."' LIMIT 1 OFFSET 1"));
                $select_user_type = json_decode(json_encode($select_user_type),true);
                if(!empty($select_user_type)){
                    $select_user_type = $select_user_type[0]['id'];

                $get_permission_list = DB::select(DB::raw("select (SELECT array_to_string(array_agg(permissions.title),',')) as title from permission_role inner join permissions on permission_role.permission_id = permissions.id where permission_role.role_id= ".$select_user_type.""));
                $get_permission_list = json_decode(json_encode($get_permission_list),true);
                session(['get_permission_list'=>$get_permission_list]);

                }else{
                    $select_user_type = "";
                }
                
                $getdata = DB::table('role_user')->join('permission_role','role_user.role_id','=','permission_role.role_id')
                        ->join('permissions','permissions.id','=','permission_role.permission_id')
                        ->join('module_headers','permissions.header_id','=','module_headers.id')
                        ->select(array('module_headers.active_segments','module_headers.redirect_link','module_headers.id','module_headers.header_name','module_headers.icon_class','module_headers.sort_order','module_headers.parent_id'))
                        ->orderBy('module_headers.sort_order','asc')
                        ->where('role_user.user_id','=',auth()->user()->id)
                        ->when($select_user_type, function ($query, $select_user_type) {
                            return $query->where('role_user.role_id', $select_user_type);
                        })
                        ->distinct()->get(); 
            }else{
                 $getdata = DB::table('role_user')->join('permission_role','role_user.role_id','=','permission_role.role_id')
                        ->join('permissions','permissions.id','=','permission_role.permission_id')
                        ->join('module_headers','permissions.header_id','=','module_headers.id')
                        ->select(array('module_headers.active_segments','module_headers.redirect_link','module_headers.id','module_headers.header_name','module_headers.icon_class','module_headers.sort_order','module_headers.parent_id','role_user.role_id','module_headers.user_type'))
                        ->orderBy('module_headers.sort_order','asc')
                        ->where('role_user.user_id','=',auth()->user()->id)
                        ->where ('role_user.role_id','=',$variableName)
                        ->distinct()
                        ->get(); 
            }
 
            $getdata = json_decode(json_encode($getdata),true);
            $view->with('getHeaders',$getdata);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        /**
         * Added missing method for package to work
         */
        \Illuminate\Support\Collection::macro('lists', function ($a, $b = null) {
            return collect($this->items)->pluck($a, $b);
        });

    }
}
