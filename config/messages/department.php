<?php 
return [
	'DEPARTMENT_LABEL'=>'Department',
	'DEPARTMENT_LIST' => 'Department List',
	'DEPARTMENT_TITLE_BAR'=>'Department Title Bar',
    'DEPARTMENT_CODE_ERROR' => 'Please Enter Department Code',
    'DEPARTMENT_TITLE_ERROR'=>'Please Enter Department',
    'DEPARTMENT_CODE_EXIST' => 'Department Code already exist',
    'DEPARTMENT_BULK_VALID_FORMAT'=>'Please upload valid format',
    'DEPARTMENT_VALID_FORMAT_SUCCESS'=>'File uploaded Successfully',
    'DEPARTMENT_INSERT_ERROR'=>'File has some issue.Please check the format'


 ];

