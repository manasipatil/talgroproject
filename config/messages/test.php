<?php 
return [
	'TEST_TITLE_BAR'=>'Personalize my Dashboard',
	'TEST_LIST' => 'Test List',
	'ADD_TEST_SUCCESS_MESSAGE' => 'Test Added successfully',
	'ADD_TEST_FAILURE_MESSAGE' => 'Failed to add test',
	'UPDATE_TEST_SUCCESS_MESSAGE' => 'Test updated successfully',
	'UPDATE_TEST_FAILURE_MESSAGE' => 'Failed to update test',	
	'INVITE_EMAIL_FORMAT_HEADER' => 'Invite Email Format',
	'CONGRATS_EMAIL_FORMAT_HEADER' => 'Congratulations Email Format',
	'ATTEMPT_ALLOW_EMAIL_FORMAT_HEADER' => 'Attempts allowed email Format',
	'RETAKE_EMAIL_FORMAT_HEADER' => 'Retake Email Format',
	'ALLOW_ANS_EMAIL_FORMAT_HEADER' => 'Allow answers to be changed Email Format',
	'RESULT_EMAIL_FORMAT_HEADER' => 'Result Email Format',
	'TEST_NAME_EXIST' => 'Test name already exist.',
    
 ];