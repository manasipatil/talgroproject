<?php 
return [
	'CATEGORY_LABEL'=>'Category',
	'CATEGORY_LIST' => 'Category List',
    'CATEGORY_CODE_ERROR' => 'Please Enter Category Code',
    'CATEGORY_TITLE_ERROR'=>'Please Enter Category Title',
    'CATEGORY_CODE_EXIST' => 'Category Code already exist',
    'CATEGORY_BULK_VALID_FORMAT'=>'Please upload valid format',
    'CATEGORY_VALID_FORMAT_SUCCESS'=>'File uploaded Successfully',
    'CATEGORY_INSERT_ERROR'=>'File has some issue.Please check the format'
 ];

