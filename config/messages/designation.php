<?php 
return [
	'DESIGNATION_LABEL'=>'Designation',
	'DESIGNATION_LIST' => 'Designation List',
	'DESIGNATION_TITLE_BAR'=>'Designation Title Bar',
    'DESIGNATION_CODE_ERROR' => 'Please Enter Designation Code',
    'DESIGNATION_TITLE_ERROR'=>'Please Enter Designation',
    'DESIGNATION_CODE_EXIST' => 'Designation Code already exist',
    'DESIGNATION_BULK_VALID_FORMAT'=>'Please upload valid format',
    'DESIGNATION_VALID_FORMAT_SUCCESS'=>'File uploaded Successfully',
    'DESIGNATION_INSERT_ERROR'=>'File has some issue.Please check the format'


 ];

