<?php 
return [
	'COMPETENCY_LABEL'=>'Competency',
	'COMPETENCY_LIST' => 'Competency List',
	'COMPETENCY_TITLE_BAR' => 'Personalize my Dashboard',
    'COMPETENCY_CODE_ERROR' => 'Please Enter Competecy Code',
    'COMPETENCY_TITLE_ERROR'=>'Please Enter Competecy Title',
    'COMPETENCY_CODE_EXIST' => 'Competency Code already exist',
    'COMPETENCY_BULK_VALID_FORMAT'=>'Please upload valid format',
    'COMPETENCY_VALID_FORMAT_SUCCESS'=>'File uploaded Successfully',
    'COMPETENCY_INSERT_ERROR'=>'File has some issue.Please check the format'


 ];

