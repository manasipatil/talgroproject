<?php 

return[

'display_date_format' => 'd-m-Y',

'current_date' => date('Y-m-d H:i:s'),

'display_calender_date_format' => 'm/d/Y',

'display_calender_time_format' => 'g:i a',

'save_datetime_format' => 'Y-m-d H:i:s',

'save_time_format' => 'H:i',
'FROM_EMAIL'=>'savita.w3i@gmail.com'
];

?>